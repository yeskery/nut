package com.yeskery.nut.core;

/**
 * Version Java Template
 * @author sprout
 * 2019-03-18 15:44
 * @version 1.0
 */
public class Version {
	/** project version */
	public static final String VERSION = "${project.version}";

	/** build timestamp */
	public static final String TIMESTAMP = "${buildtimestamp}";
}
