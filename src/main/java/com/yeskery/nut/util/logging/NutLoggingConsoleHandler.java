package com.yeskery.nut.util.logging;

import java.util.logging.ConsoleHandler;

/**
 * Nut控制台日志处理器
 * @author sprout
 * @version 1.0
 * 2022-09-12 15:05
 */
public class NutLoggingConsoleHandler extends ConsoleHandler {

    /**
     * 构建Nut控制台日志处理器
     */
    public NutLoggingConsoleHandler() {
        super();
        setOutputStream(System.out);
    }
}
