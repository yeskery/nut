package com.yeskery.nut.util.http;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.NameAndValue;
import com.yeskery.nut.util.StringUtils;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.util.List;

/**
 * HTTP工具类
 * @author sprout
 * 2022-05-20 13:35
 */
public class HttpUtils implements HttpRequest, HttpStringRequest {

    private static final HttpUtils INSTANCE = new HttpUtils();

    /** Https协议 */
    private static final String HTTPS = "HTTPS";

    /** Http客户端 */
    private volatile HttpClient httpClient;

    /** Https客户端 */
    private volatile HttpsClient httpsClient;

    /**
     * 私有化构造方法
     */
    private HttpUtils() {
    }

    /**
     * 设置https客户端
     * @param httpClient https客户端
     */
    public synchronized void setHttpsClient(HttpsClient httpClient) {
        this.httpsClient = httpClient;
    }

    /**
     * 获取默认的工具实例
     * @return 默认的工具实例
     */
    public static HttpUtils getDefaultInstance() {
        return INSTANCE;
    }

    /**
     * 获取新的工具实例
     * @return 新的工具实例
     */
    public static HttpUtils getNewInstance() {
        return new HttpUtils();
    }

    @Override
    public ResponseEntity<byte[]> doGet(String httpUrl) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doGet(httpUrl) : getHttpClient().doGet(httpUrl);
    }

    @Override
    public ResponseEntity<byte[]> doGet(String httpUrl, List<NameAndValue> headers) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doGet(httpUrl, headers) : getHttpClient().doGet(httpUrl, headers);
    }

    @Override
    public ResponseEntity<byte[]> doGet(String httpUrl, List<NameAndValue> headers, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doGet(httpUrl, headers, configuration) : getHttpClient().doGet(httpUrl, headers, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body) : getHttpClient().doPost(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body, mediaType)
                : getHttpClient().doPost(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body) : getHttpClient().doPost(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body, mediaType)
                : getHttpClient().doPost(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body) : getHttpClient().doPost(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, body, mediaType)
                : getHttpClient().doPost(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPost(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPost(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPost(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body) : getHttpClient().doPut(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body, mediaType)
                : getHttpClient().doPut(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body) : getHttpClient().doPut(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body, mediaType)
                : getHttpClient().doPut(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body) : getHttpClient().doPut(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, body, mediaType)
                : getHttpClient().doPut(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doPut(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPut(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPut(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body) : getHttpClient().doDelete(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body, mediaType)
                : getHttpClient().doDelete(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, headers, body, mediaType)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDelete(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body) : getHttpClient().doDelete(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body, mediaType)
                : getHttpClient().doDelete(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, headers, body, mediaType)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDelete(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body) : getHttpClient().doDelete(httpUrl, body);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, body, mediaType)
                : getHttpClient().doDelete(httpUrl, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDelete(httpUrl, headers, body, mediaType)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType);
    }

    @Override
    public ResponseEntity<byte[]> doDelete(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDelete(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDelete(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doGetString(String httpUrl) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doGetString(httpUrl) : getHttpClient().doGetString(httpUrl);
    }

    @Override
    public String doGetString(String httpUrl, List<NameAndValue> headers) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doGetString(httpUrl, headers) : getHttpClient().doGetString(httpUrl, headers);
    }

    @Override
    public String doGetString(String httpUrl, List<NameAndValue> headers, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doGetString(httpUrl, headers, configuration) : getHttpClient().doGetString(httpUrl, headers, configuration);
    }

    @Override
    public String doPostString(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body) : getHttpClient().doPostString(httpUrl, body);
    }

    @Override
    public String doPostString(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body, mediaType)
                : getHttpClient().doPostString(httpUrl, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPostString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doPostString(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body) : getHttpClient().doPostString(httpUrl, body);
    }

    @Override
    public String doPostString(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body, mediaType)
                : getHttpClient().doPostString(httpUrl, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPostString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doPostString(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body) : getHttpClient().doPostString(httpUrl, body);
    }

    @Override
    public String doPostString(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, body, mediaType)
                : getHttpClient().doPostString(httpUrl, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPostString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPostString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPostString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPostString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doPutString(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body) : getHttpClient().doPutString(httpUrl, body);
    }

    @Override
    public String doPutString(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body, mediaType)
                : getHttpClient().doPutString(httpUrl, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPutString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doPutString(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body) : getHttpClient().doPutString(httpUrl, body);
    }

    @Override
    public String doPutString(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body, mediaType)
                : getHttpClient().doPutString(httpUrl, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPutString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doPutString(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body) : getHttpClient().doPutString(httpUrl, body);
    }

    @Override
    public String doPutString(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, body, mediaType)
                : getHttpClient().doPutString(httpUrl, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doPutString(httpUrl, headers, body, mediaType)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doPutString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doPutString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doPutString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doDeleteString(String httpUrl, byte[] body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body) : getHttpClient().doDeleteString(httpUrl, body);
    }

    @Override
    public String doDeleteString(String httpUrl, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, headers, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDeleteString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doDeleteString(String httpUrl, InputStream body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body) : getHttpClient().doDeleteString(httpUrl, body);
    }

    @Override
    public String doDeleteString(String httpUrl, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, headers, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDeleteString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType, configuration);
    }

    @Override
    public String doDeleteString(String httpUrl, String body) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body) : getHttpClient().doDeleteString(httpUrl, body);
    }

    @Override
    public String doDeleteString(String httpUrl, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType) {
        return isHttpsRequest(httpUrl) ? getHttpsClient().doDeleteString(httpUrl, headers, body, mediaType)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType);
    }

    @Override
    public String doDeleteString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration) {
        return isHttpsRequest(httpUrl) ? getHttpsClient(configuration).doDeleteString(httpUrl, headers, body, mediaType, configuration)
                : getHttpClient().doDeleteString(httpUrl, headers, body, mediaType, configuration);
    }

    /**
     * 是否是HTTPS请求
     * @param httpUrl 请求url
     * @return 是否是HTTPS请求
     */
    private boolean isHttpsRequest(String httpUrl) {
        if (StringUtils.isEmpty(httpUrl) || httpUrl.length() < HTTPS.length()) {
            return false;
        }
        return httpUrl.substring(0, HTTPS.length()).equalsIgnoreCase(HTTPS);
    }

    /**
     * 获取Http客户端
     * @return Http客户端
     */
    private HttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (this) {
                if (httpClient == null) {
                    httpClient = new HttpClient();
                }
            }
        }
        return httpClient;
    }

    /**
     * 获取Https客户端
     * @return Https客户端
     */
    private HttpsClient getHttpsClient() {
        if (httpsClient == null) {
            synchronized (this) {
                if (httpsClient == null) {
                    httpsClient = new HttpsClient();
                }
            }
        }
        return httpsClient;
    }

    /**
     * 获取自定义证书管理工厂对象Https客户端
     * @param configuration http配置对象
     * @return 自定义证书管理工厂对象Https客户端
     */
    private HttpsClient getHttpsClient(HttpConfiguration configuration) {
        if (configuration == null) {
            return getHttpsClient();
        }
        if (configuration.getTrustManagerFactory() == null && configuration.getKeyManagerFactory() == null) {
            return getHttpsClient();
        }
        return new HttpsClient(configuration.getTrustManagerFactory(), configuration.getKeyManagerFactory());
    }
}
