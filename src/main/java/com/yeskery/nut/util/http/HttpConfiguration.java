package com.yeskery.nut.util.http;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Http 工具配置对象
 * @author YESKERY
 * 2024/1/11
 */
public class HttpConfiguration {

    /** 连接超时时间 */
    private static final int CONNECT_OUT_TIME = 3000;

    /** 读取超时时间 */
    private static final int READ_OUT_TIME = 5000;

    /** 连接超时时间 */
    private int connectOutTime;

    /** 读取超时时间 */
    private int readOutTime;

    /** 证书信任管理工厂对象 */
    private TrustManagerFactory trustManagerFactory;

    /** 证书管理工厂对象 */
    private KeyManagerFactory keyManagerFactory;

    /**
     * 获取默认Http 工具配置对象
     * @return 默认Http 工具配置对象
     */
    public static HttpConfiguration getDefaultHttpConfiguration() {
        HttpConfiguration configuration = new HttpConfiguration();
        configuration.setConnectOutTime(CONNECT_OUT_TIME);
        configuration.setReadOutTime(READ_OUT_TIME);
        return configuration;
    }

    /**
     * 获取连接超时时间
     * @return 连接超时时间
     */
    public int getConnectOutTime() {
        return connectOutTime;
    }

    /**
     * 设置连接超时时间
     * @param connectOutTime 连接超时时间
     */
    public void setConnectOutTime(int connectOutTime) {
        this.connectOutTime = connectOutTime;
    }

    /**
     * 获取读取超时时间
     * @return 读取超时时间
     */
    public int getReadOutTime() {
        return readOutTime;
    }

    /**
     * 设置读取超时时间
     * @param readOutTime 读取超时时间
     */
    public void setReadOutTime(int readOutTime) {
        this.readOutTime = readOutTime;
    }

    /**
     * 获取证书信任管理工厂对象
     * @return 证书信任管理工厂对象
     */
    public TrustManagerFactory getTrustManagerFactory() {
        return trustManagerFactory;
    }

    /**
     * 设置证书信任管理工厂对象
     * @param trustManagerFactory 证书信任管理工厂对象
     */
    public void setTrustManagerFactory(TrustManagerFactory trustManagerFactory) {
        this.trustManagerFactory = trustManagerFactory;
    }

    /**
     * 获取证书管理工厂对象
     * @return 证书管理工厂对象
     */
    public KeyManagerFactory getKeyManagerFactory() {
        return keyManagerFactory;
    }

    /**
     * 设置证书管理工厂对象
     * @param keyManagerFactory 证书管理工厂对象
     */
    public void setKeyManagerFactory(KeyManagerFactory keyManagerFactory) {
        this.keyManagerFactory = keyManagerFactory;
    }
}
