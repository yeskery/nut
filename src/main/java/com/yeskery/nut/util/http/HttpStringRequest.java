package com.yeskery.nut.util.http;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.NameAndValue;

import java.io.InputStream;
import java.util.List;

/**
 * Http请求接口
 * @author sprout
 * 2022-05-20 14:02
 */
public interface HttpStringRequest {

    /**
     * 执行HTTP GET请求
     * @param httpUrl 请求url地址
     * @return 响应结果
     */
    String doGetString(String httpUrl);

    /**
     * 执行HTTP GET请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @return 响应结果
     */
    String doGetString(String httpUrl, List<NameAndValue> headers);

    /**
     * 执行HTTP GET请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doGetString(String httpUrl, List<NameAndValue> headers, HttpConfiguration configuration);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @return 响应结果
     */
    String doPostString(String httpUrl, String body);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, String body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @return 响应结果
     */
    String doPostString(String httpUrl, byte[] body);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @return 响应结果
     */
    String doPostString(String httpUrl, InputStream body);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP POST请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPostString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @return 响应结果
     */
    String doPutString(String httpUrl, byte[] body);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @return 响应结果
     */
    String doPutString(String httpUrl, InputStream body);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @return 响应结果
     */
    String doPutString(String httpUrl, String body);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, String body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType);

    /**
     * 执行HTTP PUT请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doPutString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, byte[] body);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容字节数组
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, byte[] body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, InputStream body);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体输入流
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, InputStream body, MediaType mediaType, HttpConfiguration configuration);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, String body);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, String body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType);

    /**
     * 执行HTTP DELETE请求
     * @param httpUrl 请求url地址
     * @param headers 请求头
     * @param body 请求体内容
     * @param mediaType 请求体媒体类型
     * @param configuration http配置对象
     * @return 响应结果
     */
    String doDeleteString(String httpUrl, List<NameAndValue> headers, String body, MediaType mediaType, HttpConfiguration configuration);

}
