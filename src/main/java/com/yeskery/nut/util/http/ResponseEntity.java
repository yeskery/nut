package com.yeskery.nut.util.http;

import com.yeskery.nut.core.NameAndValue;

import java.util.List;

/**
 * 响应实体对象
 * @author YESKERY
 * 2024/6/17
 */
public class ResponseEntity<T> {

    /** 状态码 */
    private Integer status;

    /** 响应头 */
    private List<NameAndValue> headers;

    /** 响应体 */
    private T body;

    /**
     * 获取状态码
     * @return 状态码
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态码
     * @param status 状态码
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取响应头
     * @return 响应头
     */
    public List<NameAndValue> getHeaders() {
        return headers;
    }

    /**
     * 设置响应头
     * @param headers 响应头
     */
    public void setHeaders(List<NameAndValue> headers) {
        this.headers = headers;
    }

    /**
     * 获取响应体
     * @return 响应体
     */
    public T getBody() {
        return body;
    }

    /**
     * 设置响应体
     * @param body 响应体
     */
    public void setBody(T body) {
        this.body = body;
    }
}
