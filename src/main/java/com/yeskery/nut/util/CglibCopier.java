package com.yeskery.nut.util;

import com.yeskery.nut.core.NutException;
import net.sf.cglib.beans.BeanCopier;

/**
 * cglib 对象转换
 * @author YESKERY
 * 2023/11/9
 */
public class CglibCopier {

    /**
     * cglib 对象转换
     * @param source 原对象
     * @param target 目标对象类
     * @param <K> 原对象类型
     * @param <T> 目标对象类型
     * @return 转换后的目标对象
     */
    public static <K, T> T copy(K source, Class<T> target) {
        try {
            BeanCopier copier = BeanCopier.create(source.getClass(), target, false);
            T res = target.newInstance();
            copier.copy(source, res, null);
            return res;
        } catch (Exception e) {
            throw new NutException("Cglib Object Copy Failure.", e);
        }
    }

    /**
     * cglib 对象转换
     * @param source 源对象
     * @param target 目标对象
     * @param <K> 源对象类型
     * @param <T> 目标对象类型
     */
    public static <K, T> void copy(K source, T target) {
        try {
            BeanCopier copier = BeanCopier.create(source.getClass(), target.getClass(), false);
            copier.copy(source, target, null);
        } catch (Exception e) {
            throw new NutException("Cglib Object Copy Failure.", e);
        }
    }
}
