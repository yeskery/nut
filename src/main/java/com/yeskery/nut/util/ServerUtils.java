package com.yeskery.nut.util;

import com.yeskery.nut.application.ServerType;
import com.yeskery.nut.core.BasicNutConfigure;

/**
 * 服务工具类
 * @author YESKERY
 * 2023/9/8
 */
public class ServerUtils {

    /** Netty class名称 */
    private static final String NETTY_CLASS_NAME = "io.netty.bootstrap.ServerBootstrap";

    /**
     * 私有化构造方法
     */
    private ServerUtils(){
    }

    /**
     * 获取服务类型
     * @param basicNutConfigure Nut配置对象
     * @return 服务类型
     */
    public static ServerType getServerType(BasicNutConfigure basicNutConfigure) {
        if (basicNutConfigure.getServerType() == ServerType.AUTO) {
            if (ClassUtils.isExistTargetClass(NETTY_CLASS_NAME)) {
                return ServerType.NETTY;
            }
            return ServerType.SUN;
        }
        return basicNutConfigure.getServerType();
    }
}
