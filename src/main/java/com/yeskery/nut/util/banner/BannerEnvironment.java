package com.yeskery.nut.util.banner;

import com.yeskery.nut.core.Environment;
import com.yeskery.nut.core.Version;

import java.util.Map;
import java.util.Properties;

/**
 * Banner环境对象
 * @author sprout
 * @version 1.0
 * 2022-09-12 16:30
 */
public class BannerEnvironment implements Environment {

    /** Banner配置信息 */
    private final Properties properties = new Properties();

    {
        properties.setProperty("nut.version", Version.VERSION);
        properties.setProperty("nut.buildTime", Version.TIMESTAMP);
    }

    /**
     * 构建Banner环境对象
     */
    public BannerEnvironment() {
    }

    /**
     * 构建Banner环境对象
     * @param props 初始属性map
     */
    public BannerEnvironment(Map<String, String> props) {
        for (Map.Entry<String, String> entry : props.entrySet()) {
            properties.setProperty(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public String getEnvName() {
        return "banner";
    }

    @Override
    public Properties getMainEnvProperties() {
        return properties;
    }

    @Override
    public Properties loadMainEnvProperties(String propertiesPath) {
        return properties;
    }

    @Override
    public Properties getEnvProperties() {
        return properties;
    }

    @Override
    public Properties loadEnvProperties(String propertiesPath) {
        return properties;
    }
}
