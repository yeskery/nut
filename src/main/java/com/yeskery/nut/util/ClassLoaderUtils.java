package com.yeskery.nut.util;

import java.lang.reflect.Method;

/**
 * 类加载器工具类
 * @author YESKERY
 * 2023/9/12
 */
public class ClassLoaderUtils {

    /** one jar启动主类 */
    private static final String ONE_JAR_BOOT_CLASS = "com.simontuffs.onejar.Boot";

    /** 类加载器 */
    private static ClassLoader classLoader;

    static {
        try {
            Class<?> oneJarBootClass = Class.forName(ONE_JAR_BOOT_CLASS);
            Method method = oneJarBootClass.getDeclaredMethod("getClassLoader");
            classLoader = (ClassLoader) method.invoke(null);
        } catch (Exception e) {
            classLoader = ClassLoaderUtils.class.getClassLoader();
        }
    }

    /**
     * 获取当前类加载器
     * @return 类加载器
     */
    public static ClassLoader getClassLoader() {
        return classLoader;
    }
}
