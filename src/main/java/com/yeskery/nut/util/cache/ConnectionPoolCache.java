package com.yeskery.nut.util.cache;

import com.yeskery.nut.core.NutException;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 连接池缓存对象
 *
 * @param <T> 连接的类型
 * @author sprout
 * @version 1.0
 * 2023-07-09 15:35
 */
public class ConnectionPoolCache<T> implements Closeable {

    /** 默认最大超时时间 */
    private static final int DEFAULT_MAX_WAIT_TIME_OUT = 5000;

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(ConnectionPoolCache.class.getName());

    /** 连接创建器 */
    private final ConnectionCreator<T> connectionCreator;

    /** 活动连接队列 */
    private final LinkedBlockingQueue<T> busyQueue = new LinkedBlockingQueue<>();

    /** 空闲连接队列 */
    private final LinkedBlockingQueue<T> idleQueue = new LinkedBlockingQueue<>();

    /** 已创建连接数 */
    private final AtomicInteger createCounter = new AtomicInteger(0);

    /** 最大连接数 */
    private final int maxConnectionSize;

    /** 最大等待毫秒数 */
    private final int maxWaitTimeout;

    /**
     * 构建基础的不支持连接池数据源
     * @param connectionCreator 连接创建器
     * @param maxConnectionSize 最大连接数
     * @param maxWaitTimeout 最大等待毫秒数
     */
    public ConnectionPoolCache(ConnectionCreator<T> connectionCreator, int maxConnectionSize, int maxWaitTimeout) {
        if (connectionCreator == null) {
            throw new NutException("ConnectionCreator Must Not Be Null.");
        }
        this.maxConnectionSize = maxConnectionSize;
        if (maxConnectionSize < 1) {
            throw new NutException("maxConnectionSize Must Greater Than 0.");
        }
        this.connectionCreator = connectionCreator;
        if (maxWaitTimeout < 1) {
            throw new NutException("maxWaitTimeout Must Greater Than 0.");
        }
        this.maxWaitTimeout = maxWaitTimeout;
    }

    /**
     * 构建基础的不支持连接池数据源
     * @param connectionCreator 连接创建器
     * @param maxConnectionSize 最大连接数
     */
    public ConnectionPoolCache(ConnectionCreator<T> connectionCreator, int maxConnectionSize) {
        this(connectionCreator, maxConnectionSize, DEFAULT_MAX_WAIT_TIME_OUT);
    }

    /**
     * 将连接标注为可用
     *
     * @param connection 连接
     */
    public void free(T connection) {
        // 处理空连接
        if (connection == null) {
            createCounter.decrementAndGet();
            return;
        }
        // 处理移除失败的连接
        boolean removeResult = busyQueue.remove(connection);
        if (!removeResult) {
            closeConnection(connection);
            createCounter.decrementAndGet();
            return;
        }
        // 处理已经关闭的连接
        try {
            if (isConnectionClosed(connection)) {
                createCounter.decrementAndGet();
                return;
            }
        } catch (Exception e) {
            logger.logp(Level.SEVERE, this.getClass().getName(), "free",
                    "Connection Close Status Obtain Fail.", e);
        }
        // 处理添加失败的连接
        boolean offerResult = idleQueue.offer(connection);
        if (!offerResult) {
            closeConnection(connection);
            createCounter.decrementAndGet();
        }
    }

    /**
     * 关闭连接池
     */
    @Override
    public void close() throws IOException {
        for (T connection : busyQueue) {
            if (connection != null) {
                try {
                    closeConnection(connection);
                } catch (Exception e) {
                    logger.logp(Level.SEVERE, this.getClass().getName(), "close",
                            "Connection Close Fail.", e);
                }
            }
        }
        this.busyQueue.clear();

        for (T connection : idleQueue) {
            if (connection != null) {
                try {
                    closeConnection(connection);
                } catch (Exception e) {
                    logger.logp(Level.SEVERE, this.getClass().getName(), "close",
                            "Connection Close Fail.", e);
                }
            }
        }
        this.idleQueue.clear();
    }

    /**
     * 获取缓存的连接对象
     * @return 缓存的连接对象
     */
    public T getCacheConnection() {
        // 尝试获取空闲连接
        T connection = idleQueue.poll();
        if (connection == null) {
            // 尝试创建连接，使用双重CAS检查现有连接数是否小于最大连接数
            if (createCounter.get() < maxConnectionSize) {
                if (createCounter.incrementAndGet() <= maxConnectionSize) {
                    connection = connectionCreator.create();
                } else {
                    createCounter.decrementAndGet();
                }
            }
            // 尝试等待获取空闲连接，实现超时等待机制
            if (connection == null) {
                try {
                    connection = idleQueue.poll(maxWaitTimeout, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    throw new NutException("Connection Obtain Fail.", e);
                }
                if (connection == null) {
                    throw new NutException("Connection Obtain Timeout.");
                }
            }
        }
        busyQueue.offer(connection);
        return connection;

    }

    /**
     * 关闭连接对象
     * @param connection 连接对象
     */
    protected void closeConnection(T connection) {
    }

    /**
     * 是否已经关闭连接对象
     * @param connection 连接对象
     * @return 是否已经关闭连接对象
     */
    protected boolean isConnectionClosed(T connection) {
        return false;
    }
}
