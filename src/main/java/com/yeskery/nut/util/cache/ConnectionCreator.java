package com.yeskery.nut.util.cache;

/**
 * 连接创建对象
 *
 * @param <T> 连接的类型
 * @author sprout
 * 2022-06-02 17:35
 */
@FunctionalInterface
public interface ConnectionCreator<T> {

    /**
     * 创建一个连接对象
     * @return 连接对象
     */
    T create();
}
