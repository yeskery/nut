package com.yeskery.nut.util;

import com.yeskery.nut.core.MediaType;

import java.util.Arrays;

/**
 * 媒体类型工具类
 * @author sprout
 * @version 1.0
 * 2022-05-15 22:55
 */
public class MediaTypeUtils {

    /** 二进制媒体类型 */
    private static final MediaType[] BINARY_MEDIA_TYPES = { MediaType.APPLICATION_OCTET_STREAM, MediaType.IMAGE_JPEG,
            MediaType.IMAGE_PNG, MediaType.IMAGE_GIF, MediaType.IMAGE_X_ICON };

    /**
     * 私有化构造方法
     */
    private MediaTypeUtils() {
    }

    /**
     * 是否是二进制媒体类型
     * @param mediaType 需要判断的媒体类型
     * @return 是否是二进制媒体类型
     */
    public static boolean isBinaryMediaType(MediaType mediaType) {
        return Arrays.stream(BINARY_MEDIA_TYPES).anyMatch(m -> m == mediaType);
    }
}
