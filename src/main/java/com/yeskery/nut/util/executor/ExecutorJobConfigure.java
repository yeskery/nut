package com.yeskery.nut.util.executor;

/**
 * 执行器任务配置对象
 * @author sunjay
 * 2024/9/1
 */
public interface ExecutorJobConfigure {

    /**
     * 获取执行器上下文对象
     * @return 执行器上下文对象
     */
    ExecutorContext getExecutorContext();

    /**
     * 获取任务上下文对象
     * @return 任务上下文对象
     */
    JobContext getJobContext();
}
