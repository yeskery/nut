package com.yeskery.nut.util.executor;

import com.yeskery.nut.core.BasicContext;

/**
 * 任务上下文
 * @author sunjay
 * 2024/9/1
 */
public interface JobContext extends BasicContext<Object> {

    /**
     * 获取属性值
     * @param name 属性名
     * @return 属性值
     * @param <T> 属性值类型
     */
    @SuppressWarnings("unchecked")
    default <T> T getAttributeValue(String name) {
        return (T) getAttribute(name);
    }
}
