package com.yeskery.nut.util.executor;

/**
 * 任务步骤执行接口
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
@FunctionalInterface
public interface JobStepExecutor<T> {

    /**
     * 是否跳过该条数据
     * @param data 数据
     * @param executorJobConfigure 执行器任务配置对象
     * @return 跳过该条数据
     */
    default boolean skip(T data, ExecutorJobConfigure executorJobConfigure) {
        return false;
    }

    /**
     * 执行任务步骤
     * @param data 数据
     * @param executorJobConfigure 执行器任务配置对象
     * @return 执行结果
     */
    RunResult execute(T data, ExecutorJobConfigure executorJobConfigure);
}
