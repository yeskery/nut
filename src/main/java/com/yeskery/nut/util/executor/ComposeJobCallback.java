package com.yeskery.nut.util.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 组合任务回调，用于组合多个回调方法
 * @author YESKERY
 * 2024/9/2
 */
public class ComposeJobCallback<T> implements JobCallback<T> {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(ComposeJobCallback.class.getName());

    /** 任务回调方法集合 */
    private final List<JobCallback<T>> jobCallbacks = new ArrayList<>();

    /**
     * 构建组合任务回调
     * @param jobCallbacks 任务回调方法集合
     */
    public ComposeJobCallback(List<JobCallback<T>> jobCallbacks) {
        this.jobCallbacks.addAll(jobCallbacks);
    }

    /**
     * 添加任务回调
     * @param jobCallback 任务回调方法
     */
    public void addJobCallback(JobCallback<T> jobCallback) {
        jobCallbacks.add(jobCallback);
    }

    /**
     * 添加任务回调集合
     * @param jobCallbacks 任务回调方法集合
     */
    public void addJobCallbacks(List<JobCallback<T>> jobCallbacks) {
        this.jobCallbacks.addAll(jobCallbacks);
    }

    @Override
    public void callback(Job<T> job, T data, RunStatus runStatus, Map<Integer, RunResult> runResultMap, ExecutorJobConfigure executorJobConfigure) {
        for (JobCallback<T> jobCallback : jobCallbacks) {
            try {
                jobCallback.callback(job, data, runStatus, runResultMap, executorJobConfigure);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "JobCallback[" + jobCallback.getClass().getName() + "] Execute Fail.", e);
            }
        }
    }
}
