package com.yeskery.nut.util.executor;

/**
 * 运行状态
 * @author sunjay
 * 2024/9/1
 */
public enum RunStatus {
    /** 未开始 */
    NOT_START,
    /** 运行中 */
    RUNNING,
    /** 正常结束 */
    FINISH,
    /** 结束但部分任务失败失败或错误 */
    COMPLETE,
    /** 退出 */
    EXIT
}
