package com.yeskery.nut.util.executor;

/**
 * 任务步骤回调
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
public interface JobStepCallback<T> {


    /**
     * 任务步骤回调方法
     * @param job 任务对象
     * @param jobStep 任务步骤对象
     * @param index 当前任务步骤执行索引，从0开始
     * @param data 数据对象
     * @param runResult 任务步骤执行结果
     * @param executorJobConfigure 执行器任务配置对象
     */
    void callback(Job<T> job, JobStep<T> jobStep, T data, int index, RunResult runResult, ExecutorJobConfigure executorJobConfigure);
}
