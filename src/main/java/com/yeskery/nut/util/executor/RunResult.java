package com.yeskery.nut.util.executor;

/**
 * 运行结果
 * @author sunjay
 * 2024/9/1
 */
public enum RunResult {
    /** 启动 */
    STARTED,
    /** 成功 */
    SUCCESS,
    /** 跳过未执行 */
    SKIPPED,
    /** 失败，不会进行重试 */
    FAILURE,
    /** 错误，会重试 */
    ERROR
}
