package com.yeskery.nut.util.executor;

/**
 * 默认的执行器任务配置对象
 * @author sunjay
 * 2024/9/1
 */
public class DefaultExecutorJobConfigure implements ExecutorJobConfigure {

    /** 执行器上下文 */
    private ExecutorContext executorContext;

    /** 任务上下文 */
    private JobContext jobContext;

    @Override
    public ExecutorContext getExecutorContext() {
        return executorContext;
    }

    @Override
    public JobContext getJobContext() {
        return jobContext;
    }

    /**
     * 设置执行器上下文
     * @param executorContext 执行器上下文
     */
    public void setExecutorContext(ExecutorContext executorContext) {
        this.executorContext = executorContext;
    }

    /**
     * 设置任务上下文
     * @param jobContext 任务上下文
     */
    public void setJobContext(JobContext jobContext) {
        this.jobContext = jobContext;
    }
}
