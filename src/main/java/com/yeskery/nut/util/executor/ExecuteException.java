package com.yeskery.nut.util.executor;

import com.yeskery.nut.core.NutException;

/**
 * 执行器执行异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class ExecuteException extends NutException {

    /**
     * 构建一个 {@link ExecuteException}
     */
    public ExecuteException() {
    }

    /**
     * 构建一个 {@link ExecuteException}
     * @param message message
     */
    public ExecuteException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ExecuteException}
     * @param message message
     * @param cause cause
     */
    public ExecuteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ExecuteException}
     * @param cause cause
     */
    public ExecuteException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ExecuteException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ExecuteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
