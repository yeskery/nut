package com.yeskery.nut.util.executor;

/**
 * 任务步骤接口
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
public interface JobStep<T> extends JobStepExecutor<T> {

    /**
     * 获取步骤名称
     * @return 步骤名称
     */
    String getName();

    /**
     * 重试次数
     * @return 重试次数
     */
    default int retry() {
        return 0;
    }

    /**
     * 设置重试值
     * @param retryValue 重试值
     */
    void setRetryValue(int retryValue);

    /**
     * 设置执行开始前的等待时间
     * @return 执行开始前的等待时间
     */
    default long waitTime() {
        return 0L;
    }

    /**
     * 设置执行开始前的等待时间
     * @param waitTime 执行开始前的等待时间
     */
    void setWaitTime(long waitTime);

    /**
     * 是否在本步骤执行错误时退出任务
     * @return 是否在本步骤执行错误时退出任务
     */
    default boolean exitOnError() {
        return false;
    }

    /**
     * 设置是否在错误时退出任务
     * @param exitOnError 是否在错误时退出任务
     */
    void setExitOnError(boolean exitOnError);

    /**
     * 是否在本步骤执行失败时退出任务
     * @return 是否在本步骤执行失败时退出任务
     */
    default boolean exitOnFailure() {
        return false;
    }

    /**
     * 设置是否在失败时退出任务
     * @param exitOnFailure 是否在失败时退出任务
     */
    void setExitOnFailure(boolean exitOnFailure);
}
