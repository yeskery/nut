package com.yeskery.nut.util.executor;

import com.yeskery.nut.core.BasicContextImpl;

/**
 * 执行器默认实现类
 * @author sunjay
 * 2024/9/1
 */
public class ExecutorContextImpl extends BasicContextImpl<Object> implements ExecutorContext {
}
