package com.yeskery.nut.util.executor;

import java.util.function.Function;

/**
 * 任务接口
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
public interface Job<T> {

    /**
     * 获取任务名称
     * @return 任务名称
     */
    String getName();

    /**
     * 将任务步骤添加到开头
     * @param jobStep 任务步骤
     */
    void addJobStepFirst(JobStep<T> jobStep);

    /**
     * 将任务步骤添加到末尾
     * @param jobStep 任务步骤
     */
    void addJobStepLast(JobStep<T>  jobStep);

    /**
     * 添加任务步骤到指定位置
     * @param index 要添加的任务步骤索引，从0开始
     * @param jobStep 任务步骤
     */
    void addJobStep(int index, JobStep<T>  jobStep);

    /**
     * 总任务步骤数
     * @return 任务步骤数
     */
    int getJobStepCount();

    /**
     * 获取任务运行状态
     * @return 任务运行状态
     */
    RunStatus getRunStatus();

    /**
     * 重试次数
     * @return 重试次数
     */
    default int retry() {
        return 0;
    }

    /**
     * 设置重试值
     * @param retryValue 重试值
     */
    void setRetryValue(int retryValue);

    /**
     * 重试等待时间函数
     * @return 重试等待时间函数
     */
    Function<Long, Long> getRetryWaitTimeFunction();

    /**
     * 设置重试等待时间函数
     * @param retryWaitTimeFunction 重试等待时间函数
     */
    void setRetryWaitTimeFunction(Function<Long, Long> retryWaitTimeFunction);

    /**
     * 设置任务回调方法
     * @param jobCallback 任务回调方法
     */
    void setJobCallback(JobCallback<T> jobCallback);

    /**
     * 设置任务步骤回调方法
     * @param jobStepCallback 任务步骤回调方法
     */
    void setJobStepCallback(JobStepCallback<T> jobStepCallback);

    /**
     * 设置执行器上下文
     * @param executorContext 执行器上下文
     */
    void setExecutorContext(ExecutorContext executorContext);

    /**
     * 任务执行方法
     * @param data 数据对象
     * @return 运行状态
     */
    RunStatus run(T data);
}
