package com.yeskery.nut.util.executor;

/**
 * 执行器回调
 * @author sunjay
 * 2024/10/9
 */
public interface ExecutorCallback {

    /**
     * 执行器回调方法
     * @param executorContext 执行器上下文
     */
    void callback(ExecutorContext executorContext);
}
