package com.yeskery.nut.util.executor;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

/**
 * 执行器接口
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
public interface Executor<T> {

    /**
     * 添加任务
     * @param job 任务对象
     */
    void addJob(Job<T> job);

    /**
     * 添加多个任务
     * @param jobs 任务集合
     */
    void addJobs(Collection<Job<T>> jobs);

    /**
     * 添加数据源
     * @param dataSource 数据源
     */
    void addDataSource(Collection<T> dataSource);

    /**
     * 添加数据源
     * @param dataSource 数据源
     */
    void addDataSource(Stream<T> dataSource);

    /**
     * 获取运行状态
     * @return 运行状态
     */
    RunStatus getRunStatus();

    /**
     * 设置线程池，如果未线程池将以当前线程池运行
     * @param executorService 线程池对象
     * @param autoClose 是否在执行器结束时自动关闭该线程池
     */
    void setExecutorService(ExecutorService executorService, boolean autoClose);

    /**
     * 设置线程池，如果未线程池将以当前线程池运行
     * @param executorService 线程池对象
     */
    default void setExecutorService(ExecutorService executorService) {
        setExecutorService(executorService, false);
    }

    /**
     * 开始执行任务
     */
    void start();

    /**
     * 设置执行器回调
     * @param executorCallback 回调对象
     */
    void setExecutorCallback(ExecutorCallback executorCallback);
}
