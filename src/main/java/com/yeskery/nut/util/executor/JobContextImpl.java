package com.yeskery.nut.util.executor;

import com.yeskery.nut.core.BasicContextImpl;

/**
 * 任务上下文默认实现类
 * @author sunjay
 * 2024/9/1
 */
public class JobContextImpl extends BasicContextImpl<Object> implements JobContext {
}
