package com.yeskery.nut.util.executor;

import java.util.Map;

/**
 * 任务回调
 * @param <T> 执行器数据类型
 * @author sunjay
 * 2024/9/1
 */
public interface JobCallback<T> {


    /**
     * 任务回调方法
     * @param job 任务对象
     * @param data 数据对象
     * @param runStatus 任务执行状态
     * @param runResultMap 执行结果map
     * @param executorJobConfigure 执行器任务配置对象
     */
    void callback(Job<T> job, T data, RunStatus runStatus, Map<Integer, RunResult> runResultMap, ExecutorJobConfigure executorJobConfigure);
}
