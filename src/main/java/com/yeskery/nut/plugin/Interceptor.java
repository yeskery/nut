package com.yeskery.nut.plugin;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.util.ControllerUtils;

import java.lang.reflect.Method;

/**
 * 拦截器插件简短命名接口，功能和{@link InterceptorPlugin}一致
 * @author sprout
 * @version 1.0
 * 2023-07-22 19:32
 */
public interface Interceptor extends InterceptorPlugin {

    /**
     * 获取注解式Controller当前请求的执行方法
     * @param controller Controller对象
     * @param request 当前请求对象
     * @return 执行方法
     */
    default Method getExecuteMethod(Controller controller, Request request) {
        return ControllerUtils.getAnnotationControllerExecuteMethod(controller, request);
    }
}
