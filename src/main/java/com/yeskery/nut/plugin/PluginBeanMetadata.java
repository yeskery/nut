package com.yeskery.nut.plugin;

/**
 * 插件bean元数据
 * @author sprout
 * @version 1.0
 * 2022-07-07 22:05
 */
public class PluginBeanMetadata {

    /** bean名称 */
    private String beanName;

    /** bean对象 */
    private Object bean;

    /** bean类型 */
    private Class<?>[] types;

    /**
     * 构建插件bean元数据
     * @param beanName bean名称
     * @param bean bean对象
     * @param types bean类型
     */
    public PluginBeanMetadata(String beanName, Object bean, Class<?>... types) {
        this.beanName = beanName;
        this.bean = bean;
        this.types = types;
    }

    /**
     * 构建插件bean元数据
     */
    public PluginBeanMetadata() {
    }

    /**
     * 获取bean名称
     * @return bean名称
     */
    public String getBeanName() {
        return beanName;
    }

    /**
     * 设置bean名称
     * @param beanName bean名称
     */
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    /**
     * 获取bean对象
     * @return bean对象
     */
    public Object getBean() {
        return bean;
    }

    /**
     * 设置bean对象
     * @param bean bean对象
     */
    public void setBean(Object bean) {
        this.bean = bean;
    }

    /**
     * 获取bean类型
     * @return bean类型
     */
    public Class<?>[] getTypes() {
        return types;
    }

    /**
     * 设置bean类型
     * @param types bean类型
     */
    public void setTypes(Class<?>[] types) {
        this.types = types;
    }
}
