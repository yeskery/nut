package com.yeskery.nut.plugin;

/**
 * 优先级插件，需要优先执行的插件，该插件作为基础插件，需要为其他组件提供支持
 * @author Yeskery
 * 2023/8/9
 */
public interface PriorityPlugin extends Plugin {
}
