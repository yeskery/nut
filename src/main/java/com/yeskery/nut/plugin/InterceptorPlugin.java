package com.yeskery.nut.plugin;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

/**
 * 拦截器插件
 * @author sprout
 * 2022-05-12 20:32
 */
public interface InterceptorPlugin extends Plugin {

    /**
     * 处理前置方法
     * @param request 请求对象
     * @param response 响应对象
     * @param execution 执行器
     * @param controller 控制器
     * @return 是否通过当前拦截器
     * @throws Exception 异常
     */
    default boolean beforeHandle(Request request, Response response, Execution execution, Controller controller) throws Exception {
        return true;
    }

    /**
     * 处理后置方法
     * @param request 请求对象
     * @param response 响应对象
     * @param controller 控制器
     * @param e 异常
     * @throws Exception 异常
     */
    default void afterHandle(Request request, Response response, Controller controller, Exception e) throws Exception {

    }
}
