package com.yeskery.nut.plugin;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.ControllerManager;
import com.yeskery.nut.core.Request;

/**
 * controller 插件接口
 * @author sprout
 * 2022-05-12 19:27
 */
public interface ControllerPlugin extends Plugin {

    /**
     * 查找 controller 之前执行前置方法
     * @param controllerManager controller管理器
     * @param request 请求对象
     * @return 需要替换的 controller
     * @throws Exception 异常
     */
    default Controller beforeFindController(ControllerManager controllerManager, Request request) throws Exception {
        return null;
    }

    /**
     * 查找 controller 之后执行后置方法
     * @param controllerManager controller管理器
     * @param request 请求对象
     * @param controller 找到的controller
     * @return 需要替换的 controller
     * @throws Exception 异常
     */
    default Controller afterFindController(ControllerManager controllerManager, Request request, Controller controller) throws Exception {
        return null;
    }
}
