package com.yeskery.nut.plugin;

import com.yeskery.nut.core.Order;

/**
 * 拓展接口
 * @author sprout
 * 2022-05-12 19:19
 */
public interface Plugin extends Order {

    /**
     * 绑定插件信息
     * @param bindContext 绑定上下文
     */
    default void bind(BindContext bindContext) {
    }

    @Override
    default int getOrder() {
        return 0;
    }
}
