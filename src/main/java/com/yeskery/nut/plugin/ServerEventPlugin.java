package com.yeskery.nut.plugin;

import com.yeskery.nut.application.ServerEventContext;

/**
 * 服务器事件插件
 * @author sprout
 * 2022-05-14 19:54
 */
public interface ServerEventPlugin extends Plugin {

    /**
     * 服务启动前置方法
     * @param serverEventContext 服务事件上下文
     */
    default void beforeStart(ServerEventContext serverEventContext) {

    }

    /**
     * 服务启动后置方法
     * @param serverEventContext 服务事件上下文
     */
    default void afterStart(ServerEventContext serverEventContext) {

    }

    /**
     * 服务关闭前置方法
     * @param serverEventContext 服务事件上下文
     */
    default void beforeClose(ServerEventContext serverEventContext) {

    }

    /**
     * 服务关闭后置方法
     * @param serverEventContext 服务事件上下文
     */
    default void afterClose(ServerEventContext serverEventContext) {

    }
}
