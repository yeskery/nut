package com.yeskery.nut.plugin;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

/**
 * 自定义异常处理插件
 * @author sprout
 * 2022-05-13 10:17
 */
public interface ExceptionHandlePlugin extends Plugin {

    /**
     * 处理异常响应结果
     * @param controller Controller对象
     * @param request 请求对象
     * @param response 响应对象
     * @param execution 执行器
     * @param e 发生的异常
     * @return 是否通过当前拦截器
     */
    default boolean handleException(Controller controller, Request request, Response response, Execution execution, Exception e) {
        return false;
    }
}
