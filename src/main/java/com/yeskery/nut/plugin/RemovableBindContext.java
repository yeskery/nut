package com.yeskery.nut.plugin;

/**
 * 可移除的绑定上下文对象
 * @author sprout
 * 2022-06-09 11:11
 */
public interface RemovableBindContext extends BindContext {

    /**
     * 移除对象
     * @param clazz 对象类
     * @param <T> 对象类型
     */
    <T> void removeObject(Class<T> clazz);
}
