package com.yeskery.nut.plugin;

import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文支持基础插件后置处理器
 * @author sprout
 * @version 1.0
 * 2022-08-25 22:25
 */
@FunctionalInterface
public interface ApplicationContextPluginPostProcessor {

    /**
     * 处理方法
     * @param applicationContext 应用上下文
     * @throws Exception 处理过程中的异常
     */
    void process(ApplicationContext applicationContext) throws Exception;
}
