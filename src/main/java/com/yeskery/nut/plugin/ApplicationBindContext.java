package com.yeskery.nut.plugin;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.bean.FactoryBeanRegister;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.util.ReflectUtils;

/**
 * 基于应用上下文的绑定上下文
 * @author YESKERY
 * 2024/1/22
 */
public class ApplicationBindContext extends DefaultBindContext {

    /** FactoryBean注解接口 */
    private final FactoryBeanRegister factoryBeanRegister;

    /**
     * 构建基于应用上下文的绑定上下文
     * @param nutApplication Nut应用对象
     */
    public ApplicationBindContext(NutApplication nutApplication) {
        super(nutApplication);
        factoryBeanRegister = (FactoryBeanRegister) nutApplication.getApplicationContext();
    }

    @Override
    public void bindObject(Class<?> clazz, Object object) {
        factoryBeanRegister.registerBean(ReflectUtils.getDefaultBeanName(clazz), object, clazz);
    }

    @Override
    public <T> void removeObject(Class<T> clazz) {
        throw new NutException("Not Support Remove Object From ApplicationBindContext");
    }

    @Override
    public <T> T getObject(Class<T> clazz) {
        return ((ApplicationContext) factoryBeanRegister).getBean(clazz);
    }
}
