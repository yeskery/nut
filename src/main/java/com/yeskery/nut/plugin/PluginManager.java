package com.yeskery.nut.plugin;

import java.util.Collection;

/**
 * 插件管理器
 * @author sprout
 * 2022-05-12 19:33
 */
public interface PluginManager {

    /**
     * 获取所有的插件
     * @return 所有的插件
     */
    Collection<Plugin> getAllPlugins();

    /**
     * 获取所有controller插件
     * @return 所有controller插件
     */
    Collection<ControllerPlugin> getControllerPlugins();

    /**
     * 获取所有拦截器插件
     * @return 所有拦截器插件
     */
    Collection<InterceptorPlugin> getInterceptorPlugins();

    /**
     * 获取所有异常处理插件
     * @return 所有异常处理插件
     */
    Collection<ExceptionHandlePlugin> getExceptionHandlePlugins();

    /**
     * 获取所有服务器事件插件
     * @return 所有服务器事件插件
     */
    Collection<ServerEventPlugin> getServerEventPlugins();

    /**
     * 注册插件
     * @param plugin 插件
     */
    void registerPlugin(Plugin plugin);

    /**
     * 注册插件
     * @param pluginClass 插件类
     */
    void registerPlugin(Class<? extends Plugin> pluginClass);

    /**
     * 是否包含指定类型的插件
     * @param clazz 插件类型
     * @return 是否包含指定类型的插件
     */
    boolean containPlugin(Class<? extends Plugin> clazz);

    /**
     * 指定插件是否是web插件
     * @param plugin 插件对象
     * @return 是否是web插件
     */
    default boolean isWebPlugin(Plugin plugin) {
        return (plugin instanceof ControllerPlugin)
                || (plugin instanceof InterceptorPlugin)
                || (plugin instanceof  ExceptionHandlePlugin);
    }
}
