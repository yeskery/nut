package com.yeskery.nut.plugin;

import java.util.Collection;

/**
 * 插件元数据扩展器
 * @author sunjay
 * 2023/9/27
 */
public interface PluginBeanMetadataExtender {

    /**
     * 获取要扩展的插件Bean元数据集合
     * @return 要扩展的插件Bean元数据集合
     */
    Collection<PluginBeanMetadata> getExtendPluginBeanMetadata();
}
