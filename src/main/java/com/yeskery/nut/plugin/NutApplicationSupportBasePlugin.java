package com.yeskery.nut.plugin;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.bean.ApplicationContext;

/**
 * Nut应用基础支持插件
 * @author Yeskery
 * 2023/5/15
 */
public class NutApplicationSupportBasePlugin extends ApplicationContextSupportBasePlugin implements Plugin {

    /** Nut应用对象 */
    private NutApplication nutApplication;

    /**
     * 设置Nut应用对象
     * @param nutApplication Nut应用对象
     */
    public void setNutApplication(NutApplication nutApplication) {
        this.nutApplication = nutApplication;
    }

    /**
     * 获取Nut应用对象
     * @return Nut应用对象
     */
    public NutApplication getNutApplication() {
        return nutApplication;
    }

    @Override
    protected ApplicationContext getApplicationContext() {
        ApplicationContext applicationContext = super.getApplicationContext();
        return applicationContext == null ? nutApplication.getApplicationContext() : applicationContext;
    }
}
