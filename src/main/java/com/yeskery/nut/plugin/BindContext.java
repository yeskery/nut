package com.yeskery.nut.plugin;

import com.yeskery.nut.application.NutApplication;

/**
 * 绑定上下文对象
 * @author sprout
 * @version 1.0
 * 2022-05-29 21:51
 */
public interface BindContext {

    /**
     * 获取Nut应用对象
     * @return Nut应用对象
     */
    NutApplication getNutApplication();

    /**
     * 绑定对象
     * @param clazz 对象类
     * @param object 对象
     */
    void bindObject(Class<?> clazz, Object object);

    /**
     * 获取对象
     * @param clazz 对象类
     * @param <T> 对象类型
     * @return 对象
     */
    <T> T getObject(Class<T> clazz);
}
