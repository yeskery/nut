package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.util.http.HttpConfiguration;

/**
 * Http配置支持接口
 * @author YESKERY
 * 2024/8/16
 */
public interface HttpConfigurationSupport {

    /**
     * 获取Http配置对象
     * @return Http配置对象
     */
    HttpConfiguration getHttpConfiguration();
}
