package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.annotation.cloud.feign.FeignClient;

/**
 * FeignClient请求数据
 * @author YESKERY
 * 2024/9/19
 */
public class FeignClientRequestData extends FeignRequestData {

    /** feignClient注解 */
    private FeignClient feignClient;

    /**
     * 获取feignClient注解
     * @return FeignClient注解
     */
    public FeignClient getFeignClient() {
        return feignClient;
    }

    /**
     * 设置feignClient注解
     * @param feignClient feignClient注解
     */
    public void setFeignClient(FeignClient feignClient) {
        this.feignClient = feignClient;
    }
}
