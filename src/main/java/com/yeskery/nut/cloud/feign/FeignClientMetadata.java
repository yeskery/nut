package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.annotation.cloud.feign.FeignClient;

/**
 * feign客户端元数据
 * @author sunjay
 * 2024/1/7
 */
public class FeignClientMetadata {

    /** feign客户端接口类 */
    private Class<?> type;

    /** feign客户端注解对象 */
    private FeignClient feignClient;

    /**
     * 获取feign客户端接口类
     * @return feign客户端接口类
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 设置feign客户端接口类
     * @param type feign客户端接口类
     */
    public void setType(Class<?> type) {
        this.type = type;
    }

    /**
     * 获取feign客户端注解对象
     * @return feign客户端注解对象
     */
    public FeignClient getFeignClient() {
        return feignClient;
    }

    /**
     * 设置feign客户端注解对象
     * @param feignClient feign客户端注解对象
     */
    public void setFeignClient(FeignClient feignClient) {
        this.feignClient = feignClient;
    }
}
