package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.core.NutException;

/**
 * Feign客户端异常
 * @author YESKERY
 * 2024/1/4
 */
public class FeignClientException extends NutException {
    /**
     * 构建一个 {@link FeignClientException}
     */
    public FeignClientException() {
    }

    /**
     * 构建一个 {@link FeignClientException}
     * @param message message
     */
    public FeignClientException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link FeignClientException}
     * @param message message
     * @param cause cause
     */
    public FeignClientException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link FeignClientException}
     * @param cause cause
     */
    public FeignClientException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link FeignClientException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public FeignClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
