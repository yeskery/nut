package com.yeskery.nut.cloud.feign;

/**
 * feign失败回调工厂接口
 * @author YESKERY
 * 2024/1/8
 */
public interface FallbackFactory<T> {

    /**
     * 根据发生的异常返回失败回调接口类
     * @param throwable 发生的异常
     * @return 失败回调接口类
     */
    T create(Throwable throwable);

    /**
     * 默认的feign失败回调工厂接口
     * @author YESKERY
     * 2024/1/8
     */
    final class Default<T> implements FallbackFactory<T> {

        /** 工厂实例 */
        private final T instance;

        /**
         * 构建默认的feign失败回调工厂接口
         * @param instance 工厂实例
         */
        public Default(T instance) {
            this.instance = instance;
        }

        @Override
        public T create(Throwable throwable) {
            return instance;
        }
    }
}
