package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.cloud.registry.core.Instance;
import com.yeskery.nut.core.InputStreamResource;
import com.yeskery.nut.core.NameAndValue;

import java.util.List;

/**
 * feign请求数据
 * @author YESKERY
 * 2024/1/10
 */
public class FeignRequestData {

    /** 请求url */
    private String url;

    /** 服务实例 */
    private Instance instance;

    /** 请求头 */
    private List<NameAndValue> headers;

    /** 请求参数 */
    private List<NameAndValue> params;

    /** 请求体字符串 */
    private String bodyString;

    /** 请求体资源对象 */
    private InputStreamResource bodyResource;

    /**
     * 获取请求url
     * @return 请求url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置请求url
     * @param url 请求url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取服务实例对象
     * @return 服务实例对象
     */
    public Instance getInstance() {
        return instance;
    }

    /**
     * 设置服务实例对象
     * @param instance 服务实例对象
     */
    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    /**
     * 获取请求头
     * @return 请求头
     */
    public List<NameAndValue> getHeaders() {
        return headers;
    }

    /**
     * 设置请求头
     * @param headers 请求头
     */
    public void setHeaders(List<NameAndValue> headers) {
        this.headers = headers;
    }

    /**
     * 获取请求参数
     * @return 请求参数
     */
    public List<NameAndValue> getParams() {
        return params;
    }

    /**
     * 设置请求参数
     * @param params 请求参数
     */
    public void setParams(List<NameAndValue> params) {
        this.params = params;
    }

    /**
     * 获取请求体字符串
     * @return 请求体字符串
     */
    public String getBodyString() {
        return bodyString;
    }

    /**
     * 设置请求体字符串
     * @param bodyString 请求体字符串
     */
    public void setBodyString(String bodyString) {
        this.bodyString = bodyString;
    }

    /**
     * 获取请求体资源对象
     * @return 请求体资源对象
     */
    public InputStreamResource getBodyResource() {
        return bodyResource;
    }

    /**
     * 设置请求体资源对象
     * @param bodyResource 请求体资源对象
     */
    public void setBodyResource(InputStreamResource bodyResource) {
        this.bodyResource = bodyResource;
    }
}
