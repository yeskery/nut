package com.yeskery.nut.cloud.feign;

import java.lang.annotation.Annotation;

/**
 * 请求注解类型
 * @author YESKERY
 * 2024/1/10
 */
public class RequestAnnotationType {

    /** 注解类型 */
    private Class<? extends Annotation> annotationType;

    /** 注解对象 */
    private Annotation annotation;

    /**
     * 构建请求注解类型
     */
    public RequestAnnotationType() {
    }

    /**
     * 构建请求注解类型
     * @param annotationType 注解类型
     * @param annotation 注解对象
     */
    public RequestAnnotationType(Class<? extends Annotation> annotationType, Annotation annotation) {
        this.annotationType = annotationType;
        this.annotation = annotation;
    }

    /**
     * 获取注解类型
     * @return 注解类型
     */
    public Class<? extends Annotation> getAnnotationType() {
        return annotationType;
    }

    /**
     * 设置注解类型
     * @param annotationType 注解类型
     */
    public void setAnnotationType(Class<? extends Annotation> annotationType) {
        this.annotationType = annotationType;
    }

    /**
     * 获取注解对象
     * @return 注解对象
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * 设置注解对象
     * @param annotation 注解对象
     */
    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }
}
