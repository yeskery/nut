package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.core.Environment;
import com.yeskery.nut.util.StringUtils;
import com.yeskery.nut.util.http.HttpConfiguration;

/**
 * 默认的Http配置支持对象
 * @author YESKERY
 * 2024/8/16
 */
public class DefaultHttpConfigurationSupport implements HttpConfigurationSupport {

    /** http连接超时时间key */
    private static final String CONNECT_OUT_TIME_KEY = "nut.cloud.feign.http.connectOutTime";

    /** http读取超时时间key */
    private static final String READ_OUT_TIME_KEY = "nut.cloud.feign.http.readOutTime";

    /** 环境对象 */
    private final Environment environment;

    /**
     * 构建默认的Http配置支持对象
     * @param environment 环境对象
     */
    public DefaultHttpConfigurationSupport(Environment environment) {
        this.environment = environment;
    }

    @Override
    public HttpConfiguration getHttpConfiguration() {
        HttpConfiguration configuration = HttpConfiguration.getDefaultHttpConfiguration();
        String connectOutTime = environment.getEnvProperty(CONNECT_OUT_TIME_KEY);
        String readOutTime = environment.getEnvProperty(READ_OUT_TIME_KEY);
        try {
            if (StringUtils.isEmpty(connectOutTime)) {
                configuration.setConnectOutTime(Integer.parseInt(connectOutTime));
            }
            if (StringUtils.isEmpty(readOutTime)) {
                configuration.setReadOutTime(Integer.parseInt(readOutTime));
            }
        } catch (NumberFormatException e) {
            // Not Deal.
        }
        return configuration;
    }
}
