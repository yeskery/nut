package com.yeskery.nut.cloud.feign;

/**
 * 记录统计对象
 * @author YESKERY
 * 2024/1/19
 */
public class RecordStats {

    /** 开始时间戳 */
    private long startTime;

    /** 结束时间戳 */
    private long endTime;

    /** 失败的时间戳 */
    private long failTime;

    /** 请求是否成功 */
    private boolean success;

    /**
     * 获取开始时间戳
     * @return 开始时间戳
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间戳
     * @param startTime 开始时间戳
     */
    void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间戳
     * @return 结束时间戳
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间戳
     * @param endTime 结束时间戳
     */
    void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取失败时间戳
     * @return 失败时间戳
     */
    public long getFailTime() {
        return failTime;
    }

    /**
     * 设置失败时间戳
     * @param failTime 失败时间戳
     */
    void setFailTime(long failTime) {
        this.failTime = failTime;
    }

    /**
     * 请求是否成功
     * @return 是否成功
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * 设置请求成功
     */
    void setSuccess() {
        this.success = true;
    }

    /**
     * 设置请求失败
     */
    void setFail() {
        this.success = false;
    }
}
