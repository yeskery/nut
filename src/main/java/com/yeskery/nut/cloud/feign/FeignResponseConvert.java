package com.yeskery.nut.cloud.feign;

import java.lang.reflect.Type;

/**
 * Feign客户端响应转换接口
 * @author YESKERY
 * 2024/9/19
 */
public interface FeignResponseConvert {

    /**
     * 转换响应
     * @param data 响应字符串
     * @param type 类型
     * @return 转换后的对象
     * @param <T> 类型
     */
    <T> T convert(String data, Type type);
}
