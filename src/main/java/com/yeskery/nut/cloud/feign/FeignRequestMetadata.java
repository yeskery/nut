package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.core.Method;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * feign请求元数据
 * @author sunjay
 * 2024/1/7
 */
public class FeignRequestMetadata {

    /** 服务名称 */
    private String name;

    /** 服务url */
    private String url;

    /** 请求方法 */
    private Method method;

    /** 消费类型，指定<code>Content-Type</code>请求头 */
    private String[] consumes;

    /** 返回类型，指定<code>Accept</code>请求头 */
    private String[] produces;

    /** 请求参数注解map */
    private Map<Integer, RequestAnnotationType> requestAnnotationTypeMap = new HashMap<>();

    /** 返回类型 */
    private Class<?> returnType;

    /** 返回泛型类型 */
    private Type genericReturnType;

    /**
     * 获取服务名称
     * @return 服务名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置服务名称
     * @param name 服务名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取请求url
     * @return 请求url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置请求url
     * @param url 请求url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取请求方法
     * @return 请求方法
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置请求方法
     * @param method 请求方法
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * 获取消费类型
     * @return 消费类型
     */
    public String[] getConsumes() {
        return consumes;
    }

    /**
     * 设置消费类型
     * @param consumes 消费类型
     */
    public void setConsumes(String[] consumes) {
        this.consumes = consumes;
    }

    /**
     * 获取返回类型
     * @return 返回类型
     */
    public String[] getProduces() {
        return produces;
    }

    /**
     * 设置返回类型
     * @param produces 返回类型
     */
    public void setProduces(String[] produces) {
        this.produces = produces;
    }

    /**
     * 获取请求参数注解map
     * @return 请求参数注解map
     */
    public Map<Integer, RequestAnnotationType> getRequestAnnotationTypeMap() {
        return requestAnnotationTypeMap;
    }

    /**
     * 设置请求参数注解map
     * @param requestAnnotationTypeMap 请求参数注解map
     */
    public void setRequestAnnotationTypeMap(Map<Integer, RequestAnnotationType> requestAnnotationTypeMap) {
        this.requestAnnotationTypeMap = requestAnnotationTypeMap;
    }

    /**
     * 获取返回类型
     * @return 返回类型
     */
    public Class<?> getReturnType() {
        return returnType;
    }

    /**
     * 设置返回类型
     * @param returnType 返回类型
     */
    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }

    /**
     * 获取返回泛型类型
     * @return 返回泛型类型
     */
    public Type getGenericReturnType() {
        return genericReturnType;
    }

    /**
     * 设置返回泛型类型
     * @param genericReturnType 返回泛型类型
     */
    public void setGenericReturnType(Type genericReturnType) {
        this.genericReturnType = genericReturnType;
    }
}
