package com.yeskery.nut.cloud.feign;

import com.yeskery.nut.core.Order;

/**
 * Feign请求拦截器
 * @author sunjay
 * 2024/3/23
 */
public interface FeignRequestInterceptor extends Order {

    /**
     * 拦截器处理方法
     * @param feignRequestMetadata feign请求元数据
     * @param feignRequestData feign请求数据
     */
    void process(FeignRequestMetadata feignRequestMetadata, FeignRequestData feignRequestData);
}
