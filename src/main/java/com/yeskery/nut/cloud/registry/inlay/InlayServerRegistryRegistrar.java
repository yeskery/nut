package com.yeskery.nut.cloud.registry.inlay;

import com.yeskery.nut.annotation.bean.Bean;
import com.yeskery.nut.annotation.bean.Configuration;
import com.yeskery.nut.annotation.condition.ConditionalOnWebApplicationType;

/**
 * 内置服务注册注册器
 * @author sunjay
 * 2024/1/6
 */
@Configuration
public class InlayServerRegistryRegistrar {

    /**
     * 配置内置服务注册bean
     * @return 内置服务注册bean
     */
    @Bean
    @ConditionalOnWebApplicationType
    public InlayServerRegistry inlayServerRegistry() {
        return new InlayServerRegistry();
    }
}
