package com.yeskery.nut.cloud.registry.core;

import java.util.HashMap;
import java.util.Map;

/**
 * 内置元数据对象
 * @author YESKERY
 * 2024/1/4
 */
public class DefaultServerMetadata implements ServerMetadata {

    /** 元数据对象 */
    private final Map<String, String> metadataMap = new HashMap<>(8);

    /** 服务名称 */
    private String name;

    /** 服务版本号 */
    private String version;

    /** 服务注册时间戳 */
    private Long registerTime;

    /** 最后一次更新时间戳 */
    private Long latestUpdateTime;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public Long getRegisterTime() {
        return registerTime;
    }

    @Override
    public Long getLatestUpdateTime() {
        return latestUpdateTime;
    }

    @Override
    public Map<String, String> getMetadata() {
        return metadataMap;
    }

    /**
     * 设置服务名称
     * @param name 服务名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 设置服务版本号
     * @param version 服务版本号
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 设置服务注册时间
     * @param registerTime 服务注册时间
     */
    public void setRegisterTime(Long registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * 设置最后服务更新时间
     * @param latestUpdateTime 最后服务更新时间
     */
    public void setLatestUpdateTime(Long latestUpdateTime) {
        this.latestUpdateTime = latestUpdateTime;
    }
}
