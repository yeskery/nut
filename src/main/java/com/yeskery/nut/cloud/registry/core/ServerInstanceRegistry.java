package com.yeskery.nut.cloud.registry.core;

import java.util.Collection;

/**
 * 服务实例注册器
 * @author YESKERY
 * 2024/1/4
 */
public interface ServerInstanceRegistry {

    /**
     * 获取服务实例所属服务对象
     * @return 服务对象
     */
    Server getServer();

    /**
     * 获取所有的服务实例集合
     * @return 所有的服务实例集合
     */
    Collection<Instance> getInstances();

    /**
     * 注册服务实例
     * @param metadata 服务实例元数据
     */
    void registerInstance(ServerInstanceMetadata metadata);

    /**
     * 是否包含注册服务实例
     * @param metadata 注册服务实例
     * @return 是否包含注册服务实例
     */
    boolean contains(ServerInstanceMetadata metadata);

    /**
     * 移除服务实例
     * @param id 服务实例id
     */
    void removeInstance(String id);
}
