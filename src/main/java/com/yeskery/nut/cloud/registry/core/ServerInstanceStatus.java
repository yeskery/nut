package com.yeskery.nut.cloud.registry.core;

/**
 * 服务实例状态
 * @author YESKERY
 * 2024/1/5
 */
public enum ServerInstanceStatus {
    /** 状态正常 */
    OK,
    /** 状态异常 */
    WARNING,
    /** 状态失败 */
    FAIL
}
