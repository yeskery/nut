package com.yeskery.nut.cloud.registry.inlay;

import java.util.List;

/**
 * 内置服务同步对象集合
 * @author YESKERY
 * 2024/1/5
 */
public class InlayServersAsync {

    /** 内置服务同步对象 */
    private List<InlayServerAsync> servers;

    /**
     * 获取服务同步对象
     * @return 服务同步对象
     */
    public List<InlayServerAsync> getServers() {
        return servers;
    }

    /**
     * 设置服务同步对象
     * @param servers 服务同步对象
     */
    public void setServers(List<InlayServerAsync> servers) {
        this.servers = servers;
    }
}
