package com.yeskery.nut.cloud.registry.inlay;

/**
 * 内置服务实例同步对象
 * @author YESKERY
 * 2024/1/5
 */
public class InlayServerInstanceAsync {

    /** 协议头 */
    private String scheme;

    /** ip */
    private String ip;

    /** 端口号 */
    private Integer port;

    /**
     * 获取协议头
     * @return 协议头
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * 设置协议头
     * @param scheme 协议头
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * 获取ip
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * 设置ip
     * @param ip ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 获取端口号
     * @return 端口号
     */
    public Integer getPort() {
        return port;
    }

    /**
     * 设置端口号
     * @param port 端口号
     */
    public void setPort(Integer port) {
        this.port = port;
    }
}
