package com.yeskery.nut.cloud.registry.core;

import java.util.Map;

/**
 * 服务元数据
 * @author YESKERY
 * 2024/1/4
 */
public interface ServerMetadata {

    /**
     * 获取服务名称
     * @return 服务名称
     */
    String getName();

    /**
     * 获取服务版本号
     * @return 服务版本号
     */
    String getVersion();

    /**
     * 获取注册时间戳
     * @return 注册时间戳
     */
    Long getRegisterTime();

    /**
     * 获取最近更新时间戳
     * @return 更新时间戳
     */
    Long getLatestUpdateTime();

    /**
     * 获取元数据map
     * @return 元数据map
     */
    Map<String, String> getMetadata();
}
