package com.yeskery.nut.cloud.registry.core;

import java.util.ArrayList;
import java.util.List;

/**
 * 内置服务类
 * @author YESKERY
 * 2024/1/4
 */
public class DefaultServer implements Server {

    /** 服务id */
    private String id;

    /** 服务实例注册器 */
    private ServerInstanceRegistry serverInstanceRegistry;

    /** 服务元数据 */
    private ServerMetadata serverMetadata;

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return serverMetadata.getName();
    }

    @Override
    public List<Instance> getInstances() {
        return new ArrayList<>(serverInstanceRegistry.getInstances());
    }

    @Override
    public ServerInstanceRegistry getServerInstanceRegistry() {
        return serverInstanceRegistry;
    }

    @Override
    public ServerMetadata getMetadata() {
        return serverMetadata;
    }

    /**
     * 设置服务id
     * @param id 服务id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 设置服务实例注册器
     * @param serverInstanceRegistry 服务实例注册器
     */
    public void setServerInstanceRegistry(ServerInstanceRegistry serverInstanceRegistry) {
        this.serverInstanceRegistry = serverInstanceRegistry;
    }

    /**
     * 设置服务元数据
     * @param serverMetadata 服务元数据
     */
    public void setServerMetadata(ServerMetadata serverMetadata) {
        this.serverMetadata = serverMetadata;
    }
}
