package com.yeskery.nut.cloud.registry.core;

import com.yeskery.nut.core.NutException;

/**
 * 注册中心异常
 * @author YESKERY
 * 2024/1/4
 */
public class RegistryException extends NutException {
    /**
     * 构建一个 {@link RegistryException}
     */
    public RegistryException() {
    }

    /**
     * 构建一个 {@link RegistryException}
     * @param message message
     */
    public RegistryException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link RegistryException}
     * @param message message
     * @param cause cause
     */
    public RegistryException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link RegistryException}
     * @param cause cause
     */
    public RegistryException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link RegistryException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public RegistryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
