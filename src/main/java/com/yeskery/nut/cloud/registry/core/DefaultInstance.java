package com.yeskery.nut.cloud.registry.core;

/**
 * 内置服务实例
 * @author YESKERY
 * 2024/1/4
 */
public class DefaultInstance implements Instance {

    /** id */
    private String id;

    /** 服务实例元数据 */
    private ServerInstanceMetadata serverInstanceMetadata;

    @Override
    public String id() {
        return id;
    }

    @Override
    public String getScheme() {
        return serverInstanceMetadata.getScheme();
    }

    @Override
    public String getIp() {
        return serverInstanceMetadata.getIp();
    }

    @Override
    public ServerInstanceStatus getStatus() {
        return serverInstanceMetadata.getStatus();
    }

    @Override
    public void setStatus(ServerInstanceStatus status) {
        ((DefaultServerInstanceMetadata) serverInstanceMetadata).setStatus(status);
    }

    @Override
    public Integer getPort() {
        return serverInstanceMetadata.getPort();
    }

    @Override
    public ServerInstanceMetadata getMetadata() {
        return serverInstanceMetadata;
    }

    /**
     * 设置服务实例id
     * @param id 服务实例id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 设置服务实例元数据
     * @param serverInstanceMetadata 服务实例元数据
     */
    public void setServerInstanceMetadata(ServerInstanceMetadata serverInstanceMetadata) {
        this.serverInstanceMetadata = serverInstanceMetadata;
    }
}
