package com.yeskery.nut.cloud.registry.inlay;

import java.util.List;

/**
 * 内置服务同步对象
 * @author YESKERY
 * 2024/1/5
 */
public class InlayServerAsync {

    /** 服务名称 */
    private String name;

    /** 服务版本号 */
    private String version;

    /** 服务实例 */
    private List<InlayServerInstanceAsync> instances;

    /**
     * 获取服务名称
     * @return 服务名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置服务名称
     * @param name 服务名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取版本号
     * @return 版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置版本号
     * @param version 版本号
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 获取服务实例
     * @return 服务实例
     */
    public List<InlayServerInstanceAsync> getInstances() {
        return instances;
    }

    /**
     * 设置服务实例
     * @param instances 服务实例
     */
    public void setInstances(List<InlayServerInstanceAsync> instances) {
        this.instances = instances;
    }
}
