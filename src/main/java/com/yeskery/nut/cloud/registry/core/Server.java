package com.yeskery.nut.cloud.registry.core;

import java.util.List;

/**
 * 服务接口
 * @author YESKERY
 * 2024/1/4
 */
public interface Server {

    /**
     * 服务id
     * @return 服务id
     */
    String id();

    /**
     * 服务名称
     * @return 服务名称
     */
    String name();

    /**
     * 获取服务的所有实例
     * @return 所有的服务实例
     */
    List<Instance> getInstances();

    /**
     * 获取服务实例注册器
     * @return 服务实例注册器
     */
    ServerInstanceRegistry getServerInstanceRegistry();

    /**
     * 获取服务元数据
     * @return 服务元数据
     */
    ServerMetadata getMetadata();
}
