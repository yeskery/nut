package com.yeskery.nut.cloud.registry.nacos;

import com.yeskery.nut.cloud.registry.core.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Nacos服务对象
 * @author YESKERY
 * 2024/1/16
 */
public class NacosServer implements Server {

    /** 服务id */
    private String id;

    /** 服务名称 */
    private String name;

    /** 服务实例注册器 */
    private NacosServerInstanceRegistry serverInstanceRegistry;

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public List<Instance> getInstances() {
        return new ArrayList<>(serverInstanceRegistry.getInstances());
    }

    @Override
    public ServerInstanceRegistry getServerInstanceRegistry() {
        return serverInstanceRegistry;
    }

    @Override
    public ServerMetadata getMetadata() {
        throw new RegistryException("UnSupport Server Metadata.");
    }

    /**
     * 设置服务id
     * @param id 服务id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 设置服务名称
     * @param name 服务名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 设置服务实例注册器
     * @param serverInstanceRegistry 服务实例注册器
     */
    public void setServerInstanceRegistry(NacosServerInstanceRegistry serverInstanceRegistry) {
        this.serverInstanceRegistry = serverInstanceRegistry;
    }
}
