package com.yeskery.nut.cloud.registry.core;

import com.yeskery.nut.cloud.registry.core.ServerInstanceStatus;

import java.util.Map;

/**
 * 服务实例元数据
 * @author YESKERY
 * 2024/1/4
 */
public interface ServerInstanceMetadata {

    /**
     * 获取协议头
     * @return 协议头
     */
    String getScheme();

    /**
     * 获取ip
     * @return ip
     */
    String getIp();

    /**
     * 获取端口号
     * @return 端口号
     */
    Integer getPort();

    /**
     * 获取服务状态
     * @return 服务状态
     */
    ServerInstanceStatus getStatus();

    /**
     * 获取元数据map
     * @return 元数据map
     */
    Map<String, String> getMetadata();
}
