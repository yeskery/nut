package com.yeskery.nut.cloud.registry.core;

/**
 * 服务实例
 * @author YESKERY
 * 2024/1/4
 */
public interface Instance {

    /**
     * 实例id
     * @return 实例id
     */
    String id();

    /**
     * 获取协议头
     * @return 协议头
     */
    String getScheme();

    /**
     * 获取ip
     * @return ip
     */
    String getIp();

    /**
     * 获取服务状态
     * @return 服务状态
     */
    ServerInstanceStatus getStatus();

    /**
     * 设置服务状态
     * @param status 服务实例状态
     */
    void setStatus(ServerInstanceStatus status);

    /**
     * 获取端口号
     * @return 端口号
     */
    Integer getPort();

    /**
     * 获取实例元数据
     * @return 实例元数据
     */
    ServerInstanceMetadata getMetadata();
}
