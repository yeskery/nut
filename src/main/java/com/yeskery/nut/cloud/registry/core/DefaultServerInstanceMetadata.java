package com.yeskery.nut.cloud.registry.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 内置元数据对象
 * @author YESKERY
 * 2024/1/4
 */
public class DefaultServerInstanceMetadata implements ServerInstanceMetadata {

    /** 元数据对象 */
    private final Map<String, String> metadataMap = new HashMap<>(8);

    /** 协议头 */
    private String scheme;

    /** ip */
    private String ip;

    /** 端口号 */
    private Integer port;

    /** 服务实例状态 */
    private ServerInstanceStatus status;

    @Override
    public String getScheme() {
        return scheme;
    }

    @Override
    public String getIp() {
        return ip;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public ServerInstanceStatus getStatus() {
        return status;
    }

    @Override
    public Map<String, String> getMetadata() {
        return metadataMap;
    }

    /**
     * 设置协议头
     * @param scheme 协议头
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * 设置ip
     * @param ip ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 设置端口号
     * @param port 端口号
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * 设置服务实例状态
     * @param status 服务实例状态
     */
    public void setStatus(ServerInstanceStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheme, ip, port);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DefaultServerInstanceMetadata that = (DefaultServerInstanceMetadata) object;
        return scheme.equals(that.scheme) && ip.equals(that.ip) && port.equals(that.port);
    }

    @Override
    public String toString() {
        return "DefaultServerInstanceMetadata{" +
                "scheme='" + scheme + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
