package com.yeskery.nut.cloud.registry.core;

import java.util.Collection;

/**
 * 服务注册器
 * @author YESKERY
 * 2024/1/4
 */
public interface ServerRegistry {

    /**
     * 根据名称查询服务对象
     * @param name 服务名称
     * @return 服务对象
     */
    Server getServer(String name);

    /**
     * 获取所有的服务对象集合
     * @return 所有的服务对象集合
     */
    Collection<Server> getServers();

    /**
     * 注册服务
     * @param metadata 服务元数据
     * @return 注册后的服务对象
     */
    Server registerServer(ServerMetadata metadata);

    /**
     * 移除服务
     * @param name 服务名称
     */
    void removeServer(String name);
}
