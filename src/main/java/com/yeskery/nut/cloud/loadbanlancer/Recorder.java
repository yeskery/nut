package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.cloud.feign.RecordStats;
import com.yeskery.nut.cloud.registry.core.Instance;

/**
 * 记录接口
 * @author YESKERY
 * 2024/1/19
 */
public interface Recorder {

    /**
     * 开始调用回调
     * @param recordStats 记录统计对象
     * @param instance 服务实例对象
     */
    default void start(RecordStats recordStats, Instance instance) {}

    /**
     * 成功调用回调
     * @param recordStats 记录统计对象
     * @param instance 服务实例对象
     */
    default void success(RecordStats recordStats, Instance instance) {}

    /**
     * 失败调用回调
     * @param recordStats 记录统计对象
     * @param instance 服务实例对象
     * @param throwable 异常对象
     */
    default void failure(RecordStats recordStats, Instance instance, Throwable throwable) {}
}
