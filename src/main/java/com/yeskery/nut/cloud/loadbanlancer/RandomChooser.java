package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.cloud.registry.core.Instance;
import com.yeskery.nut.cloud.registry.core.Server;

import java.security.SecureRandom;
import java.util.List;

/**
 * 随机选择器
 * @author YESKERY
 * 2024/1/4
 */
public class RandomChooser extends BaseChooser {

    /** 随机器 */
    private final SecureRandom secureRandom = new SecureRandom();

    @Override
    protected Instance doChoose(Server server) {
        List<Instance> instances = server.getInstances();
        return instances.get(secureRandom.nextInt(instances.size()));
    }
}
