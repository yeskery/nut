package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.cloud.registry.core.Instance;
import com.yeskery.nut.cloud.registry.core.Server;

/**
 * 负载均衡选择器
 * @author YESKERY
 * 2024/1/4
 */
public interface Chooser {

    /**
     * 从服务中选择指定的实例
     * @param server 服务对象
     * @return 选择后的服务实例
     */
    Instance choose(Server server);
}
