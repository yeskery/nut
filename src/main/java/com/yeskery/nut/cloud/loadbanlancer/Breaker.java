package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.cloud.registry.core.Instance;

/**
 * 熔断器接口
 * @author YESKERY
 * 2024/1/17
 */
public interface Breaker {

    /**
     * 是否需要熔断
     * @param instance 服务实例对象
     * @return 是否需要熔断
     */
    boolean shouldBreak(Instance instance);
}
