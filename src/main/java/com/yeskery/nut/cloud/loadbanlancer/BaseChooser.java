package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.cloud.registry.core.Instance;
import com.yeskery.nut.cloud.registry.core.Server;

import java.util.List;

/**
 * 基础选择器
 * @author YESKERY
 * 2024/1/4
 */
public abstract class BaseChooser implements Chooser {

    @Override
    public Instance choose(Server server) {
        List<Instance> instances = server.getInstances();
        if (instances.isEmpty()) {
            throw new NoAvailableServerInstanceException("Server[" + server.name() + "] No Available Instance.");
        }
        int instanceSize = instances.size();
        if (instanceSize == 1) {
            return instances.get(0);
        }
        return doChoose(server);
    }

    /**
     * 选择实例
     * @param server 服务对象
     * @return 选择后的实例
     */
    protected abstract Instance doChoose(Server server);
}
