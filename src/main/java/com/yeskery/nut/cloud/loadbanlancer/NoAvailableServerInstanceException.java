package com.yeskery.nut.cloud.loadbanlancer;

import com.yeskery.nut.core.NutException;

/**
 * 无有效服务实例时抛出的异常
 * @author YESKERY
 * 2024/1/4
 */
public class NoAvailableServerInstanceException extends NutException {
    /**
     * 构建一个 {@link NutException}
     */
    public NoAvailableServerInstanceException() {
    }

    /**
     * 构建一个 {@link NoAvailableServerInstanceException}
     * @param message message
     */
    public NoAvailableServerInstanceException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link NoAvailableServerInstanceException}
     * @param message message
     * @param cause cause
     */
    public NoAvailableServerInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link NoAvailableServerInstanceException}
     * @param cause cause
     */
    public NoAvailableServerInstanceException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link NoAvailableServerInstanceException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public NoAvailableServerInstanceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
