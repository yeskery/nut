package com.yeskery.nut.test;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.bean.AnnotationApplicationContext;

/**
 * 用于测试的注解应用上下文
 * @author Yeskery
 * 2023/8/17
 */
public class TestAnnotationApplicationContext extends AnnotationApplicationContext {
    /**
     * 构建用于测试的应用上下文
     * @param nutApplication Nut应用对象
     */
    public TestAnnotationApplicationContext(NutApplication nutApplication) {
        super(nutApplication);
    }
}
