package com.yeskery.nut.view;

import com.yeskery.nut.core.MediaType;

/**
 * 视图的抽象接口
 * @author sprout
 * @version 1.0
 * 2022-07-02 01:23
 */
public interface View extends Render {

    /**
     * 获取视图的媒体类型
     * @return 视图的媒体类型
     */
    MediaType getContentType();
}
