package com.yeskery.nut.view;

import com.yeskery.nut.application.ApplicationType;
import com.yeskery.nut.core.AutoDetector;
import com.yeskery.nut.core.AutoDetectorProvider;
import com.yeskery.nut.core.BaseClassNameOnAutoDetector;
import com.yeskery.nut.core.MultiCondition;
import com.yeskery.nut.view.beetl.BeetlViewResolver;
import com.yeskery.nut.view.freemarker.FreeMarkerViewResolver;
import com.yeskery.nut.view.thymeleaf.ThymeleafViewResolver;

/**
 * 视图解析器自动探测器提供器
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:48
 */
public class ViewResolverAutoDetectorProvider implements AutoDetectorProvider {

    /** Thymeleaf class 名称 */
    private static final String THYMELEAF_CLASS_NAME = "org.thymeleaf.TemplateEngine";

    /** Freemarker class 名称 */
    private static final String FREEMARKER_CLASS_NAME = "freemarker.template.Configuration";

    /** Beetl class 名称 */
    private static final String BEETL_CLASS_NAME = "org.beetl.core.Configuration";

    /** Web视图配置 */
    private final WebViewConfigure webViewConfigure;

    /**
     * 构建视图解析器自动探测器
     * @param webViewConfigure Web视图配置
     */
    public ViewResolverAutoDetectorProvider(WebViewConfigure webViewConfigure) {
        this.webViewConfigure = webViewConfigure;
    }

    @Override
    public AutoDetector[] getAutoDetectors() {
        return new AutoDetector[]{new ThymeleafViewResolverAutoDetector(), new FreeMarkerViewResolverAutoDetector(),
                new BeetlViewResolverAutoDetector()};
    }

    /**
     * 是否支持当前应用类型
     * @param applicationType 应用类型
     * @return 是否支持当前应用类型
     */
    private boolean isSupport(ApplicationType applicationType) {
        return ApplicationType.isWebApplicationType(applicationType);
    }

    /**
     * Thymeleaf视图解析器自动探测器
     * @author sprout
     * 2022-09-14 15:31
     */
    private class ThymeleafViewResolverAutoDetector extends BaseClassNameOnAutoDetector {

        @Override
        public boolean isSupport(ApplicationType applicationType) {
            return ViewResolverAutoDetectorProvider.this.isSupport(applicationType);
        }

        @Override
        protected void completeDetect() {
            webViewConfigure.registerViewResolver(new ThymeleafViewResolver());
        }

        @Override
        protected MultiCondition getMultiCondition() {
            return MultiCondition.ANY;
        }

        @Override
        protected String[] getMultiResources() {
            return new String[]{THYMELEAF_CLASS_NAME};
        }
    }

    /**
     * FreeMarker视图解析器自动探测器
     * @author sprout
     * 2022-09-14 15:31
     */
    private class FreeMarkerViewResolverAutoDetector extends BaseClassNameOnAutoDetector {

        @Override
        public boolean isSupport(ApplicationType applicationType) {
            return ViewResolverAutoDetectorProvider.this.isSupport(applicationType);
        }

        @Override
        protected void completeDetect() {
            webViewConfigure.registerViewResolver(new FreeMarkerViewResolver());
        }

        @Override
        protected MultiCondition getMultiCondition() {
            return MultiCondition.ANY;
        }

        @Override
        protected String[] getMultiResources() {
            return new String[]{FREEMARKER_CLASS_NAME};
        }
    }

    /**
     * Beetl视图解析器自动探测器
     * @author sprout
     * 2022-09-14 15:31
     */
    private class BeetlViewResolverAutoDetector extends BaseClassNameOnAutoDetector {

        @Override
        public boolean isSupport(ApplicationType applicationType) {
            return ViewResolverAutoDetectorProvider.this.isSupport(applicationType);
        }

        @Override
        protected void completeDetect() {
            webViewConfigure.registerViewResolver(new BeetlViewResolver());
        }

        @Override
        protected MultiCondition getMultiCondition() {
            return MultiCondition.ANY;
        }

        @Override
        protected String[] getMultiResources() {
            return new String[]{BEETL_CLASS_NAME};
        }
    }
}
