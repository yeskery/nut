package com.yeskery.nut.view;

import java.util.Locale;

/**
 * 视图解析器
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:41
 */
public interface ViewResolver {

    /**
     * 创建视图对象
     * @param viewName 视图名称
     * @param locale 地区
     * @return 创建好的视图对象
     * @throws Exception 创建异常
     */
    View createView(String viewName, Locale locale) throws Exception;

    /**
     * 获取视图后缀名
     * @return 视图后缀名
     */
    String getPostfix();
}
