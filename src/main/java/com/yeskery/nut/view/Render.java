package com.yeskery.nut.view;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

import java.util.Map;

/**
 * 视图渲染接口
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:24
 */
@FunctionalInterface
public interface Render {

    /**
     * 渲染视图
     * @param model 模型对象
     * @param request 请求对象
     * @param response 响应对象
     * @throws Exception 解析异常
     */
    void render(Map<String, ?> model, Request request, Response response) throws Exception;
}
