package com.yeskery.nut.view.freemarker;

import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;
import com.yeskery.nut.util.StringUtils;
import com.yeskery.nut.view.BaseHtmlView;
import com.yeskery.nut.view.WebRender;
import freemarker.template.Configuration;

import java.util.Map;

/**
 * FreeMark视图对象
 * @author sprout
 * @version 1.0
 * 2022-07-02 21:52
 */
public class FreeMarkerView extends BaseHtmlView {

    /** FreeMarker配置对象 */
    private final Configuration configuration;

    /** 模板名称 */
    private final String viewName;

    /**
     * 构建FreeMark视图对象
     * @param configuration FreeMarker配置对象
     * @param viewName 模板名称
     */
    public FreeMarkerView(Configuration configuration, String viewName) {
        if (StringUtils.isEmpty(viewName)) {
            throw new NutException("View Name Must Not Be Empty.");
        }
        this.configuration = configuration;
        this.viewName = viewName;
    }

    @Override
    public void render(Map<String, ?> model, Request request, Response response) throws Exception {
        WebRender webRender = new FreeMarkerWebRender(configuration, getContentType(), model);
        webRender.render(viewName, request, response);
    }
}
