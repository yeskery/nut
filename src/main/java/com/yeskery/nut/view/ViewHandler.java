package com.yeskery.nut.view;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

/**
 * 视图处理器
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:52
 */
@FunctionalInterface
public interface ViewHandler {

    /**
     * 处理视图
     * @param modelAndView 模型和视图对象
     * @param request 请求对象
     * @param response 响应对象
     */
    void handle(ModelAndView modelAndView, Request request, Response response);
}
