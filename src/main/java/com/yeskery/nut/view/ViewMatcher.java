package com.yeskery.nut.view;

/**
 * 视图匹配器
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:43
 */
@FunctionalInterface
public interface ViewMatcher {

    /**
     * 视图路径是否匹配
     * @param viewPath 视图路径
     * @return 视图路径是否匹配
     */
    boolean match(String viewPath);
}
