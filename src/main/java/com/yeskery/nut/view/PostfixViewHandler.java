package com.yeskery.nut.view;

import java.util.Collection;

/**
 * 后缀视图处理器
 * @author sprout
 * @version 1.0
 * 2022-07-02 15:36
 */
public interface PostfixViewHandler extends ViewHandler {

    /**
     * 设置全局前缀
     * @param globalPrefix 全局前缀
     */
    void setGlobalPrefix(String globalPrefix);

    /**
     * 设置全局后缀
     * @param globalPostfix 全局后缀
     */
    void setGlobalPostfix(String globalPostfix);

    /**
     * 注册视图解析器
     * @param viewResolver 视图解析器
     */
    void registerViewResolver(ViewResolver viewResolver);

    /**
     * 获取已注册的试图解析器
     * @return 已注册的试图解析器
     */
    Collection<ViewResolver> getRegisteredViewResolvers();
}
