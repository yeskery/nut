package com.yeskery.nut.view;

import java.util.Collection;

/**
 * 视图配置实现类
 * @author sprout
 * @version 1.0
 * 2022-07-02 15:35
 */
public class WebViewConfigureImpl implements WebViewConfigure {

    /** 后缀视图处理器 */
    private final PostfixViewHandler postfixViewHandler;

    /**
     * 构建视图配置实现类
     * @param postfixViewHandler 后缀视图处理器
     */
    public WebViewConfigureImpl(PostfixViewHandler postfixViewHandler) {
        this.postfixViewHandler = postfixViewHandler;
    }

    @Override
    public WebViewConfigure setGlobalPrefix(String globalPrefix) {
        postfixViewHandler.setGlobalPrefix(globalPrefix);
        return this;
    }

    @Override
    public WebViewConfigure setGlobalPostfix(String globalPostfix) {
        postfixViewHandler.setGlobalPostfix(globalPostfix);
        return this;
    }

    @Override
    public WebViewConfigure registerViewResolver(ViewResolver viewResolver) {
        postfixViewHandler.registerViewResolver(viewResolver);
        return this;
    }

    @Override
    public Collection<ViewResolver> getRegisteredViewResolvers() {
        return postfixViewHandler.getRegisteredViewResolvers();
    }
}
