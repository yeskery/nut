package com.yeskery.nut.view;

import com.yeskery.nut.core.Model;

/**
 * 视图和模型对象
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:53
 */
public class ModelAndView {

    /** 模型对象 */
    private Model model;

    /** 视图对象 */
    private String view;

    /**
     * 构建视图和模型对象
     * @param model 模型对象
     * @param view 视图对象
     */
    public ModelAndView(Model model, String view) {
        this.model = model;
        this.view = view;
    }

    /**
     * 构建视图和模型对象
     */
    public ModelAndView() {
    }

    /**
     * 获取模型对象
     * @return 模型对象
     */
    public Model getModel() {
        return model == null ? Model.DEFAULT_MODEL : model;
    }

    /**
     * 设置模型对象
     * @param model 模型对象
     */
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * 获取视图对象
     * @return 视图对象
     */
    public String getView() {
        return view;
    }

    /**
     * 设置视图对象
     * @param view 视图对象
     */
    public void setView(String view) {
        this.view = view;
    }
}
