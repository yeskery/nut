package com.yeskery.nut.view;

import com.yeskery.nut.core.MediaType;

/**
 * Html视图对象
 * @author sprout
 * @version 1.0
 * 2022-07-02 21:53
 */
public abstract class BaseHtmlView implements View {
    @Override
    public MediaType getContentType() {
        return MediaType.TEXT_HTML;
    }
}
