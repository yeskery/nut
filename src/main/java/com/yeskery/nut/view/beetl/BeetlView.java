package com.yeskery.nut.view.beetl;

import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;
import com.yeskery.nut.util.StringUtils;
import com.yeskery.nut.view.BaseHtmlView;
import com.yeskery.nut.view.WebRender;
import org.beetl.core.GroupTemplate;

import java.util.Map;

/**
 * Beetl视图对象
 * @author sprout
 * @version 1.0
 * 2022-07-02 15:47
 */
public class BeetlView extends BaseHtmlView {

    /** 组合模板对象 */
    private final GroupTemplate groupTemplate;

    /** 模板名称 */
    private final String viewName;

    /**
     * 构建Beetl视图对象
     * @param groupTemplate 组合模板对象
     * @param viewName Beetl视图对象
     */
    public BeetlView(GroupTemplate groupTemplate, String viewName) {
        if (StringUtils.isEmpty(viewName)) {
            throw new NutException("View Name Must Not Be Empty.");
        }
        this.groupTemplate = groupTemplate;
        this.viewName = viewName;
    }

    @Override
    public void render(Map<String, ?> model, Request request, Response response) throws Exception {
        WebRender webRender = new BeetlWebRender(groupTemplate, getContentType(), model);
        webRender.render(viewName, request, response);
    }
}
