package com.yeskery.nut.view;

import com.yeskery.nut.util.StringUtils;

/**
 * 抽象的视图解析器
 * @author sprout
 * @version 1.0
 * 2022-07-02 13:44
 */
public abstract class AbstractViewResolver implements ViewResolver, ViewMatcher {

    /** 视图后后缀 */
    private final String postfix;

    /**
     * 构建抽象的视图解析器
     * @param postfix 视图后后缀
     */
    public AbstractViewResolver(String postfix) {
        this.postfix = postfix;
    }

    @Override
    public String getPostfix() {
        return postfix;
    }

    @Override
    public boolean match(String viewPath) {
        return StringUtils.isEmpty(postfix) || viewPath.endsWith(postfix);
    }
}
