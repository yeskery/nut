package com.yeskery.nut.view;

import java.util.Collection;

/**
 * Web视图配置
 * @author sprout
 * @version 1.0
 * 2022-07-02 15:31
 */
public interface WebViewConfigure {

    /**
     * 设置全局前缀
     * @param globalPrefix 全局前缀
     * @return Web视图配置
     */
    WebViewConfigure setGlobalPrefix(String globalPrefix);

    /**
     * 设置全局后缀
     * @param globalPostfix 全局后缀
     * @return Web视图配置
     */
    WebViewConfigure setGlobalPostfix(String globalPostfix);

    /**
     * 注册视图解析器
     * @param viewResolver 视图解析器
     * @return Web视图配置
     */
    WebViewConfigure registerViewResolver(ViewResolver viewResolver);

    /**
     * 获取已注册的试图解析器
     * @return 已注册的试图解析器
     */
    Collection<ViewResolver> getRegisteredViewResolvers();
}
