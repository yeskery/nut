package com.yeskery.nut.view;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

/**
 * Web渲染器
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:01
 */
@FunctionalInterface
public interface WebRender {

    /**
     * 视图渲染
     * @param key 模板资源id
     * @param request 请求对象
     * @param response 响应对象
     */
    void render(String key, Request request, Response response);
}
