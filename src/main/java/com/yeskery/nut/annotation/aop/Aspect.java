package com.yeskery.nut.annotation.aop;

import com.yeskery.nut.annotation.bean.Component;

import java.lang.annotation.*;

/**
 * 切面注解
 * @author Yeskery
 * 2023/8/7
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface Aspect {
}
