package com.yeskery.nut.annotation.aop;

import java.lang.annotation.*;

/**
 * 切点注解
 * @author Yeskery
 * 2023/8/4
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Pointcut {

    /**
     * 切点表达式，目前支持以下表达式：
     * <ol>
     *     <li>execution：基于方法正则匹配的切点表达式，可以使用<code>*</code>来匹配任意字符串，返回值、方法、参数均需要使用完整类路径</li>
     *     <li>within：用于匹配指定类型内的方法执行</li>
     *     <li>target：用于匹配当前目标对象类型的执行方法；注意是目标对象的类型匹配，这样就不包括引入接口也类型匹配</li>
     *     <li>args：用于匹配当前执行的方法传入的参数为指定类型的执行方法</li>
     *     <li>@annotation：基于方法是否存在注解的切点表达式，需要使用注解类的完整类名</li>
     *     <li>@within：用于匹配所以持有指定注解类型内的方法</li>
     *     <li>@target：用于匹配当前目标对象类型的执行方法，其中目标对象持有指定的注解</li>
     *     <li>@args：用于匹配当前执行的方法传入的参数持有指定注解的执行</li>
     *     <li>bean：Nut AOP扩展的，用于匹配特定名称的Bean对象的执行方法</li>
     * </ol>
     *
     * 切点表达式支持<code>&amp;&amp;</code>、<code>||</code>、<code>!</code>三种逻辑运算符，其中<code>&amp;&amp;</code>、<code>||</code>不可混合使用
     * 示例：
     * <pre>
     *     Pointcut("execution(public void com.yeskery.nut.demo.Test.say())")
     *     public void test() {
     *     }
     *
     *     Pointcut("execution(* void com.yeskery.nut.core.A.*(java.lang.String))")
     *     public void test() {
     *     }
     *
     *     Pointcut("@annotation(com.yeskery.nut.core.A.say)")
     *     public void test() {
     *     }
     * </pre>
     * @return 切点表达式
     */
    String value();
}
