package com.yeskery.nut.annotation.aop;

import java.lang.annotation.*;

/**
 * AOP组合注解，用于复合多个AOP注解
 * @author sprout
 * @version 1.0
 * 2023-04-09 00:38
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Compose {

    /**
     * 前置注解数组
     * @return 前置注解数组
     */
    Before[] beforeArray() default {};

    /**
     * 后置注解数组
     * @return 后置注解数组
     */
    After[] afterArray() default {};

    /**
     * 环绕注解数组
     * @return 环绕注解数组
     */
    Around[] aroundArray() default {};

    /**
     * 异常注解数组
     * @return 异常注解数组
     */
    Throwing[] throwingArray() default {};
}
