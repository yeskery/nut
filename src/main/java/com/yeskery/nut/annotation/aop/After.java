package com.yeskery.nut.annotation.aop;

import java.lang.annotation.*;

/**
 * 后置注解
 * @author sprout
 * @version 1.0
 * 2022-08-28 11:56
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface After {

    /**
     * bean名称，如果和{@link #beanType()}都为默认值，则调用当前对象的{@link #method()}方法
     * @return bean名称
     */
    String beanName() default "";

    /**
     * bean类型，如果和{@link #beanName()}都为默认值，则调用当前对象的{@link #method()}方法
     * @return bean类型
     */
    Class<?> beanType() default void.class;

    /**
     * 需要执行的方法名称，该方法可以接收一个类型为{@link com.yeskery.nut.aop.Execution}的参数
     * <pre>
     *     public void after(Execution execution) {
     *
     *     }
     *
     *     public void after() {
     *
     *     }
     * </pre>
     * @return 需要执行的方法名称
     */
    String method() default "";

    /**
     * 切点表达式，使用方式为：
     * <pre>
     * `@After(methodName())`;
     * </pre>
     * @return 切点表达式
     */
    String pointcut() default "";

    /**
     * 等价{@link #pointcut()}
     * @return 切点表达式
     */
    String value() default "";

    /**
     * 执行顺序值
     * @return 顺序值
     */
    int order() default 0;
}
