package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * Api参数
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiParam {

    /**
     * 参数名称
     * @return 参数名称
     */
    String value();
}
