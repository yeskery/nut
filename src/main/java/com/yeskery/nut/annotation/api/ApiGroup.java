package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * api注解，作用于Controller上，用于api分组
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiGroup {
    /**
     * api分组名称
     * @return 分组名称
     */
    String value();

    /**
     * api分组描述
     * @return 分组描述
     */
    String description() default "";

    /**
     * 扩展文档描述
     * @return 扩展文档描述
     */
    String externalDocsDescription() default "";

    /**
     * 扩展文档url
     * @return 扩展文档url
     */
    String externalDocsUrl() default "";
}
