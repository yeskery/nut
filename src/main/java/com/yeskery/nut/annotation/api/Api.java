package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * Api注解
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Api {

    /**
     * 接口名称
     * @return 接口名称
     */
    String value();

    /**
     * 接口描述
     * @return 接口描述
     */
    String description() default "";

    /**
     * 响应类型
     * @return 响应类型
     */
    Class<?> responseClass() default void.class;
}
