package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * Api模型属性
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiModelProperty {

    /**
     * 模型名称
     * @return 模型名称
     */
    String value();

    /**
     * 示例值
     * @return 示例值
     */
    String example() default "";

    /**
     * 是否必须
     * @return 是否必须
     */
    boolean required() default false;

    /**
     * 是否忽略
     * @return 是否忽略该属性
     */
    boolean ignore() default false;
}
