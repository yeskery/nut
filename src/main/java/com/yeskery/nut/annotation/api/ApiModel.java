package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * Api模型
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiModel {

    /**
     * 模型名称
     * @return 模型名称
     */
    String value();
}
