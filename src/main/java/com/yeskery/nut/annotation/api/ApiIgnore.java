package com.yeskery.nut.annotation.api;

import java.lang.annotation.*;

/**
 * Api忽略注解
 * @author YESKERY
 * 2023/9/21
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiIgnore {

    /**
     * 是否忽略
     * @return 是否忽略
     */
    boolean value() default true;
}
