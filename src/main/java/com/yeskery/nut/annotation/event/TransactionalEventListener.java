package com.yeskery.nut.annotation.event;

import com.yeskery.nut.core.Order;
import com.yeskery.nut.event.ApplicationEvent;
import com.yeskery.nut.transaction.TransactionPhase;

import java.lang.annotation.*;

/**
 * 事务事件监听器
 * @author Yeskery
 * 2023/8/9
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TransactionalEventListener {

    /**
     * 需要监听的事务阶段
     * @return 需要监听的事务阶段
     */
    TransactionPhase phase();

    /**
     * 需要监听的事件类型
     * @return 需要监听的事件类型
     */
    Class<? extends ApplicationEvent> value();

    /**
     * 顺序值
     * @return 顺序值
     */
    int order() default Order.MIN;
}
