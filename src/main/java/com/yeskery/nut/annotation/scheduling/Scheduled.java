package com.yeskery.nut.annotation.scheduling;

import java.lang.annotation.*;

/**
 * 定时调度注解
 * @author sprout
 * @version 1.0
 * 2024-06-04 22:20
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Scheduled {

    /**
     * cron表达式
     * @return cron表达式
     */
    String cron() default "";

    /**
     * crontab表达式所在的时区
     * @return crontab表达式所在的时区
     */
    String zone() default "";

    /**
     * 固定延迟执行时间
     * @return 固定延迟执行时间
     */
    long fixedDelay() default -1L;

    /**
     * 固定延迟执行时间字符串，支持环境变量
     * @return 固定延迟执行时间字符串
     * @see com.yeskery.nut.annotation.environment.Value
     */
    String fixedDelayString() default "";

    /**
     * 固定频率执行时间
     * @return 固定频率执行时间
     */
    long fixedRate() default -1L;

    /**
     * 固定频率执行时间字符串，支持环境变量
     * @return 固定频率执行时间字符串
     * @see com.yeskery.nut.annotation.environment.Value
     */
    String fixedRateString() default "";

    /**
     * 初始化延迟时间
     * @return 初始化延迟时间
     */
    long initialDelay() default -1L;

    /**
     * 初始化延迟时间字符串，支持环境变量
     * @return 初始化延迟时间字符串
     * @see com.yeskery.nut.annotation.environment.Value
     */
    String initialDelayString() default "";
}
