package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnProfileCondition;

import java.lang.annotation.*;

/**
 * 环境条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnProfileCondition.class)
public @interface ConditionalOnProfile {

    /**
     * 环境判断值数组，可使用<code>!</code>取反
     * @return 环境判断值数组
     */
    String[] value() default {};
}
