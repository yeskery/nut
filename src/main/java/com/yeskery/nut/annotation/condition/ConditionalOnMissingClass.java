package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnMissingClassCondition;

import java.lang.annotation.*;

/**
 * 类不存在条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnMissingClassCondition.class)
public @interface ConditionalOnMissingClass {

    /**
     * 类数组
     * @return 类数组
     */
    Class<?>[] value() default {};

    /**
     * 类名称数组
     * @return 类名称数组
     */
    String[] name() default {};
}
