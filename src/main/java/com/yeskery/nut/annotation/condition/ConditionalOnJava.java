package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnJavaCondition;
import com.yeskery.nut.core.JavaVersion;

import java.lang.annotation.*;

/**
 * Java版本条件判断注解
 * @author YESKERY
 * 2023/9/8
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnJavaCondition.class)
public @interface ConditionalOnJava {

    /**
     * 判断的范围(版本更高或版本更低)
     * @return 判断的范围
     */
    Range range() default Range.EQUAL_OR_NEWER;

    /**
     * 要判断的Java版本
     * @return 要判断的Java版本
     */
    JavaVersion value();

    /**
     * Range options.
     */
    enum Range {

        /**
         * 版本相等或更高
         */
        EQUAL_OR_NEWER,

        /**
         * 版本更低
         */
        OLDER_THAN

    }
}
