package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnMissingBeanCondition;

import java.lang.annotation.*;

/**
 * bean不存在条件判断注解，通过插件或使用{@link com.yeskery.nut.bean.FactoryBeanRegister}接口动态注入的Bean对象无法通过条件装配进行判断
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnMissingBeanCondition.class)
public @interface ConditionalOnMissingBean {

    /**
     * bean类型
     * @return bean类型
     */
    Class<?>[] value() default {};

    /**
     * bean名称
     * @return bean名称
     */
    String[] name() default {};
}
