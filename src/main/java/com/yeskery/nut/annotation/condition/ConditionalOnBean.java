package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnBeanCondition;

import java.lang.annotation.*;

/**
 * bean存在条件判断注解，通过插件或使用{@link com.yeskery.nut.bean.FactoryBeanRegister}接口动态注入的Bean对象无法通过条件装配进行判断
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnBeanCondition.class)
public @interface ConditionalOnBean {

    /**
     * bean类型数组
     * @return bean类型数组
     */
    Class<?>[] value() default {};

    /**
     * bean名称数组
     * @return bean名称数组
     */
    String[] name() default {};
}
