package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnStandardApplicationTypeCondition;

import java.lang.annotation.*;

/**
 * Standard应用类型条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnStandardApplicationTypeCondition.class)
public @interface ConditionalOnStandardApplicationType {
}
