package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnClassCondition;

import java.lang.annotation.*;

/**
 * 类存在条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnClassCondition.class)
public @interface ConditionalOnClass {

    /**
     * 类数组
     * @return 类数组
     */
    Class<?>[] value() default {};

    /**
     * 类名称数组
     * @return 类名称数组
     */
    String[] name() default {};
}
