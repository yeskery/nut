package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.application.ServerType;
import com.yeskery.nut.bean.condition.OnServerTypeCondition;

import java.lang.annotation.*;

/**
 * 服务类型条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnServerTypeCondition.class)
public @interface ConditionalOnServerType {

    /**
     * 应用类型
     * @return 应用类型
     */
    ServerType value();
}
