package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnPluginCondition;
import com.yeskery.nut.plugin.Plugin;

import java.lang.annotation.*;

/**
 * 插件存在条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnPluginCondition.class)
public @interface ConditionalOnPlugin {

    /**
     * 插件类型数组
     * @return bean类型数组
     */
    Class<? extends Plugin>[] value() default {};
}
