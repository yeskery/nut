package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.application.ApplicationType;
import com.yeskery.nut.bean.condition.OnApplicationTypeCondition;

import java.lang.annotation.*;

/**
 * 应用类型条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnApplicationTypeCondition.class)
public @interface ConditionalOnApplicationType {

    /**
     * 应用类型
     * @return 应用类型
     */
    ApplicationType[] value() default {};
}
