package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.OnPropertyCondition;

import java.lang.annotation.*;

/**
 * 环境参数值条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnPropertyCondition.class)
public @interface ConditionalOnProperty {

    /**
     * 环境参数值key数组
     * @return 环境参数值key数组
     */
    String[] value() default {};

    /**
     * 环境参数值key前缀
     * @return 环境参数值key前缀
     */
    String prefix() default "";

    /**
     * 要判断的环境参数值value
     * @return 要判断的环境参数值value
     */
    String havingValue() default "";

    /**
     * 环境参数值不存在时是否匹配成功
     * @return 环境参数值不存在时是否匹配成功
     */
    boolean matchIfMissing() default false;
}
