package com.yeskery.nut.annotation.condition;

import com.yeskery.nut.bean.condition.Condition;

import java.lang.annotation.*;

/**
 * 条件判断注解
 * @author sunjay
 * 2023/9/6
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Conditional {

    /**
     * 条件判断接口类型数组
     * @return 条件判断接口类型数组
     */
    Class<? extends Condition>[] value() default {};
}
