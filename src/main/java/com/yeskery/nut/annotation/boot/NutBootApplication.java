package com.yeskery.nut.annotation.boot;

import com.yeskery.nut.annotation.bean.Configuration;
import com.yeskery.nut.application.ApplicationType;
import com.yeskery.nut.application.ServerType;
import com.yeskery.nut.core.AutoDetector;
import com.yeskery.nut.plugin.Plugin;

import java.lang.annotation.*;

/**
 * <p>Nut应用启动主注解，非必须，可以直接通过{@link com.yeskery.nut.application.NutApplication}来启动Nut应用，但是通过该注解可以在
 * 应用启动前设置一部分Nut的配置，当然也可以使用代码的方式进行设置。</p>
 *
 * <p>如果需要使用该注解进行配置Nut应用，应该将该注解标注在应用的启动类上，例如：</p>
 * <pre>
 *     <code>@NutBootApplication(serverType=ServerType.JETTY)</code>
 *     public class Application {
 *         public static void main(String[] args) {
 *             Nut.run(Application.class, args);
 *         }
 *     }
 * </pre>
 * @author sprout
 * @version 1.0
 * 2022-07-31 16:03
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
public @interface NutBootApplication {

    /**
     * 扫描的基础包路径
     * @return 扫描的基础包路径
     */
    String[] baseScanPackage() default {};

    /**
     * 扫描的记录class对象
     * @return 扫描的记录class对象
     */
    Class<?>[] baseScanClass() default {};

    /**
     * 获取应用类型
     * @return 应用类型
     *
     * @see ApplicationType
     */
    ApplicationType applicationType() default ApplicationType.WEB;

    /**
     * 获取服务启动类型
     * @return 服务启动类型
     *
     * @see ServerType
     */
    ServerType serverType() default ServerType.AUTO;

    /**
     * 启用的插件类数组，只能注册有无参且public构造方法的插件类，
     * 其他插件需要通过{@link com.yeskery.nut.plugin.PluginManager#registerPlugin(Plugin)}进行注册
     * @return 启用的插件类数组
     */
    Class<? extends Plugin>[] enablePlugins() default {};

    /**
     * 要忽略的自动探测类数组
     * 与调用{@link com.yeskery.nut.application.NutConfigure#addIgnoreAutoDetectClass(Class)} 方法等效
     * @return 要忽略的自动探测类数组
     */
    Class<? extends AutoDetector>[] ignoreAutoDetectors() default {};
}
