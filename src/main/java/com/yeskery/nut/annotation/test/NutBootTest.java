package com.yeskery.nut.annotation.test;

import com.yeskery.nut.application.ApplicationType;
import com.yeskery.nut.application.ServerType;
import com.yeskery.nut.core.AutoDetector;
import com.yeskery.nut.plugin.Plugin;
import com.yeskery.nut.test.junit.NutExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.*;

/**
 * Nut应用测试注解
 * @author Yeskery
 * 2023/8/17
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ExtendWith(NutExtension.class)
public @interface NutBootTest {

    /**
     * 扫描的基础包路径
     * @return 扫描的基础包路径
     */
    String[] baseScanPackage() default {};

    /**
     * 扫描的记录class对象
     * @return 扫描的记录class对象
     */
    Class<?>[] baseScanClass() default {};

    /**
     * 获取应用类型，只能使用测试模式
     * @return 应用类型
     *
     * @see ApplicationType
     */
    ApplicationType applicationType() default ApplicationType.TEST;

    /**
     * 获取服务启动类型
     * @return 服务启动类型
     *
     * @see ServerType
     */
    ServerType serverType() default ServerType.AUTO;

    /**
     * 启用的插件类数组，只能注册有无参且public构造方法的插件类，
     * 其他插件需要通过{@link com.yeskery.nut.plugin.PluginManager#registerPlugin(Plugin)}进行注册
     * @return 启用的插件类数组
     */
    Class<? extends Plugin>[] enablePlugins() default {};

    /**
     * 要忽略的自动探测类数组
     * 与调用{@link com.yeskery.nut.application.NutConfigure#addIgnoreAutoDetectClass(Class)} 方法等效
     * @return 要忽略的自动探测类数组
     */
    Class<? extends AutoDetector>[] ignoreAutoDetectors() default {};

    /**
     * 日志级别
     * @return 日志级别
     */
    String logLevel() default "INFO";
}
