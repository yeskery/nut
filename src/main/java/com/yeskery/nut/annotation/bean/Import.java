package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>导入注解，用于导入某些不在指定扫描包路径下的bean对象</p>
 * @author sunjay
 * 2024/1/6
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
public @interface Import {

    /**
     * 需要导入的类数组
     * @return 需要导入的类数组
     */
    Class<?>[] value() default {};
}
