package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>延迟注入注解，该注解不会像{@link Lazy}注解一样使用AOP方式在使用bean时进行注入，而是在容器启动完成后进行注入，
 * 不会在bean初始化阶段进行注入，因为是在容器启动完成的时候进行注入并且未使用AOP，无法在{@link com.yeskery.nut.bean.InitializingBean}
 * 接口和{@link javax.annotation.PostConstruct}初始化方法中进行对该bean方法的调用</p>
 *
 * <p>使用该注解只是将bean的注入过程放缓到nut启动完成后，但依然会在bean初始化时对依赖的bean进行依赖验证和初始化，如果依赖的bean是通过
 * {@link com.yeskery.nut.plugin.Plugin}动态加载的，此时需要使用{@link LazyInitialized}注解，否则依赖会出现
 * {@link com.yeskery.nut.bean.NoSuchBeanException}的异常，因为在{@link com.yeskery.nut.plugin.Plugin}插件初始化之前，nut无法获取动态注入到
 * {@link com.yeskery.nut.bean.ApplicationContext}的bean对象。</p>
 *
 * @see LazyInitialized
 *
 * @author YESKERY
 * 2024/10/31
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LazyInject {
}
