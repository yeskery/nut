package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>Bean定义注解，该注解需要与{@link Configuration}注解一起使用，只有在{@link Configuration}注解所标注的类下的{@link Bean}才会进行解析，
 * 可以通过{@link #value()}设置bean的名称</p>
 *
 * @see Configuration
 *
 * @author sprout
 * @version 1.0
 * 2022-07-09 19:15
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Bean {

    /**
     * bean的名称
     * @return bean的名称
     */
    String value() default "";
}
