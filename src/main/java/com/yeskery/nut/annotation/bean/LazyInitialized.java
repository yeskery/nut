package com.yeskery.nut.annotation.bean;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>延迟注入注解，该注解不会像{@link javax.annotation.PostConstruct}注解和{@link com.yeskery.nut.bean.InitializingBean}接口
 * 在bean初始化完成后再执行初始化方法的调用，而是在容器启动完成后进行调用，而是在容器初始化完成后再统计进行bean初始化方法的调用，在初始化方法
 * 中，可以调用使用{@link LazyInject}注解和{@link Autowired}进行进入的对象，因为使用该联合注解的对象将会先被注入。</p>
 *
 * <p>该注解的主要用途是解决{@link javax.annotation.PostConstruct}注解中需要使用使用通过插件动态注入的bean对象而执行报错的问题，
 * 这些bean对象在nut运行前无法确定，需要在nut初始化所有{@link com.yeskery.nut.plugin.Plugin}后再进行使用，
 * 而{@link LazyInitialized}注解的调用时机是等待所有{@link com.yeskery.nut.plugin.Plugin}初始化完成后再进行调用。</p>
 *
 * @see Autowired
 * @see Lazy
 * @see LazyInject
 *
 * @author YESKERY
 * 2024/11/6
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface LazyInitialized {
}
