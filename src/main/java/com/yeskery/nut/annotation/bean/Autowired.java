package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>容器{@link com.yeskery.nut.bean.ApplicationContext}自动注入注解，使用该注解后将会通过容器{@link com.yeskery.nut.bean.ApplicationContext}
 * 对依赖的bean进行注入，使用该注解进行注入时，会通过bean的类型进行匹配注入，如果通过类型匹配到多个该类型的bean，需要使用{@link Qualifier}
 * 注解指定bean的名称来进行匹配，否则将会抛出{@link com.yeskery.nut.bean.NoSuchBeanException}异常</p>
 *
 * <p>如果多个类型的bean在定义时标注了{@link Primary}注解，则优先注入该bean对象，而不会抛出{@link com.yeskery.nut.bean.NoSuchBeanException}异常</p>
 *
 * @see Qualifier
 * @see Primary
 *
 * @author sprout
 * 2022-06-21 10:26
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {

    /**
     * 是否必须存在依赖的bean，如果依赖的bean不存在，则抛出异常{@link com.yeskery.nut.bean.NoSuchBeanException}，
     * 如果允许不存在依赖的bean，则返回<code>false</code>
     * @return 是否必须存在依赖的bean
     */
    boolean required() default true;
}
