package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>bean配置注解，该注解用户定义一批{@link Bean}注解所标注的bean对象，该类下的所有带有{@link Bean}注解的方法都将会注入到
 * {@link com.yeskery.nut.bean.ApplicationContext}中。</p>
 *
 * <pre>
 *     <code>@Configuration</code>
 *     public class Configuration {
 *         <code>@Bean</code>
 *         public Object bean() {
 *             return new Object();
 *         }
 *     }
 * </pre>
 *
 * @see Bean
 *
 * @author sprout
 * @version 1.0
 * 2022-07-09 19:14
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Configuration {
}
