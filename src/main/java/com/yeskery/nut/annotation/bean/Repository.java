package com.yeskery.nut.annotation.bean;

import com.yeskery.nut.aop.ProxyType;

import java.lang.annotation.*;

/**
 * <p>Bean组件定义注解，用于区别是持久化相关的组件，该组件的使用与{@link Component}一致。</p>
 *
 * @see Component
 *
 * @author sprout
 * 2022-06-29 11:38
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Repository {

    /**
     * bean的名称
     * @return bean的名称
     */
    String value() default "";

    /***
     * 如果Bean需要代理，使用的代理类型
     * @return 代理类型
     */
    ProxyType proxyType() default ProxyType.AUTO;
}
