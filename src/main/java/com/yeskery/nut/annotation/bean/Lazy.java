package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>延迟加载的注解，该注解通过在bean创建阶段，注入一个该bean的AOP对象，bean使用的时候在对bean进行初始化，目前支持如下方式的延迟加载：</p>
 * <ol>
 *     <li>@Configuration：该配置内下的所有@Bean都会延迟加载</li>
 *     <li>@Bean：该bean会延迟加载</li>
 *     <li>@Component：该bean会延迟加载</li>
 *     <li>@Component的构造方法：该构造方法的所有参数都延迟加载</li>
 *     <li>@Component的构造方法参数：该参数会延迟加载</li>
 *     <li>@Autowired：属性注入会延迟加载</li>
 *     <li>@Resource：属性注入会延迟加载</li>
 * </ol>
 *
 * @see Autowired
 * @see LazyInject
 * @see LazyInitialized
 *
 * @author Yeskery
 * 2023/8/16
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lazy {

    /**
     * 是否开启延迟加载
     * @return 是否开启延迟加载
     */
    boolean value() default true;
}
