package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>bean别名注解，用来指定要注入的bean的名称，该注解的用途是与{@link Autowired}注解一起使用，在存在多个类型的bean情况下通过名称进行匹配。</p>
 *
 * @see Autowired
 *
 * @author sprout
 * 2022-06-20 21:09
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Qualifier {

    /**
     * 别名名称
     * @return 别名名称
     */
    String value() default "";
}
