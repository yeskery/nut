package com.yeskery.nut.annotation.bean;

import java.lang.annotation.*;

/**
 * <p>优先注入的bean注解，在有多个类型bean同时在容器中，优先注入标志有该注解的bean。该注解需要与{@link Bean}注解配合使用，使用该注解标记的bean对象，
 * 当{@link com.yeskery.nut.bean.ApplicationContext}在注入时存在多个该类型的bean时，会优先使用该bean进行注入，
 * 而不会抛出{@link com.yeskery.nut.bean.NoSuchBeanException}。</p>
 *
 * @see Bean
 * @see Autowired
 *
 * @author Yeskery
 * 2023/8/14
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Primary {
}
