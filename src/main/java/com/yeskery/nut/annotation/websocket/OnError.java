package com.yeskery.nut.annotation.websocket;

import java.lang.annotation.*;

/**
 * 连接发生异常时的处理注解
 *
 * 只可接收以下参数：
 * <ol>
 *     <li>无参</li>
 *     <li>{@link com.yeskery.nut.websocket.Session}参数</li>
 *     <li>{@link com.yeskery.nut.websocket.Session}参数及{@link java.lang.Throwable}的子类，
 *     果用子类异常接收父类异常，该参数值将会为<code>null</code></li>
 * </ol>
 * WebSocket处理器类内如存在多个OnError注解方法，只会有一个方法生效
 *
 * <p>例如:</p>
 * <pre>
 *     <code>
 *     &#64;OnError
 *     public void OnError(Session session, Throwable throwable) {
 *         System.out.println("发生异常" + throwable);
 *     }
 *
 *     &#64;OnError
 *     public void OnError(Session session) {
 *         System.out.println("发生异常" + session.getId());
 *     }
 *
 *     &#64;OnError
 *     public void OnError() {
 *         System.out.println("发生异常");
 *     }
 *     </code>
 * </pre>
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OnError {
}
