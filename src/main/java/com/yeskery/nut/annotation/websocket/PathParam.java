package com.yeskery.nut.annotation.websocket;

import java.lang.annotation.*;

/**
 * 路径参数获取注解
 * @author sprout
 * @version 1.0
 * 2023-04-16 18:02
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@Documented
public @interface PathParam {

    /**
     * 路径参数名
     * @return 路径参数名
     */
    String value() default "";
}
