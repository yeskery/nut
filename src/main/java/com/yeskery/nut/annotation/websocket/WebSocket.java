package com.yeskery.nut.annotation.websocket;

import com.yeskery.nut.annotation.bean.Component;

import java.lang.annotation.*;

/**
 * Websocket服务端点配置注解
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:07
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface WebSocket {

    /**
     * 端点uri
     * @return 端点uri
     */
    String path();
}
