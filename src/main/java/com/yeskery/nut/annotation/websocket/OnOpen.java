package com.yeskery.nut.annotation.websocket;

import java.lang.annotation.*;

/**
 * 连接创建时的处理注解
 *
 * 只可接收无参或{@link com.yeskery.nut.websocket.Session}参数的方法，
 * WebSocket处理器类内如存在多个OnOpen注解方法，只会有一个方法生效
 *
 * <p>例如:</p>
 * <pre>
 *     <code>
 *     &#64;OnOpen
 *     public void onOpen(Session session) {
 *         System.out.println("开启连接" + session.getId());
 *     }
 *
 *     &#64;OnOpen
 *     public void onOpen() {
 *         System.out.println("开启连接");
 *     }
 *     </code>
 * </pre>
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:09
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OnOpen {
}
