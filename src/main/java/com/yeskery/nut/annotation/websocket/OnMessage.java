package com.yeskery.nut.annotation.websocket;

import java.lang.annotation.*;

/**
 * 收到客户端消息时处理注解
 *
 * 只可接收以下参数：
 * <ol>
 *     <li>无参</li>
 *     <li>{@link java.lang.String}及{@link com.yeskery.nut.websocket.Session}参数</li>
 *     <li><code>byte[]</code>及{@link com.yeskery.nut.websocket.Session}参数</li>
 * </ol>
 *
 * WebSocket处理器类内可以存在分别处理字符串和字节数组的两个OnMessage方法，超出的方法将不会生效
 *
 * <p>例如:</p>
 * <pre>
 *     <code>
 *     &#64;OnMessage
 *     public void OnMessage(String text, Session session) {
 *         System.out.println("收到文本消息" + text);
 *     }
 *
 *     &#64;OnMessage
 *     public void OnError(byte[] bytes, Session session) {
 *         System.out.println("收到二进制消息" + bytes);
 *     }
 *
 *     &#64;OnMessage
 *     public void OnMessage() {
 *         System.out.println("收到消息");
 *     }
 *     </code>
 * </pre>
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:13
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OnMessage {
}
