package com.yeskery.nut.annotation.cloud.feign;

import com.yeskery.nut.annotation.bean.Import;
import com.yeskery.nut.cloud.feign.FeignClientsRegistrar;

import java.lang.annotation.*;

/**
 * 启用feign客户端
 * @author sunjay
 * 2024/1/6
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(FeignClientsRegistrar.class)
public @interface EnableFeignClients {

    /**
     * 基础包路径数组
     * @return 基础包路径数组
     */
    String[] basePackages() default {};

    /**
     * 基础类数组
     * @return 基础类数组
     */
    Class<?>[] basePackageClasses() default {};

    /**
     * 要启用的feign客户端类(需要标注{@link FeignClient}注解)数组，如为空则扫描基础包路径下所有标注{@link FeignClient}注解的feign客户端
     * @return 启用的feign客户端数组
     */
    Class<?>[] clients() default {};
}
