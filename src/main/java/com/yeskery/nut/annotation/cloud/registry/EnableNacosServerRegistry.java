package com.yeskery.nut.annotation.cloud.registry;

import com.yeskery.nut.annotation.bean.Import;
import com.yeskery.nut.cloud.registry.nacos.NacosServerRegistryRegistrar;

import java.lang.annotation.*;

/**
 * 启用nacos服务注册
 * @author sunjay
 * 2024/1/6
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(NacosServerRegistryRegistrar.class)
public @interface EnableNacosServerRegistry {
}
