package com.yeskery.nut.annotation.cloud.registry;

import com.yeskery.nut.annotation.bean.Import;
import com.yeskery.nut.cloud.registry.redis.RedisServerRegistryRegistrar;

import java.lang.annotation.*;

/**
 * 启用redis服务注册
 * @author sunjay
 * 2024/1/6
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(RedisServerRegistryRegistrar.class)
public @interface EnableRedisServerRegistry {
}
