package com.yeskery.nut.annotation.cloud.registry;

import com.yeskery.nut.annotation.bean.Import;
import com.yeskery.nut.cloud.registry.inlay.InlayServerRegistryRegistrar;

import java.lang.annotation.*;

/**
 * 启用内置服务注册
 * @author sunjay
 * 2024/1/6
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(InlayServerRegistryRegistrar.class)
public @interface EnableInlayServerRegistry {
}
