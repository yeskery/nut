package com.yeskery.nut.annotation.cloud.feign;

import com.yeskery.nut.cloud.feign.FeignRequestInterceptor;

import java.lang.annotation.*;

/**
 * feign客户端注解，接口仅支持响应<code>application/json</code>
 * @author sunjay
 * 2024/1/7
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeignClient {

    /**
     * 客户端名称
     * @return 客户端名称
     */
    String name();

    /**
     * 客户端url地址，如果设置该值，则直接调用该地址，不使用服务发现
     * @return 客户端url地址
     */
    String url() default "";

    /**
     * 统一请求前缀
     * @return 统一请求前缀
     */
    String path() default "";

    /**
     * 失败的回调处理类，需要实现标注feign客户端标注的接口，并且将该处理类注入到{@link com.yeskery.nut.bean.ApplicationContext}中，
     * 如同时指定{@link #fallbackFactory()}，则只使用回调处理类，回调工厂类无效
     * @return 失败的回调处理类
     */
    Class<?> callback() default void.class;

    /**
     * 失败的回调工厂类，需要实现{@link com.yeskery.nut.cloud.feign.FallbackFactory}，并且将该工厂类注入到{@link com.yeskery.nut.bean.ApplicationContext}中，
     * 如同时指定{@link #callback()}，则只使用回调处理类，回调工厂类无效
     * @return 失败的回调工厂类
     */
    Class<?> fallbackFactory() default void.class;

    /**
     * 发生熔断时的回调处理类，需要实现标注feign客户端标注的接口，并且将该处理类注入到{@link com.yeskery.nut.bean.ApplicationContext}中，
     * 如同时指定{@link #fallbackFactory()}，则只使用回调处理类，回调工厂类无效
     * @return 失败的回调处理类
     */
    Class<?> breakerCallback() default void.class;

    /**
     * 客户端请求拦截器，需要实现{@link com.yeskery.nut.cloud.feign.FeignRequestInterceptor}，默认会从{@link com.yeskery.nut.bean.ApplicationContext}中获取实例bean，
     * 如果不存在，则使用反射创建实例，需要实例有无参构造方法
     * @return 客户端请求拦截器
     */
    Class<? extends FeignRequestInterceptor>[] interceptors() default {};
}
