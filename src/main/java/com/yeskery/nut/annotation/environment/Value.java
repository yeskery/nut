package com.yeskery.nut.annotation.environment;

import java.lang.annotation.*;

/**
 * Value注解，用来获取环境变量中的值
 * @author sprout
 * @version 1.0
 * 2022-07-09 14:18
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Value {

    /**
     * 环境变量的参数名或者实际值
     * @return 环境变量的参数名或者实际值
     */
    String value();
}
