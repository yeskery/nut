package com.yeskery.nut.annotation.environment;

import java.lang.annotation.*;

/**
 * ConfigurationProperties 注解
 * @author sprout
 * @version 1.0
 * 2022-07-12 10:27
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConfigurationProperties {

    /**
     * 获取配置前缀名
     * @return 配置前缀名
     */
    String value() default "";

    /**
     * 获取配置前缀名
     * @return 配置前缀名
     */
    String prefix() default "";
}
