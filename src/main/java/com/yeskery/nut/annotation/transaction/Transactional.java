package com.yeskery.nut.annotation.transaction;

import com.yeskery.nut.transaction.Propagation;
import com.yeskery.nut.transaction.TransactionIsolationLevel;

import java.lang.annotation.*;

/**
 * 事务注解
 * @author sprout
 * @version 1.0
 * 2022-08-28 11:23
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transactional {

    /**
     * 事务隔离级别
     * @return 事务隔离级别
     */
    TransactionIsolationLevel isolation() default TransactionIsolationLevel.DEFAULT;

    /**
     * 事务传播行为
     * @return 事务传播行为
     */
    Propagation propagation() default Propagation.REQUIRED;

    /**
     * 指定异常进行回滚
     * @return 指定异常
     */
    Class<? extends Exception>[] rollbackFor() default Exception.class;
}
