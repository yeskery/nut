package com.yeskery.nut.annotation.async;

import java.lang.annotation.*;

/**
 * 异步执行注解
 * @author sprout
 * @version 1.0
 * 2022-08-29 16:17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Async {

    /**
     * 使用的线程池bean名称，为空则使用系统默认提供的线程池，线程池类型需要为{@link java.util.concurrent.ExecutorService}
     * @return 线程池bean名称
     */
    String value() default "";
}
