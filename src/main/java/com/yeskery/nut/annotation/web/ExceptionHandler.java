package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 异常处理器
 * @author sprout
 * @version 1.0
 * 2022-06-14 23:38
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExceptionHandler {

    /**
     * 要处理的异常数组
     * @return 要处理的异常数组
     */
    Class<? extends Throwable>[] value() default {};

    /**
     * 指定向客户端返回的媒体类型
     * @return 返回的媒体类型
     */
    String[] produces() default {};
}
