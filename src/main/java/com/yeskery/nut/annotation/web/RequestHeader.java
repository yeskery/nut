package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 请求头
 * @author sprout
 * 2022-06-10 21:10
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestHeader {

    /**
     * 获取参数名
     * @return 参数名
     */
    String value() default "";

    /**
     * 是否必须有值
     * @return 是否必须有值
     */
    boolean required() default false;
}
