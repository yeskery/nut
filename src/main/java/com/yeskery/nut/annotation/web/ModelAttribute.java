package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 全局属性处理
 * @author sprout
 * 2022-06-16 17:44
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ModelAttribute {

    /**
     * 属性的key
     * @return 属性的key
     */
    String value() default "";
}
