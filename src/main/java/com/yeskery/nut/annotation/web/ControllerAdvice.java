package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * Controller拦截
 * @author sprout
 * @version 1.0
 * 2022-06-14 23:34
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ControllerAdvice {

    /**
     * Controller扫描的包数组
     * @return Controller扫描的包数组
     */
    String[] value() default {};

    /**
     * Controller扫描的包数组
     * @return Controller扫描的包数组
     */
    String[] basePackages() default {};
}
