package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * Request键值
 * @author sprout
 * 2022-06-10 21:11
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestAttribute {

    /**
     * 获取参数名
     * @return 参数名
     */
    String value() default "";

    /**
     * 是否必须有值
     * @return 是否必须有值
     */
    boolean required() default false;
}
