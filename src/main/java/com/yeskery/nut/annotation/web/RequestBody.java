package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 请求体注解
 * @author sprout
 * 2022-06-10 13:53
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestBody {

    /**
     * 是否必须有值
     * @return 是否必须有值
     */
    boolean required() default false;
}
