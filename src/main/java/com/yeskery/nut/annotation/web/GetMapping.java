package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * GET请求注解
 * @author sprout
 * 2022-06-10 13:55
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GetMapping {

    /**
     * 请求路径
     * @return 请求处理
     */
    String[] value() default {};

    /**
     * 请求路径
     * @return 请求处理
     */
    String[] path() default {};

    /**
     * 请求参数中含有指定参数才能匹配
     * @return 请求参数
     */
    String[] params() default {};

    /**
     * 请求头含有指定参数才能匹配
     * @return 请求头
     */
    String[] headers() default {};

    /**
     * 指定类型的请求媒体类型才能匹配，匹配<code>Content-Type</code>请求头
     * @return 请求媒体类型
     */
    String[] consumes() default {};

    /**
     * 客户端希望接受的媒体类型，匹配<code>Accept</code>请求头，同时会使用该媒体类型对服务进行响应
     * @return 请求媒体类型
     */
    String[] produces() default {};
}
