package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 跨域注解
 * @author YESKERY
 * 2024/6/20
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CrossOrigin {

    /**
     * 允许的请求头
     * @return 允许的请求头
     */
    String[] allowedHeaders() default {};

    /**
     * 允许的请求方法
     * @return 允许的请求方法
     */
    String[] allowedMethods() default {};

    /**
     * 允许的请求域
     * @return 允许的请求域
     */
    String[] allowedOrigins() default {};

    /**
     * 是否允许携带凭证
     * @return 是否允许携带凭证
     */
    boolean allowedCredentials() default true;

    /**
     * 预检请求的有效期
     * @return 预检请求的有效期
     */
    long maxAge() default 3600;
}
