package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 响应体注解
 * @author sprout
 * 2022-06-10 13:53
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseBody {
}
