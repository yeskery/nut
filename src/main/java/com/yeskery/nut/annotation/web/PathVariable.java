package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * 路径请求参数
 * @author sprout
 * 2022-06-10 21:09
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PathVariable {

    /**
     * 获取参数名
     * @return 参数名
     */
    String value() default "";

    /**
     * 是否必须有值
     * @return 是否必须有值
     */
    boolean required() default false;
}
