package com.yeskery.nut.annotation.web;

import com.yeskery.nut.core.ResponseCode;

import java.lang.annotation.*;

/**
 * 响应状态码注解
 * @author sprout
 * 2022-06-10 13:53
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseStatus {

    /**
     * 获取响应状态码
     * @return 响应状态码
     */
    int value() default 200;

    /**
     * 获取状态码枚举
     * @return 状态码枚举
     */
    ResponseCode status() default ResponseCode.OK;
}
