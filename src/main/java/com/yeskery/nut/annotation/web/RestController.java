package com.yeskery.nut.annotation.web;

import java.lang.annotation.*;

/**
 * RestController注解
 * @author sprout
 * 2022-06-10 12:55
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RestController {

    /**
     * controller的名称
     * @return controller的名称
     */
    String value() default "";
}
