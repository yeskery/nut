package com.yeskery.nut.bind;

import com.yeskery.nut.core.NutException;

/**
 * 数据绑定异常
 * @author sprout
 * 2022-06-02 18:42
 */
public class DataBindException extends NutException {


    /**
     * 构建一个 {@link DataBindException}
     */
    public DataBindException() {
    }

    /**
     * 构建一个 {@link DataBindException}
     * @param message message
     */
    public DataBindException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link DataBindException}
     * @param message message
     * @param cause cause
     */
    public DataBindException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link DataBindException}
     * @param cause cause
     */
    public DataBindException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link DataBindException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public DataBindException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
