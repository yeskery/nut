package com.yeskery.nut.bind;

import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.scan.controller.AnnotationRequestMethodAttributes;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

/**
 * 获取方法参数对象的处理接口
 * @author sprout
 * 2022-06-11 15:54
 */
@FunctionalInterface
public interface MethodParamHandler {
    /**
     * 获取方法参数对象
     * @param attributes 注解请求方法属性
     * @param annotation 注解对象
     * @param parameter 参数对象
     * @param request 请求对象
     * @param execution 执行器
     * @return 方法参数对象
     */
    AnnotationBindObject handle(AnnotationRequestMethodAttributes attributes, Annotation annotation, Parameter parameter, Request request, Execution execution);
}
