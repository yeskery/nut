package com.yeskery.nut.bind.handler;

import com.yeskery.nut.annotation.web.PathVariable;
import com.yeskery.nut.annotation.web.RequestAttribute;
import com.yeskery.nut.bind.AnnotationBindObject;
import com.yeskery.nut.bind.FitValueHelper;
import com.yeskery.nut.bind.MethodParamHandler;
import com.yeskery.nut.core.RequestApplicationContext;
import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.scan.controller.AnnotationRequestMethodAttributes;
import com.yeskery.nut.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

/**
 * PathVariable方法参数对象处理接口
 * @author sprout
 * 2022-06-11 15:58
 */
public class PathVariableMethodParamHandler implements MethodParamHandler {

    @Override
    public AnnotationBindObject handle(AnnotationRequestMethodAttributes attributes, Annotation annotation, Parameter parameter, Request request, Execution execution) {
        PathVariable pathVariable = (PathVariable) annotation;
        String name = pathVariable.value();
        if (StringUtils.isEmpty(name)) {
            name = parameter.getName();
        }
        Object resource = RequestApplicationContext.getResource(name);
        String value = resource == null ? null : resource.toString();
        return new AnnotationBindObject(FitValueHelper.getInstance().getFitParamValue(name, value, parameter.getType()),
                RequestAttribute.class, pathVariable.required(), pathVariable.value());
    }
}
