package com.yeskery.nut.bind.handler;

import com.yeskery.nut.annotation.web.SessionAttribute;
import com.yeskery.nut.bind.AnnotationBindObject;
import com.yeskery.nut.bind.FitValueHelper;
import com.yeskery.nut.bind.MethodParamHandler;
import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.scan.controller.AnnotationRequestMethodAttributes;
import com.yeskery.nut.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

/**
 * RequestAttribute方法参数对象处理接口
 * @author sprout
 * 2022-06-11 15:58
 */
public class SessionAttributeMethodParamHandler implements MethodParamHandler {

    @Override
    public AnnotationBindObject handle(AnnotationRequestMethodAttributes attributes, Annotation annotation, Parameter parameter, Request request, Execution execution) {
        SessionAttribute sessionAttribute = (SessionAttribute) annotation;
        String name = sessionAttribute.value();
        if (StringUtils.isEmpty(name)) {
            name = parameter.getName();
        }
        Object attribute = request.getSession().getAttribute(name);
        String value = attribute == null ? null : attribute.toString();
        return new AnnotationBindObject(FitValueHelper.getInstance().getFitParamValue(name, value, parameter.getType()),
                SessionAttribute.class, sessionAttribute.required(), sessionAttribute.value());
    }
}
