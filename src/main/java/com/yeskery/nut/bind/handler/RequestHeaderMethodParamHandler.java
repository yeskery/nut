package com.yeskery.nut.bind.handler;

import com.yeskery.nut.annotation.web.RequestHeader;
import com.yeskery.nut.bind.AnnotationBindObject;
import com.yeskery.nut.bind.FitValueHelper;
import com.yeskery.nut.bind.MethodParamHandler;
import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.scan.controller.AnnotationRequestMethodAttributes;
import com.yeskery.nut.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

/**
 * RequestHeader方法参数对象处理接口
 * @author sprout
 * 2022-06-11 15:58
 */
public class RequestHeaderMethodParamHandler implements MethodParamHandler {

    @Override
    public AnnotationBindObject handle(AnnotationRequestMethodAttributes attributes, Annotation annotation, Parameter parameter, Request request, Execution execution) {
        RequestHeader requestHeader = (RequestHeader) annotation;
        String name = requestHeader.value();
        if (StringUtils.isEmpty(name)) {
            name = parameter.getName();
        }
        String value = request.getHeader(name);
        return new AnnotationBindObject(FitValueHelper.getInstance().getFitParamValue(name, value, parameter.getType()),
                RequestHeader.class, requestHeader.required(), requestHeader.value());
    }
}
