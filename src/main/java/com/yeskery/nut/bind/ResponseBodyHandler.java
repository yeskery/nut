package com.yeskery.nut.bind;

import com.yeskery.nut.core.Order;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;

import java.lang.reflect.Method;

/**
 * 响应体处理器
 * @author YESKERY
 * 2024/7/12
 */
public interface ResponseBodyHandler extends Order {

    /**
     * 是否支持该响应对象
     * @param method 响应方法对象
     * @param result 响应对象
     * @return 是否支持该响应对象
     */
    boolean support(Method method, Object result);

    /**
     * 处理方法
     * @param result 响应对象
     * @param request 请求对象
     * @param response 响应对象
     */
    void handle(Object result, Request request, Response response);

    /**
     * 是否已经输出响应
     * @return 是否已经输出响应
     */
    default boolean outputResponse() {
        return false;
    }

    @Override
    default int getOrder() {
        return 0;
    }
}
