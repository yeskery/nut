package com.yeskery.nut.bind.validator;

import com.yeskery.nut.bind.DataBindException;

/**
 * 数据绑定验证异常
 * @author sprout
 * @version 1.0
 * 2022-09-01 19:34
 */
public class DataBindValidException extends DataBindException {

    /** 绑定结果对象 */
    private final BindingResult bindingResult;

    /**
     * 构建一个 {@link DataBindValidException}
     * @param bindingResult 绑定结果对象
     */
    public DataBindValidException(BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    /**
     * 构建一个 {@link DataBindValidException}
     * @param message message
     * @param bindingResult 绑定结果对象
     */
    public DataBindValidException(String message, BindingResult bindingResult) {
        super(message);
        this.bindingResult = bindingResult;
    }

    /**
     * 构建一个 {@link DataBindValidException}
     * @param message message
     * @param cause cause
     * @param bindingResult 绑定结果对象
     */
    public DataBindValidException(String message, Throwable cause, BindingResult bindingResult) {
        super(message, cause);
        this.bindingResult = bindingResult;
    }

    /**
     * 构建一个 {@link DataBindValidException}
     * @param cause cause
     * @param bindingResult 绑定结果对象
     */
    public DataBindValidException(Throwable cause, BindingResult bindingResult) {
        super(cause);
        this.bindingResult = bindingResult;
    }

    /**
     * 构建一个 {@link DataBindValidException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     * @param bindingResult 绑定结果对象
     */
    public DataBindValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, BindingResult bindingResult) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.bindingResult = bindingResult;
    }

    /**
     * 获取绑定结果对象
     * @return 绑定结果对象
     */
    public BindingResult getBindingResult() {
        return bindingResult;
    }
}
