package com.yeskery.nut.bind.validator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

/**
 * 基于注解的自定义方法参数验证处理器
 * @author sprout
 * @version 1.0
 * 2022-09-01 17:59
 */
public abstract class BaseAnnotationCustomMethodParamValidatorHandler implements CustomMethodParamValidatorHandler {

    /** 注解类型 */
    private final Class<? extends Annotation> annotationClass;

    /**
     * 构建基于注解的自定义方法参数验证处理器
     * @param annotationClass 注解类型
     */
    public BaseAnnotationCustomMethodParamValidatorHandler(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    @Override
    public boolean isSupport(Parameter parameter) {
        return parameter.isAnnotationPresent(annotationClass);
    }
}
