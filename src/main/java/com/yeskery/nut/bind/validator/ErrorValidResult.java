package com.yeskery.nut.bind.validator;

import java.util.Objects;

/**
 * 方法参数验证不通过的结果对象
 * @author sprout
 * @version 1.0
 * 2022-09-01 17:35
 */
public class ErrorValidResult {

    /** 字段名称 */
    private final String fieldName;

    /** 验证信息 */
    private final String message;

    /**
     * 构建方法参数验证结果对象
     * @param fieldName 字段名称
     */
    public ErrorValidResult(String fieldName) {
        this.fieldName = fieldName;
        this.message = null;
    }

    /**
     * 构建方法参数验证结果对象
     * @param fieldName 字段名称
     * @param message 验证信息
     */
    public ErrorValidResult(String fieldName, String message) {
        this.fieldName = fieldName;
        this.message = message;
    }

    /**
     * 获取字段名称
     * @return 字段名称
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * 获取验证信息
     * @return 验证信息
     */
    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        ErrorValidResult that = (ErrorValidResult) object;
        return Objects.equals(fieldName, that.fieldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName);
    }
}
