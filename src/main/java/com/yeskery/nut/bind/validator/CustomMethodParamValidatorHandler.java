package com.yeskery.nut.bind.validator;

import java.lang.reflect.Parameter;
import java.util.Set;

/**
 * 自定义方法参数验证处理器
 * @author sprout
 * @version 1.0
 * 2022-09-01 17:32
 */
public interface CustomMethodParamValidatorHandler {

    /**
     * 是否支持指定的参数对象
     * @param parameter 参数对象
     * @return 是否支持指定的参数对象
     */
    boolean isSupport(Parameter parameter);

    /**
     * 验证后的方法参数验证结果对象
     * @param parameter 参数对象
     * @return 方法参数验证结果对象
     */
    Set<ErrorValidResult> handle(Object parameter);
}
