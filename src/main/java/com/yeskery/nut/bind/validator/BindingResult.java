package com.yeskery.nut.bind.validator;

import java.util.Set;

/**
 * 绑定结果
 * @author sprout
 * @version 1.0
 * 2022-09-01 18:32
 */
public class BindingResult {

    /** 获取错误的验证结果集合 */
    private final Set<ErrorValidResult> errorValidResults;

    /**
     * 构建一个绑定结果对象
     * @param errorValidResults 获取错误的验证结果集合
     */
    public BindingResult(Set<ErrorValidResult> errorValidResults) {
        this.errorValidResults = errorValidResults;
    }

    /**
     * 是否有错误的验证结果对象
     * @return 是否有错误的验证结果对象
     */
    public boolean hasErrors() {
        return !errorValidResults.isEmpty();
    }

    /**
     * 指定字段是否有验证错误的结果
     * @param fieldName 字段名称
     * @return 指定字段是否有验证错误的结果
     */
    public boolean hasFieldErrors(String fieldName) {
        return errorValidResults.stream().map(ErrorValidResult::getFieldName).anyMatch(n -> n.equals(fieldName));
    }

    /**
     * 获取指定字段的验证错误结果
     * @param fieldName 字段名称
     * @return 指定字段的验证错误结果
     */
    public ErrorValidResult getFieldErrors(String fieldName) {
        return errorValidResults.stream().filter(n -> n.getFieldName().equals(fieldName)).findFirst().orElse(null);
    }

    /**
     * 获取获取错误的验证结果集合
     * @return 获取错误的验证结果集合
     */
    public Set<ErrorValidResult> getErrorValidResults() {
        return errorValidResults;
    }

}
