package com.yeskery.nut.bind;

/**
 * 数据绑定处理器接口
 * @author sprout
 * 2022-06-11 16:24
 */
@FunctionalInterface
public interface DataBindHandler<T, R> {

    /**
     * 数据绑定处理方法
     * @param data 需要处理的对象
     * @return 处理后的对象
     */
    R handle(T data);
}
