package com.yeskery.nut.bind;

/**
 * 数据绑定对象
 * @author sprout
 * 2022-06-11 15:54
 */
public class BindObject {

    /** 数据对象 */
    private final Object data;

    /** 是否为空 */
    private final boolean empty;

    /**
     * 构建数据绑定对象
     * @param data 数据对象
     * @param empty 是否为空
     */
    public BindObject(Object data, boolean empty) {
        this.data = data;
        this.empty = empty;
    }

    /**
     * 获取数据对象
     * @return 数据对象
     */
    public Object getData() {
        return data;
    }

    /**
     * 是否为空
     * @return 是否为空
     */
    public boolean isEmpty() {
        return empty;
    }
}
