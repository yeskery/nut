package com.yeskery.nut.bind;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.Order;
import com.yeskery.nut.util.StringUtils;

import java.util.Arrays;

/**
 * http响应体处理器
 * @author YESKERY
 * 2024/9/23
 */
public interface HttpResponseBodyHandler extends Order {

    /** 任意媒体类型 */
    String ANY_CONTENT_TYPE = "*/*";

    /**
     * 处理方法
     * @param metadata http响应体元数据
     */
    void handle(HttpResponseBodyMetadata metadata);

    /**
     * 支持的媒体类型
     * @return 媒体类型数组
     */
    default MediaType[] supportMediaTypes(){
        return new MediaType[0];
    }

    /**
     * 是否支持该响应对象
     * @param metadata http响应体元数据
     * @return 是否支持该响应对象
     */
    default boolean support(HttpResponseBodyMetadata metadata) {
        MediaType[] mediaTypes = supportMediaTypes();
        if (mediaTypes == null || mediaTypes.length == 0) {
            return true;
        }
        String[] produces = metadata.getAttributes().getProduces();
        if (produces == null || produces.length == 0) {
            return true;
        }
        if (Arrays.stream(produces).anyMatch(r -> StringUtils.isBlank(r) || ANY_CONTENT_TYPE.equals(r))) {
            return true;
        }
        return Arrays.stream(produces).anyMatch(r -> Arrays.stream(mediaTypes).map(MediaType::getValue).anyMatch(v -> v.equalsIgnoreCase(r)));
    }

    @Override
    default int getOrder() {
        return 0;
    }
}
