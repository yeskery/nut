package com.yeskery.nut.bind;

import com.yeskery.nut.core.Execution;
import com.yeskery.nut.core.Request;

import java.lang.reflect.Parameter;

/**
 * 自定义绑定处理器接口
 * @author sprout
 * @version 1.0
 * 2022-06-12 17:01
 */
public interface CustomMethodParamBindHandler<T> {

    /**
     * 自定义获取方法参数对象
     * @param parameter 参数对象
     * @param request 请求对象
     * @param execution 执行器
     * @return 方法参数对象
     */
    T handle(Parameter parameter, Request request, Execution execution);

    /**
     * 获取要处理的beanClass对象
     * @return 要处理的beanClass对象
     */
    Class<T> getHandleType();
}
