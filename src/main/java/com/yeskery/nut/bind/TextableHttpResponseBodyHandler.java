package com.yeskery.nut.bind;

/**
 * 文本类型响应体处理器
 * @author YESKERY
 * 2024/11/4
 */
public interface TextableHttpResponseBodyHandler extends HttpResponseBodyHandler {

    /**
     * 将http响应体元数据转换为文本格式
     * @param metadata http响应体元数据
     * @return 转换后的文本
     */
    String getExtractText(HttpResponseBodyMetadata metadata);
}
