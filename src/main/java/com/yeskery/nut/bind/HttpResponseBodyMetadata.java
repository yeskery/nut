package com.yeskery.nut.bind;

import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;
import com.yeskery.nut.plugin.BindContext;
import com.yeskery.nut.scan.controller.AnnotationRequestMethodAttributes;

/**
 * http响应体元数据
 * @author YESKERY
 * 2024/9/23
 */
public class HttpResponseBodyMetadata {

    /** 目标方法元数据 */
    private AnnotationRequestMethodAttributes attributes;

    /** 目标方法执行结果 */
    private Object result;

    /** 请求对象 */
    private Request request;

    /** 响应对象 */
    private Response response;

    /** 应用上下文 */
    private ApplicationContext applicationContext;

    /** 绑定上下文 */
    private BindContext bindContext;

    /**
     * 获取目标方法元数据
     * @return 目标方法元数据
     */
    public AnnotationRequestMethodAttributes getAttributes() {
        return attributes;
    }

    /**
     * 设置目标方法元数据
     * @param attributes 目标方法元数据
     */
    public void setAttributes(AnnotationRequestMethodAttributes attributes) {
        this.attributes = attributes;
    }

    /**
     * 获取目标方法执行结果
     * @return 目标方法执行结果
     */
    public Object getResult() {
        return result;
    }

    /**
     * 设置目标方法执行结果
     * @param result 目标方法执行结果
     */
    public void setResult(Object result) {
        this.result = result;
    }

    /**
     * 获取请求对象
     * @return 请求对象
     */
    public Request getRequest() {
        return request;
    }

    /**
     * 设置请求对象
     * @param request 请求对象
     */
    public void setRequest(Request request) {
        this.request = request;
    }

    /**
     * 获取响应对象
     * @return 响应对象
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 设置响应对象
     * @param response 响应对象
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 获取应用上下文
     * @return 应用上下文
     */
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 设置应用上下文
     * @param applicationContext 应用上下文
     */
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取绑定上下文
     * @return 绑定上下文
     */
    public BindContext getBindContext() {
        return bindContext;
    }

    /**
     * 设置绑定上下文
     * @param bindContext 绑定上下文
     */
    public void setBindContext(BindContext bindContext) {
        this.bindContext = bindContext;
    }
}
