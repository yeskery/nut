package com.yeskery.nut.bind;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.Response;
import com.yeskery.nut.core.ResponseBodyEmitter;
import com.yeskery.nut.http.BaseResponse;
import com.yeskery.nut.http.DefaultResponseEmitterHandler;

import java.lang.reflect.Method;

/**
 * 响应发射器处理器
 * @author YESKERY
 * 2024/7/12
 */
public class EmitterResponseBodyHandler implements ResponseBodyHandler {
    @Override
    public boolean support(Method method, Object result) {
        return ResponseBodyEmitter.class.isAssignableFrom(result.getClass());
    }

    @Override
    public void handle(Object result, Request request, Response response) {
        ResponseBodyEmitter responseBodyEmitter = (ResponseBodyEmitter) result;
        DefaultResponseEmitterHandler handler = new DefaultResponseEmitterHandler(responseBodyEmitter, (BaseResponse) response);
        responseBodyEmitter.setHandler(handler);
        handler.park();
    }

    @Override
    public boolean outputResponse() {
        return true;
    }
}
