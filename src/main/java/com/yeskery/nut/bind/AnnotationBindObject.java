package com.yeskery.nut.bind;

import java.lang.annotation.Annotation;

/**
 * 注解数据绑定对象
 * @author sprout
 * 2022-06-11 15:55
 */
public class AnnotationBindObject extends BindObject {

    /** 注解类型 */
    private final Class<? extends Annotation> type;

    /** 是否必须 */
    private final boolean required;

    /** 注解值 */
    private final String value;

    /**
     * 构建数据绑定对象
     * @param bindObject bindObject
     * @param type 注解类型
     * @param required 是否必须
     * @param value 注解值
     */
    public AnnotationBindObject(BindObject bindObject, Class<? extends Annotation> type, boolean required, String value) {
        super(bindObject == null ? null : bindObject.getData(), bindObject == null || bindObject.isEmpty());
        this.type = type;
        this.required = required;
        this.value = value;
    }

    /**
     * 获取注解类型
     * @return 注解类型
     */
    public Class<? extends Annotation> getType() {
        return type;
    }

    /**
     * 是否必须
     * @return 是否必须
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * 获取注解值
     * @return 注解值
     */
    public String getValue() {
        return value;
    }
}
