package com.yeskery.nut.scan;

import java.lang.annotation.Annotation;

/**
 * 注解Class扫描器
 * @author sprout
 * 2022-06-14 18:23
 */
public interface AnnotationClassScanner extends Scanner<Void> {

    /**
     * 注册注解处理器
     * @param clazz 注解类型
     * @param annotationHandler 注解处理器
     */
    void registerAnnotationHandler(Class<? extends Annotation> clazz, AnnotationHandler annotationHandler);
}
