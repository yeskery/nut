package com.yeskery.nut.scan.controller;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.ControllerMetadata;
import com.yeskery.nut.core.Method;

import java.util.Map;

/**
 * Controller 组合元信息
 * @author sprout
 * 2022-06-10 14:17
 */
public class ControllerCombinationMetadata {

    /** Controller元数据 */
    private ControllerMetadata controllerMetadata;

    /** Controller对象 */
    private Controller controller;

    /** 处理方法持有对象 */
    private Map<Method, AnnotationRequestMethodAttributes> holdMethodAttributesMap;

    /**
     * 获取Controller元数据
     * @return Controller元数据
     */
    public ControllerMetadata getControllerMetadata() {
        return controllerMetadata;
    }

    /**
     * 设置Controller元数据
     * @param controllerMetadata Controller元数据
     */
    public void setControllerMetadata(ControllerMetadata controllerMetadata) {
        this.controllerMetadata = controllerMetadata;
    }

    /**
     * 获取Controller对象
     * @return Controller对象
     */
    public Controller getController() {
        return controller;
    }

    /**
     * 设置Controller对象
     * @param controller Controller对象
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * 获取处理方法持有对象
     * @return 处理方法持有对象
     */
    public Map<Method, AnnotationRequestMethodAttributes> getHoldMethodAttributesMap() {
        return holdMethodAttributesMap;
    }

    /**
     * 设置处理方法持有对象
     * @param holdMethodAttributesMap 处理方法持有对象
     */
    public void setHoldMethodAttributesMap(Map<Method, AnnotationRequestMethodAttributes> holdMethodAttributesMap) {
        this.holdMethodAttributesMap = holdMethodAttributesMap;
    }
}
