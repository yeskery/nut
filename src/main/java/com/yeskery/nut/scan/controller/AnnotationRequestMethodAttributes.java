package com.yeskery.nut.scan.controller;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.extend.auth.CrossConfiguration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 注解请求方法属性
 * @author sprout
 * 2022-06-10 17:41
 */
public class AnnotationRequestMethodAttributes {

    /** 请求类型 */
    private Class<?> type;

    /** 方法对象 */
    private Method method;

    /** 是否需要处理响应体注解 */
    private boolean responseBody;

    /** 响应码 */
    private int responseCode = ResponseCode.OK.getCode();

    /** 跨域配置 */
    Collection<CrossConfiguration> crossConfigurations;

    /** 匹配请求参数 */
    private String[] params;

    /** 匹配请求头 */
    private String[] headers;

    /** 匹配媒体类型 */
    private String[] consumes;

    /** 匹配客户端接受媒体类型 */
    private String[] produces;

    /** 注解属性索引集合 */
    private Map<Integer, List<Annotation>> annotationIndexMap;

    /**
     * 获取请求类型
     * @return 请求类型
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 设置请求类型
     * @param type 请求类型
     */
    public void setType(Class<?> type) {
        this.type = type;
    }

    /**
     * 获取方法对象
     * @return 方法对象
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置方法对象
     * @param method 方法对象
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * 获取是否需要处理响应体注解
     * @return 是否需要处理响应体注解
     */
    public boolean isResponseBody() {
        return responseBody;
    }

    /**
     * 设置是否需要处理响应体注解
     * @param responseBody 是否需要处理响应体注解
     */
    public void setResponseBody(boolean responseBody) {
        this.responseBody = responseBody;
    }

    /**
     * 获取响应码
     * @return 响应码
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * 设置响应码
     * @param responseCode 响应码
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * 获取跨域配置
     * @return 跨域配置
     */
    public Collection<CrossConfiguration> getCrossConfigurations() {
        return crossConfigurations;
    }

    /**
     * 设置跨域配置
     * @param crossConfigurations 跨域配置
     */
    public void setCrossConfigurations(Collection<CrossConfiguration> crossConfigurations) {
        this.crossConfigurations = crossConfigurations;
    }

    /**
     * 获取匹配请求参数
     * @return 匹配请求参数
     */
    public String[] getParams() {
        return params;
    }

    /**
     * 设置匹配请求参数
     * @param params 匹配请求参数
     */
    public void setParams(String[] params) {
        this.params = params;
    }

    /**
     * 获取匹配请求头
     * @return 匹配请求头
     */
    public String[] getHeaders() {
        return headers;
    }

    /**
     * 设置匹配请求头
     * @param headers 匹配请求头
     */
    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    /**
     * 获取匹配媒体类型
     * @return 匹配媒体类型
     */
    public String[] getConsumes() {
        return consumes;
    }

    /**
     * 设置匹配媒体类型
     * @param consumes 匹配媒体类型
     */
    public void setConsumes(String[] consumes) {
        this.consumes = consumes;
    }

    /**
     * 获取匹配客户端接受媒体类型
     * @return 匹配客户端接受媒体类型
     */
    public String[] getProduces() {
        return produces;
    }

    /**
     * 设置匹配客户端接受媒体类型
     * @param produces 匹配客户端接受媒体类型
     */
    public void setProduces(String[] produces) {
        this.produces = produces;
    }

    /**
     * 获取注解属性索引集合
     * @return 注解属性索引集合
     */
    public Map<Integer, List<Annotation>> getAnnotationIndexMap() {
        return annotationIndexMap;
    }

    /**
     * 设置注解属性索引集合
     * @param annotationIndexMap 注解属性索引集合
     */
    public void setAnnotationIndexMap(Map<Integer, List<Annotation>> annotationIndexMap) {
        this.annotationIndexMap = annotationIndexMap;
    }
}
