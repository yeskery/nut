package com.yeskery.nut.scan;

import com.yeskery.nut.core.Order;

import java.util.Collection;

/**
 * 注解处理器
 * @author sprout
 * 2022-06-14 18:27
 */
@FunctionalInterface
public interface AnnotationHandler extends Order {

    /**
     * 注解处理方法
     * @param beanClassCollection 注解类集合
     * @param beanMetadataCollection bean元数据集合
     */
    void handle(Collection<Class<?>> beanClassCollection, Collection<BeanAnnotationScanMetadata> beanMetadataCollection);

    @Override
    default int getOrder() {
        return 0;
    }
}
