package com.yeskery.nut.scan;

import java.util.Collection;

/**
 * 扫描器接口
 * @author sprout
 * 2022-06-10 14:18
 */
@FunctionalInterface
public interface Scanner<T> {

    /**
     * 扫描方法
     * @return 扫描的结果对象集合
     */
    Collection<T> scan();
}
