package com.yeskery.nut.scan;

import com.yeskery.nut.bean.BeanDefinitionRegistry;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 默认的注解bean定义注册器对象
 * @author sprout
 * @version 1.0
 * 2022-08-24 10:25
 */
public class DefaultAnnotationBeanDefinitionRegistry implements BeanDefinitionRegistry {

    /** 注解处理器map */
    private final Map<Class<? extends Annotation>, AnnotationHandler> annotationHandlerMap;

    /** 注册bean定义对象map */
    private final Map<Class<? extends Annotation>, Collection<Class<?>>> annotationClassMap;

    /**
     * 构建默认的注解bean定义注册器对象
     * @param annotationHandlerMap 注解处理器map
     * @param annotationClassMap 注册bean定义对象map
     */
    public DefaultAnnotationBeanDefinitionRegistry(Map<Class<? extends Annotation>, AnnotationHandler> annotationHandlerMap,
                                                   Map<Class<? extends Annotation>, Collection<Class<?>>> annotationClassMap) {
        if (annotationHandlerMap == null) {
            annotationHandlerMap = new LinkedHashMap<>();
        }
        if (annotationClassMap == null) {
            annotationClassMap = new HashMap<>();
        }
        this.annotationHandlerMap = annotationHandlerMap;
        this.annotationClassMap = annotationClassMap;
    }

    @Override
    public Map<Class<? extends Annotation>, AnnotationHandler> getSupportAnnotationHandlerMap() {
        return annotationHandlerMap;
    }

    @Override
    public Map<Class<? extends Annotation>, Collection<Class<?>>> getAnnotationBeanDefinitions() {
        return annotationClassMap;
    }
}
