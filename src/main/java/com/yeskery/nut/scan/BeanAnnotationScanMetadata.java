package com.yeskery.nut.scan;

import com.yeskery.nut.util.BeanUtils;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;

/**
 * Bean扫描元数据
 * @author sprout
 * @version 1.0
 * 2022-07-14 14:51
 */
public class BeanAnnotationScanMetadata {

    /** bean名称 */
    private final String name;

    /** 类型 */
    private final Class<?> type;

    /** Bean元数据来源 */
    private final Source source;

    /** 是否是优先级bean */
    private final boolean primary;

    /** 注解类型 */
    private final Class<? extends Annotation> annotationType;

    /** 注解处理类 */
    private final AnnotationHandler annotationHandler;

    /** 超类集合 */
    private final Collection<Class<?>> superClasses;

    /** 接口集合 */
    private final Collection<Class<?>> interfaceClasses;

    /**
     * 构造Bean扫描元数据
     * @param name bean名称
     * @param type 类型
     * @param source Bean元数据来源
     * @param primary 优先级bean
     * @param annotationType 注解类型
     * @param annotationHandler 注解处理类
     */
    public BeanAnnotationScanMetadata(String name, Class<?> type, Source source, boolean primary,
                                      Class<? extends Annotation> annotationType, AnnotationHandler annotationHandler) {
        this.name = name;
        this.type = type;
        this.source = source;
        this.primary = primary;
        this.annotationType = annotationType;
        this.annotationHandler = annotationHandler;
        BeanUtils.ClassDeclaredMetadata declaredMetadata = BeanUtils.getDeclaredMetadata(type);
        this.superClasses = Arrays.asList(declaredMetadata.getDeclaredClasses());
        this.interfaceClasses = Arrays.asList(declaredMetadata.getDeclaredInterfaces());
    }

    /**
     * 获取bean名称
     * @return bean名称
     */
    public String getName() {
        return name;
    }

    /**
     * 获取类型
     * @return 类型
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 获取Bean元数据来源
     * @return Bean元数据来源
     */
    public Source getSource() {
        return source;
    }

    /**
     * 是否是优先级bean
     * @return 是否是优先级bean
     */
    public boolean isPrimary() {
        return primary;
    }

    /**
     * 获取注解类型
     * @return 注解类型
     */
    public Class<? extends Annotation> getAnnotationType() {
        return annotationType;
    }

    /**
     * 获取注解处理类
     * @return 注解处理类
     */
    public AnnotationHandler getAnnotationHandler() {
        return annotationHandler;
    }

    /**
     * 获取超类集合
     * @return 超类集合
     */
    public Collection<Class<?>> getSuperClasses() {
        return superClasses;
    }

    /**
     * 获取接口集合
     * @return 接口集合
     */
    public Collection<Class<?>> getInterfaceClasses() {
        return interfaceClasses;
    }

    /**
     * Bean元数据来源
     * @author sprout
     * @version 1.0
     * 2022-07-14 14:51
     */
    public enum Source {
        /** 容器注解 */
        COMPONENT,
        /** Bean配置注解 */
        CONFIGURATION,
        /** 配置对象注解 */
        CONFIGURATION_PROPERTIES,
        /** 切面 */
        ASPECT
    }
}
