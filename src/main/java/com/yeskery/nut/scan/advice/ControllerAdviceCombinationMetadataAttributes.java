package com.yeskery.nut.scan.advice;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 注解请求方法属性
 * @author sprout
 * 2022-06-15 13:45
 */
public class ControllerAdviceCombinationMetadataAttributes {

    /** 请求类型 */
    private Class<?> type;

    /** 注解对象 */
    private Annotation annotation;

    /** 方法对象 */
    private Method method;

    /** 是否需要处理响应体注解 */
    private boolean responseBody;

    /**
     * 获取请求类型
     * @return 请求类型
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 设置请求类型
     * @param type 请求类型
     */
    public void setType(Class<?> type) {
        this.type = type;
    }

    /**
     * 获取注解对象
     * @return 注解对象
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * 设置注解对象
     * @param annotation 注解对象
     */
    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

    /**
     * 获取方法对象
     * @return 方法对象
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置方法对象
     * @param method 方法对象
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * 获取是否需要处理响应体注解
     * @return 是否需要处理响应体注解
     */
    public boolean isResponseBody() {
        return responseBody;
    }

    /**
     * 设置是否需要处理响应体注解
     * @param responseBody 是否需要处理响应体注解
     */
    public void setResponseBody(boolean responseBody) {
        this.responseBody = responseBody;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ControllerAdviceCombinationMetadataAttributes that = (ControllerAdviceCombinationMetadataAttributes) o;
        return responseBody == that.responseBody
                && Objects.equals(type, that.type)
                && Objects.equals(annotation, that.annotation)
                && Objects.equals(method, that.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, annotation, method, responseBody);
    }
}
