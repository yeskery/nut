package com.yeskery.nut.scan.advice;

import com.yeskery.nut.annotation.web.ExceptionHandler;
import com.yeskery.nut.plugin.PluginManager;

/**
 * ExceptionHandler注解处理器
 * @author sprout
 * 2022-06-15 14:32
 */
public class ExceptionHandlerControllerAdviceHandler extends BasePluginControllerAdviceHandler {

    /**
     * 构建ExceptionHandler注解处理器
     * @param pluginManager 插件管理器
     */
    public ExceptionHandlerControllerAdviceHandler(PluginManager pluginManager) {
        super(pluginManager, new ExceptionHandlerPlugin());
    }

    @Override
    public void annotationHandle(String[] basePackages, ControllerAdviceCombinationMetadataAttributes metadataAttributes) {
        ExceptionHandler exceptionHandler = (ExceptionHandler) metadataAttributes.getAnnotation();
        ((ExceptionHandlerPlugin) getPlugin()).registerThrowable(exceptionHandler.value(), exceptionHandler.produces(), basePackages, metadataAttributes);
        if (isEmpty()) {
            setNotEmpty();
        }
    }
}
