package com.yeskery.nut.scan.advice;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * ControllerAdvice元数据
 * @author sprout
 * 2022-06-15 11:45
 */
public class ControllerAdviceCombinationMetadata {

    /** 基础扫描包 */
    private String[] basePackages;

    /** 注解map */
    private final Map<Class<?>, Collection<ControllerAdviceCombinationMetadataAttributes>> annotationMap = new HashMap<>();

    /**
     * 获取基础扫描包
     * @return 基础扫描包
     */
    public String[] getBasePackages() {
        return basePackages;
    }

    /**
     * 设置基础扫描包
     * @param basePackages 基础扫描包
     */
    public void setBasePackages(String[] basePackages) {
        this.basePackages = basePackages;
    }

    /**
     * 获取注解map
     * @return 注解map
     */
    public Map<Class<?>, Collection<ControllerAdviceCombinationMetadataAttributes>> getAnnotationMap() {
        return annotationMap;
    }

}
