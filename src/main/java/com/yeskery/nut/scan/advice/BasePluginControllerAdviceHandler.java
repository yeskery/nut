package com.yeskery.nut.scan.advice;

import com.yeskery.nut.plugin.Plugin;
import com.yeskery.nut.plugin.PluginManager;

/**
 * 基础插件ControllerAdvice处理器
 * @author sprout
 * 2022-06-16 18:03
 */
public abstract class BasePluginControllerAdviceHandler implements ControllerAdviceHandler {

    /** 插件管理器 */
    private final PluginManager pluginManager;

    /** 插件对象 */
    private final Plugin plugin;

    /** 空处理 */
    private volatile boolean empty = true;

    /**
     * 构建ExceptionHandler注解处理器
     * @param pluginManager 插件管理器
     * @param plugin 插件对象
     */
    public BasePluginControllerAdviceHandler(PluginManager pluginManager, Plugin plugin) {
        this.pluginManager = pluginManager;
        this.plugin = plugin;
    }

    @Override
    public void completeHandle() {
        if (!empty && !pluginManager.containPlugin(plugin.getClass())) {
            pluginManager.registerPlugin(plugin);
        }
    }

    /**
     * 获取插件对象
     * @return 插件对象
     */
    protected Plugin getPlugin() {
        return plugin;
    }

    /**
     * 获取当前是否为空
     * @return 当前是否为空
     */
    protected boolean isEmpty() {
        return empty;
    }

    /**
     * 设置当前不为空
     */
    protected void setNotEmpty() {
        this.empty = false;
    }
}
