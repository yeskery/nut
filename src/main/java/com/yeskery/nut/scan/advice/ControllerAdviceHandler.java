package com.yeskery.nut.scan.advice;

/**
 * ControllerAdvice处理器
 * @author sprout
 * 2022-06-15 14:31
 */
public interface ControllerAdviceHandler{

    /**
     * 注解处理方法
     * @param basePackages 基础包
     * @param metadataAttributes 属性元数据
     */
    void annotationHandle(String[] basePackages, ControllerAdviceCombinationMetadataAttributes metadataAttributes);

    /**
     * 完成处理
     */
    void completeHandle();
}
