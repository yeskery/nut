package com.yeskery.nut.scan.advice;

import com.yeskery.nut.annotation.web.ModelAttribute;
import com.yeskery.nut.plugin.PluginManager;

/**
 * ModelAttribute注解处理器
 * @author sprout
 * 2022-06-15 14:32
 */
public class ModelAttributeControllerAdviceHandler extends BasePluginControllerAdviceHandler {

    /**
     * 构建ExceptionHandler注解处理器
     * @param pluginManager 插件管理器
     */
    public ModelAttributeControllerAdviceHandler(PluginManager pluginManager) {
        super(pluginManager, new ModelAttributePlugin());
    }

    @Override
    public void annotationHandle(String[] basePackages, ControllerAdviceCombinationMetadataAttributes metadataAttributes) {
        ModelAttribute modelAttribute = (ModelAttribute) metadataAttributes.getAnnotation();
        ((ModelAttributePlugin) getPlugin()).registerModelAttribute(modelAttribute.value(), basePackages, metadataAttributes);
        if (isEmpty()) {
            setNotEmpty();
        }
    }
}
