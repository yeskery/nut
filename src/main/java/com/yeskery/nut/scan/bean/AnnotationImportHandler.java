package com.yeskery.nut.scan.bean;

import com.yeskery.nut.scan.AnnotationHandler;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;

/**
 * 导入注解处理类，空实现
 * @author sunjay
 * 2024/1/6
 */
public class AnnotationImportHandler implements AnnotationHandler {
    @Override
    public void handle(Collection<Class<?>> beanClassCollection, Collection<BeanAnnotationScanMetadata> beanMetadataCollection) {

    }
}
