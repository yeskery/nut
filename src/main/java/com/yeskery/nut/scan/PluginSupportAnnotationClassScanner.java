package com.yeskery.nut.scan;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.plugin.BasePluginInitializer;
import com.yeskery.nut.plugin.BindContext;
import com.yeskery.nut.plugin.Plugin;

import java.util.Collection;
import java.util.Set;

/**
 * 支持插件注解Class扫描器
 * @author Yeskery
 * 2023/7/24
 */
public class PluginSupportAnnotationClassScanner extends DefaultAnnotationClassScanner {

    /** 插件初始化器 */
    private final BasePluginInitializer pluginInitializer;

    /** 插件绑定上下文对象 */
    private final BindContext bindContext;

    /**
     * 构建默认的注解Class扫描器
     *
     * @param nutApplication    Nut应用对象
     * @param basePackages      扫描的基础路径
     * @param pluginInitializer 插件初始化器
     * @param bindContext 插件绑定上下文
     */
    public PluginSupportAnnotationClassScanner(NutApplication nutApplication, Set<String> basePackages,
                                               BasePluginInitializer pluginInitializer, BindContext bindContext) {
        super(nutApplication, basePackages);
        this.pluginInitializer = pluginInitializer;
        this.bindContext = bindContext;
    }

    @Override
    protected void doAfterAnnotationBeanRegisterInitialize() {
        super.doAfterAnnotationBeanRegisterInitialize();
        Collection<Plugin> plugins = getNutApplication().getApplicationContext().getBeans(Plugin.class);
        pluginInitializer.initialize(getNutApplication(), bindContext, plugins);
    }
}
