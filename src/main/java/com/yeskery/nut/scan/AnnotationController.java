package com.yeskery.nut.scan;

import com.yeskery.nut.core.Controller;

/**
 * 注解controller对象
 * @author sprout
 * @version 1.0
 * 2022-09-20 00:37
 *
 * @see com.yeskery.nut.core.ControllerSource#ANNOTATION
 */
public class AnnotationController implements Controller {
}
