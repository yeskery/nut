package com.yeskery.nut.scan;

/**
 * Bean创建接口
 * @author sprout
 * @version 1.0
 * 2022-07-17 21:56
 */
@FunctionalInterface
public interface BeanCreator {

    /**
     * 创建一个bean实例
     * @param beanAnnotationScanMetadata Bean扫描元数据
     */
    void createBean(BeanAnnotationScanMetadata beanAnnotationScanMetadata);
}
