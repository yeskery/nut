package com.yeskery.nut.scan;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Configuration注解的Bean元数据
 * @author sprout
 * @version 1.0
 * 2022-07-14 17:02
 */
public class ConfigurationBeanAnnotationScanMetadata extends BeanAnnotationScanMetadata {

    /** 原始类型 */
    private final Class<?> originalType;

    /** 注解方法 */
    private final Method method;

    /**
     * 构造Bean扫描元数据
     * @param name bean名称
     * @param type 类型
     * @param source Bean元数据来源
     * @param primary 优先级bean
     * @param annotationType 注解类型
     * @param annotationHandler 注解处理类
     * @param originalType 原始类型
     * @param method 注解方法
     */
    public ConfigurationBeanAnnotationScanMetadata(String name, Class<?> type, Source source, boolean primary,
                                                   Class<? extends Annotation> annotationType, AnnotationHandler annotationHandler,
                                                   Class<?> originalType, Method method) {
        super(name, type, source, primary, annotationType, annotationHandler);
        this.originalType = originalType;
        this.method = method;
    }

    /**
     * 获取原始类型
     * @return 原始类型
     */
    public Class<?> getOriginalType() {
        return originalType;
    }

    /**
     * 获取注解方法
     * @return 注解方法
     */
    public Method getMethod() {
        return method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConfigurationBeanAnnotationScanMetadata that = (ConfigurationBeanAnnotationScanMetadata) o;
        return Objects.equals(getName(), that.getName())
                && Objects.equals(getType(), that.getType())
                && Objects.equals(getSource(), that.getSource())
                && Objects.equals(originalType, that.originalType)
                && Objects.equals(method, that.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getType(), getSource(), originalType, method);
    }
}
