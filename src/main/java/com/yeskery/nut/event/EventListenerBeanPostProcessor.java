package com.yeskery.nut.event;

import com.yeskery.nut.annotation.event.EventListener;
import com.yeskery.nut.bean.ApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 事件监听器bean后置处理器
 * @author Yeskery
 * 2023/8/9
 */
public class EventListenerBeanPostProcessor extends BaseEventListenerBeanPostProcessor {

    @Override
    protected Class<? extends Annotation> getEventListenerAnnotationType() {
        return EventListener.class;
    }

    @Override
    protected Class<? extends ApplicationEvent> getSupportApplicationEventType(Object eventListenerAnnotation) {
        return ((EventListener) eventListenerAnnotation).value();
    }

    @Override
    protected ApplicationListener<?> getApplicationListener(Object bean, Class<?> beanClass, Method method,
                                                            ApplicationContext applicationContext,
                                                            Object eventListenerAnnotation) {
        EventListener eventListener = (EventListener) eventListenerAnnotation;
        return new ProxySmartApplicationListener(eventListener.value(), eventListener.order(), bean, method);
    }
}
