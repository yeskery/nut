package com.yeskery.nut.event;

import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文启动事件
 *
 * @see ApplicationContextStartingListener
 * @author YESKERY
 * 2023/9/12
 */
public class ApplicationContextStartingEvent extends ApplicationEvent {

    /**
     * 构建应用上下文启动完成事件
     * @param applicationContext 应用上下文
     */
    public ApplicationContextStartingEvent(ApplicationContext applicationContext) {
        super(applicationContext);
    }
}
