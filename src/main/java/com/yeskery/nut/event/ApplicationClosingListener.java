package com.yeskery.nut.event;

/**
 * 应用关闭时事件监听器接口，监听{@link ApplicationClosingEvent}事件
 * @author sunjay
 * 2023/9/9
 */
public interface ApplicationClosingListener extends SmartApplicationListener {

    @Override
    default boolean supportEventType(Class<? extends ApplicationEvent> eventType) {
        return ApplicationClosingEvent.class.isAssignableFrom(eventType);
    }

    /**
     * 获取应用关闭时事件
     * @param applicationEvent 事件对象
     * @return 应用关闭时事件
     */
    default ApplicationClosingEvent getApplicationClosingEvent(ApplicationEvent applicationEvent) {
        return (ApplicationClosingEvent) applicationEvent;
    }
}
