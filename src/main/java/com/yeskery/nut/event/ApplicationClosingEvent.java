package com.yeskery.nut.event;

import com.yeskery.nut.application.NutApplication;

/**
 * 应用关闭时的事件
 *
 * @see ApplicationClosingListener
 * @author sunjay
 * 2023/9/9
 */
public class ApplicationClosingEvent extends ApplicationEvent {

    /**
     * 构建应用关闭时的事件
     * @param nutApplication nut应用对象
     */
    public ApplicationClosingEvent(NutApplication nutApplication) {
        super(nutApplication);
    }
}
