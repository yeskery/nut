package com.yeskery.nut.event;

/**
 * 默认的事件发布器
 * @author Yeskery
 * 2023/8/9
 */
public class DefaultApplicationEventPublisher implements ApplicationEventPublisher {

    /** 事件广播器 */
    private final ApplicationEventMulticaster applicationEventMulticaster;

    /**
     * 构建默认的事件发布器
     * @param applicationEventMulticaster 事件广播器
     */
    public DefaultApplicationEventPublisher(ApplicationEventMulticaster applicationEventMulticaster) {
        this.applicationEventMulticaster = applicationEventMulticaster;
    }

    @Override
    public void publishEvent(ApplicationEvent event) {
        applicationEventMulticaster.multicastEvent(event);
    }
}
