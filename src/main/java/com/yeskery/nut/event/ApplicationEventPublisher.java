package com.yeskery.nut.event;

/**
 * 应用事件发布器
 * @author Yeskery
 * 2023/8/9
 */
public interface ApplicationEventPublisher {

    /**
     * 发布事件
     * @param event 事件对象
     */
    void publishEvent(ApplicationEvent event);
}
