package com.yeskery.nut.event;

import com.yeskery.nut.core.Order;

/**
 * 智能事件监听器
 * @author Yeskery
 * 2023/8/8
 */
public interface SmartApplicationListener extends ApplicationListener<ApplicationEvent>, Order {

    /**
     * 是否支持该事件类型
     * @param eventType 事件类型
     * @return 是否支持该事件类型
     */
    boolean supportEventType(Class<? extends ApplicationEvent> eventType);
}
