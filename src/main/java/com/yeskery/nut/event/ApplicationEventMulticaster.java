package com.yeskery.nut.event;

import java.util.function.Predicate;

/**
 * 应用事件广播器
 * @author Yeskery
 * 2023/8/8
 */
public interface ApplicationEventMulticaster {


    /**
     * 添加事件监听器
     * @param listener 事件监听器
     */
    void addApplicationListener(ApplicationListener<?> listener);


    /**
     * 移除事件监听器
     * @param listener 事件监听器
     */
    void removeApplicationListener(ApplicationListener<?> listener);

    /**
     * 移除事件监听器
     * @param predicate 事件监听器谓词
     */
    void removeApplicationListeners(Predicate<ApplicationListener<?>> predicate);

    /**
     * 移除所有事件监听器
     */
    void removeAllListeners();

    /**
     * 发布广播事件
     * @param event 广播事件
     */
    void multicastEvent(ApplicationEvent event);
}
