package com.yeskery.nut.event;

/**
 * <p>应用上下文启动事件监听器接口，监听{@link ApplicationContextStartingEvent}事件，
 * 该事件发布时，应用上下文还未开启，无法通过应用上下文的方式进行事件监听。需要通过SPI的
 * 方式进行监听。</p>
 *
 * <p>SPI方式需要在<code>/META-INF/services</code>新建<code>com.yeskery.nut.event.ApplicationListener</code>文件，
 * 将需要注册的事件监听器全路径名输入到上述文件中，一行一个。</p>
 * @author sunjay
 * 2023/9/9
 */
public interface ApplicationContextStartingListener extends SmartApplicationListener {

    @Override
    default boolean supportEventType(Class<? extends ApplicationEvent> eventType) {
        return ApplicationContextStartingEvent.class.isAssignableFrom(eventType);
    }

    /**
     * 应用上下文事件
     * @param applicationEvent 事件对象
     * @return 应用上下文事件
     */
    default ApplicationContextStartingEvent getApplicationContextStartingEvent(ApplicationEvent applicationEvent) {
        return (ApplicationContextStartingEvent) applicationEvent;
    }
}
