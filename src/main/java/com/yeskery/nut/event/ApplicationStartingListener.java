package com.yeskery.nut.event;

/**
 * <p>应用启动时事件监听器接口，监听{@link ApplicationStartingEvent}事件，
 * 该事件发布时，应用上下文还未完成初始化，无法通过应用上下文的方式进行事件监听。需要通过SPI的
 * 方式进行监听。</p>
 *
 * <p>SPI方式需要在<code>/META-INF/services</code>新建<code>com.yeskery.nut.event.ApplicationListener</code>文件，
 * 将需要注册的事件监听器全路径名输入到上述文件中，一行一个。</p>
 * @author sunjay
 * 2023/9/9
 */
public interface ApplicationStartingListener extends SmartApplicationListener {

    @Override
    default boolean supportEventType(Class<? extends ApplicationEvent> eventType) {
        return ApplicationStartingEvent.class.isAssignableFrom(eventType);
    }

    /**
     * 获取应用启动时事件
     * @param applicationEvent 事件对象
     * @return 应用启动时事件
     */
    default ApplicationStartingEvent getApplicationStartingEvent(ApplicationEvent applicationEvent) {
        return (ApplicationStartingEvent) applicationEvent;
    }
}
