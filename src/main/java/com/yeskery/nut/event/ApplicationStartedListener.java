package com.yeskery.nut.event;

/**
 * 服务启动完成后事件监听器，监听{@link ApplicationStartedEvent}事件
 * @author sunjay
 * 2023/9/9
 */
public interface ApplicationStartedListener extends SmartApplicationListener {

    @Override
    default boolean supportEventType(Class<? extends ApplicationEvent> eventType) {
        return ApplicationStartedEvent.class.isAssignableFrom(eventType);
    }

    /**
     * 获取应用启动完成后事件
     * @param applicationEvent 事件对象
     * @return 应用启动完成后事件
     */
    default ApplicationStartedEvent getApplicationStartedEvent(ApplicationEvent applicationEvent) {
        return (ApplicationStartedEvent) applicationEvent;
    }
}
