package com.yeskery.nut.event;

import java.time.Clock;
import java.util.EventObject;

/**
 * 应用事件对象
 *
 * @see ApplicationListener
 * @author Yeskery
 * 2023/8/8
 */
public abstract class ApplicationEvent extends EventObject {

    /** 事件发生的时间戳 */
    private final long timestamp;

    /**
     * 构建应用事件对象
     * @param source 事件源对象
     */
    public ApplicationEvent(Object source) {
        super(source);
        timestamp = System.currentTimeMillis();
    }

    /**
     * 构建应用事件对象
     * @param source 事件源对象
     * @param clock 时钟对象
     */
    public ApplicationEvent(Object source, Clock clock) {
        super(source);
        this.timestamp = clock.millis();
    }

    /**
     * 获取事件发生的时间戳
     * @return 事件发生的时间戳
     */
    public long getTimestamp() {
        return timestamp;
    }
}
