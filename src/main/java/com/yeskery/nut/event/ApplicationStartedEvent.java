package com.yeskery.nut.event;

import com.yeskery.nut.application.NutApplication;

/**
 * 应用启动完成后的事件
 *
 * @see ApplicationStartedListener
 * @author sunjay
 * 2023/9/9
 */
public class ApplicationStartedEvent extends ApplicationEvent {

    /**
     * 构建应用启动完成后的事件
     * @param nutApplication Nut应用对象
     */
    public ApplicationStartedEvent(NutApplication nutApplication) {
        super(nutApplication);
    }
}
