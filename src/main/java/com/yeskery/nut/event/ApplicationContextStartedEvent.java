package com.yeskery.nut.event;

import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文启动完成事件
 *
 * @see ApplicationContextStartedListener
 * @author YESKERY
 * 2023/9/12
 */
public class ApplicationContextStartedEvent extends ApplicationEvent {

    /**
     * 构建应用上下文启动完成事件
     * @param applicationContext 应用上下文
     */
    public ApplicationContextStartedEvent(ApplicationContext applicationContext) {
        super(applicationContext);
    }
}
