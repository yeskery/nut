package com.yeskery.nut.event;

/**
 * 应用上下文启动完成后事件监听器接口，监听{@link ApplicationContextStartedEvent}事件
 * @author sunjay
 * 2023/9/9
 */
public interface ApplicationContextStartedListener extends SmartApplicationListener {

    @Override
    default boolean supportEventType(Class<? extends ApplicationEvent> eventType) {
        return ApplicationContextStartedEvent.class.isAssignableFrom(eventType);
    }

    /**
     * 应用上下文启动完成后事件
     * @param applicationEvent 事件对象
     * @return 应用上下文启动完成后事件
     */
    default ApplicationContextStartedEvent getApplicationContextStartedEvent(ApplicationEvent applicationEvent) {
        return (ApplicationContextStartedEvent) applicationEvent;
    }
}
