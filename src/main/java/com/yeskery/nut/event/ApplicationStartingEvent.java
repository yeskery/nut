package com.yeskery.nut.event;

import com.yeskery.nut.application.NutApplication;

/**
 * 服务启动时事件
 *
 * @see ApplicationStartingListener
 * @author sunjay
 * 2023/9/9
 */
public class ApplicationStartingEvent extends ApplicationEvent {

    /**
     * 构建服务启动时事件
     * @param nutApplication nut应用对象
     */
    public ApplicationStartingEvent(NutApplication nutApplication) {
        super(nutApplication);
    }
}
