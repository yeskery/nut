package com.yeskery.nut.event;

import java.util.EventListener;

/**
 * 事件监听对象
 * @author Yeskery
 * 2023/8/8
 */
public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {

    /**
     * 事件发生的回调方法
     * @param event 事件对象
     */
    void onApplicationEvent(E event);
}
