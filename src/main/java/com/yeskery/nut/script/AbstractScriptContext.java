package com.yeskery.nut.script;

import com.yeskery.nut.script.function.*;
import com.yeskery.nut.script.parser.ScriptMetadataReader;

import java.util.LinkedList;
import java.util.List;

/**
 * 抽象的脚本上下文
 * @author sprout
 * @version 1.0
 * 2022-05-15 13:23
 */
public abstract class AbstractScriptContext extends DefaultFunctionContext implements ScriptContext {

    /** 脚本读取器 */
    protected final List<ScriptMetadataReader> scriptMetadataReaders = new LinkedList<>();

    /** 函数转换器 */
    protected FunctionParser functionParser = new DefaultFunctionParser(this);

    /** 函数执行器 */
    protected FunctionExecutor functionExecutor = new DefaultFunctionExecutor(functionParser);

    /**
     * 获取函数转换器
     * @return 函数转换器
     */
    public FunctionParser getFunctionParser() {
        return functionParser;
    }

    /**
     * 获取函数执行器
     * @return 函数执行器
     */
    public FunctionExecutor getFunctionExecutor() {
        return functionExecutor;
    }
}
