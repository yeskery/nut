package com.yeskery.nut.script;

/**
 * 控制器脚本
 * @author sprout
 * 2022-05-13 16:53
 */
public interface ControllerScript extends Script {

    /**
     * 将脚本转换为controller对象
     * @return 将脚本转换为controller对象
     */
    ControllerPath toController();
}
