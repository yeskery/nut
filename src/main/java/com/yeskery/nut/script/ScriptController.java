package com.yeskery.nut.script;

import com.yeskery.nut.core.Controller;

/**
 * 脚本controller对象
 * @author sprout
 * 2022-05-14 14:59
 *
 * @see com.yeskery.nut.core.ControllerSource#SCRIPT
 */
public class ScriptController implements Controller {
}
