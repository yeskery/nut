package com.yeskery.nut.script.expression;

import com.yeskery.nut.util.IOUtils;

import java.io.InputStream;

/**
 * 基础的二进制表达式处理逻辑
 * @author sprout
 * 2022-05-13 14:10
 */
public abstract class BaseBinaryExpression extends BaseNutExpression {

    @Override
    public byte[] handle(String expression) {
        InputStream inputStream = getInputStream(expression);
        if (inputStream == null) {
            return new byte[0];
        }
        return IOUtils.readByteArray(inputStream);
    }

    /**
     * 获取二进制资源的输入流
     * @param expression 资源所在的表达式
     * @return 二进制资源的输入流
     */
    protected abstract InputStream getInputStream(String expression);
}
