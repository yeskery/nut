package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * XML表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class XmlExpression extends BaseTextExpression {
    @Override
    public String getName() {
        return "xml";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_XML;
    }
}
