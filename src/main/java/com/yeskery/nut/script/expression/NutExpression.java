package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * Nut 表达式
 * @author sprout
 * 2022-05-13 12:42
 */
public interface NutExpression extends Expression<byte[]> {

    /**
     * 获取表达式所对应的媒体类型
     * @return 媒体类型
     */
    MediaType getMediaType();
}
