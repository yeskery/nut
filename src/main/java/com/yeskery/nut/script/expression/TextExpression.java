package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * Text表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class TextExpression extends BaseTextExpression {
    @Override
    public String getName() {
        return "text";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_PLAIN;
    }
}
