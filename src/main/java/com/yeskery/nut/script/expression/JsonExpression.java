package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * Json表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class JsonExpression extends BaseTextExpression {
    @Override
    public String getName() {
        return "json";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.APPLICATION_JSON;
    }
}
