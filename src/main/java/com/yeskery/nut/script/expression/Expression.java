package com.yeskery.nut.script.expression;

/**
 * 表达式接口
 * @author sprout
 * 2022-05-13 12:36
 */
public interface Expression<T> {

    /**
     * 表达式名称
     * @return 表达式名称
     */
    String getName();

    /**
     * 处理表达式
     * @param expression 表达式
     * @return 处理后的表达式结果
     */
    T handle(String expression);
}
