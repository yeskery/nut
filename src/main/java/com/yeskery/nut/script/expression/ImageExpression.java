package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * 图片表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class ImageExpression extends FileExpression {
    @Override
    public String getName() {
        return "image";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.IMAGE_JPEG;
    }
}
