package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;

/**
 * Html表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class HtmlExpression extends BaseTextExpression {
    @Override
    public String getName() {
        return "html";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_HTML;
    }
}
