package com.yeskery.nut.script.expression;

/**
 * 基础的Nut表达式类
 * @author sprout
 * 2022-05-13 14:05
 */
public abstract class BaseNutExpression implements NutExpression {

    /**
     * 判断是否匹配当前的表达式
     * @param name 表达式名称
     * @return 是否匹配当前的表达式
     */
    public boolean match(String name) {
        return getName().equalsIgnoreCase(name);
    }
}
