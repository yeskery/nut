package com.yeskery.nut.script.expression;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.core.ResponseNutException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * 文件表达式
 * @author sprout
 * 2022-05-13 14:06
 */
public class FileExpression extends BaseBinaryExpression {
    @Override
    public String getName() {
        return "file";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.APPLICATION_OCTET_STREAM;
    }

    @Override
    protected InputStream getInputStream(String expression) {
        try {
            return new FileInputStream(expression);
        } catch (FileNotFoundException e) {
            throw new ResponseNutException("Resource Not Found.", ResponseCode.NOT_FOUND);
        }
    }
}
