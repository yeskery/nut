package com.yeskery.nut.script.expression;

import com.yeskery.nut.util.StringUtils;

import java.nio.charset.StandardCharsets;

/**
 * 基础的文本表达式处理逻辑
 * @author sprout
 * 2022-05-13 14:10
 */
public abstract class BaseTextExpression extends BaseNutExpression {

    @Override
    public byte[] handle(String expression) {
        if (StringUtils.isEmpty(expression)) {
            return new byte[0];
        }
        return expression.getBytes(StandardCharsets.UTF_8);
    }
}
