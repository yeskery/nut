package com.yeskery.nut.script;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.Path;

import java.util.Set;

/**
 * Controller及路径对象
 * @author sprout
 * 2022-05-14 17:49
 */
public class ControllerPath {

    /** 控制器 */
    private Controller controller;

    /** 路径 */
    private Set<Path> paths;

    /**
     * 获取控制器
     * @return 控制器
     */
    public Controller getController() {
        return controller;
    }

    /**
     * 设置控制器
     * @param controller 控制器
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * 获取路径
     * @return 路径
     */
    public Set<Path> getPaths() {
        return paths;
    }

    /**
     * 设置路径
     * @param paths 路径
     */
    public void setPaths(Set<Path> paths) {
        this.paths = paths;
    }
}
