package com.yeskery.nut.script;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.Method;
import com.yeskery.nut.core.NameAndValue;
import com.yeskery.nut.core.Path;
import com.yeskery.nut.util.StringUtils;

import java.util.Collections;
import java.util.Set;

/**
 * 脚本原数据
 * @author sprout
 * 2022-05-13 17:00
 */
public class Metadata {

    /** path 名称 */
    public static final String FIELD_PATH = "path";

    /** method 名称 */
    public static final String FIELD_METHOD = "method";

    /** response_cookie名称 */
    public static final String FIELD_RESPONSE_COOKIE = "response_cookie";

    /** response_header 名称 */
    public static final String FIELD_RESPONSE_HEADER = "response_header";

    /** body 名称 */
    public static final String FIELD_BODY = "body";

    /** 空脚本原数据 */
    public static final Metadata EMPTY_METADATA = new Metadata();

    /** 访问路径 */
    private Set<Path> paths;

    /** 请求方法 */
    private Set<Method> methods;

    /** 响应Cookie */
    private Set<NameAndValue> responseCookies;

    /** 响应头 */
    private Set<NameAndValue> responseHeaders;

    /** 响应体 */
    private String body;

    /** 媒体类型 */
    private MediaType mediaType;

    /**
     * 获取访问路径
     * @return 访问路径
     */
    public Set<Path> getPaths() {
        return paths;
    }

    /**
     * 设置访问路径
     * @param paths 访问路径
     */
    public void setPaths(Set<Path> paths) {
        this.paths = paths;
    }

    /**
     * 获取请求方法
     * @return 请求方法
     */
    public Set<Method> getMethods() {
        return methods;
    }

    /**
     * 设置请求方法
     * @param methods 请求方法
     */
    public void setMethods(Set<Method> methods) {
        this.methods = methods;
    }

    /**
     * 获取响应cookie
     * @return 响应cookie
     */
    public Set<NameAndValue> getResponseCookies() {
        return responseCookies;
    }

    /**
     * 设置响应cookie
     * @param responseCookies 响应cookie
     */
    public void setResponseCookies(Set<NameAndValue> responseCookies) {
        this.responseCookies = responseCookies;
    }

    /**
     * 获取响应头
     * @return 响应头
     */
    public Set<NameAndValue> getResponseHeaders() {
        return responseHeaders;
    }

    /**
     * 设置响应头
     * @param responseHeaders 响应头
     */
    public void setResponseHeaders(Set<NameAndValue> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    /**
     * 获取响应体
     * @return 响应体
     */
    public String getBody() {
        return body;
    }

    /**
     * 设置响应体
     * @param body 响应体
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * 获取媒体类型
     * @return 媒体类型
     */
    public MediaType getMediaType() {
        return mediaType;
    }

    /**
     * 设置媒体类型
     * @param mediaType 媒体类型
     */
    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * 获取有效的元数据对象
     * @return 有效的元数据对象
     */
    public Metadata getValidMetadata() {
        boolean invalid = this.paths == null || this.paths.isEmpty()
                || this.methods == null || this.methods.isEmpty()
                || StringUtils.isEmpty(this.body);
        Metadata metadata = invalid ? EMPTY_METADATA : this;
        if (metadata.responseCookies == null) {
            metadata.responseCookies = Collections.emptySet();
        }
        if (metadata.responseHeaders == null) {
            metadata.responseHeaders = Collections.emptySet();
        }
        return metadata;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "paths=" + paths +
                ", methods=" + methods +
                ", mediaType=" + mediaType +
                ", responseHeaders=" + responseHeaders +
                ", body='" + body + '\'' +
                '}';
    }
}
