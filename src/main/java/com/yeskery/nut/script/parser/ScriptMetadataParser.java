package com.yeskery.nut.script.parser;

import com.yeskery.nut.script.Metadata;

/**
 * 脚本元数据转换器
 * @author sprout
 * 2022-05-13 17:37
 */
@FunctionalInterface
public interface ScriptMetadataParser {

    /** 分隔符 */
    char SEPARATOR = '=';

    /**
     * 获取原数据
     * @param script 脚本内容
     * @return 原数据
     */
    Metadata parseMetadata(String script);
}
