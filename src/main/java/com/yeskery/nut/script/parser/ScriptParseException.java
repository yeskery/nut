package com.yeskery.nut.script.parser;

import com.yeskery.nut.core.NutException;

/**
 * 脚本解析异常
 * @author sprout
 * 2022-05-13 18:05
 */
public class ScriptParseException extends NutException {

    /**
     * 构建一个 {@link ScriptParseException}
     * @param index 索引
     * @param nearContent 附近的内容
     */
    public ScriptParseException(int index, String nearContent) {
        super("Script parsing exception on index [" + index + "], Nearby content [" + nearContent + "]");
    }

    /**
     * 构建一个 {@link ScriptParseException}
     * @param message message
     */
    public ScriptParseException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ScriptParseException}
     * @param message message
     * @param cause cause
     */
    public ScriptParseException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ScriptParseException}
     * @param index 索引
     * @param nearContent 附近的内容
     * @param cause cause
     */
    public ScriptParseException(int index, String nearContent, Throwable cause) {
        super("Script parsing exception on index [" + index + "], Nearby content [" + nearContent + "]", cause);
    }

    /**
     * 构建一个 {@link ScriptParseException}
     * @param cause cause
     */
    public ScriptParseException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ScriptParseException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ScriptParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
