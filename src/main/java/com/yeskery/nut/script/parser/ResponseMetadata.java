package com.yeskery.nut.script.parser;

import com.yeskery.nut.script.Metadata;

/**
 * 响应原数据
 * @author sprout
 * 2022-05-14 14:21
 */
public class ResponseMetadata extends Metadata {

    /**
     * 响应内容
     */
    private byte[] response;

    /**
     * 从元数据中创建一个响应原数据对象
     * @param metadata 元数据
     * @return 响应原数据对象
     */
    public static ResponseMetadata fromMetadata(Metadata metadata) {
        ResponseMetadata responseMetadata = new ResponseMetadata();
        responseMetadata.setPaths(metadata.getPaths());
        responseMetadata.setMethods(metadata.getMethods());
        responseMetadata.setResponseCookies(metadata.getResponseCookies());
        responseMetadata.setResponseHeaders(metadata.getResponseHeaders());
        responseMetadata.setMediaType(metadata.getMediaType());
        responseMetadata.setBody(metadata.getBody());
        return responseMetadata;
    }

    /**
     * 获取响应内容
     * @return 响应内容
     */
    public byte[] getResponse() {
        return response;
    }

    /**
     * 设置响应内容
     * @param response 响应内容
     */
    public void setResponse(byte[] response) {
        this.response = response;
    }
}
