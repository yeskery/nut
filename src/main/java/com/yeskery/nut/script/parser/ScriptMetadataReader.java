package com.yeskery.nut.script.parser;

import com.yeskery.nut.script.Metadata;

import java.util.List;

/**
 * 脚本元数据读取接口
 * @author sprout
 * 2022-05-13 17:39
 */
@FunctionalInterface
public interface ScriptMetadataReader {

    /** 开始标志 */
    char SCRIPT_START_SYMBOL = '[';

    /** 结束标志 */
    char SCRIPT_END_SYMBOL = ']';

    /**
     * 转换脚本原数据
     * @return 脚本原数据
     */
    List<Metadata> parseMetadata();
}
