package com.yeskery.nut.script.parser;

/**
 * 元数据索引对象
 * @author sprout
 * 2022-05-13 17:56
 */
public class MetadataIndex {

    /** 索引 */
    private int index;

    /** 符号 */
    private char symbol;

    /**
     * 构建元数据索引对象
     */
    public MetadataIndex() {
    }

    /**
     * 构建元数据索引对象
     * @param index 索引
     * @param symbol 符号
     */
    public MetadataIndex(int index, char symbol) {
        this.index = index;
        this.symbol = symbol;
    }

    /***
     * 获取当前索引
     * @return 当前索引
     */
    public int getIndex() {
        return index;
    }

    /**
     * 设置当前索引
     * @param index 当前索引
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 获取符号
     * @return 获取符号
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * 设置符号
     * @param symbol 符号
     */
    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
}
