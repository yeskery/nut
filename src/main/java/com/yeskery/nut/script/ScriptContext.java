package com.yeskery.nut.script;

import com.yeskery.nut.script.function.FunctionExecutor;

import java.util.List;

/**
 * 脚本上下文空间
 * @author sprout
 * 2022-05-14 14:45
 */
public interface ScriptContext {

    /**
     * 获取所有的控制器脚本
     * @return 所有的控制器脚本
     */
    List<ControllerScript> getControllerScripts();

    /**
     * 获取所有的控制器脚本
     * @param functionExecutor 函数执行器
     * @return 所有的控制器脚本
     */
    List<ControllerScript> getControllerScripts(FunctionExecutor functionExecutor);
}
