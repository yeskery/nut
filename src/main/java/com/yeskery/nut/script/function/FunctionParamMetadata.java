package com.yeskery.nut.script.function;

/**
 * 函数参数元数据信息
 * @author sprout
 * 2022-05-17 14:41
 */
public class FunctionParamMetadata<T> {

    /** 原始文本对象 */
    private String text;

    /** 参数对象 */
    private T param;

    /** 参数对象类型 */
    private Class<T> source;

    /**
     * 获取原始文本对象
     * @return 原始文本对象
     */
    public String getText() {
        return text;
    }

    /**
     * 设置原始文本对象
     * @param text 原始文本对象
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 获取参数对象
     * @return 参数对象
     */
    public T getParam() {
        return param;
    }

    /**
     * 设置参数对象
     * @param param 参数对象
     */
    public void setParam(T param) {
        this.param = param;
    }

    /**
     * 获取参数对象类型
     * @return 参数对象类型
     */
    public Class<T> getSource() {
        return source;
    }

    /**
     * 设置参数对象类型
     * @param source 参数对象类型
     */
    public void setSource(Class<T> source) {
        this.source = source;
    }
}
