package com.yeskery.nut.script.function.extend;

import com.yeskery.nut.script.function.Function;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机IP函数
 * @author sprout
 * 2022-05-20 17:04
 */
public class IpFunction implements Function {

    /** 指定 IP 范围 */
    private static final int[][] RANGE = {
            // 36.56.0.0-36.63.255.255
            {607649792, 608174079},
            // 61.232.0.0-61.237.255.255
            {1038614528, 1039007743},
            // 106.80.0.0-106.95.255.255
            {1783627776, 1784676351},
            // 121.76.0.0-121.77.255.255
            {2035023872, 2035154943},
            // 123.232.0.0-123.235.255.255
            {2078801920, 2079064063},
            // 139.196.0.0-139.215.255.255
            {-1950089216, -1948778497},
            // 171.8.0.0-171.15.255.255
            {-1425539072, -1425014785},
            // 182.80.0.0-182.92.255.255
            {-1236271104, -1235419137},
            // 210.25.0.0-210.47.255.255
            {-770113536, -768606209},
            // 222.16.0.0-222.95.255.255
            {-569376768, -564133889},
    };

    @Override
    public String getName() {
        return "ipGenerator";
    }

    @Override
    public String apply(String content) throws Exception {
        int index = ThreadLocalRandom.current().nextInt(10);
        return num2ip(RANGE[index][0] + ThreadLocalRandom.current().nextInt(RANGE[index][1] - RANGE[index][0]));
    }

    /**
     * 将十进制转换成IP地址
     * @param ip ip
     */
    private String num2ip(int ip) {
        int[] b = new int[4];
        b[0] = (ip >> 24) & 0xff;
        b[1] = (ip >> 16) & 0xff;
        b[2] = (ip >> 8) & 0xff;
        b[3] = ip & 0xff;
        return b[0] + "." + b[1] + "." + b[2] + "." + b[3];
    }
}
