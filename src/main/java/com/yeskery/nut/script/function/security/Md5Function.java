package com.yeskery.nut.script.function.security;

import com.yeskery.nut.script.function.Function;
import com.yeskery.nut.util.DigestUtils;

/**
 * md5函数
 * @author sprout
 * @version 1.0
 * 2022-07-26 15:02
 */
public class Md5Function implements Function {
    @Override
    public String getName() {
        return "md5";
    }

    @Override
    public String apply(String content) throws Exception {
        return DigestUtils.md5DigestAsHex(content);
    }
}
