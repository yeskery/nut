package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Cookie;
import com.yeskery.nut.core.Method;
import com.yeskery.nut.core.Request;
import com.yeskery.nut.script.function.Function;

/**
 * WEB请求基础函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:09
 */
public abstract class AbstractWebRequestFunction implements Function {

    /** 请求对象 */
    private final Request request;

    /**
     * 构建WEB请求基础函数
     * @param request 请求对象
     */
    protected AbstractWebRequestFunction(Request request) {
        this.request = request;
    }

    /**
     * 获取请求对象
     * @return 请求对象
     */
    protected Request getRequest() {
        return request;
    }

    /**
     * 获取请求方法
     * @return 请求方法
     */
    protected Method getMethod() {
        return request.getMethod();
    }

    /**
     * 获取参数值
     * @param name 参数名
     * @return 参数值
     */
    protected String getParameter(String name) {
        return request.getParameter(name);
    }

    /**
     * 获取cookie值
     * @param name cookie名
     * @return cookie值
     */
    protected String getCookie(String name) {
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals(name)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    /**
     * 获取请求头值
     * @param name 请求头名
     * @return 请求头值
     */
    protected String getHeader(String name) {
        return request.getHeader(name);
    }

    /**
     * 获取请求路径
     * @return 请求路径
     */
    protected String getPath() {
        return request.getPath();
    }
}
