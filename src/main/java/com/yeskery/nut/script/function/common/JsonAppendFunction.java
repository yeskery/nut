package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.BaseMultiParamFunction;

/**
 * 追加json字段
 * @author sprout
 * @version 1.0
 * 2022-07-22 13:46
 */
public class JsonAppendFunction extends BaseMultiParamFunction {
    /** 空格符 */
    private static final char JSON_SPACE = ' ';
    /** 字段分隔符 */
    private static final char JSON_SEPARATOR = ',';
    /** 字段值分隔符 */
    private static final char JSON_VALUE_SEPARATOR = ':';
    /** 对象起始符 */
    private static final char JSON_OBJECT_START = '{';
    /** 对象结束符 */
    private static final char JSON_OBJECT_END = '}';
    /** 对象字段访问符 */
    private static final String JSON_FIELD_ACCESS_SYMBOL = ".";
    /** 对象字段访问正则 */
    private static final String JSON_FIELD_ACCESS_PATTERN = "\\.";
    /** 字符串值符号 */
    private static final String JSON_STRING_VALUE_SYMBOL = "\"";

    @Override
    public String getName() {
        return "jsonAppend";
    }

    @Override
    protected String doParams3(String[] params, String content) {
        int insertIndex = findInsertIndex(params[0], params[1]);
        if (insertIndex == -1) {
            return content;
        }
        String value = JSON_STRING_VALUE_SYMBOL + getAppendFieldName(params[1]) + JSON_STRING_VALUE_SYMBOL
                + JSON_VALUE_SEPARATOR + JSON_SPACE + params[2];
        StringBuilder stringBuilder = new StringBuilder(params[0]).insert(insertIndex, value);
        for (int i = insertIndex + value.length(); i < stringBuilder.length(); i++) {
            char c = stringBuilder.charAt(i);
            if (c == JSON_SPACE) {
                continue;
            } else if (c == JSON_SEPARATOR || c == JSON_OBJECT_END) {
                break;
            } else {
                stringBuilder.insert(i, JSON_SEPARATOR);
                break;
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 查找要插入的索引位置
     * @param json json字符串
     * @param appendFieldName 要插入的字段名称
     * @return 要插入的索引位置
     */
    private int findInsertIndex(String json, String appendFieldName) {
        if (!appendFieldName.contains(JSON_FIELD_ACCESS_SYMBOL)) {
            int index = json.indexOf(JSON_OBJECT_START);
            if (index == -1) {
                return -1;
            }
            return index + 1;
        }
        String[] splits = appendFieldName.split(JSON_FIELD_ACCESS_PATTERN);
        int startIndex = 0;
        for (int i = 0; i <= splits.length - 1; i++) {
            String field = JSON_STRING_VALUE_SYMBOL + splits[0] + JSON_STRING_VALUE_SYMBOL;
            int index = json.indexOf(field, startIndex);
            if (index == -1) {
                return -1;
            }
            int j;
            for (j = index + field.length();; j++) {
                char c = json.charAt(j);
                if (c == JSON_SPACE || c == JSON_VALUE_SEPARATOR) {
                    continue;
                } else if (c == JSON_OBJECT_START) {
                    break;
                } else {
                    return -1;
                }
            }

            if (i == splits.length - 2) {
                return j + 1;
            }
            startIndex = j + 1;
        }
        return -1;
    }

    /**
     * 获取要插入的字段名称
     * @param appendFieldName 指定的追加字段名称
     * @return 要插入的字段名称
     */
    private String getAppendFieldName(String appendFieldName) {
        String[] splits = appendFieldName.split(JSON_FIELD_ACCESS_PATTERN);
        if (splits.length <= 1) {
            return appendFieldName;
        }
        return splits[splits.length - 1];
    }
}
