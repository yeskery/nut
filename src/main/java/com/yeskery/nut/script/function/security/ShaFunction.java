package com.yeskery.nut.script.function.security;

import com.yeskery.nut.script.function.Function;
import com.yeskery.nut.util.DigestUtils;

/**
 * SHA函数
 * @author sprout
 * @version 1.0
 * 2022-07-26 15:17
 */
public class ShaFunction implements Function {
    @Override
    public String getName() {
        return "sha";
    }

    @Override
    public String apply(String content) throws Exception {
        return DigestUtils.shaDigestAsHex(content);
    }
}
