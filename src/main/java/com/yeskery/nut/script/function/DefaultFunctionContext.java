package com.yeskery.nut.script.function;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 默认的单次函数上下文
 * @author sprout
 * @version 1.0
 * 2022-05-15 13:03
 */
public class DefaultFunctionContext implements FunctionContext {

    /** 函数 */
    private final Map<String, Function> functions = new ConcurrentHashMap<>();

    @Override
    public void registerFunction(Function function) {
        functions.put(function.getName(), function);
    }

    @Override
    public Function getFunction(String name) {
        return functions.get(name);
    }

    @Override
    public Collection<Function> getAllFunctions() {
        return Collections.unmodifiableCollection(functions.values());
    }
}
