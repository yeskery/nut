package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.RequestApplicationContext;
import com.yeskery.nut.util.StringUtils;

import java.nio.charset.StandardCharsets;

/**
 * 请求体函数
 * @author sprout
 * @version 1.0
 * 2022-07-19 11:12
 */
public class RequestBodyFunction extends AbstractWebRequestFunction {
    /**
     * 构建请求体函数
     * @param request 请求对象
     */
    public RequestBodyFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "requestBody";
    }

    @Override
    public String apply(String content) throws Exception {
        return new String(getRequest().getBody(), StandardCharsets.UTF_8);
    }
}
