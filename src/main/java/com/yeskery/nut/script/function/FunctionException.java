package com.yeskery.nut.script.function;

import com.yeskery.nut.core.NutException;

/**
 * 函数异常
 * @author sprout
 * @version 1.0
 * 2022-07-26 15:08
 */
public class FunctionException extends NutException {

    /** 函数对象 */
    private final Function function;

    /**
     * 构建一个 {@link FunctionException}
     * @param message message
     * @param function 函数对象
     */
    public FunctionException(String message, Function function) {
        super(message);
        this.function = function;
    }

    /**
     * 构建一个 {@link FunctionException}
     * @param message message
     * @param cause cause
     * @param function 函数对象
     */
    public FunctionException(String message, Throwable cause, Function function) {
        super(message, cause);
        this.function = function;
    }

    /**
     * 构建一个 {@link FunctionException}
     * @param cause cause
     * @param function 函数对象
     */
    public FunctionException(Throwable cause, Function function) {
        super(cause);
        this.function = function;
    }

    /**
     * 构建一个 {@link FunctionException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     * @param function 函数对象
     */
    public FunctionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Function function) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.function = function;
    }

    /**
     * 获取函数对象
     * @return 函数对象
     */
    public Function getFunction() {
        return function;
    }
}
