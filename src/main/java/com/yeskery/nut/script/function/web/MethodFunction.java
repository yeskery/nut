package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;

/**
 * 请求方法函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:20
 */
public class MethodFunction extends AbstractWebRequestFunction {
    /**
     * 构建请求方法函数
     * @param request 请求对象
     */
    public MethodFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "method";
    }

    @Override
    public String apply(String content) throws Exception {
        return getMethod().toString();
    }
}
