package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Response;
import com.yeskery.nut.script.function.BaseMultiParamFunction;

/**
 * 返回状态码函数
 * @author sprout
 * @version 1.0
 * 2022-07-26 14:47
 */
public class ResponseStatusFunction extends BaseMultiParamFunction {

    /** 响应对象 */
    private final Response response;

    /**
     * 构建返回状态码函数
     * @param response 响应对象
     */
    public ResponseStatusFunction(Response response) {
        this.response = response;
    }

    @Override
    public String getName() {
        return "responseStatus";
    }

    @Override
    protected String doParams2(String[] params, String content) {
        response.setCode(Integer.parseInt(params[0]));
        return params[1];
    }
}
