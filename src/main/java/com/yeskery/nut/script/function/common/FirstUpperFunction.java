package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.Function;

/**
 * 首字母大写函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 13:33
 */
public class FirstUpperFunction implements Function {
    @Override
    public String getName() {
        return "fupper";
    }

    @Override
    public String apply(String content) throws Exception {
        if (content.length() <= 0) {
            return content;
        } else if (content.length() == 1) {
            return content.toUpperCase();
        } else {
            return String.valueOf(content.charAt(0)).toUpperCase() + content.substring(1).toLowerCase();
        }
    }
}
