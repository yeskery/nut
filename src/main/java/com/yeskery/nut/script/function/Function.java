package com.yeskery.nut.script.function;

/**
 * 函数接口
 * @author sprout
 * @version 1.0
 * 2022-05-15 01:56
 */
public interface Function {

    /** 默认成功时候返回的字符串 */
    String SUCCESS = "success";

    /** 默认失败时候返回的字符串 */
    String FAILURE = "failure";

    /** 函数分隔符 */
    String FUNCTION_SEPARATOR = "$";

    /** 函数开始符号 */
    String FUNCTION_START_SYMBOL = "(";

    /** 函数结束符号 */
    String FUNCTION_END_SYMBOL = ")";

    /**
     * 函数名称
     * @return 函数名称
     */
    String getName();

    /**
     * 函数执行方法
     * @param content 函数内容
     * @return 函数执行结果
     * @throws Exception 函数执行异常
     */
    String apply(String content) throws Exception;
}
