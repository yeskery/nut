package com.yeskery.nut.script.function.extend;

import com.yeskery.nut.script.function.Function;
import com.yeskery.nut.util.StringUtils;

import java.util.UUID;

/**
 * UUID函数
 * @author sprout
 * 2022-05-20 17:01
 */
public class UuidFunction implements Function {
    @Override
    public String getName() {
        return "uuidGenerator";
    }

    @Override
    public String apply(String content) throws Exception {
        if (StringUtils.isEmpty(content) || Boolean.TRUE.toString().equalsIgnoreCase(content)) {
            return UUID.randomUUID().toString();
        }
        return UUID.randomUUID().toString().replace("-", "");
    }
}
