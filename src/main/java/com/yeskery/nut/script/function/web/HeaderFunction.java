package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;

/**
 * 请求头函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:19
 */
public class HeaderFunction extends AbstractWebRequestFunction {
    /**
     * 构建请求头函数
     * @param request 请求对象
     */
    public HeaderFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "header";
    }

    @Override
    public String apply(String content) throws Exception {
        return getHeader(content);
    }
}
