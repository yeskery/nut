package com.yeskery.nut.script.function;

import com.yeskery.nut.util.StringUtils;

import java.util.List;

/**
 * 函数信息
 * @author sprout
 * 2022-05-16 15:45
 */
public class FunctionInfo {

    /** 函数名 */
    private String name;

    /** 函数体 */
    private String body;

    /** 函数体开始索引 */
    private Integer startIndex;

    /** 函数体结束索引 */
    private Integer endIndex;

    /** 子函数信息 */
    private List<FunctionInfo> childFunctionInfos;

    /**
     * 获取函数名
     * @return 函数名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置函数名
     * @param name 函数名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取函数体
     * @return 函数体
     */
    public String getBody() {
        return body;
    }

    /**
     * 设置函数体
     * @param body 函数体
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * 获取函数体开始索引
     * @return 函数体开始索引
     */
    public Integer getStartIndex() {
        return startIndex;
    }

    /**
     * 设置函数体开始索引
     * @param startIndex 函数体开始索引
     */
    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * 获取函数体结束索引
     * @return 函数体结束索引
     */
    public Integer getEndIndex() {
        return endIndex;
    }

    /**
     * 设置函数体结束索引
     * @param endIndex 函数体结束索引
     */
    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

    /**
     * 获取子函数信息
     * @return 子函数信息
     */
    public List<FunctionInfo> getChildFunctionInfos() {
        return childFunctionInfos;
    }

    /**
     * 设置子函数信息
     * @param childFunctionInfos 子函数信息
     */
    public void setChildFunctionInfos(List<FunctionInfo> childFunctionInfos) {
        this.childFunctionInfos = childFunctionInfos;
    }

    /**
     * 获取有效的函数对象
     * @return 有效的函数对象
     */
    public FunctionInfo getValidFunction() {
        if (StringUtils.isEmpty(name) || startIndex == null || endIndex == null) {
            return null;
        }
        return this;
    }
}
