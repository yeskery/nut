package com.yeskery.nut.script.function;

/**
 * 函数执行器
 * @author sprout
 * @version 1.0
 * 2022-05-15 21:18
 */
public interface FunctionExecutor {

    /**
     * 对包含函数的字符串行进行函数执行
     * @param line 字符串行
     * @return 执行完函数的字符串
     */
    String execute(String line);

    /**
     * 对包含函数的字符串行进行函数执行
     * @param line 字符串行
     * @param extendFunctionContext 额外扩展的函数空间，用于自定义函数
     * @return 执行完函数的字符串
     */
    String execute(String line, FunctionContext extendFunctionContext);
}
