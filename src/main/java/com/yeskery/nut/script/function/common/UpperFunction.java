package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.Function;

/**
 * 字符串大写函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:27
 */
public class UpperFunction implements Function {
    @Override
    public String getName() {
        return "upper";
    }

    @Override
    public String apply(String content) throws Exception {
        return content.toUpperCase();
    }
}
