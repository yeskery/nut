package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;
import com.yeskery.nut.core.RequestApplicationContext;
import com.yeskery.nut.util.StringUtils;

/**
 * 路径变量函数
 * @author sprout
 * @version 1.0
 * 2022-07-19 11:12
 */
public class PathVariableFunction extends AbstractWebRequestFunction {
    /**
     * 构建路径变量函数
     * @param request 请求对象
     */
    public PathVariableFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "pathVariable";
    }

    @Override
    public String apply(String content) throws Exception {
        if (StringUtils.isEmpty(content)) {
            return content;
        }
        Object resource = RequestApplicationContext.getResource(content);
        return resource == null ? "null" : resource.toString();
    }
}
