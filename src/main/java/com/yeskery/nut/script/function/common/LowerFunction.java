package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.Function;

/**
 * 字符串小写函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:29
 */
public class LowerFunction implements Function {
    @Override
    public String getName() {
        return "lower";
    }

    @Override
    public String apply(String content) throws Exception {
        return content.toLowerCase();
    }
}
