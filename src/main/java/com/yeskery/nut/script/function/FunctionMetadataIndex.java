package com.yeskery.nut.script.function;

/**
 * 函数元数据索引对象
 * @author sprout
 * @version 1.0
 * 2022-05-15 15:27
 */
public class FunctionMetadataIndex {

    /** 函数元数据 */
    private final FunctionMetadata functionMetadata;

    /** 开始索引 */
    private final int startIndex;

    /** 结束索引 */
    private final int endIndex;

    /** 函数体 */
    private final String body;

    /** 上级函数信息对象 */
    private final FunctionMetadata parentFunctionMetadata;

    /**
     * 构建函数元数据索引对象
     * @param functionMetadata 函数元数据
     * @param startIndex 开始索引
     * @param endIndex 结束索引
     * @param body 函数体
     * @param parentFunctionMetadata 上级函数信息对象
     */
    public FunctionMetadataIndex(FunctionMetadata functionMetadata, int startIndex, int endIndex, String body, FunctionMetadata parentFunctionMetadata) {
        this.functionMetadata = functionMetadata;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.body = body;
        this.parentFunctionMetadata = parentFunctionMetadata;
    }

    /**
     * 构建函数元数据索引对象
     * @param functionMetadata 函数元数据
     * @param startIndex 开始索引
     * @param endIndex 结束索引
     * @param body 函数体
     */
    public FunctionMetadataIndex(FunctionMetadata functionMetadata, int startIndex, int endIndex, String body) {
        this(functionMetadata, startIndex, endIndex, body, null);
    }

    /**
     * 获取函数元数据
     * @return 函数元数据
     */
    public FunctionMetadata getFunctionMetadata() {
        return functionMetadata;
    }

    /**
     * 获取结束索引
     * @return 结束索引
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * 获取结束索引
     * @return 结束索引
     */
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * 获取函数体
     * @return 函数体
     */
    public String getBody() {
        return body;
    }

    /**
     * 获取上级函数信息对象
     * @return 上级函数信息对象
     */
    public FunctionMetadata getParentFunctionMetadata() {
        return parentFunctionMetadata;
    }
}
