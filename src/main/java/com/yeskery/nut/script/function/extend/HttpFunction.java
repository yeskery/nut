package com.yeskery.nut.script.function.extend;

import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.Method;
import com.yeskery.nut.core.NameAndValue;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.script.function.BaseMultiParamFunction;
import com.yeskery.nut.util.StringUtils;
import com.yeskery.nut.util.http.HttpUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Http函数
 * @author sprout
 * 2022-05-20 13:41
 */
public class HttpFunction extends BaseMultiParamFunction {
    @Override
    public String getName() {
        return "http";
    }

    @Override
    protected boolean validParams(String content) {
        return content.length() < getName().length() || !content.substring(0, getName().length()).equalsIgnoreCase(getName());
    }

    @Override
    protected String doParams1(String param) {
        return HttpUtils.getDefaultInstance().doGetString(param);
    }

    @Override
    protected String doParams2(String[] params, String content) {
        return doParams4(new String[]{params[0], params[1], "", null}, content);
    }

    @Override
    protected String doParams3(String[] params, String content) {
        return doParams4(new String[]{params[0], params[1], params[2], null}, content);
    }

    @Override
    protected String doParams4(String[] params, String content) {
        String httpUrl = params[0];
        Method method = Method.valueOf(params[1].toUpperCase());
        String[] values = params[2].split("&");
        List<NameAndValue> headers = new ArrayList<>(values.length);
        for (String header : values) {
            String[] splits = header.trim().split("=");
            if (splits.length == 2) {
                String name = splits[0].trim();
                String value = splits[1].trim();
                if (!StringUtils.isEmpty(name) && !StringUtils.isEmpty(value)) {
                    headers.add(new NameAndValue(name, value));
                }
            }
        }
        return doRequest(httpUrl, method, headers, params[3]);
    }

    /**
     * 执行HTTP请求
     * @param httpUrl HTTP URL
     * @param method 请求方法
     * @param headers 请求头
     * @param body 方法体
     * @return 响应结果
     */
    public String doRequest(String httpUrl, Method method, List<NameAndValue> headers, String body) {
        return doRequest(httpUrl, method, headers, null, body);
    }

    /**
     * 执行HTTP请求
     * @param httpUrl HTTP URL
     * @param method 请求方法
     * @param headers 请求头
     * @param mediaType 媒体类型
     * @param body 方法体
     * @return 响应结果
     */
    public String doRequest(String httpUrl, Method method, List<NameAndValue> headers, MediaType mediaType, String body) {
        if (Method.GET == method) {
            return headers == null || headers.isEmpty()
                    ? HttpUtils.getDefaultInstance().doGetString(httpUrl)
                    : HttpUtils.getDefaultInstance().doGetString(httpUrl, headers);
        } else if (Method.POST == method) {
            return headers == null || headers.isEmpty()
                    ? HttpUtils.getDefaultInstance().doPostString(httpUrl, body)
                    : HttpUtils.getDefaultInstance().doPostString(httpUrl, headers, body, mediaType);
        } else if (Method.PUT == method) {
            return headers == null || headers.isEmpty()
                    ? HttpUtils.getDefaultInstance().doPutString(httpUrl, body)
                    : HttpUtils.getDefaultInstance().doPutString(httpUrl, headers, body, mediaType);
        } else if (Method.DELETE == method) {
            return headers == null || headers.isEmpty()
                    ? HttpUtils.getDefaultInstance().doDeleteString(httpUrl, body)
                    : HttpUtils.getDefaultInstance().doDeleteString(httpUrl, headers, body, mediaType);
        } else {
            throw new NutException("Unsupported Http Method.");
        }
    }
}
