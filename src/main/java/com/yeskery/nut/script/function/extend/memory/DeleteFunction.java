package com.yeskery.nut.script.function.extend.memory;

/**
 * Delete函数，删除数据函数
 * @author sprout
 * @version 1.0
 * 2022-07-19 10:52
 */
public class DeleteFunction extends BaseMemoryPluginSupportFunction {
    @Override
    public String getName() {
        return "mdelete";
    }

    @Override
    protected String doParams1(String param) {
        getStorage().remove(param);
        return SUCCESS;
    }

    @Override
    protected String doParams2(String[] params, String content) {
        getStorage().remove(params[0]);
        return params[1];
    }
}
