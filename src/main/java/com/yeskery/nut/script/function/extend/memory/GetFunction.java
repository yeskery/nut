package com.yeskery.nut.script.function.extend.memory;

/**
 * Get函数，获取数据函数
 * @author sprout
 * @version 1.0
 * 2022-07-19 10:52
 */
public class GetFunction extends BaseMemoryPluginSupportFunction {
    @Override
    public String getName() {
        return "mget";
    }

    @Override
    protected String doParams1(String param) {
        String result = getStorage().get(param, String.class);
        return result == null ? FAILURE : result;
    }

    @Override
    protected String doParams2(String[] params, String content) {
        String result = getStorage().get(params[0], String.class);
        return result == null ? params[1] : result;
    }
}
