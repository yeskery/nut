package com.yeskery.nut.script.function;

import java.util.Collection;
import java.util.Stack;

/**
 * 函数转换器
 * @author sprout
 * @version 1.0
 * 2022-05-15 13:29
 */
public interface FunctionParser {

    /**
     * 将表达式转换为函数元数据
     * @param expression 表达式
     * @return 函数元数据栈
     */
    Collection<Stack<FunctionMetadataIndex>> parse(String expression);

    /**
     * 将表达式转换为函数元数据
     * @param expression 表达式
     * @param extendFunctionContext 额外扩展的函数空间，用于自定义函数
     * @return 函数元数据栈
     */
    Collection<Stack<FunctionMetadataIndex>> parse(String expression, FunctionContext extendFunctionContext);
}
