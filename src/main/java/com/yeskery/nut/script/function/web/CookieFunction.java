package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;

/**
 * cookie函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:18
 */
public class CookieFunction extends AbstractWebRequestFunction {
    /**
     * 构建cookie函数
     * @param request 请求对象
     */
    public CookieFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "cookie";
    }

    @Override
    public String apply(String content) throws Exception {
        return getCookie(content);
    }
}
