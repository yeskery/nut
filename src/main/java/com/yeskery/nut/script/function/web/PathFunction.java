package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;

/**
 * 请求路径函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:21
 */
public class PathFunction extends AbstractWebRequestFunction {

    /**
     * 构建请求路径函数
     * @param request 请求对象
     */
    public PathFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "path";
    }

    @Override
    public String apply(String content) throws Exception {
        return getPath();
    }
}
