package com.yeskery.nut.script.function.web;

import com.yeskery.nut.core.Request;

/**
 * WEB请求参数函数
 * @author sprout
 * @version 1.0
 * 2022-05-15 02:05
 */
public class ParameterFunction extends AbstractWebRequestFunction {
    /**
     * 构建WEB请求基础函数
     * @param request 请求对象
     */
    public ParameterFunction(Request request) {
        super(request);
    }

    @Override
    public String getName() {
        return "param";
    }

    @Override
    public String apply(String content) throws Exception {
        return getParameter(content);
    }
}
