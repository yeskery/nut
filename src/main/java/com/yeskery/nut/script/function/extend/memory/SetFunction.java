package com.yeskery.nut.script.function.extend.memory;

/**
 * Set函数，保存数据函数
 * @author sprout
 * @version 1.0
 * 2022-07-19 10:52
 */
public class SetFunction extends BaseMemoryPluginSupportFunction {
    @Override
    public String getName() {
        return "mset";
    }

    @Override
    protected String doParams2(String[] params, String content) {
        getStorage().set(params[0], params[1]);
        return SUCCESS;
    }

    @Override
    protected String doParams3(String[] params, String content) {
        getStorage().set(params[0], params[1]);
        return params[2];
    }
}
