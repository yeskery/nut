package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.BaseMultiParamFunction;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 日期函数
 * @author sprout
 * 2022-05-20 16:41
 */
public class DateFunction extends BaseMultiParamFunction {

    @Override
    public String getName() {
        return "date";
    }

    @Override
    protected String doNoParams(String param) {
        return String.valueOf(System.currentTimeMillis());
    }

    @Override
    protected String doParams1(String param) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        return simpleDateFormat.format(new Date());
    }

    @Override
    protected String doParams2(String[] params, String content) {
        long startTime = Long.parseLong(params[0]);
        long endTime = Long.parseLong(params[1]);
        long fakeTime = ThreadLocalRandom.current().nextLong(startTime, endTime);
        return String.valueOf(fakeTime);
    }

    @Override
    protected String doParams3(String[] params, String content) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        long startTime = Long.parseLong(params[1]);
        long endTime = Long.parseLong(params[2]);
        long fakeTime = ThreadLocalRandom.current().nextLong(startTime, endTime);
        return simpleDateFormat.format(new Date(fakeTime));
    }

    @Override
    protected String doDefault(String[] params, String content) {
        return doNoParams(content);
    }

    @Override
    protected String doException(String[] params, String content, Exception e) {
        return doNoParams(content);
    }
}
