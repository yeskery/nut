package com.yeskery.nut.script.function.common;

import com.yeskery.nut.script.function.BaseMultiParamFunction;
import com.yeskery.nut.util.JsonUtils;
import com.yeskery.nut.util.StringUtils;

import java.util.logging.Logger;

/**
 * 获取json的字段
 * @author sprout
 * 2022-05-17 10:55
 */
public class JsonObjectFunction extends BaseMultiParamFunction {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(JsonObjectFunction.class.getName());

    @Override
    public String getName() {
        return "jsonObject";
    }

    @Override
    protected String doParams2(String[] params, String content) {
        if (StringUtils.isEmpty(params[0])) {
            return content;
        }
        try {
            return JsonUtils.accessMember(params[0], params[1], String.class);
        } catch (Exception e) {
            logger.severe("eval() execute fail. eval code: [" + params[0] + "], source code: [" + content + "]");
            return content;
        }
    }
}
