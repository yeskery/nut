package com.yeskery.nut.script.function;

import com.yeskery.nut.script.function.common.*;
import com.yeskery.nut.script.function.extend.*;
import com.yeskery.nut.script.function.extend.memory.DeleteFunction;
import com.yeskery.nut.script.function.extend.memory.GetFunction;
import com.yeskery.nut.script.function.extend.memory.SetFunction;
import com.yeskery.nut.script.function.security.Md5Function;
import com.yeskery.nut.script.function.security.ShaFunction;

/**
 * 内部函数注册器
 * @author sprout
 * @version 1.0
 * 2022-05-15 23:18
 */
public class InternalFunctionRegister {

    /** 实例对象 */
    private static final InternalFunctionRegister INSTANCE = new InternalFunctionRegister();

    /**
     * 私有化构造方法
     */
    private InternalFunctionRegister() {
    }

    /**
     * 获取实例对象
     * @return 实例对象
     */
    public static InternalFunctionRegister getInstance() {
        return INSTANCE;
    }

    /**
     * 注册内部函数
     * @param functionContext 函数上下文
     */
    public void registerInternalFunction(FunctionContext functionContext) {
        // 注册通用函数
        functionContext.registerFunction(new FirstUpperFunction());
        functionContext.registerFunction(new LowerFunction());
        functionContext.registerFunction(new UpperFunction());
        functionContext.registerFunction(new RandomFunction());
        functionContext.registerFunction(new SubStringFunction());
        functionContext.registerFunction(new DateFunction());
        functionContext.registerFunction(new ValidFunction());
        functionContext.registerFunction(new JsonObjectFunction());
        functionContext.registerFunction(new JsonAppendFunction());

        // 注册安全函数
        registerInternalSecurityFunction(functionContext);

        // 注册扩展函数
        registerInternalExtendFunction(functionContext);
    }

    /**
     * 注册安全函数
     * @param functionContext 函数上下文
     */
    private void registerInternalSecurityFunction(FunctionContext functionContext) {
        functionContext.registerFunction(new Md5Function());
        functionContext.registerFunction(new ShaFunction());
    }

    /**
     * 注册扩展函数
     * @param functionContext 函数上下文
     */
    private void registerInternalExtendFunction(FunctionContext functionContext) {
        functionContext.registerFunction(new UuidFunction());
        functionContext.registerFunction(new IpFunction());
        functionContext.registerFunction(new NameGeneratorFunction());
        functionContext.registerFunction(new PhoneFunction());
        functionContext.registerFunction(new HttpFunction());
        functionContext.registerFunction(new JsFunction());

        // 注册内存存储函数
        functionContext.registerFunction(new GetFunction());
        functionContext.registerFunction(new SetFunction());
        functionContext.registerFunction(new DeleteFunction());
    }
}
