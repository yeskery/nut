package com.yeskery.nut.script.function;

/**
 * 默认的函数元数据对象
 * @author sprout
 * @version 1.0
 * 2022-05-15 15:25
 */
public class DefaultFunctionMetadata implements FunctionMetadata {

    /** 函数 */
    private Function function;

    /** 函数表达式 */
    private String expression;

    /** 函数执行结果 */
    private String result;

    /**
     * 构建默认的函数元数据对象
     * @param function 函数
     * @param expression 函数表达式
     */
    public DefaultFunctionMetadata(Function function, String expression) {
        this.function = function;
        this.expression = expression;
    }

    /**
     * 构建默认的函数元数据对象
     * @param function 函数
     */
    public DefaultFunctionMetadata(Function function) {
        this.function = function;
    }

    /**
     * 构建默认的函数元数据对象
     */
    public DefaultFunctionMetadata() {
    }

    @Override
    public Function getFunction() {
        return function;
    }

    /**
     * 设置函数
     * @param function 函数
     */
    @Override
    public void setFunction(Function function) {
        this.function = function;
    }

    @Override
    public String getExpression() {
        return expression;
    }

    /**
     * 设置函数表达式
     * @param expression 函数表达式
     */
    @Override
    public void setExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public String getExecuteResult() {
        return result;
    }

    @Override
    public void setExecuteResult(String result) {
        this.result = result;
    }
}
