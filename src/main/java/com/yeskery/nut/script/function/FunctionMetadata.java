package com.yeskery.nut.script.function;

/**
 * 函数元数据
 * @author sprout
 * @version 1.0
 * 2022-05-15 13:29
 */
public interface FunctionMetadata {

    /**
     * 获取函数对象
     * @return 函数对象
     */
    Function getFunction();

    /**
     * 设置函数
     * @param function 函数
     */
    void setFunction(Function function);

    /**
     * 获取函数表达式
     * @return 函数表达式
     */
    String getExpression();

    /**
     * 设置函数表达式
     * @param expression 函数表达式
     */
    void setExpression(String expression);

    /**
     * 获取函数执行结果
     * @return 函数执行结果
     */
    String getExecuteResult();

    /**
     * 设置函数执行结果
     * @param result 函数执行结果
     */
    void setExecuteResult(String result);
}
