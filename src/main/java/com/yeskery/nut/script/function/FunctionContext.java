package com.yeskery.nut.script.function;

import java.util.Collection;

/**
 * 函数上下文
 * @author sprout
 * @version 1.0
 * 2022-05-15 12:52
 */
public interface FunctionContext {

    /**
     * 注册函数
     * @param function 函数
     */
    void registerFunction(Function function);

    /**
     * 通过名称获取函数
     * @param name 函数名称
     * @return 函数
     */
    Function getFunction(String name);

    /**
     * 获取所有的函数
     * @return 所有的函数
     */
    Collection<Function> getAllFunctions();
}
