package com.yeskery.nut.script;

/**
 * 脚本接口
 * @author sprout
 * 2022-05-13 16:49
 */
public interface Script {

    /**
     * 获取脚本id
     * @return 脚本id
     */
    long getId();

    /**
     * 获取脚本名称
     * @return 脚本名称
     */
    String getName();
}
