package com.yeskery.nut.http;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.core.ResponseNutException;

/**
 * Http媒体类型不支持异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class HttpMediaTypeNotSupportException extends ResponseNutException {

    /**
     * 构建一个 {@link HttpMediaTypeNotSupportException}
     */
    public HttpMediaTypeNotSupportException() {
        super(ResponseCode.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     * 构建一个 {@link HttpMediaTypeNotSupportException}
     * @param message message
     */
    public HttpMediaTypeNotSupportException(String message) {
        super(message, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     * 构建一个 {@link HttpMediaTypeNotSupportException}
     * @param message message
     * @param cause cause
     */
    public HttpMediaTypeNotSupportException(String message, Throwable cause) {
        super(message, cause, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     * 构建一个 {@link HttpMediaTypeNotSupportException}
     * @param cause cause
     */
    public HttpMediaTypeNotSupportException(Throwable cause) {
        super(cause, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     * 构建一个 {@link HttpMediaTypeNotSupportException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public HttpMediaTypeNotSupportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
    }
}
