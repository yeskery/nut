package com.yeskery.nut.http;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.bind.HttpResponseBodyMetadata;
import com.yeskery.nut.bind.TextableHttpResponseBodyHandler;
import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.extend.responsive.DefaultResponsive;
import com.yeskery.nut.extend.responsive.Responsive;
import com.yeskery.nut.extend.responsive.XmlResponsive;
import com.yeskery.nut.extend.responsive.XmlResponsivePlugin;

/**
 * application/xml响应体处理器
 * @author YESKERY
 * 2024/9/23
 */
public class ApplicationXmlHttpResponseBodyHandler implements TextableHttpResponseBodyHandler {

    @Override
    public void handle(HttpResponseBodyMetadata metadata) {
        if (!metadata.getApplicationContext().getBean(NutApplication.class).getNutWebConfigure().getPluginManager().containPlugin(XmlResponsivePlugin.class)) {
            throw new NutException("Use HttpResponseBodyHandler[ApplicationXmlHttpResponseBodyHandler], But Unable Find Plugin[" + XmlResponsivePlugin.class + "]");
        }
        Responsive responsive = metadata.getBindContext().getObject(XmlResponsive.class);
        responsive.writeBody(metadata.getResult());
    }

    @Override
    public MediaType[] supportMediaTypes() {
        return new MediaType[]{MediaType.APPLICATION_XML};
    }

    @Override
    public int getOrder() {
        return TextableHttpResponseBodyHandler.super.getOrder() + 1;
    }

    @Override
    public String getExtractText(HttpResponseBodyMetadata metadata) {
        if (!metadata.getApplicationContext().getBean(NutApplication.class).getNutWebConfigure().getPluginManager().containPlugin(XmlResponsivePlugin.class)) {
            throw new NutException("Use HttpResponseBodyHandler[ApplicationXmlHttpResponseBodyHandler], But Unable Find Plugin[" + XmlResponsivePlugin.class + "]");
        }
        DefaultResponsive responsive = metadata.getBindContext().getObject(XmlResponsive.class);
        return responsive.getResponsiveConvert().convertTo(metadata.getResult());
    }
}
