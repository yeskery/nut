package com.yeskery.nut.http;

import com.yeskery.nut.core.MediaType;

import java.io.InputStream;

/**
 * 响应发射器Response接口
 * @author YESKERY
 * 2024/7/12
 */
public interface EmitterResponse {

    /**
     * 发送响应头数据
     * @param mediaType 响应类型
     */
    void sendHeaders(MediaType mediaType);

    /**
     * 发送响应头数据
     */
    default void sendHeaders() {
        sendHeaders(null);
    }

    /**
     * 发送响应体字节数据
     * @param bytes 响应体字节数据
     */
    void sendBodyBytes(byte[] bytes);

    /**
     * 发送响应体数据流
     * @param inputStream 响应体数据流
     */
    void sendBodyInputStream(InputStream inputStream);

    /**
     * 发送完成
     */
    void sendCompleted();
}
