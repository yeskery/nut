package com.yeskery.nut.http;

import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.Request;

/**
 * 基础的包装请求对象
 * @author YESKERY
 * 2024/8/29
 */
public abstract class AbstractWrapperRequest implements Request {

    /** 用于承载请求数据的 {@link BaseRequest} 对象 */
    private final BaseRequest baseRequest;

    /**
     * 构建一个 {@link AbstractWrapperRequest} 对象
     * @param baseRequest 基础数据对象 {@link BaseRequest} 对象
     */
    public AbstractWrapperRequest(BaseRequest baseRequest) {
        if (baseRequest == null) {
            throw new NutException("Request Must Not Be Null.");
        }
        this.baseRequest = baseRequest;
    }

    /**
     * 获取基础数据对象 {@link BaseRequest} 对象
     * @return 基础数据对象 {@link BaseRequest} 对象
     */
    public BaseRequest getBaseRequest() {
        return baseRequest;
    }

    /**
     * 是否超过请求体大小限制
     * @return 是否超过请求体大小限制
     */
    public boolean isOverflowRequestBodySize() {
        return baseRequest.isOverflowBodySize;
    }
}
