package com.yeskery.nut.http;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.core.ResponseNutException;

/**
 * Http请求头信息不匹配异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class HttpRequestHeaderNotMatchException extends ResponseNutException {

    /**
     * 构建一个 {@link HttpRequestHeaderNotMatchException}
     */
    public HttpRequestHeaderNotMatchException() {
        super(ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestHeaderNotMatchException}
     * @param message message
     */
    public HttpRequestHeaderNotMatchException(String message) {
        super(message, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestHeaderNotMatchException}
     * @param message message
     * @param cause cause
     */
    public HttpRequestHeaderNotMatchException(String message, Throwable cause) {
        super(message, cause, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestHeaderNotMatchException}
     * @param cause cause
     */
    public HttpRequestHeaderNotMatchException(Throwable cause) {
        super(cause, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestHeaderNotMatchException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public HttpRequestHeaderNotMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, ResponseCode.BAD_REQUEST);
    }
}
