package com.yeskery.nut.http;

import com.yeskery.nut.bind.HttpResponseBodyHandler;
import com.yeskery.nut.bind.HttpResponseBodyMetadata;
import com.yeskery.nut.bind.TextableHttpResponseBodyHandler;
import com.yeskery.nut.core.MediaType;
import com.yeskery.nut.core.Order;

/**
 * text/plain响应体处理器
 * @author YESKERY
 * 2024/10/11
 */
public class TextPlainHttpResponseBodyHandler implements TextableHttpResponseBodyHandler {

    @Override
    public void handle(HttpResponseBodyMetadata metadata) {
        metadata.getResponse().write(metadata.getResult().toString());
    }

    @Override
    public MediaType[] supportMediaTypes() {
        return new MediaType[]{MediaType.TEXT_PLAIN};
    }

    @Override
    public int getOrder() {
        return Order.MIN;
    }

    @Override
    public String getExtractText(HttpResponseBodyMetadata metadata) {
        return metadata.getResult().toString();
    }
}
