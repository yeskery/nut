package com.yeskery.nut.http;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.core.ResponseNutException;

/**
 * Http请求参数不匹配异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class HttpRequestParamNotMatchException extends ResponseNutException {

    /**
     * 构建一个 {@link HttpRequestParamNotMatchException}
     */
    public HttpRequestParamNotMatchException() {
        super(ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestParamNotMatchException}
     * @param message message
     */
    public HttpRequestParamNotMatchException(String message) {
        super(message, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestParamNotMatchException}
     * @param message message
     * @param cause cause
     */
    public HttpRequestParamNotMatchException(String message, Throwable cause) {
        super(message, cause, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestParamNotMatchException}
     * @param cause cause
     */
    public HttpRequestParamNotMatchException(Throwable cause) {
        super(cause, ResponseCode.BAD_REQUEST);
    }

    /**
     * 构建一个 {@link HttpRequestParamNotMatchException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public HttpRequestParamNotMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, ResponseCode.BAD_REQUEST);
    }
}
