package com.yeskery.nut.http;

import com.yeskery.nut.core.Session;

import java.util.Map;

/**
 * 无状态 {@link com.yeskery.nut.core.Session} 的实现
 * @author sprout
 * 2022-08-24 14:33
 * @version 1.0
 *
 * @see com.yeskery.nut.core.Session
 */
public class StateLessSession implements Session {
    @Override
    public void addAttribute(String name, Object object) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public void removeAttribute(String name) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public String getSessionId() {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public Object getAttribute(String name) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public Map<String, Object> getAttributes() {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }
}
