package com.yeskery.nut.http;

import com.yeskery.nut.core.BaseResponseEmitterHandler;
import com.yeskery.nut.core.ResponseBodyEmitter;

import java.io.IOException;
import java.io.InputStream;

/**
 * 默认的响应发射器处理类
 * @author YESKERY
 * 2024/7/12
 */
public class DefaultResponseEmitterHandler extends BaseResponseEmitterHandler {

    /** 基础响应对象 */
    private final BaseResponse baseResponse;

    /**
     * 构建默认的响应发射器处理类
     * @param responseBodyEmitter 响应发射器
     * @param baseResponse 基础响应对象
     */
    public DefaultResponseEmitterHandler(ResponseBodyEmitter responseBodyEmitter, BaseResponse baseResponse) {
        super(responseBodyEmitter);
        this.baseResponse = baseResponse;
    }

    @Override
    protected void doSendHeaders() throws IOException {
        baseResponse.sendHeaders();
    }

    @Override
    protected void doCompleted() throws IOException {
        baseResponse.sendCompleted();
    }

    @Override
    protected void doSend(byte[] bytes) throws IOException {
        baseResponse.sendBodyBytes(bytes);
    }

    @Override
    protected void doSend(InputStream inputStream) throws IOException {
        baseResponse.sendBodyInputStream(inputStream);
    }
}
