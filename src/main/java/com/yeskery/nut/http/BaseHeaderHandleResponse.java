package com.yeskery.nut.http;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.util.IOUtils;
import com.yeskery.nut.util.StringUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * 请求头处理的基础响应对象
 * @author sprout
 * @version 1.0
 * 2022-08-05 01:44
 */
public abstract class BaseHeaderHandleResponse extends BaseResponse {
    /**
     * 构建一个请求头处理的基础响应对象
     * @param nutApplication Nut应用对象
     */
    public BaseHeaderHandleResponse(NutApplication nutApplication) {
        super(nutApplication);
    }

    @Override
    protected void writeHeaderBytes(byte[] headers) {
        doWriteHeaderResponse(headers);
        try {
            IOUtils.writeByteArray(getOutputStream(), new byte[0]);
        } catch (Exception e) {
            throw new NutException("Response OutputStream IO Exception.", e);
        }
    }

    @Override
    protected void writeBodyBytes(byte[] bytes) {
        try {
            IOUtils.writeByteArray(getOutputStream(), bytes);
        } catch (Exception e) {
            throw new NutException("Response OutputStream IO Exception.", e);
        }
    }

    @Override
    protected void writeBodyInputStream(InputStream inputStream) {
        try {
            IOUtils.transferInputStream(inputStream, getOutputStream());
        } catch (Exception e) {
            throw new NutException("Response IO Exception", e);
        }
    }

    @Override
    protected void writeBytes(byte[] headers, byte[] bytes) {
        doWriteHeaderResponse(headers);
        try {
            IOUtils.writeByteArray(getOutputStream(), bytes);
        } catch (Exception e) {
            throw new NutException("Response OutputStream IO Exception.", e);
        }
        output = true;
    }

    @Override
    protected void writeInputStream(byte[] headers, InputStream inputStream) {
        doWriteHeaderResponse(headers);
        try {
            IOUtils.transferInputStream(inputStream, getOutputStream());
        } catch (Exception e) {
            throw new NutException("Response IO Exception", e);
        }
        output = true;
    }

    /**
     * 添加请求头
     * @param name 请求头名称
     * @param value 请求头值
     */
    @Override
    public abstract void addHeader(String name, String value);

    /**
     * 获取响应流对象
     * @return 响应流对象
     * @throws Exception 获取响应流对象异常
     */
    protected abstract OutputStream getOutputStream() throws Exception;

    /**
     * 输出响应头
     * @param headers 请求头数组
     */
    private void doWriteHeaderResponse(byte[] headers) {
        String headerValue = new String(headers, StandardCharsets.UTF_8);
        String[] headerValues = headerValue.split("\r\n");

        for (int i = 1; i < headerValues.length; i++) {
            String header = headerValues[i];
            if (!StringUtils.isEmpty(header)) {
                String[] splits = header.split(":");
                if (splits.length == 2) {
                    addHeader(splits[0], splits[1]);
                }
            }
        }
    }
}
