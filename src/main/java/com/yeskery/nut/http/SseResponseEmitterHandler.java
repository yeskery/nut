package com.yeskery.nut.http;

import com.yeskery.nut.core.HttpHeader;

/**
 * sse响应发射器处理类
 * @author YESKERY
 * 2024/7/12
 */
public class SseResponseEmitterHandler extends DefaultResponseEmitterHandler {
    /**
     * 构建默认的响应发射器处理类
     * @param sseEmitter sse响应发射器
     * @param baseResponse        基础响应对象
     */
    public SseResponseEmitterHandler(SseEmitter sseEmitter, BaseResponse baseResponse) {
        super(sseEmitter, baseResponse);
        baseResponse.addHeader(HttpHeader.CONTENT_TYPE, "text/event-stream");
        baseResponse.addHeader("Cache-Control", "no-cache");
    }
}
