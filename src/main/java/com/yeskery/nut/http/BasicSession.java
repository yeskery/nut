package com.yeskery.nut.http;

import com.yeskery.nut.core.BasicContextImpl;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.Session;

/**
 * 该类是 {@link com.yeskery.nut.core.Session} 的默认实现
 * @author sprout
 * 2019-03-16 14:53
 * @version 1.0
 *
 * @see com.yeskery.nut.core.Session
 */
public class BasicSession extends BasicContextImpl<Object> implements Session, Cloneable {

	/** Session 的唯一标识 */
	private final String sessionId;

	/**
	 * 构建一个 {@link BasicSession}
	 * @param sessionId Session ID
	 */
	public BasicSession(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public BasicSession clone() {
		try {
			return (BasicSession) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new NutException(e);
		}
	}
}
