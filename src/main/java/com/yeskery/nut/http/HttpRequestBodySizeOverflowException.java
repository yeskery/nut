package com.yeskery.nut.http;

import com.yeskery.nut.core.NutException;

/**
 * Http请求体超出限制异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class HttpRequestBodySizeOverflowException extends NutException {

    /**
     * 构建一个 {@link HttpRequestBodySizeOverflowException}
     */
    public HttpRequestBodySizeOverflowException() {
    }

    /**
     * 构建一个 {@link HttpRequestBodySizeOverflowException}
     * @param message message
     */
    public HttpRequestBodySizeOverflowException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link HttpRequestBodySizeOverflowException}
     * @param message message
     * @param cause cause
     */
    public HttpRequestBodySizeOverflowException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link HttpRequestBodySizeOverflowException}
     * @param cause cause
     */
    public HttpRequestBodySizeOverflowException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link HttpRequestBodySizeOverflowException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public HttpRequestBodySizeOverflowException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
