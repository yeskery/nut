package com.yeskery.nut.http.netty;

import com.yeskery.nut.core.Method;
import com.yeskery.nut.core.ServerContext;
import com.yeskery.nut.core.ServerRequestConfiguration;
import com.yeskery.nut.core.SessionManager;
import com.yeskery.nut.http.BaseRequest;
import io.netty.handler.codec.http.FullHttpRequest;

import java.net.InetSocketAddress;
import java.util.*;

/**
 * Netty的Http 请求对象
 * @author sprout
 * 2022-06-16 11:38
 */
public class NettyRequest extends BaseRequest {

    /** 远程InetSocketAddress对象 */
    private final InetSocketAddress remoteSocketAddress;

    /** 本地InetSocketAddress对象 */
    private final InetSocketAddress localSocketAddress;

    /** Netty请求对象 */
    private final FullHttpRequest request;

    /**
     * 构建一个 {@link NettyRequest}
     * @param remoteSocketAddress 远程socket地址
     * @param localSocketAddress 本地socket地址
     * @param serverContext  服务上下文
     * @param sessionManager Session 管理器
     * @param serverRequestConfiguration 服务请求配置对象
     * @param request Netty请求对象
     */
    public NettyRequest(InetSocketAddress remoteSocketAddress, InetSocketAddress localSocketAddress,
                        ServerContext serverContext, SessionManager sessionManager,
                        ServerRequestConfiguration serverRequestConfiguration, FullHttpRequest request) {
        super(serverContext, sessionManager, serverRequestConfiguration);
        this.remoteSocketAddress = remoteSocketAddress;
        this.localSocketAddress = localSocketAddress;
        this.request = request;

        method = Method.valueOf(request.method().name());
        path = request.uri();
        originalPath = path;
        protocol = request.protocolVersion().text();
        initUriParameters();

        Iterator<Map.Entry<String, String>> iterator = request.headers().iteratorAsString();
        Map<String, List<String>> headerMap = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            headerMap.put(entry.getKey(), Collections.singletonList(entry.getValue()));
        }

        headers = headerMap;
        if (checkRequestBodySizeOverflow(request.content().readableBytes())) {
            body = new byte[0];
        } else {
            body = new byte[request.content().readableBytes()];
            request.content().readBytes(body);
        }
    }

    @Override
    protected byte[] readRequestBytes() {
        return new byte[0];
    }

    @Override
    public String getRemoteAddress() {
        return remoteSocketAddress.getAddress().getHostAddress();
    }

    @Override
    public String getRemoteHost() {
        return remoteSocketAddress.getHostName();
    }

    @Override
    public int getRemotePort() {
        return remoteSocketAddress.getPort();
    }

    @Override
    public String getLocalAddress() {
        return localSocketAddress.getAddress().getHostAddress();
    }

    @Override
    public String getLocalHost() {
        return localSocketAddress.getHostName();
    }

    @Override
    public int getLocalPort() {
        return localSocketAddress.getPort();
    }

    /**
     * 获取Netty请求对象
     * @return Netty请求对象
     */
    public FullHttpRequest getFullHttpRequest() {
        return request;
    }
}
