package com.yeskery.nut.http.jakarta;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.http.BaseHeaderHandleResponse;
import jakarta.servlet.http.HttpServletResponse;

import java.io.OutputStream;

/**
 * Jakarta的Http 响应对象
 * @author sprout
 * @version 1.0
 * 2022-08-05 01:00
 */
public class JakartaResponse extends BaseHeaderHandleResponse {

    /** HttpServlet响应对象 */
    private final HttpServletResponse response;

    /**
     * 构建一个 {@link JakartaResponse} 对象
     * @param nutApplication Nut应用对象
     * @param response HttpServlet响应对象
     */
    public JakartaResponse(NutApplication nutApplication, HttpServletResponse response) {
        super(nutApplication);
        this.response = response;
    }

    @Override
    public void addHeader(String name, String value) {
        response.addHeader(name, value);
    }

    @Override
    protected OutputStream getOutputStream() throws Exception {
        return response.getOutputStream();
    }

    @Override
    public void setCode(int code) {
        super.setCode(code);
        response.setStatus(code);
    }
}
