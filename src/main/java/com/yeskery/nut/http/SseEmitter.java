package com.yeskery.nut.http;

import com.yeskery.nut.core.ResponseBodyEmitter;

/**
 * SSE发射器
 * @author YESKERY
 * 2024/7/12
 */
public class SseEmitter extends ResponseBodyEmitter {

    /**
     * 构建sse事件构造器
     * @return sse事件构造器
     */
    public static SseEventBuilder event() {
        return new SseEventBuilderImpl();
    }

    /**
     * 发送sse事件
     * @param event sse事件构造器
     */
    public void send(SseEventBuilder event) {
        send(event.getEventString());
    }

    /**
     * sse事件构造器接口
     * @author YESKERY
     * 2024/7/12
     */
    public interface SseEventBuilder {

        /**
         * 添加sse comment行
         * @param comment comment内容
         * @return sse事件构造器
         */
        SseEventBuilder comment(String comment);

        /**
         * 添加sse event行
         * @param eventName event名称
         * @return sse事件构造器
         */
        SseEventBuilder name(String eventName);

        /**
         * 添加sse id行
         * @param id id
         * @return sse事件构造器
         */
        SseEventBuilder id(String id);

        /**
         * 添加sse retry行
         * @param reconnectTimeMillis 超时事件(毫秒)
         * @return sse事件构造器
         */
        SseEventBuilder reconnectTime(long reconnectTimeMillis);

        /**
         * 添加sse data行
         * @param data 数据内容
         * @return sse事件构造器
         */
        SseEventBuilder data(String data);

        /**
         * 获取事件字符串
         * @return 事件字符串
         */
        String getEventString();
    }

    /**
     * 默认的sse事件构造器
     */
    private static class SseEventBuilderImpl implements SseEventBuilder {

        /** 字符串构造器 */
        private StringBuilder sb;

        @Override
        public SseEventBuilder comment(String comment) {
            append(":").append(comment != null ? comment : "").append("\n");
            return this;
        }

        @Override
        public SseEventBuilder name(String name) {
            append("event:").append(name != null ? name : "").append("\n");
            return this;
        }

        @Override
        public SseEventBuilder id(String id) {
            append("id:").append(id != null ? id : "").append("\n");
            return this;
        }

        @Override
        public SseEventBuilder reconnectTime(long reconnectTimeMillis) {
            append("retry:").append(String.valueOf(reconnectTimeMillis)).append("\n");
            return this;
        }

        @Override
        public SseEventBuilder data(String data) {
            append("data:").append(data).append("\n");
            return this;
        }

        @Override
        public String getEventString() {
            append("\n\n");
            return sb.toString();
        }

        /**
         * 追加文本
         * @param text 文本
         * @return sse事件构造器
         */
        private SseEventBuilderImpl append(String text) {
            if (this.sb == null) {
                this.sb = new StringBuilder();
            }
            this.sb.append(text);
            return this;
        }
    }
}
