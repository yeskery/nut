package com.yeskery.nut.http;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.core.ResponseNutException;

/**
 * Http请求类型不支持异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class HttpNotAcceptableException extends ResponseNutException {

    /**
     * 构建一个 {@link HttpNotAcceptableException}
     */
    public HttpNotAcceptableException() {
        super(ResponseCode.NOT_ACCEPTABLE);
    }

    /**
     * 构建一个 {@link HttpNotAcceptableException}
     * @param message message
     */
    public HttpNotAcceptableException(String message) {
        super(message, ResponseCode.NOT_ACCEPTABLE);
    }

    /**
     * 构建一个 {@link HttpNotAcceptableException}
     * @param message message
     * @param cause cause
     */
    public HttpNotAcceptableException(String message, Throwable cause) {
        super(message, cause, ResponseCode.NOT_ACCEPTABLE);
    }

    /**
     * 构建一个 {@link HttpNotAcceptableException}
     * @param cause cause
     */
    public HttpNotAcceptableException(Throwable cause) {
        super(cause, ResponseCode.NOT_ACCEPTABLE);
    }

    /**
     * 构建一个 {@link HttpNotAcceptableException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public HttpNotAcceptableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, ResponseCode.NOT_ACCEPTABLE);
    }
}
