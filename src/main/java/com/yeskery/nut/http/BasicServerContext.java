package com.yeskery.nut.http;

import com.yeskery.nut.core.BasicContextImpl;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.ServerContext;

/**
 * 该类是 {@link com.yeskery.nut.core.ServerContext} 的默认实现
 * @author sprout
 * 2022-05-31 11:10
 */
public class BasicServerContext extends BasicContextImpl<Object> implements ServerContext, Cloneable {

    @Override
    public BasicServerContext clone() {
        try {
            return (BasicServerContext) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new NutException(e);
        }
    }
}
