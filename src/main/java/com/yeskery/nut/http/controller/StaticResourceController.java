package com.yeskery.nut.http.controller;

import com.yeskery.nut.core.*;

/**
 * 静态资源 Controller
 * @author sprout
 * 2022-05-12 13:07
 */
public interface StaticResourceController extends Controller {

    /**
     * 获取静态资源路径
     * @return 静态资源路径数组
     */
    String[] getStaticResourceDirectories();

    /**
     * 设置静态资源路径
     * @param dirs 静态资源路径
     */
    void setStaticResourceDirectories(String[] dirs);

    /**
     * 获取静态资源请求路径
     * @return 静态资源请求路径
     */
    Path getStaticResourceRequestPath();

    /**
     * 设置静态资源请求路径
     * @param path 静态资源请求路径
     */
    void setStaticResourceRequestPath(Path path);

    /**
     * 设置目录预览状态
     * @param directoryPreviewStatus 目录预览状态
     */
    void setDirectoryPreviewStatus(boolean directoryPreviewStatus);
}
