package com.yeskery.nut.http.controller;

import com.yeskery.nut.core.Version;

/**
 * Nut Server 默认的响应 HTML 代码
 * @author sprout
 * 2019-03-16 11:02
 * @version 1.0
 */
final class DefaultContentHelper {

	/**
	 * 获取默认的响应 HTML 代码
	 * @param content 主要内容，用于在 <code>title</code> 和 <code>body</code> 中显示
	 * @return 默认的响应 HTML 代码
	 */
	static String getDefaultHtml(String content) {
		return getDefaultTopHtml() + content + getDefaultMiddleHtml() + content + getDefaultBottomHtml();
	}

	/**
	 * 获取默认响应的顶部 HTML 代码
	 * @return 默认响应的顶部 HTML 代码
	 */
	private static String getDefaultTopHtml() {
		return "<!doctype html><html><head><meta charset=\"utf-8\" /><title>";
	}

	/**
	 * 获取默认响应的中部 HTML 代码
	 * @return 默认响应的中部 HTML 代码
	 */
	private static String getDefaultMiddleHtml() {
		return "</title><style type=\"text/css\">.center{text-align: center;}.small-size{font-size: 13px;}</style></head><body><h1 class=\"center\">Sprout Nut Server</h1><h3 class=\"center\">";
	}

	/**
	 * 获取默认响应的底部 HTML 代码
	 * @return 默认响应的底部 HTML 代码
	 */
	private static String getDefaultBottomHtml() {
		return "</h3><p class=\"center small-size\">Power By Sprout Nut. Version: " + Version.VERSION
				+ "</p><p class=\"center small-size\">Build Timestamp: " + Version.TIMESTAMP
				+ "</p></body></html>";
	}
}
