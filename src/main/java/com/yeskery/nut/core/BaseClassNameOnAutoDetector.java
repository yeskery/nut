package com.yeskery.nut.core;

import com.yeskery.nut.util.ClassUtils;

import java.util.Arrays;

/**
 * 类名称是否存在检测器
 * @author sprout
 * @version 1.0
 * 2022-09-14 14:55
 */
public abstract class BaseClassNameOnAutoDetector extends BaseMultiConditionAutoDetector<String> {
    @Override
    public void autoDetect() {
        String[] multiResources = getMultiResources();
        boolean result;
        switch (getMultiCondition()) {
            case ANY: result = Arrays.stream(multiResources).anyMatch(ClassUtils::isExistTargetClass); break;
            case ALL: result = Arrays.stream(multiResources).allMatch(ClassUtils::isExistTargetClass); break;
            case NONE: result = Arrays.stream(multiResources).noneMatch(ClassUtils::isExistTargetClass); break;
            default: result = false;
        }
        if (result) {
            completeDetect();
        }
    }

    /**
     * 完成探测的回调方法
     */
    protected abstract void completeDetect();
}
