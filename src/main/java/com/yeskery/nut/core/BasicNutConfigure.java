package com.yeskery.nut.core;

import com.yeskery.nut.application.SecureKeyType;
import com.yeskery.nut.application.ServerType;

import java.nio.charset.Charset;

/**
 * Nut 基础配置信息接口
 * @author sprout
 * 2022-07-27 16:19
 * @version 1.0
 */
public interface BasicNutConfigure {

    /**
     * 获取服务启动类型
     * @return 服务启动类型
     */
    default ServerType getServerType() {
        return ServerType.AUTO;
    }

    /**
     * 获取服务运行环境
     * @return 服务运行环境
     */
    String getEnv();

    /**
     * 获取服务启动的端口号
     * @return 服务启动的端口号
     */
    Integer getServerPort();

    /**
     * 获取请求最大限制字节数
     * @return 请求最大限制字节数
     */
    Long getMaxRequestSize();

    /**
     * 获取服务是否以SSL启动
     * @return 是否以SSL启动
     */
    Boolean isServerSecurity();

    /**
     * 获取安全证书类型
     * @return 安全证书类型
     */
    SecureKeyType getSecureKeyType();

    /**
     * 获取jks证书库路径
     * @return jks证书库路径
     */
    String getJksPath();

    /**
     * 获取jks证书库密码
     * @return jks证书库密码
     */
    String getJksPassword();

    /**
     * 获取pem证书公钥路径
     * @return pem证书公钥路径
     */
    String getPemPublicKeyPath();

    /**
     * 获取pem证书私钥路径
     * @return pem证书私钥路径
     */
    String getPemPrivateKeyPath();

    /**
     * 获取静态资源目录
     * @return 静态资源目录
     */
    String[] getStaticResourceDirectory();

    /**
     * 获取静态资源目录
     * @return 静态资源目录
     */
    String[] getStaticResourcePaths();

    /**
     * 静态资源目录预览是否开启
     * @return 静态资源目录预览是否开启
     */
    boolean isEnableStaticDirectoryPreview();

    /**
     * 是否开启脚本
     * @return 是否开启脚本
     */
    boolean isEnableScript();

    /**
     * 获取脚本路径
     * @return 脚本路径
     */
    String[] getScriptPath();

    /**
     * 获取脚本字符编码
     * @return 脚本字符编码
     */
    Charset getScriptEncoding();

    /**
     * 是否开启脚本文件监听
     * @return 是否开启脚本文件监听
     */
    boolean isEnableScriptWatch();

    /**
     * 获取日志处理器
     * @return 日志处理器
     */
    String getLogHandlers();

    /**
     * 获取日志格式化器
     * @return 日志格式化器
     */
    String getLogFormatter();

    /**
     * 获取日志等级
     * @return 日志等级
     */
    String getLogLevel();

    /**
     * 获取日志存储目录
     * @return 日志存储目录
     */
    String getLogDir();
}
