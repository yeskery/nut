package com.yeskery.nut.core;

import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * 资源对象接口
 * @author YESKERY
 * 2024/8/13
 */
public interface Resource extends InputStreamResource {

    /**
     * 获取资源名称
     * @return 资源名称
     */
    String getName();

    /**
     * 获取资源的长度
     * @return 资源的长度
     */
    Long length();

    /**
     * 获取可重复读字节channel
     * @return 可重复读字节channel
     */
    default ReadableByteChannel readableChannel() {
        return Channels.newChannel(getInputStream());
    }
}
