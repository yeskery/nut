package com.yeskery.nut.core;

/**
 * 条件自动探测接口
 * @author sprout
 * @version 1.0
 * 2022-09-14 14:51
 */
public abstract class BaseMultiConditionAutoDetector<T> implements AutoDetector {

    /**
     * 获取多条件类型
     * @return 多条件类型
     */
    protected abstract MultiCondition getMultiCondition();

    /**
     * 获取多条件资源
     * @return 多条件资源
     */
    protected abstract T[] getMultiResources();
}
