package com.yeskery.nut.core;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 模型对象
 * @author sprout
 * 2022-06-16 21:18
 */
public class Model {

    /** 默认模型对象 */
    public static final Model DEFAULT_MODEL = new Model();

    /**
     * 添加属性
     * @param attributeName 属性名称
     * @param attributeValue 属性值
     */
    public void addAttribute(String attributeName, Object attributeValue) {
        RequestApplicationContext.putResource(attributeName, attributeValue);
    }

    /**
     * 添加多个属性
     * @param attributes 属性map
     */
    public void addAllAttributes(Map<String, ?> attributes) {
        for (Map.Entry<String, ?> entry : attributes.entrySet()) {
            addAttribute(entry.getKey(), entry.getValue());
        }
    }

    /**
     * 添加属性
     * @param attributeValue 属性值
     */
    public void addAttribute(Object attributeValue) {
        RequestApplicationContext.putResource(attributeValue);
    }

    /**
     * 添加多个属性
     * @param attributeValues 属性map
     */
    public void addAllAttributes(Collection<?> attributeValues) {
        for (Object attributeValue : attributeValues) {
            RequestApplicationContext.putResource(attributeValue);
        }
    }

    /**
     * 获取属性对象
     * @param name 属性名称
     * @return 属性对象
     */
    public Object getResource(String name) {
        return RequestApplicationContext.getResource(name);
    }

    /**
     * 获取属性对象
     * @param name 属性名称
     * @param clazz 属性类型
     * @param <T> 属性泛型
     * @return 属性对象
     */
    public <T> T getResource(String name, Class<T> clazz) {
        return RequestApplicationContext.getResource(name, clazz);
    }

    /**
     * 获取属性对象
     * @param clazz 属性类型
     * @param <T> 属性泛型
     * @return 属性对象
     */
    public <T> T getResource(Class<T> clazz) {
        return RequestApplicationContext.getResource(clazz);
    }

    /**
     * 获取属性对象
     * @param clazz 属性类型
     * @param <T> 属性泛型
     * @return 属性对象
     */
    public <T> List<T> getResources(Class<T> clazz) {
        return RequestApplicationContext.getResources(clazz);
    }

    /**
     * 获取资源属性map
     * @return 资源属性map
     */
    public Map<String, Object> toMap() {
        return RequestApplicationContext.getMap();
    }
}
