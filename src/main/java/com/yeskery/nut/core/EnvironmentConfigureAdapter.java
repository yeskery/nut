package com.yeskery.nut.core;

import com.yeskery.nut.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 环境参数配置文件配置适配器
 * @author sprout
 * @version 1.0
 * 2022-09-08 13:05
 */
public class EnvironmentConfigureAdapter extends BaseNutConfigure {

    /** 配置文件key */
    private static final String ENVIRONMENT_KEY_NAME = "nut.profile.active";

    /** 环境对象 */
    private final DefaultEnvironment environment = new DefaultEnvironment(false);

    @Override
    protected Map<String, String> getConfiguration() {
        String env = environment.getMainEnvProperties(false).getProperty(ENVIRONMENT_KEY_NAME);
        Set<String> configurationKeySet = getConfigurationKeySet();
        Map<String, String> configuration = new HashMap<>();
        if (!StringUtils.isEmpty(env)) {
            configuration.put(PREFIX + CONFIGURE_ENV_NAME, env);
        }
        environment.getMainEnvProperties(false).entrySet().stream()
                .filter(r -> configurationKeySet.contains(r.getKey().toString()))
                .forEach(r -> configuration.put(r.getKey().toString(), r.getValue().toString()));
        return configuration;
    }
}
