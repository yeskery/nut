package com.yeskery.nut.core;

/**
 * <p>Nut应用处理用来Web请求的核心接口，其中的{@link #dispatch(Request, Response)}是用来处理Web请求的核心方法，所有服务类型的应用的
 * {@link Request}请求，最终都会进入到{@link #dispatch(Request, Response)}方法中。</p>
 *
 * @author sprout
 * 2019-03-16 10:17
 * @version 1.0
 *
 * @see Controller
 * @see Request
 * @see Response
 * @see NutDispatcher
 */
public interface Dispatcher {

	/**
	 * 将请求转发到指定的 {@link Controller} 中
	 * @param request request 对象
	 * @param response response 对象
	 * @throws Exception 转发异常
	 */
	void dispatch(Request request, Response response) throws Exception;
}
