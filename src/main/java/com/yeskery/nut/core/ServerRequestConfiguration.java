package com.yeskery.nut.core;

/**
 * 服务请求配置接口
 * @author YESKERY
 * 2024/8/29
 */
public interface ServerRequestConfiguration {

    /**
     * 获取请求最大限制字节数
     * @return 请求最大限制字节数
     */
    Long getMaxRequestSize();
}
