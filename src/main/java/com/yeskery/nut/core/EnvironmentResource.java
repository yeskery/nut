package com.yeskery.nut.core;

import java.io.Reader;

/**
 * 环境资源对象
 * @author sprout
 * @version 1.0
 * 2022-07-09 11:23
 */
public interface EnvironmentResource extends InputStreamResource {

    /**
     * 获取环境资源的读取流
     * @return 环境资源的读取流
     */
    Reader getReader();
}
