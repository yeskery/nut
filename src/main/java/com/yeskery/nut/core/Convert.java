package com.yeskery.nut.core;

/**
 * 转换接口
 * @author sprout
 * 2022-06-08 16:57
 */
public interface Convert<T, K> {

    /**
     * 从 {@link T} 转换成 {@link K}
     * @param data {@link T} 对象
     * @return {@link K} 对象
     */
    K convertTo(T data);

    /**
     * 从 {@link K} 转换成 {@link T}
     * @param data {@link K} 对象
     * @return {@link T} 对象
     */
    T convertFrom(K data);
}
