package com.yeskery.nut.core;

import java.io.File;
import java.net.URI;
import java.net.URL;

/**
 * 文件资源对象接口
 * @author YESKERY
 * 2024/8/13
 */
public interface FileResource extends Resource {

    /**
     * 文件是否存在
     * @return 文件是否存在
     */
    boolean exists();

    /**
     * 获取文件URI
     * @return 文件URI
     */
    URI getURI();

    /**
     * 获取文件URL
     * @return 文件URL
     */
    URL getURL();

    /**
     * 获取文件对象
     * @return 文件对象
     */
    File getFile();

    /**
     * 文件最后修改时间
     * @return 文件最后修改时间
     */
    long lastModified();

    /**
     * 文件是否可读
     * @return 文件是否可读
     */
    boolean isReadable();

    /**
     * 文件是否打开
     * @return 文件是否打开
     */
    boolean isOpen();

    /**
     * 文件是否为文件
     * @return 文件是否为文件
     */
    boolean isFile();
}
