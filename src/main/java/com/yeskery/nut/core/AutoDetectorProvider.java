package com.yeskery.nut.core;

/**
 * 自动探测器提供器
 * @author sprout
 * @version 1.0
 * 2022-09-14 15:06
 */
public interface AutoDetectorProvider {

    /**
     * 获取自动探测器数组
     * @return 自动探测器数组
     */
    AutoDetector[] getAutoDetectors();
}
