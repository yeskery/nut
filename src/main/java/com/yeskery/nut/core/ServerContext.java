package com.yeskery.nut.core;

/**
 * 服务上下文
 * @author sprout
 * 2022-05-31 11:08
 */
public interface ServerContext extends BasicContext<Object> {
}
