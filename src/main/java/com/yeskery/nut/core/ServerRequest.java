package com.yeskery.nut.core;

/**
 * <p>服务请求对象，用于获取请求的网络等相关信息</p>
 * @author YESKERY
 * 2023/11/15
 */
public interface ServerRequest {

    /**
     * 获取远程客户端ip地址
     * @return 远程客户端ip地址
     */
    String getRemoteAddress();

    /**
     * 获取远程客户端主机名
     * @return 远程客户端主机名
     */
    String getRemoteHost();

    /**
     * 获取远程客户端端口号
     * @return 远程客户端端口号
     */
    int getRemotePort();

    /**
     * 获取本地客户端ip地址
     * @return 本地客户端ip地址
     */
    String getLocalAddress();

    /**
     * 获取本地客户端主机名
     * @return 本地客户端主机名
     */
    String getLocalHost();

    /**
     * 获取本地客户端端口号
     * @return 本地客户端端口号
     */
    int getLocalPort();

    /**
     * 获取服务请求配置接口
     * @return 服务请求配置接口
     */
    ServerRequestConfiguration getServerRequestConfiguration();
}
