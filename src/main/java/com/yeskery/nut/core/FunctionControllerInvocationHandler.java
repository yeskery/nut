package com.yeskery.nut.core;

import java.util.Set;

/**
 * 基于请求处理函数接口的controller动态代理处理对象
 * @author sprout
 * 2022-05-31 14:09
 */
public class FunctionControllerInvocationHandler extends AbstractControllerInvocationHandler {

    /** 请求处理函数接口 */
    private final RequestHandler requestHandler;

    /** HTTP请求方法 */
    private final Set<Method> methods;

    /**
     * 构建controller动态代理处理对象
     * @param requestHandler 请求处理器
     * @param methods 支持的所有请求方法
     * @param target 目标对象
     */
    public FunctionControllerInvocationHandler(RequestHandler requestHandler, Set<Method> methods, Object target) {
        super(target);
        this.requestHandler = requestHandler;
        this.methods = methods;
    }

    @Override
    protected void doRequest(Request request, Response response, Execution execution) {
        requestHandler.handle(request, response, execution);
    }

    @Override
    protected boolean isTargetRequest(Request request, Method method) {
        return methods.contains(com.yeskery.nut.core.Method.ALL) || methods.contains(method);
    }
}
