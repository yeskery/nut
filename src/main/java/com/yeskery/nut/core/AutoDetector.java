package com.yeskery.nut.core;

import com.yeskery.nut.application.ApplicationType;

/**
 * 自动探测接口
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:29
 */
public interface AutoDetector {

    /**
     * 自动探测方法
     */
    void autoDetect();

    /**
     * 指定的应用类型是否支持
     * @param applicationType 应用类型
     * @return 是否支持
     */
    boolean isSupport(ApplicationType applicationType);
}
