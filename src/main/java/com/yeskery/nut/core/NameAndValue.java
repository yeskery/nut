package com.yeskery.nut.core;

/**
 * 字符串键值对类
 * @author sprout
 * 2019-03-13 16:37
 * @version 1.0
 */
public class NameAndValue extends KeyAndValue<String, String> {
	/**
	 * 构建一个字符串键值对类
	 * @param key   键
	 * @param value 值
	 */
	public NameAndValue(String key, String value) {
		super(key, value);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
