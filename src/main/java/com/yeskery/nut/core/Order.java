package com.yeskery.nut.core;

/**
 * 顺序接口，顺序值越小，优先级越高
 * @author sprout
 * 2022-05-12 21:04
 */
public interface Order {

    /** 最大优先级 */
    int MAX = Integer.MIN_VALUE;

    /** 最小优先级 */
    int MIN = Integer.MAX_VALUE;

    /**
     * 获取顺序值
     * @return 顺序值
     */
    int getOrder();
}
