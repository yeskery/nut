package com.yeskery.nut.core;

/**
 * Nut 的 Session 管理器接口，用于管理 Nut 请求的 Session 环境
 * @author sprout
 * 2019-03-16 15:15
 * @version 1.0
 *
 * @see BasicSessionManager
 */
public interface SessionManager {

	/** 用于标识 Session ID 的字符串 */
	String SESSION_NAME = "NSessionId";

	/**
	 * 获取 Session 的过期时间
	 * @return Session 的过期时间
	 */
	int getExpire();

	/**
	 * 根据 SessionId 获取 Session 对象
	 * @param sessionId Session ID
	 * @return Session 对象
	 */
	Session getSession(String sessionId);

	/**
	 * 获取 SessionId
	 * @param request 请求对象
	 * @return SessionId
	 */
	String getSessionId(Request request);

	/**
	 * 判断当前的 SessionId 是否存在
	 * @param sessionId Session ID
	 * @return SessionId 是否存在
	 */
	boolean hasSession(String sessionId);

	/**
	 * 添加一个 Session 对象
	 * @param session 要添加的 Session 对象
	 * @return 添加后的 SessionId
	 */
	String addSession(Session session);

	/**
	 * 在每次请求分发前初始化 Session 环境
	 * @param request 请求对象
	 * @param response 相应对象
	 *
	 * @see Session
	 * @see Request
	 * @see Response
	 */
	void initSessionEnv(Request request, Response response);
}
