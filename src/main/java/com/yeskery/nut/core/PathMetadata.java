package com.yeskery.nut.core;

import java.util.regex.Pattern;

/**
 * 路径元数据
 * @author sprout
 * 2022-06-14 11:05
 */
public class PathMetadata {

    /** 是否动态路径 */
    private final boolean dynamic;

    /** 原始路径 */
    private final String originalPath;

    /** 去除正则表达式后的基础路径 */
    private final String basePath;

    /** 路径正则表达式 */
    private final Pattern pathPattern;

    /** 参数名称和正则表达式 */
    private final KeyAndValue<String, Pattern>[] paramNameAndPatterns;

    /**
     * 构建路径元数据
     * @param dynamic 是否动态路径
     * @param originalPath 原始路径
     * @param basePath 基础路径
     * @param pathPattern 路径正则表达式
     * @param paramNameAndPatterns 参数名称和正则表达式
     */
    public PathMetadata(boolean dynamic, String originalPath, String basePath, Pattern pathPattern,
                        KeyAndValue<String, Pattern>[] paramNameAndPatterns) {
        this.dynamic = dynamic;
        this.originalPath = originalPath;
        this.basePath = basePath;
        this.pathPattern = pathPattern;
        this.paramNameAndPatterns = paramNameAndPatterns;
    }

    /**
     * 构建非动态路径元数据
     * @param original 原始路径
     */
    @SuppressWarnings("unchecked")
    public PathMetadata(String original) {
        this(false, original, original, null, new KeyAndValue[0]);
    }

    /**
     * 是否动态路径
     * @return 是否动态路径
     */
    public boolean isDynamic() {
        return dynamic;
    }

    /**
     * 获取原始路径
     * @return 原始路径
     */
    public String getOriginalPath() {
        return originalPath;
    }

    /**
     * 获取基础路径
     * @return 基础路径
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * 获取路径正则表达式
     * @return 路径正则表达式
     */
    public Pattern getPathPattern() {
        return pathPattern;
    }

    /**
     * 获取参数名称和正则表达式
     * @return 参数名称和正则表达式
     */
    public KeyAndValue<String, Pattern>[] getParamNameAndPatterns() {
        return paramNameAndPatterns;
    }
}
