package com.yeskery.nut.core;

/**
 * 执行函数
 *
 * @author sprout
 * 2022-07-12 17:07
 * @version 1.0
 */
@FunctionalInterface
public interface Runner<T> {

    /**
     * 执行方法
     * @param t 参数
     * @throws Exception 发生的异常
     */
    void run(T t) throws Exception;
}
