package com.yeskery.nut.core;

/**
 * 默认服务器请求配置
 * @author YESKERY
 * 2024/8/29
 */
public class DefaultServerRequestConfiguration implements ServerRequestConfiguration {

    /** 最大请求大小 */
    private Long maxRequestSize;

    @Override
    public Long getMaxRequestSize() {
        return maxRequestSize;
    }

    /**
     * 设置最大请求大小
     * @param maxRequestSize 最大请求大小
     */
    public void setMaxRequestSize(Long maxRequestSize) {
        this.maxRequestSize = maxRequestSize;
    }
}
