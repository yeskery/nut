package com.yeskery.nut.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 字节环境资源
 * @author sprout
 * @version 1.0
 * 2022-09-13 11:49
 */
public class ByteArrayEnvironmentResource extends AbstractEnvironmentResource {

    /** 字节数组 */
    private final byte[] bytes;

    /**
     * 构建字节环境资源
     * @param environment 环境对象
     * @param name 资源名称
     * @param bytes 字节数组
     */
    public ByteArrayEnvironmentResource(Environment environment, String name, byte[] bytes) {
        super(environment, name);
        this.bytes = bytes;
    }

    @Override
    protected InputStream getOriginalInputStream() {
        return new ByteArrayInputStream(bytes);
    }
}
