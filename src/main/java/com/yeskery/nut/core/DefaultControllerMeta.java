package com.yeskery.nut.core;

import java.util.Objects;

/**
 * 默认的Controller元信息
 * @author sprout
 * 2022-06-10 10:36
 */
public class DefaultControllerMeta implements ControllerMetadata {

    /** Controller id */
    private long id;

    /** Controller注册路径 */
    private String path;

    /** Controller注册别名 */
    private String[] alias;

    /** Controller 来源 */
    private ControllerSource controllerSource;

    /**
     * 构建默认的Controller元信息
     */
    public DefaultControllerMeta() {
    }

    /**
     * 构建默认的Controller元信息
     * @param id Controller id
     * @param path Controller注册路径
     * @param alias Controller注册别名
     * @param controllerSource Controller 来源
     */
    public DefaultControllerMeta(long id, String path, String[] alias, ControllerSource controllerSource) {
        this.id = id;
        this.path = path;
        this.alias = alias;
        this.controllerSource = controllerSource;
    }

    @Override
    public long getId() {
        return id;
    }

    /**
     * 设置 Controller id
     * @param id Controller id
     */
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getPath() {
        return path;
    }

    /**
     * 设置Controller注册路径
     * @param path Controller注册路径
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String[] getAlias() {
        return alias;
    }

    /**
     * 设置Controller注册别名
     * @param alias Controller注册别名
     */
    public void setAlias(String[] alias) {
        this.alias = alias;
    }

    @Override
    public ControllerSource getControllerSource() {
        return controllerSource;
    }

    /**
     * 设置Controller 来源
     * @param controllerSource Controller 来源
     */
    public void setControllerSource(ControllerSource controllerSource) {
        this.controllerSource = controllerSource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DefaultControllerMeta that = (DefaultControllerMeta) o;
        return path.equals(that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
