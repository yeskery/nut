package com.yeskery.nut.core;

import com.yeskery.nut.plugin.BindContext;
import com.yeskery.nut.view.ViewHandler;

/**
 * 执行接口
 * @author sprout
 * @version 1.0
 * 2022-05-29 22:05
 */
public interface Execution {

    /**
     * 获取转发器
     * @return 转发器
     */
    Forward getForward();

    /**
     * 获取绑定上下文
     * @return 绑定上下文
     */
    BindContext getBindContext();

    /**
     * 获取视图处理器
     * @return 视图处理器
     */
    ViewHandler getViewHandler();
}
