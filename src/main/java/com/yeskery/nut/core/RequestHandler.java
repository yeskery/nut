package com.yeskery.nut.core;

/**
 * 请求处理函数接口
 * @author sprout
 * 2022-05-31 13:41
 */
@FunctionalInterface
public interface RequestHandler {

    /**
     * 请求处理方法
     * @param request 请求对象
     * @param response 响应对象
     * @param execution 执行器
     */
    void handle(Request request, Response response, Execution execution);
}
