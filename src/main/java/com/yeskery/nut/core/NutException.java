package com.yeskery.nut.core;

/**
 * Nut 默认的异常类
 * @author sprout
 * 2019-03-14 16:34
 * @version 1.0
 */
public class NutException extends RuntimeException {

	/**
	 * 构建一个 {@link NutException}
	 */
	public NutException() {
	}

	/**
	 * 构建一个 {@link NutException}
	 * @param message message
	 */
	public NutException(String message) {
		super(message);
	}

	/**
	 * 构建一个 {@link NutException}
	 * @param message message
	 * @param cause cause
	 */
	public NutException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 构建一个 {@link NutException}
	 * @param cause cause
	 */
	public NutException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构建一个 {@link NutException}
	 * @param message message
	 * @param cause cause
	 * @param enableSuppression enableSuppression
	 * @param writableStackTrace writableStackTrace
	 */
	public NutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
