package com.yeskery.nut.core;

import com.yeskery.nut.application.NutWebConfigure;

/**
 * Nut Web配置接口，用户自动配置应用的接口
 * @author sunjay
 * 2023/10/10
 */
public interface WebConfigurer {

    /**
     * 配置Nut Web的方法
     * @param nutWebConfigure nut web配置对象
     */
    void configure(NutWebConfigure nutWebConfigure);
}
