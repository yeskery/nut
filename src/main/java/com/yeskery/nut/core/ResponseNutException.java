package com.yeskery.nut.core;

/**
 * 带响应状态的Nut异常
 * @author sprout
 * @version 1.0
 * 2019-04-12 23:34
 */
public class ResponseNutException extends NutException {

    /** 响应码 */
    private final Integer code;

    /**
     * 构建一个 {@link ResponseNutException}
     */
    public ResponseNutException() {
        code = ResponseCode.INTERNAL_SERVER_ERROR.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     */
    public ResponseNutException(String message) {
        super(message);
        code = ResponseCode.INTERNAL_SERVER_ERROR.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause 异常对象
     */
    public ResponseNutException(String message, Throwable cause) {
        super(message, cause);
        code = ResponseCode.INTERNAL_SERVER_ERROR.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param cause 异常对象
     */
    public ResponseNutException(Throwable cause) {
        super(cause);
        code = ResponseCode.INTERNAL_SERVER_ERROR.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ResponseNutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        code = ResponseCode.INTERNAL_SERVER_ERROR.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param code 响应码
     */
    public ResponseNutException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause cause
     * @param code 响应码
     */
    public ResponseNutException(String message, Throwable cause, Integer code) {
        super(message, cause);
        this.code = code;
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param cause cause
     * @param code 响应码
     */
    public ResponseNutException(Throwable cause, Integer code) {
        super(cause);
        this.code = code;
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     * @param code 响应码
     */
    public ResponseNutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Integer code) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param responseCode 响应码对象
     */
    public ResponseNutException(String message, ResponseCode responseCode) {
        super(message);
        code = responseCode.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param responseCode 响应码对象
     */
    public ResponseNutException(ResponseCode responseCode) {
        super(responseCode.getDescription());
        code = responseCode.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause cause
     * @param responseCode 响应码对象
     */
    public ResponseNutException(String message, Throwable cause, ResponseCode responseCode) {
        super(message, cause);
        code = responseCode.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param cause cause
     * @param responseCode 响应码对象
     */
    public ResponseNutException(Throwable cause, ResponseCode responseCode) {
        super(cause);
        code = responseCode.getCode();
    }

    /**
     * 构建一个 {@link ResponseNutException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     * @param responseCode 响应码对象
     */
    public ResponseNutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ResponseCode responseCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        code = responseCode.getCode();
    }

    /**
     * 获取响应码
     * @return 响应码
     */
    public Integer getCode() {
        return code;
    }
}
