package com.yeskery.nut.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * 默认自动探测器
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:30
 */
public class DefaultAutoDetectorRegister implements AutoDetectorRegister {

    /** 自动探测器集合 */
    private final Collection<AutoDetector> autoDetectors = new ArrayList<>();

    @Override
    public void registerAutoDetector(AutoDetector autoDetector) {
        autoDetectors.add(autoDetector);
    }

    /**
     * 获取所有的自动探测器
     * @return 所有的自动探测器
     */
    public Collection<AutoDetector> getAutoDetectors() {
        return Collections.unmodifiableCollection(autoDetectors);
    }
}
