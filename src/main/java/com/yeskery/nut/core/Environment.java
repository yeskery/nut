package com.yeskery.nut.core;

import java.util.Properties;

/**
 * 系统环境接口
 * @author sprout
 * @version 1.0
 * 2022-07-09 10:48
 */
public interface Environment {

    /**
     * 获取当前系统环境
     * @return 当前系统环境
     */
    String getEnvName();

    /**
     * 获取系统主配置键值对
     * @return 系统主配置键值对
     */
    Properties getMainEnvProperties();

    /**
     * 加载系统主配置键值对
     * @param propertiesPath 配置文件路径
     * @return 系统主配置键值对
     */
    Properties loadMainEnvProperties(String propertiesPath);

    /**
     * 获取系统配置键值对
     * @return 系统配置键值对
     */
    Properties getEnvProperties();

    /**
     * 加载系统配置键值对
     * @param propertiesPath 配置文件路径
     * @return 系统配置键值对
     */
    Properties loadEnvProperties(String propertiesPath);

    /**
     * 获取系统配置值
     * @param key 键
     * @return 配置值
     */
    default String getEnvProperty(String key) {
        return getEnvProperties().getProperty(key);
    }
}
