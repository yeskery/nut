package com.yeskery.nut.core;

import java.util.regex.Pattern;

/**
 * 支持基础通配符的路径表达式，只支持*表达式，表示任意多个字符，*不能为空字符串
 * @author sprout
 * @version 1.0
 * 2022-06-19 17:32
 */
public class AntPath extends Path {

    /** 支持的正则表达式符号 */
    public static final String PATTERN_SYMBOL = "*";

    /** 正则表达式 */
    private final Pattern pattern;

    /**
     * 构建一个支持基础通配符的路径表达式
     * @param path 路径
     */
    public AntPath(String path) {
        super(path);
        if (isPatternModel()) {
            path = getPath();
            path = path.replace(PATTERN_SYMBOL + PATTERN_SYMBOL, PATTERN_SYMBOL);
            path = path.replace(PATTERN_SYMBOL, "\\S+");
            pattern = Pattern.compile(path);
        } else {
            pattern = null;
        }
    }

    @Override
    public boolean match(String path) {
        if (isPatternModel()) {
            return pattern.matcher(path).matches();
        }
        return super.match(path);
    }

    /**
     * 是否是通配符模式路径
     * @return 是否是通配符模式路径
     */
    public boolean isPatternModel() {
        return getPath().contains(PATTERN_SYMBOL);
    }
}
