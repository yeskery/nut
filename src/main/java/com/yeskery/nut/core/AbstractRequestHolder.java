package com.yeskery.nut.core;

import com.yeskery.nut.bean.BeanException;

/**
 * 基础请求持有类
 * @author YESKERY
 * 2023/10/17
 */
public class AbstractRequestHolder {

    /** 请求持有类 */
    private final ThreadLocal<Request> requestHolder = new ThreadLocal<>();

    /**
     * 移除当前的请求对象
     */
    public void removeRequest() {
        requestHolder.remove();
    }

    /**
     * 设置当前的请求对象
     * @param request {@link Request}请求对象
     */
    public void setRequest(Request request) {
        requestHolder.set(request);
    }

    /**
     * 获取{@link Request}请求对象
     * @return {@link Request}请求对象
     */
    protected Request getRequest() {
        Request request = requestHolder.get();
        if (request == null) {
            throw new BeanException("Current Request Not Initialized.");
        }
        return request;
    }
}
