package com.yeskery.nut.core;

/**
 * <p>Nut的Controller定义接口，用于描述和处理{@link Request}的处理和响应，其中所有请求默认会进入到{@link #doRequest(Request, Response, Execution)}
 * 方法进行处理，在默认的方法实现中，针对不同的请求会再调用到不同的处理方法中，例如：{@link #doGet(Request, Response, Execution)}、
 * {@link #doPost(Request, Response, Execution)}等，如果默认方法不能满足处理需要，可以重写该方法以达到更加自定义的业务需要。</p>
 *
 * @author sprout
 * 2019-03-16 09:49
 * @version 1.0
 *
 * @see Request
 * @see Response
 */
public interface Controller {

	/**
	 * 处理客户端请求的主方法，默认的实现是根据客户端对应的请求方法进行对应的处理
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doRequest(Request request, Response response, Execution execution) {
		Method method = request.getMethod();
		if (method == null || method == Method.ALL) {
			throw new ResponseNutException("Http Method Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
		}
		switch (method) {
			case GET: doGet(request, response, execution);break;
			case POST: doPost(request, response, execution);break;
			case PUT: doPut(request, response, execution);break;
			case DELETE: doDelete(request, response, execution);break;
			case OPTIONS: doOptions(request, response, execution);break;
			case TRACE: doTrace(request, response, execution);break;
			default: throw new ResponseNutException("Http Method [" + method + "] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
		}
	}

	/**
	 * 处理客户端 {@link Method#GET} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doGet(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [GET] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}

	/**
	 * 处理客户端 {@link Method#POST} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doPost(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [POST] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}

	/**
	 * 处理客户端 {@link Method#PUT} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doPut(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [PUT] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}

	/**
	 * 处理客户端 {@link Method#DELETE} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doDelete(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [DELETE] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}

	/**
	 * 处理客户端 {@link Method#OPTIONS} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doOptions(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [OPTIONS] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}

	/**
	 * 处理客户端 {@link Method#TRACE} 请求
	 * @param request 请求对象
	 * @param response 相应对象
	 * @param execution 执行器
	 */
	default void doTrace(Request request, Response response, Execution execution) {
		throw new ResponseNutException("Http Method [TRACE] Not Support.", ResponseCode.METHOD_NOT_SUPPORT);
	}
}
