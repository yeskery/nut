package com.yeskery.nut.core;

/**
 * 持有对象
 * @author sunjay
 * 2024/8/29
 */
public class Holder<T> {

    /** 持有对象 */
    private T object;

    /**
     * 获取持有对象
     * @return 持有对象
     */
    public T getObject() {
        return object;
    }

    /**
     * 设置持有对象
     * @param object 持有对象
     */
    public void setObject(T object) {
        this.object = object;
    }
}
