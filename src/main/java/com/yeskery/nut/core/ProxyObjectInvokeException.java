package com.yeskery.nut.core;

/**
 * 代理对象执行异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class ProxyObjectInvokeException extends NutException {

    /**
     * 构建一个 {@link ProxyObjectInvokeException}
     */
    public ProxyObjectInvokeException() {
    }

    /**
     * 构建一个 {@link ProxyObjectInvokeException}
     * @param message message
     */
    public ProxyObjectInvokeException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ProxyObjectInvokeException}
     * @param message message
     * @param cause cause
     */
    public ProxyObjectInvokeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ProxyObjectInvokeException}
     * @param cause cause
     */
    public ProxyObjectInvokeException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ProxyObjectInvokeException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ProxyObjectInvokeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
