package com.yeskery.nut.core;

import java.util.function.Consumer;

/**
 * 响应发射器，可多次对客户端进行响应
 * @author YESKERY
 * 2024/7/8
 */
public interface ResponseEmitter {

    /**
     * 获取超时时间(秒)
     * @return 超时时间(秒)
     */
    long getTimeout();

    /**
     * 发送字符类型响应数据
     * @param data 字符类型响应数据
     */
    void send(String data);

    /**
     * 发送字节类型响应数据
     * @param data 字节类型响应数据
     */
    void send(byte[] data);

    /**
     * 发送响应完成
     */
    void completed();

    /**
     * 响应完成回调
     * @param runnable 响应完成回调
     */
    void onCompletion(Runnable runnable);

    /**
     * 响应错误回调
     * @param consumer 响应错误回调
     */
    void onError(Consumer<Throwable> consumer);

    /**
     * 响应超时回调
     * @param runnable 响应超时回调
     */
    void onTimeout(Runnable runnable);
}
