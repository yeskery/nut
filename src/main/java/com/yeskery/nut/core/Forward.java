package com.yeskery.nut.core;

/**
 * 转发接口，执行转发后，{@link com.yeskery.nut.plugin.ControllerPlugin}，将不会再次执行
 * @author sprout
 * 2022-05-12 18:57
 */
public interface Forward {

    /**
     * 将请求再次转发到其它的 {@link Controller} 中，该方法默认转发到
     * request 中的方法
     * @param request request 对象
     * @param response response 对象
     * @param execution 执行器
     * @param path 转发的路径
     */
    default void forward(Request request, Response response, Execution execution, String path) {
        forward(request, response, execution, path, Method.GET);
    }

    /**
     * 将请求再次转发到其它的 {@link Controller} 中，并且转发到指定
     * 的方法中
     * @param request request 对象
     * @param response response 对象
     * @param execution 执行器
     * @param path 转发的路径
     * @param method 指定的方法
     */
    void forward(Request request, Response response, Execution execution, String path, Method method);

}
