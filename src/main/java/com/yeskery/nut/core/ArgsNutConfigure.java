package com.yeskery.nut.core;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 启动参数的配置信息类
 * @author sprout
 * 2019-03-20 11:22
 * @version 1.0
 *
 * @see com.yeskery.nut.core.BaseNutConfigure
 */
public class ArgsNutConfigure extends BaseNutConfigure {

	/** 键值对分割符 */
	private static final String KEY_VALUE_SEPARATOR = "=";

	/** 配置参数数组 */
	private final String[] args;

	/**
	 * 用配置参数数组创建一个配置信息类
	 * @param args 配置参数数组
	 */
	public ArgsNutConfigure(String[] args) {
		this.args = args;
	}

	@Override
	protected Map<String, String> getConfiguration() {
		Set<String> configurationKeySet = getConfigurationKeySet();
		return Arrays.stream(args)
				.filter(arg -> arg.contains(KEY_VALUE_SEPARATOR))
				.map(arg -> arg.split(KEY_VALUE_SEPARATOR))
				.map(s -> new String[]{s[0].trim(), s[1].trim()})
				.filter(s -> configurationKeySet.contains(s[0]))
				.collect(Collectors.toMap(s -> s[0], s -> s[1]));
	}
}
