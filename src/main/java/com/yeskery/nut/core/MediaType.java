package com.yeskery.nut.core;

/**
 * 定义的常见媒体类型
 * @author sprout
 * @version 1.0
 * 2019-03-15 22:32
 */
public enum MediaType {
	/** html */
    TEXT_HTML("text/html", "htm", "html", "htx", "jsp", "plg", "stm", "xsl"),
	/** xml */
	TEXT_XML("text/xml", "biz", "dcd", "dtd", "rdf", "svg", "xml"),
	/** plain */
    TEXT_PLAIN("text/plain", "sol", "sor", "txt"),
	/** javascript */
    TEXT_JAVASCRIPT("application/javascript", "js"),
	/** css */
	TEXT_CSS("text/css", "css"),
	/** json */
    APPLICATION_JSON("application/json", "json"),
	/** xml */
	APPLICATION_XML("application/xml", "xml"),
	/** icon */
	IMAGE_X_ICON("image/x-icon", "ico"),
	/** jpg */
	IMAGE_JPEG("image/jpeg", "jpg", "jpeg", "jfif", "jpe"),
	/** gif */
	IMAGE_GIF("image/gif", "gif"),
	/** png */
	IMAGE_PNG("image/png", "png"),
	/** pdf */
	APPLICATION_PDF("application/pdf", "pdf"),
	/** word */
	APPLICATION_MS_WORD("application/msword", "doc", "docx"),
	/** form-urlencoded */
	APPLICATION_X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded", "x-www-form-urlencoded"),
	/** form-data */
	APPLICATION_FORM_DATA("multipart/form-data", "form-data"),
	/** 二进制文件 */
    APPLICATION_OCTET_STREAM("application/octet-stream", "*");

    /** 媒体类型的值 */
    private final String value;

    /** 媒体类型对应的后缀名 */
    private final String[] postfix;

	/**
	 * 构建一个媒体类型对象
	 * @param value 媒体类型的值
	 * @param postfix 媒体类型对应的后缀名数组
	 */
	MediaType(String value, String... postfix) {
		this.value = value;
		this.postfix = postfix;
	}

	/**
	 * 获取媒体类型的值
	 * @return 媒体类型的值
	 */
	public String getValue() {
        return value;
    }

	/**
	 * 获取 媒体类型对应的后缀名数组
	 * @return 媒体类型对应的后缀名数组
	 */
	public String[] getPostfix() {
		return postfix;
	}

	/**
	 * 通过后缀名获取媒体类型
	 * @param postfix 后缀名
	 * @return 媒体类型
	 */
	public static MediaType getMediaTypeByPostfix(String postfix) {
		for (MediaType mediaType : values()) {
			for (String fix : mediaType.postfix) {
				if (fix.equalsIgnoreCase(postfix)) {
					return mediaType;
				}
			}
		}
		return MediaType.APPLICATION_OCTET_STREAM;
	}

	/**
	 * 通过媒体类型名称获取媒体类型
	 * @param value 媒体类型名称
	 * @return 媒体类型
	 */
	public static MediaType getMediaTypeByValue(String value) {
		for (MediaType mediaType : values()) {
			if (mediaType.value.equalsIgnoreCase(value)) {
				return mediaType;
			}
		}
		return MediaType.APPLICATION_OCTET_STREAM;
	}
}
