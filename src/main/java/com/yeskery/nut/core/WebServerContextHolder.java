package com.yeskery.nut.core;

import java.util.Map;

/**
 * 服务上下文持有类
 * @author YESKERY
 * 2023/10/17
 */
public class WebServerContextHolder extends AbstractRequestHolder implements  ServerContext {
    @Override
    public void addAttribute(String name, Object object) {
        getRequest().getServerContext().addAttribute(name, object);
    }

    @Override
    public void removeAttribute(String name) {
        getRequest().getServerContext().removeAttribute(name);
    }

    @Override
    public Object getAttribute(String name) {
        return getRequest().getServerContext().getAttribute(name);
    }

    @Override
    public Map<String, Object> getAttributes() {
        return getRequest().getServerContext().getAttributes();
    }
}
