package com.yeskery.nut.core;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * 合并Nut基础配置信息类
 * @author sprout
 * @version 1.0
 * 2022-07-27 15:12
 */
public class MergeNutConfigure extends BaseNutConfigure {

    /** 合并后的配置map */
    private final Map<String, String> mergeConfigurationMap = new HashMap<>();

    /**
     * 合并Nut基础配置信息类
     * @param baseNutConfigure Nut基础配置信息类
     * @return 当前的合并Nut基础配置信息类
     */
    public MergeNutConfigure merge(BaseNutConfigure baseNutConfigure) {
        baseNutConfigure.initialize();
        mergeConfigurationMap.putAll(baseNutConfigure.getConfiguration());
        return this;
    }

    /**
     * 更改Nut基础配置信息类
     * @param mergeFunction Nut基础配置信息处理函数
     * @return 当前的合并Nut基础配置信息类
     */
    public MergeNutConfigure apply(Function<MergeNutConfigure, MergeNutConfigure> mergeFunction) {
        return mergeFunction.apply(this);
    }

    /**
     * 获取合并后的配置map
     * @return 合并后的配置map
     */
    public Map<String, String> getMergeConfigurationMap() {
        return mergeConfigurationMap;
    }

    @Override
    protected Map<String, String> getConfiguration() {
        return getMergeConfigurationMap();
    }
}
