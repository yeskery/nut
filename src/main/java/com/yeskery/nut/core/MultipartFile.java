package com.yeskery.nut.core;

import java.io.File;

/**
 * Nut 的以 Multipart 表单提交的二进制文件的接口
 * @author sprout
 * 2019-03-15 09:45
 * @version 1.0
 */
public interface MultipartFile extends InputStreamResource {

	/**
	 * 获取客户端提交文件的名称
	 * @return 客户端提交文件的名称
	 */
	String getFileName();

	/**
	 * 获取客户端提交文件的类型
	 * @return 客户端提交文件的类型
	 */
	String getContentType();

	/**
	 * 获取客户端提交文件的字节数组
	 * @return 客户端提交文件的字节数组
	 */
	byte[] getData();

	/**
	 * 将文件保存在服务器指定的文件对象上
	 * @param file 服务器指定的文件对象
	 */
	void saveAsFile(File file);
}
