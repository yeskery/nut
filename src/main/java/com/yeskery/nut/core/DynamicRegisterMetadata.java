package com.yeskery.nut.core;

import java.util.Arrays;
import java.util.Objects;

/**
 * 动态注册元数据
 * @author sprout
 * @version 1.0
 * 2023-04-15 23:45
 */
public class DynamicRegisterMetadata {

    /** 路径参数名称数组 */
    private final String[] pathParameterNames;

    /** 路径参数匹配器 */
    private final PathMatcher pathMatcher;

    /**
     * 构建动态注册元数据
     * @param pathParameterNames 路径参数名称数组
     * @param pathMatcher 路径参数匹配器
     */
    public DynamicRegisterMetadata(String[] pathParameterNames, PathMatcher pathMatcher) {
        this.pathParameterNames = pathParameterNames;
        this.pathMatcher = pathMatcher;
    }

    /**
     * 构建动态注册元数据
     */
    public DynamicRegisterMetadata() {
        this(new String[0], null);
    }

    /**
     * 获取路径参数名称数组
     * @return 路径参数名称数组
     */
    public String[] getPathParameterNames() {
        return pathParameterNames;
    }

    /**
     * 获取路径参数匹配器
     * @return 路径参数匹配器
     */
    public PathMatcher getPathMatcher() {
        return pathMatcher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DynamicRegisterMetadata that = (DynamicRegisterMetadata) o;
        return Arrays.equals(pathParameterNames, that.pathParameterNames)
                && Objects.equals(pathMatcher, that.pathMatcher);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(pathMatcher);
        result = 31 * result + Arrays.hashCode(pathParameterNames);
        return result;
    }
}
