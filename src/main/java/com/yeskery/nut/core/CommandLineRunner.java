package com.yeskery.nut.core;

/**
 * 命令行执行函数
 *
 * @author sprout
 * 2022-07-12 16:57
 * @version 1.0
 */
@FunctionalInterface
public interface CommandLineRunner extends Runner<String[]> {
}
