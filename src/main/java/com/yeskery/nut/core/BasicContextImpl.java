package com.yeskery.nut.core;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 该类是 {@link BasicContext} 的默认实现
 * @author sprout
 * 2022-05-31 11:10
 */
public class BasicContextImpl<T> implements BasicContext<T>, Serializable {

    /** 用于承载数据的集合 */
    private final Map<String, T> attributes = new ConcurrentHashMap<>(64);

    @Override
    public void addAttribute(String name, T object) {
        attributes.put(name, object);
    }

    @Override
    public void removeAttribute(String name) {
        attributes.remove(name);
    }

    @Override
    public T getAttribute(String name) {
        return attributes.get(name);
    }

    @Override
    public Map<String, T> getAttributes() {
        return attributes;
    }
}
