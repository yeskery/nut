package com.yeskery.nut.core;

import java.util.Map;

/**
 * Web会话持有类
 * @author YESKERY
 * 2023/10/17
 */
public class WebSessionHolder extends AbstractRequestHolder implements Session {

    @Override
    public void addAttribute(String name, Object object) {
        getRequest().getSession().addAttribute(name, object);
    }

    @Override
    public void removeAttribute(String name) {
        getRequest().getSession().removeAttribute(name);
    }

    @Override
    public String getSessionId() {
        return getRequest().getSession().getSessionId();
    }

    @Override
    public Object getAttribute(String name) {
        return getRequest().getSession().getAttribute(name);
    }

    @Override
    public Map<String, Object> getAttributes() {
        return getRequest().getSession().getAttributes();
    }
}
