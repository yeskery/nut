package com.yeskery.nut.core;

/**
 * 同一请求客户端的 <code>Session</code> 域通用定义接口
 * @author sprout
 * 2019-03-16 14:48
 * @version 1.0
 */
public interface Session extends BasicContext<Object> {

	/**
	 * 获取当前的 <code>Session</code> 域中的id
	 * @return 当前的 <code>Session</code> 域中的id
	 */
	String getSessionId();
}
