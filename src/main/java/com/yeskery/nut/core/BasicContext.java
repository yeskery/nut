package com.yeskery.nut.core;

import java.util.Map;

/**
 * 基础上下文对象
 * @author sunjay
 * 2024/9/1
 */
public interface BasicContext<T> {

    /**
     * 向上下文添加一个参数
     * @param name 参数的名
     * @param object 参数的值
     */
    void addAttribute(String name, T object);

    /**
     * 从上下文域移除一个参数
     * @param name 参数的名
     */
    void removeAttribute(String name);

    /**
     * 获取上下文中的指定参数
     * @param name 参数的名
     * @return 参数的值
     */
    T getAttribute(String name);

    /**
     * 获取上下文中的所有参数集合
     * @return 上下文中的所有参数集合
     */
    Map<String, T> getAttributes();
}
