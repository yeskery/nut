package com.yeskery.nut.core;

/**
 * Controller元信息
 * @author sprout
 * 2022-06-10 10:20
 */
public interface ControllerMetadata {

    /**
     * 获取Controller id
     * @return Controller id
     */
    long getId();

    /**
     * 获取Controller 注册路径
     * @return Controller 注册路径
     */
    String getPath();

    /**
     * 获取Controller 注册的别名路径
     * @return Controller 注册的别名路径
     */
    String[] getAlias();

    /**
     * 获取Controller来源
     * @return Controller来源
     */
    ControllerSource getControllerSource();
}
