package com.yeskery.nut.core;

import java.util.Arrays;

/**
 * 自动探测器注册器
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:31
 */
public interface AutoDetectorRegister {

    /**
     * 注册自动探测器
     * @param autoDetector 自动探测器
     */
    void registerAutoDetector(AutoDetector autoDetector);

    /**
     * 注册自动探测器
     * @param autoDetectorProvider 注册自动探测器提供器
     */
    default void registerAutoDetector(AutoDetectorProvider autoDetectorProvider) {
        Arrays.stream(autoDetectorProvider.getAutoDetectors()).forEach(this::registerAutoDetector);
    }
}
