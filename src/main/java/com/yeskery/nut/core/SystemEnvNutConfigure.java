package com.yeskery.nut.core;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 系统环境变量的配置信息类
 * @author sprout
 * @version 1.0
 * 2022-07-27 16:14
 */
public class SystemEnvNutConfigure extends BaseNutConfigure{
    @Override
    protected Map<String, String> getConfiguration() {
        Set<String> configurationKeySet = getConfigurationKeySet();
        return System.getenv().entrySet().stream()
                .filter(e -> configurationKeySet.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
