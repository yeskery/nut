package com.yeskery.nut.core;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * JVM变量的配置信息类
 * @author sprout
 * @version 1.0
 * 2022-07-27 14:59
 */
public class JvmPropertiesNutConfigure extends BaseNutConfigure {

    @Override
    protected Map<String, String> getConfiguration() {
        Set<String> configurationKeySet = getConfigurationKeySet();
        return System.getProperties().entrySet().stream()
                .filter(e -> configurationKeySet.contains(e.getKey().toString()))
                .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString()));
    }
}
