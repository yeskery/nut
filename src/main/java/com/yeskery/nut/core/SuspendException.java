package com.yeskery.nut.core;

/**
 * 中止异常，当抛出该异常时，将会中止Nut的启动过程，该异常仅作用于{@link com.yeskery.nut.bean.BeanFactoryPostProcessor}
 * 及{@link com.yeskery.nut.bean.BeanFactoryPriorityPostProcessor}BeanFactory后置处理器
 *
 * @author sprout
 * @version 1.0
 * 2023-04-16 00:42
 */
public class SuspendException extends NutException {

    /**
     * 构建一个 {@link SuspendException}
     */
    public SuspendException() {
    }

    /**
     * 构建一个 {@link SuspendException}
     * @param message message
     */
    public SuspendException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link SuspendException}
     * @param message message
     * @param cause cause
     */
    public SuspendException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link SuspendException}
     * @param cause cause
     */
    public SuspendException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link SuspendException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public SuspendException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
