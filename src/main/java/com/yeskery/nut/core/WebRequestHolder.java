package com.yeskery.nut.core;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Web请求持有类
 * @author YESKERY
 * 2023/10/16
 */
public class WebRequestHolder extends AbstractRequestHolder implements Request {

    @Override
    public List<String> getHeaders(String key) {
        return getRequest().getHeaders(key);
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return getRequest().getHeaders();
    }

    @Override
    public Set<String> getParameterKeys() {
        return getRequest().getParameterKeys();
    }

    @Override
    public Method getMethod() {
        return getRequest().getMethod();
    }

    @Override
    public String getOriginalPath() {
        return getRequest().getOriginalPath();
    }

    @Override
    public String getPath() {
        return getRequest().getPath();
    }

    @Override
    public String getProtocol() {
        return getRequest().getProtocol();
    }

    @Override
    public String getRemoteAddress() {
        return getRequest().getRemoteAddress();
    }

    @Override
    public String getRemoteHost() {
        return getRequest().getRemoteHost();
    }

    @Override
    public int getRemotePort() {
        return getRequest().getRemotePort();
    }

    @Override
    public String getLocalAddress() {
        return getRequest().getLocalAddress();
    }

    @Override
    public String getLocalHost() {
        return getRequest().getLocalHost();
    }

    @Override
    public int getLocalPort() {
        return getRequest().getLocalPort();
    }

    @Override
    public ServerRequestConfiguration getServerRequestConfiguration() {
        return getRequest().getServerRequestConfiguration();
    }

    @Override
    public Cookie[] getCookies() {
        return getRequest().getCookies();
    }

    @Override
    public List<MultipartFile> getFiles(String key) {
        return getRequest().getFiles(key);
    }

    @Override
    public byte[] getBody() {
        return getRequest().getBody();
    }

    @Override
    public Map<String, List<String>> getParametersMap() {
        return getRequest().getParametersMap();
    }

    @Override
    public Map<String, List<String>> getQueryParametersMap() {
        return getRequest().getQueryParametersMap();
    }

    @Override
    public void addAttribute(String name, Object value) {
        getRequest().addAttribute(name, value);
    }

    @Override
    public void removeAttribute(String name) {
        getRequest().removeAttribute(name);
    }

    @Override
    public Object getAttribute(String name) {
        return getRequest().getAttribute(name);
    }

    @Override
    public Map<String, Object> getAttributes() {
        return getRequest().getAttributes();
    }

    @Override
    public Session getSession() {
        return getRequest().getSession();
    }

    @Override
    public ServerContext getServerContext() {
        return getRequest().getServerContext();
    }

    @Override
    public boolean isEmpty() {
        return getRequest().isEmpty();
    }
}
