package com.yeskery.nut.core;

/**
 * 常见的响应码
 * @author sprout
 * @version 1.0
 * 2019-03-15 22:06
 */
public enum ResponseCode {
    /** OK */
    OK(200, "OK"),
    /** OK */
    CREATED(201, "Created"),
    /** Multiple Choices */
    MULTIPLE_CHOICES(300, "Multiple Choices"),
    /** Found */
    FOUND(302, "Found"),
    /** Bad Request */
    BAD_REQUEST(400, "Bad Request"),
    /** Unauthorized */
    UNAUTHORIZED(401, "Unauthorized"),
    /** Payment Required */
    PAYMENT_REQUIRED(402, "Payment Required"),
    /** Forbidden */
    FORBIDDEN(403, "Forbidden"),
    /** Not Found */
    NOT_FOUND(404, "Not Found"),
    /** Method Not Support */
    METHOD_NOT_SUPPORT(405, "Method Not Support"),
    /** Not Acceptable */
    NOT_ACCEPTABLE(406, "Not Acceptable"),
    /** Request Timeout */
    REQUEST_TIMEOUT(408, "Request Timeout"),
    /** Conflict */
    CONFLIC(409, "Conflict "),
    /** Request Entity Too Large */
    REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
    /** Unsupported Media Type */
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
    /** Too Many Requests */
    TOO_MANY_REQUESTS(429, "Too Many Requests"),
    /** Internal Server Error */
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    /** Service Unavailable */
    SERVICE_UNAVAILABLE(503, "Service Unavailable");

    /** 响应码 */
    private final int code;

    /** 描述 */
    private final String description;

    /**
     * 构建一个响应码对象
     * @param code 响应码
     * @param description 响应码描述
     */
    ResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 通过响应码获取响应码对象
     * @param code 响应码
     * @return 响应码对象
     */
    public static ResponseCode valueOf(int code) {
        for (ResponseCode responseCode : ResponseCode.values()) {
            if (responseCode.code == code) {
                return responseCode;
            }
        }
        return null;
    }

    /**
     * 获取响应码
     * @return 响应码
     */
    public int getCode() {
        return code;
    }

    /**
     * 获取响应码描述
     * @return 响应码描述
     */
    public String getDescription() {
        return description;
    }
}
