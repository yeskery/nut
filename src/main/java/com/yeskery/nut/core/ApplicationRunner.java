package com.yeskery.nut.core;

import com.yeskery.nut.application.ApplicationMetadata;

/**
 * 程序执行函数
 *
 * @author sprout
 * 2022-07-12 16:58
 * @version 1.0
 */
@FunctionalInterface
public interface ApplicationRunner extends Runner<ApplicationMetadata> {
}
