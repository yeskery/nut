package com.yeskery.nut.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 路径参数匹配器
 * @author sprout
 * 2022-06-13 20:10
 */
public class PathMatcher {

    /** 路径正则表达式 */
    private final Pattern pattern;

    /** 路径匹配器 */
    private Matcher matcher;

    /**
     * 构建路径参数匹配器
     * @param pattern 路径正则表达式
     */
    public PathMatcher(Pattern pattern) {
        this.pattern = pattern;
    }

    /**
     * 路径是否匹配
     * @param path 要匹配的路径
     * @return 路径是否匹配
     */
    public boolean match(String path) {
        matcher = pattern.matcher(path);
        return matcher.matches();
    }

    /**
     * 获取路径匹配器
     * @return 路径匹配器
     */
    public Matcher getMatcher() {
        return matcher;
    }
}
