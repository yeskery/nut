package com.yeskery.nut.core;

/**
 * 请求上下文持有类
 * @author Yeskery
 * 2023/8/11
 */
public class RequestContextHolder {

    /**
     * 私有化构造方法
     */
    private RequestContextHolder() {
    }

    /**
     * 获取当前的Request对象
     * @return 当前的Request对象
     */
    public static Request getRequest() {
        return RequestApplicationContext.getResource(RequestApplicationContext.REQUEST_HOLDER_KEY, Request.class);
    }
}
