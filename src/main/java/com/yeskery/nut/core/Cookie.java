package com.yeskery.nut.core;

/**
 * Nut 的 Cookie 接口
 * @author sprout
 * 2019-03-13 16:52
 * @version 1.0
 *
 * @see Request
 */
public interface Cookie {

	/**
	 * 获取 Cookie 的名称
	 * @return Cookie 的名称
	 */
	String getName();

	/**
	 * 获取 Cookie 的值
	 * @return Cookie 的值
	 */
	String getValue();

	/**
	 * 获取 Cookie 的域
	 * @return Cookie 的域
	 */
	String getDomain();

	/**
	 * 获取 Cookie 的过期时间
	 * @return Cookie 的过期时间
	 */
	String getExpires();

	/**
	 * 获取 Cookie 的路径
	 * @return Cookie 的路径
	 */
	String getPath();

	/**
	 * 获取 Cookie 是否强制 SSL
	 * @return 是否强制 SSL
	 */
	boolean isSecure();

	/**
	 * 获取 Cookie 是否仅对 Http 请求生效
	 * @return 是否仅对 Http 请求生效
	 */
	boolean isHttpOnly();

	/**
	 * 将当前 Cookie 对象转换为一个 {@link NameAndValue}
	 * @return {@link NameAndValue}
	 *
	 * @see NameAndValue
	 */
	NameAndValue toNameAndValue();


	/**
	 * 将当前 Cookie 对象转换为 Http 协议中指定的字符串格式
	 * @return 当前 Cookie 对象转换为 Http 协议中指定的字符串格式
	 */
	String getCookiePair();
}
