package com.yeskery.nut.core;

/**
 * 动态Controller适配器
 * @author sprout
 * 2022-06-14 15:35
 */
public class DynamicControllerAdapter implements Controller {

    /** Controller接口 */
    private final Controller controller;

    /** 动态Controller元数据 */
    private final DynamicControllerRegisterMetadata metadata;

    /**
     * 构建动态Controller适配器
     * @param controller Controller接口
     * @param metadata 动态Controller元数据
     */
    public DynamicControllerAdapter(Controller controller, DynamicControllerRegisterMetadata metadata) {
        this.controller = controller;
        this.metadata = metadata;
    }

    /**
     * 获取动态Controller元数据
     * @return 动态Controller元数据
     */
    public DynamicControllerRegisterMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void doRequest(Request request, Response response, Execution execution) {
        controller.doRequest(request, response, execution);
    }

    @Override
    public void doGet(Request request, Response response, Execution execution) {
        controller.doGet(request, response, execution);
    }

    @Override
    public void doPost(Request request, Response response, Execution execution) {
        controller.doPost(request, response, execution);
    }

    @Override
    public void doPut(Request request, Response response, Execution execution) {
        controller.doPut(request, response, execution);
    }

    @Override
    public void doDelete(Request request, Response response, Execution execution) {
        controller.doDelete(request, response, execution);
    }

    @Override
    public void doOptions(Request request, Response response, Execution execution) {
        controller.doOptions(request, response, execution);
    }

    @Override
    public void doTrace(Request request, Response response, Execution execution) {
        controller.doTrace(request, response, execution);
    }
}
