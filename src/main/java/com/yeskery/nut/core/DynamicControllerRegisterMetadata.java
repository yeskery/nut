package com.yeskery.nut.core;

import java.util.Arrays;
import java.util.Objects;

/**
 * 动态Controller注册元数据
 * @author sprout
 * 2022-06-13 19:44
 */
public class DynamicControllerRegisterMetadata extends DynamicRegisterMetadata {

    /** Controller对象 */
    private Controller controller;

    /**
     * 构建Controller注册元数据
     * @param pathParameterNames 路径参数名称数组
     * @param pathMatcher 路径参数匹配器
     * @param controller Controller对象
     */
    public DynamicControllerRegisterMetadata(String[] pathParameterNames, PathMatcher pathMatcher, Controller controller) {
        super(pathParameterNames, pathMatcher);
        this.controller = controller;
    }

    /**
     * 构建Controller注册元数据
     * @param controller Controller对象
     */
    public DynamicControllerRegisterMetadata(Controller controller) {
        this.controller = controller;
    }

    /**
     * 获取Controller对象
     * @return Controller对象
     */
    public Controller getController() {
        return controller;
    }

    /**
     * 设置Controller对象
     * @param controller Controller对象
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DynamicControllerRegisterMetadata that = (DynamicControllerRegisterMetadata) o;
        return Arrays.equals(getPathParameterNames(), that.getPathParameterNames())
                && Objects.equals(getPathMatcher(), that.getPathMatcher())
                && Objects.equals(controller, that.controller);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getPathMatcher(), controller);
        result = 31 * result + Arrays.hashCode(getPathParameterNames());
        return result;
    }
}
