package com.yeskery.nut.core;

/**
 * 错误Controller
 * @author sprout
 * @version 1.0
 * 2022-09-02 09:39
 */
public interface ErrorController {

    /** 请求异常对象key */
    String REQUEST_EXCEPTION_HOLDER_KEY = "$_request_exception";

    /**
     * 获取错误页面的请求路径
     * @return 错误页面的请求路径
     */
    String getErrorPath();

    /**
     * 获取默认错误的处理方法
     * @return 默认错误的处理方法
     */
    default Method getErrorMethod() {
        return Method.GET;
    }
}
