package com.yeskery.nut.core;

/**
 * Controller来源
 * @author sprout
 * 2022-06-10 10:21
 */
public enum ControllerSource {
    /** 系统默认注册的Controller */
    DEFAULT,
    /** 以注解注册的Controller */
    ANNOTATION,
    /** 以静态类注册的Controller */
    STATIC,
    /** 以脚本注册的Controller */
    SCRIPT,
    /** 以{@link RequestHandler} 注册的Controller */
    LAMBDA
}
