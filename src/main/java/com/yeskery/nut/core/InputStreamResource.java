package com.yeskery.nut.core;

import java.io.InputStream;

/**
 * 输入流资源对象接口
 * @author YESKERY
 * 2024/8/13
 */
@FunctionalInterface
public interface InputStreamResource {

    /**
     * 获取资源的输入流
     * @return 资源的输入流
     */
    InputStream getInputStream();
}
