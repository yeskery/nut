package com.yeskery.nut.core;

import java.io.InputStream;

/**
 * 类路径下的环境资源对象
 * @author sprout
 * @version 1.0
 * 2022-07-09 11:26
 */
public class ClassPathEnvironmentResource extends AbstractEnvironmentResource {

    /**
     * 构建类路径下的环境资源对象
     * @param environment 环境对象
     * @param name 资源名称
     */
    public ClassPathEnvironmentResource(Environment environment, String name) {
        super(environment, name);
    }

    @Override
    protected InputStream getOriginalInputStream() {
        return new ClassPathResource(getName()).getOriginalInputStream();
    }
}
