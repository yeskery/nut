package com.yeskery.nut.core;

import java.util.Collection;

/**
 * 响应发射器处理类
 * @author YESKERY
 * 2024/7/9
 */
public interface ResponseEmitterHandler {

    /**
     * 发送字符类型响应数据
     * @param data 字符类型响应数据
     */
    void send(String data);

    /**
     * 发送字节类型响应数据
     * @param data 字节类型响应数据
     */
    void send(byte[] data);

    /**
     * 发送响应完成
     */
    void completed();

    /**
     * 等待直到超过设置的超时时间
     */
    void park();
}
