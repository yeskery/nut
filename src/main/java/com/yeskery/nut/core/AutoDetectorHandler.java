package com.yeskery.nut.core;

/**
 * 自动检测处理接口
 * @author sprout
 * @version 1.0
 * 2022-07-02 22:39
 */
public interface AutoDetectorHandler {

    /**
     * 自动检测处理
     */
    void handle();
}
