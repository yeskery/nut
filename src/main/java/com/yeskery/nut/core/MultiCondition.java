package com.yeskery.nut.core;

/**
 * 多选类型
 * @author sprout
 * @version 1.0
 * 2022-09-14 14:50
 */
public enum MultiCondition {
    /** 任意条件符合 */
    ANY,
    /** 所有条件符合 */
    ALL,
    /** 没有条件符合 */
    NONE
}
