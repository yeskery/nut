package com.yeskery.nut.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/**
 * 线程池接口
 * @author sprout
 * @version 1.0
 * 2021-02-23 18:19
 */
public interface ThreadPool {
    /**
     * 获取定时线程池对象
     * @return 定时线程池对象
     */
    ScheduledExecutorService getScheduledThreadPool();

    /**
     * 获取线程池对象
     * @return 线程池对象
     */
    ExecutorService getThreadPool();

    /**
     * 关闭所有的线程池
     */
    void close();
}
