package com.yeskery.nut.core;

import com.yeskery.nut.http.StateLessSession;

/**
 * 无状态的 {@link SessionManager} 接口的实现类
 * @author sprout
 * 2022-08-24 14:31
 * @version 1.0
 *
 * @see com.yeskery.nut.core.SessionManager
 */
public class StateLessSessionManager implements SessionManager {

    /** 默认的无状态session */
    private final StateLessSession defaultSession = new StateLessSession();

    @Override
    public int getExpire() {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public Session getSession(String sessionId) {
        return defaultSession;
    }

    @Override
    public String getSessionId(Request request) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public boolean hasSession(String sessionId) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public String addSession(Session session) {
        throw new UnsupportedOperationException("Application Type [STATE_LESS_WEB] UnSupport Session.");
    }

    @Override
    public void initSessionEnv(Request request, Response response) {

    }
}
