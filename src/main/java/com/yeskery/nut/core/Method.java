package com.yeskery.nut.core;

/**
 * Http 请求的方法
 * @author sprout
 * @version 1.0
 * 2019-03-15 00:40
 */
public enum Method {
    /** ALL METHOD */
    ALL,
    /** GET */
    GET,
    /** HEAD */
    HEAD,
    /** POST */
    POST,
    /** PUT */
	PUT,
    /** DELETE */
    DELETE,
    /** CONNECT */
    CONNECT,
    /** OPTIONS */
    OPTIONS,
    /** TRACE */
    TRACE,
    /** PATCH */
    PATCH,
    /** MOVE */
    MOVE,
    /** COPY */
    COPY,
    /** LINK */
    LINK,
    /** UNLINK */
    UNLINK,
    /** WRAPPED */
    WRAPPED,
    /** EXTENSION_METHOD */
    EXTENSION_METHOD
}
