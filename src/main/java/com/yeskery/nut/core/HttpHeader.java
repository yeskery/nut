package com.yeskery.nut.core;

/**
 * Http请求头
 * @author sprout
 * @version 1.0
 * 2022-07-21 23:17
 */
public interface HttpHeader {
    /** 服务器信息 */
    String SERVER = "Server";
    /** 服务器时间 */
    String DATE = "Date";
    /** 连接类型 */
    String CONNECTION = "Connection";
    /** 媒体类型 */
    String CONTENT_TYPE = "Content-Type";
    /** 接受的响应类型 */
    String ACCEPT = "Accept";
    /** 请求来源信息 */
    String LOCATION = "Location";
    /** 媒体内容长度 */
    String CONTENT_LENGTH = "Content-Length";
    /** 设置Cookie */
    String SET_COOKIE = "Set-Cookie";
    /** Cookie */
    String COOKIE = "Cookie";
    /** Origin */
    String ORIGIN = "Origin";
    /** 认证 */
    String WWW_AUTHENTICATE = "WWW-Authenticate";
    /** 认证方式 */
    String AUTHORIZATION = "Authorization";
    /** 客户端信息 */
    String USER_AGENT = "User-Agent";
}
