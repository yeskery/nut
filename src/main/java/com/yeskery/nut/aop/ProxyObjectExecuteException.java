package com.yeskery.nut.aop;

import com.yeskery.nut.core.NutException;

/**
 * 代理对象执行异常
 * @author sprout
 * @version 1.0
 * 2022-08-29 00:05
 */
public class ProxyObjectExecuteException extends NutException {

    /**
     * 构建一个 {@link ProxyObjectExecuteException}
     */
    public ProxyObjectExecuteException() {
    }

    /**
     * 构建一个 {@link ProxyObjectExecuteException}
     * @param message message
     */
    public ProxyObjectExecuteException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ProxyObjectExecuteException}
     * @param message message
     * @param cause cause
     */
    public ProxyObjectExecuteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ProxyObjectExecuteException}
     * @param cause cause
     */
    public ProxyObjectExecuteException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ProxyObjectExecuteException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ProxyObjectExecuteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
