package com.yeskery.nut.aop;

import java.lang.reflect.Method;

/**
 * 默认目标方法执行接口
 * @author Yeskery
 * 2023/8/9
 */
public class DefaultExecution implements Execution {

    /** 原始方法 */
    private final Method method;

    /** 原始方法参数 */
    private final Object[] methodParameters;

    /** 原始对象 */
    private final Object object;

    /** 代理对象 */
    private final Object proxyObject;

    /**
     * 构建默认目标方法执行接口
     * @param method 原始方法
     * @param methodParameters 原始方法参数
     * @param object 原始对象
     * @param proxyObject 代理对象
     */
    public DefaultExecution(Method method, Object[] methodParameters, Object object, Object proxyObject) {
        this.method = method;
        this.methodParameters = methodParameters;
        this.object = object;
        this.proxyObject = proxyObject;
    }

    @Override
    public ExecutionMetadata getExecutionMetadata() {
        return new ExecutionMetadata(method, methodParameters, object, proxyObject);
    }
}
