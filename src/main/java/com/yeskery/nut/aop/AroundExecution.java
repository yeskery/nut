package com.yeskery.nut.aop;

import com.yeskery.nut.aop.handler.ProxyMethodTarget;

import java.lang.reflect.Method;

/**
 * 环绕注解执行对象
 * @author sprout
 * @version 1.0
 * 2023-04-22 13:06
 */
public class AroundExecution {

    /** 代理方法目标类 */
    private ProxyMethodTarget proxyMethodTarget;

    /** 目标对象 */
    private Object target;

    /** 目标方法 */
    private Method method;

    /**
     * 获取代理方法目标类
     * @return 代理方法目标类
     */
    public ProxyMethodTarget getProxyMethodTarget() {
        return proxyMethodTarget;
    }

    /**
     * 设置代理方法目标类
     * @param proxyMethodTarget 代理方法目标类
     */
    public void setProxyMethodTarget(ProxyMethodTarget proxyMethodTarget) {
        this.proxyMethodTarget = proxyMethodTarget;
    }

    /**
     * 获取目标对象
     * @return 目标对象
     */
    public Object getTarget() {
        return target;
    }

    /**
     * 设置目标对象
     * @param target 目标对象
     */
    public void setTarget(Object target) {
        this.target = target;
    }

    /**
     * 获取目标方法
     * @return 目标方法
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置目标方法
     * @param method 目标方法
     */
    public void setMethod(Method method) {
        this.method = method;
    }
}
