package com.yeskery.nut.aop;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 代理对象上下文
 * @author sprout
 * @version 1.0
 * 2022-08-28 14:51
 */
public interface ProxyObjectContext {

    /**
     * 是否需要代理该方法
     * @param method 方法对象
     * @return 是否需要代理该方法
     */
    boolean shouldProxy(Method method);

    /**
     * 是否需要代理该类对象
     * @param clazz 类对象
     * @return 是否需要代理该类对象
     */
    boolean shouldProxy(Class<?> clazz);

    /**
     * 获取适配的代理方法处理器集合
     * @param method 方法对象
     * @return 适配的代理方法处理器集合
     */
    List<ProxyMethodHandler> getAdapterProxyMethodHandlers(Method method);

    /**
     * 创建代理对象
     * @param object 原始对象
     * @return 代理对象
     */
    Object createProxyObject(Object object);

    /**
     * 创建代理对象
     * @param object 原始对象
     * @param proxyType 代理类型
     * @return 代理对象
     */
    Object createProxyObject(Object object, ProxyType proxyType);

    /**
     * 创建代理对象
     * @param object 原始对象
     * @param argumentTypes 参数类型列表
     * @param arguments 参数列表
     * @param proxyType 代理类型
     * @return 代理对象
     */
    Object createProxyObject(Object object, Class<?>[] argumentTypes, Object[] arguments, ProxyType proxyType);

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @param wrapInvocationHandler 包装的拦截处理器
     * @return 代理对象
     */
    Object createProxyObject(Class<?> clazz, WrapInvocationHandler wrapInvocationHandler);

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @param proxyType 代理类型
     * @param wrapInvocationHandler 包装的拦截处理器
     * @return 代理对象
     */
    Object createProxyObject(Class<?> clazz, ProxyType proxyType, WrapInvocationHandler wrapInvocationHandler);

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @param argumentTypes 参数类型列表
     * @param arguments 参数列表
     * @param wrapInvocationHandler 包装的拦截处理器
     * @return 代理对象
     */
    Object createProxyObject(Class<?> clazz, Class<?>[] argumentTypes, Object[] arguments,
                             WrapInvocationHandler wrapInvocationHandler);

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @param argumentTypes 参数类型列表
     * @param arguments 参数列表
     * @param proxyType 代理类型
     * @param wrapInvocationHandler 包装的拦截处理器
     * @return 代理对象
     */
    Object createProxyObject(Class<?> clazz, Class<?>[] argumentTypes, Object[] arguments, ProxyType proxyType,
                             WrapInvocationHandler wrapInvocationHandler);
}
