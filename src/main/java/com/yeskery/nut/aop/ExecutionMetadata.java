package com.yeskery.nut.aop;

import java.lang.reflect.Method;

/**
 * 目标方法执行接口元数据
 * @author sunjay
 * 2023/10/6
 */
public class ExecutionMetadata {

    /** 原始方法 */
    private final Method method;

    /** 原始方法参数 */
    private final Object[] methodParameters;

    /** 原始对象 */
    private final Object object;

    /** 代理对象 */
    private final Object proxyObject;

    /**
     * 构建目标方法执行接口元数据
     * @param method 原始方法
     * @param methodParameters 原始方法参数
     * @param object 原始对象
     * @param proxyObject 代理对象
     */
    public ExecutionMetadata(Method method, Object[] methodParameters, Object object, Object proxyObject) {
        this.method = method;
        this.methodParameters = methodParameters;
        this.object = object;
        this.proxyObject = proxyObject;
    }

    /**
     * 获取原始方法对象
     * @return 原始对象方法
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 获取原始方法参数
     * @return 原始方法参数
     */
    public Object[] getMethodParameters() {
        return methodParameters;
    }

    /**
     * 获取原始对象
     * @return 原始对象
     */
    public Object getObject() {
        return object;
    }

    /**
     * 获取代理对象
     * @return 代理对象
     */
    public Object getProxyObject() {
        return proxyObject;
    }
}
