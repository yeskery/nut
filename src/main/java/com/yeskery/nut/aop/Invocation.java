package com.yeskery.nut.aop;

/**
 * 环绕处理器目标方法执行接口
 * @author sprout
 * @version 1.0
 * 2023-04-08 23:17
 */
public interface Invocation extends Execution {

    /**
     * 调用目标方法
     * @return 目标方法的返回值
     */
    Object invoke();
}
