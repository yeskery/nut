package com.yeskery.nut.aop;

import com.yeskery.nut.core.Order;

import java.lang.reflect.Method;

/**
 * 代理方法处理器
 * @author sprout
 * @version 1.0
 * 2022-08-28 12:35
 */
public interface ProxyMethodHandler extends Order {

    /**
     * 是否支持处理该方法
     * @param method 方法对象
     * @return 是否支持处理该方法
     */
    default boolean support(Method method) {
        return false;
    }

    /**
     * 前置处理方法
     * @param method 方法对象
     * @param execution 目标方法执行接口
     * @param target 被代理的对象
     * @throws Exception 发生的异常
     */
    default void before(Method method, Execution execution, Object target) throws Exception {}

    /**
     * 后置处理方法
     * @param method 方法对象
     * @param execution 目标方法执行接口
     * @param target 被代理的对象
     * @throws Exception 发生的异常
     */
    default void after(Method method, Execution execution, Object target) throws Exception {}

    /**
     * 环绕处理方法
     * @param method 方法对象
     * @param invocation 环绕调用接口对象
     * @param target 被代理的对象
     * @throws Exception 发生的异常
     */
    default void around(Method method, Invocation invocation, Object target) throws Exception {}

    /**
     * 异常处理方法
     * @param method 方法对象
     * @param execution 目标方法执行接口
     * @param target 被代理的对象
     * @param throwable 异常对象
     * @throws Exception 发生的异常
     */
    default void throwing(Method method, Execution execution, Object target, Throwable throwable) throws Exception {}

    @Override
    default int getOrder() {
        return 0;
    }
}
