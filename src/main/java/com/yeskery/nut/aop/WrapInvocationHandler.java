package com.yeskery.nut.aop;

import java.lang.reflect.Method;

/**
 * 包装的拦截处理器
 * @author sprout
 * @version 1.0
 * 2022-08-28 14:44
 */
@FunctionalInterface
public interface WrapInvocationHandler {

    /**
     * 执行方法
     * @param proxy 代理对象
     * @param method 方法对象
     * @param args 方法参数
     * @return 方法执行后的结果
     * @throws Throwable 发生的异常
     */
    Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
