package com.yeskery.nut.aop;

/**
 * 代理对象工厂
 * @author sprout
 * @version 1.0
 * 2022-08-28 12:15
 */
public interface ProxyObjectFactory {

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @return 创建好的代理对象
     */
    Object createProxyInstance(Class<?> clazz);

    /**
     * 创建代理对象
     * @param clazz 代理对象类型
     * @param argumentTypes 参数类型列表
     * @param arguments 参数列表
     * @return 创建好的代理对象
     */
    Object createProxyInstance(Class<?> clazz, Class<?>[] argumentTypes, Object[] arguments);

    /**
     * 创建代理对象
     * @param original 原始对象
     * @return 创建好的代理对象
     */
    default Object createProxyInstance(Object original) {
        return createProxyInstance(original.getClass());
    }
}
