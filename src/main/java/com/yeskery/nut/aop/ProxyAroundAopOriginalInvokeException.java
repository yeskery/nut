package com.yeskery.nut.aop;

import com.yeskery.nut.core.NutException;

/**
 * 代理环绕AOP原始方法执行异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class ProxyAroundAopOriginalInvokeException extends NutException {

    /**
     * 构建一个 {@link ProxyAroundAopOriginalInvokeException}
     */
    public ProxyAroundAopOriginalInvokeException() {
    }

    /**
     * 构建一个 {@link ProxyAroundAopOriginalInvokeException}
     * @param message message
     */
    public ProxyAroundAopOriginalInvokeException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ProxyAroundAopOriginalInvokeException}
     * @param message message
     * @param cause cause
     */
    public ProxyAroundAopOriginalInvokeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ProxyAroundAopOriginalInvokeException}
     * @param cause cause
     */
    public ProxyAroundAopOriginalInvokeException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ProxyAroundAopOriginalInvokeException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ProxyAroundAopOriginalInvokeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
