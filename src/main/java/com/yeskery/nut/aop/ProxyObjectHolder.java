package com.yeskery.nut.aop;

/**
 * 代理持有对象
 * @author sprout
 * @version 1.0
 * 2022-08-29 12:10
 */
public class ProxyObjectHolder {

    /** 代理对象 */
    private final Object proxyObject;

    /** 原始对象 */
    private final Object originalObject;

    /**
     * 构建一个代理持有对象
     * @param proxyObject 代理对象
     * @param originalObject 原始对象
     */
    public ProxyObjectHolder(Object proxyObject, Object originalObject) {
        this.proxyObject = proxyObject;
        this.originalObject = originalObject;
    }

    /**
     * 获取代理对象
     * @return 代理对象
     */
    public Object getProxyObject() {
        return proxyObject;
    }

    /**
     * 获取原始对象
     * @return 原始对象
     */
    public Object getOriginalObject() {
        return originalObject;
    }
}