package com.yeskery.nut.aop.handler;

import com.yeskery.nut.annotation.aop.After;
import com.yeskery.nut.annotation.aop.Compose;
import com.yeskery.nut.aop.Execution;
import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.core.Order;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * After注解的方法处理器
 * @author sprout
 * @version 1.0
 * 2022-08-28 13:20
 */
public class AfterProxyMethodHandler extends AspectProxyMethodHandler implements ComposeProxyMethodHandler {

    /**
     * 构建After注解的方法处理器
     * @param applicationContext 应用上下文
     */
    public AfterProxyMethodHandler(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    @Override
    public boolean support(Method method) {
        return getProxyMethodAspectCacheMap().containsKey(method.toString()) || method.isAnnotationPresent(After.class)
                || support(method, c -> c.afterArray().length > 0);
    }

    @Override
    public void after(Method method, Execution execution, Object target) throws Exception {
        List<ProxyMethodTarget> proxyMethodTargetList = new ArrayList<>();
        After afterAnnotation = method.getAnnotation(After.class);
        if (afterAnnotation != null) {
            proxyMethodTargetList.add(new ProxyMethodTarget(afterAnnotation.beanName(), afterAnnotation.beanType(),
                    afterAnnotation.method(), afterAnnotation.order()));
        }
        Compose compose = getComposeAnnotation(method);
        if (compose != null) {
            for (After after : compose.afterArray()) {
                proxyMethodTargetList.add(new ProxyMethodTarget(after.beanName(), after.beanType(),
                        after.method(), after.order()));
            }
        }

        appendAspectProxyTarget(method, After.class, proxyMethodTargetList);
        proxyMethodTargetList.sort(Comparator.comparing(Order::getOrder));
        for (ProxyMethodTarget proxyMethodTarget : proxyMethodTargetList) {
            doInjectMethod(target, proxyMethodTarget.getBeanName(), proxyMethodTarget.getBeanType(),
                    proxyMethodTarget.getMethodName(), new Object[]{execution});
        }
    }
}
