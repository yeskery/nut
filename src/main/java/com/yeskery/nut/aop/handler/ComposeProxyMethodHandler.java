package com.yeskery.nut.aop.handler;

import com.yeskery.nut.annotation.aop.Compose;
import com.yeskery.nut.aop.ProxyMethodHandler;

import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * 组合注解代理方法处理器
 * @author sprout
 * @version 1.0
 * 2023-04-09 00:51
 */
public interface ComposeProxyMethodHandler extends ProxyMethodHandler {

    /**
     * 是否支持处理该方法
     * @param method 方法对象
     * @param supportFunction 是否支持的判断函数
     * @return 是否支持处理该方法
     */
    default boolean support(Method method, Function<Compose, Boolean> supportFunction) {
        Compose compose = method.getAnnotation(Compose.class);
        return compose != null && supportFunction.apply(compose);
    }

    /**
     * 获取组合注解对象
     * @param method 方法对象
     * @return 组合注解对象
     */
    default Compose getComposeAnnotation(Method method) {
        return method.getAnnotation(Compose.class);
    }
}
