package com.yeskery.nut.aop.handler;

import com.yeskery.nut.core.Order;

/**
 * 代理方法目标类
 * @author Yeskery
 * 2023/8/7
 */
public class ProxyMethodTarget implements Order {

    /** bean名称 */
    private String beanName;

    /** bean类型 */
    private Class<?> beanType;

    /** 方法名称 */
    private String methodName;

    /** 指定异常进行方法回调 */
    private Class<? extends Exception>[] causeException;

    /** 排序值 */
    private int order;

    /**
     * 构建代理方法目标类
     */
    public ProxyMethodTarget() {
    }

    /**
     * 构建代理方法目标类
     * @param beanName bean名称
     * @param beanType bean类型
     * @param methodName 方法名称
     * @param order 排序值
     */
    public ProxyMethodTarget(String beanName, Class<?> beanType, String methodName, int order) {
        this.beanName = beanName;
        this.beanType = beanType;
        this.methodName = methodName;
        this.order = order;
    }

    /**
     * 构建代理方法目标类
     * @param beanName bean名称
     * @param beanType bean类型
     * @param methodName 方法名称
     * @param causeException 指定异常进行方法回调
     * @param order 排序值
     */
    public ProxyMethodTarget(String beanName, Class<?> beanType, String methodName, Class<? extends Exception>[] causeException, int order) {
        this.beanName = beanName;
        this.beanType = beanType;
        this.methodName = methodName;
        this.causeException = causeException;
        this.order = order;
    }

    /**
     * 获取bean名称
     * @return bean名称
     */
    public String getBeanName() {
        return beanName;
    }

    /**
     * 设置bean名称
     * @param beanName bean名称
     */
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    /**
     * 获取bean类型
     * @return bean类型
     */
    public Class<?> getBeanType() {
        return beanType;
    }

    /**
     * 设置bean类型
     * @param beanType bean类型
     */
    public void setBeanType(Class<?> beanType) {
        this.beanType = beanType;
    }

    /**
     * 获取方法名称
     * @return 方法名称
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * 设置方法名称
     * @param methodName 方法名称
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * 设置排序值
     * @param order 排序值
     */
    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int getOrder() {
        return order;
    }

    /**
     * 获取指定异常进行方法回调
     * @return 指定异常进行方法回调
     */
    public Class<? extends Exception>[] getCauseException() {
        return causeException;
    }

    /**
     * 设置指定异常进行方法回调
     * @param causeException 指定异常进行方法回调
     */
    public void setCauseException(Class<? extends Exception>[] causeException) {
        this.causeException = causeException;
    }
}
