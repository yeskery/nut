package com.yeskery.nut.aop.handler;

import com.yeskery.nut.aop.ProxyMethodHandler;
import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文支持的代理方法处理器
 * @author sprout
 * @version 1.0
 * 2022-08-28 12:51
 */
public abstract class BaseApplicationSupportProxyMethodHandler implements ProxyMethodHandler {

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /**
     * 构建应用上下文支持的代理方法处理器
     * @param applicationContext 应用上下文
     */
    public BaseApplicationSupportProxyMethodHandler(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取应用上下文对象
     * @return 应用上下文对象
     */
    protected ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
