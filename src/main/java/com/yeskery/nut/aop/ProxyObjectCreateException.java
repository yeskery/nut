package com.yeskery.nut.aop;

import com.yeskery.nut.core.NutException;

/**
 * 代理对象创建异常
 * @author sprout
 * @version 1.0
 * 2022-08-28 12:25
 */
public class ProxyObjectCreateException extends NutException {

    /**
     * 构建一个 {@link ProxyObjectCreateException}
     */
    public ProxyObjectCreateException() {
    }

    /**
     * 构建一个 {@link ProxyObjectCreateException}
     * @param message message
     */
    public ProxyObjectCreateException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ProxyObjectCreateException}
     * @param message message
     * @param cause cause
     */
    public ProxyObjectCreateException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ProxyObjectCreateException}
     * @param cause cause
     */
    public ProxyObjectCreateException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ProxyObjectCreateException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ProxyObjectCreateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
