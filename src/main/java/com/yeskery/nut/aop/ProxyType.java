package com.yeskery.nut.aop;

/**
 * 代理类型
 * @author sprout
 * @version 1.0
 * 2022-08-29 00:37
 */
public enum ProxyType {
    /** 自动模式 */
    AUTO,
    /** JDK动态代理 */
    JDK,
    /** Cglib动态代理 */
    CGLIB
}
