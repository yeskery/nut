package com.yeskery.nut.aop;

/**
 * 目标方法执行接口
 * @author Yeskery
 * 2023/8/9
 */
public interface Execution {

    /**
     * 获取目标方法执行接口元数据
     * @return 目标方法执行接口元数据
     */
    ExecutionMetadata getExecutionMetadata();
}
