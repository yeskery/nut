package com.yeskery.nut.aop.aspect;

/**
 * 基于前缀匹配的连接点解析器
 * @author Yeskery
 * 2023/8/8
 */
public abstract class BasePrefixMatchJoinParser implements JoinPointParser {

    @Override
    public boolean support(String value) {
        return value.startsWith(getPrefix());
    }

    /**
     * 获取前缀值
     * @return 前缀值
     */
    protected abstract String getPrefix();
}
