package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.ExecutionJoinPoint;

import java.util.regex.Pattern;

/**
 * 基于方法匹配的连接点解析器
 * @author Yeskery
 * 2023/8/8
 */
public class ExecutionJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    public JoinPoint createJoinPoint(String value) {
        String express = getExpress(value);
        express = express.replace("(", "\\(");
        express = express.replace(")", "\\)");
        express = express.replace("*", "\\S+");
        express = express.replace(".", "\\.");
        express = express.replace(" ", "\\s*");
        return new ExecutionJoinPoint(Pattern.compile(express));
    }

    @Override
    protected String getPrefix() {
        return "execution";
    }
}
