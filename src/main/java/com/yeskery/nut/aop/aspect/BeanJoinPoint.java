package com.yeskery.nut.aop.aspect;

import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.lang.reflect.Method;

/**
 * bean连接点接口
 * @author Yeskery
 * 2023/8/8
 */
public interface BeanJoinPoint extends JoinPoint {

    /**
     * 是否是目标方法
     * @param metadata Bean扫描元数据
     * @param method 要判断的方法
     * @return 是否是目标方法
     */
    boolean isTargetMethod(BeanAnnotationScanMetadata metadata, Method method);
}
