package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.BeanNameJoinPoint;

/**
 * bean名称连接点解析器
 * @author Yeskery
 * 2023/8/4
 */
public class BeanNameJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    public JoinPoint createJoinPoint(String value) {
        return new BeanNameJoinPoint(getExpress(value));
    }

    @Override
    protected String getPrefix() {
        return "bean";
    }
}
