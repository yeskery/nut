package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.BeanJoinPoint;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * 逻辑与连接点
 * @author Yeskery
 * 2023/8/8
 */
public class LogicAndJoinPoint implements BeanJoinPoint {

    /** 代理的连接点集合 */
    private final Collection<JoinPoint> joinPoints;

    /**
     * 构建逻辑与连接点
     * @param joinPoints 代理的连接点集合
     */
    public LogicAndJoinPoint(Collection<JoinPoint> joinPoints) {
        this.joinPoints = joinPoints;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        for (JoinPoint joinPoint : joinPoints) {
            if (!joinPoint.isTargetMethod(method)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isTargetMethod(BeanAnnotationScanMetadata metadata, Method method) {
        for (JoinPoint joinPoint : joinPoints) {
            if (joinPoint instanceof BeanJoinPoint) {
                if (!(((BeanJoinPoint) joinPoint).isTargetMethod(metadata, method))) {
                    return false;
                }
            } else {
                if (!joinPoint.isTargetMethod(method)) {
                    return false;
                }
            }
        }
        return true;
    }
}
