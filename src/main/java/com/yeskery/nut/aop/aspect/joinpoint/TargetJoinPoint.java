package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.reflect.Method;

/**
 * 基于目标对象类型匹配的连接点
 * @author Yeskery
 * 2023/8/4
 */
public class TargetJoinPoint implements JoinPoint {

    /** 目标对象类型 */
    private final Class<?> clazz;

    /**
     * 构建基于目标对象类型匹配的连接点
     * @param clazz 目标对象类型
     */
    public TargetJoinPoint(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return clazz.isAssignableFrom(method.getDeclaringClass());
    }
}
