package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 基于方法参数匹配的连接点
 * @author Yeskery
 * 2023/8/8
 */
public class ArgsJoinPoint implements JoinPoint {

    /** 方法匹配的正则表达式 */
    private final Pattern pattern;

    /**
     * 构建基于方法匹配的连接点
     * @param pattern 方法匹配的正则表达式
     */
    public ArgsJoinPoint(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        String express = Arrays.stream(method.getParameters())
                .map(Parameter::getType)
                .map(Class::getName)
                .collect(Collectors.joining(",", "(", ")"));
        return pattern.matcher(express).matches();
    }
}
