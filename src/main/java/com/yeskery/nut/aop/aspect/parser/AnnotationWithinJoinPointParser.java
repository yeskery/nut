package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.AnnotationWithinJoinPoint;
import com.yeskery.nut.core.NutException;

import java.lang.annotation.Annotation;

/**
 * 基于目标对象类型存在某个注解的连接点解析器
 * @author Yeskery
 * 2023/8/4
 */
public class AnnotationWithinJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    @SuppressWarnings("unchecked")
    public JoinPoint createJoinPoint(String value) {
        String className = getExpress(value);
        Class<?> clazz = parseClass(className);
        if (!Annotation.class.isAssignableFrom(clazz)) {
            throw new NutException("Only Support Annotation Class, Current Class[" + className + "] Invalid.");
        }
        return new AnnotationWithinJoinPoint((Class<? extends Annotation>) parseClass(className));
    }

    @Override
    protected String getPrefix() {
        return "@within";
    }
}
