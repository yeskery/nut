package com.yeskery.nut.aop.aspect;

import java.lang.reflect.Method;

/**
 * 连接点接口
 * @author Yeskery
 * 2023/8/4
 */
public interface JoinPoint {

    /**
     * 是否是目标方法
     * @param method 要判断的方法
     * @return 是否是目标方法
     */
   boolean isTargetMethod(Method method);
}
