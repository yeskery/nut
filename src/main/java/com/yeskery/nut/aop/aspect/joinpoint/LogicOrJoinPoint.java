package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.BeanJoinPoint;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * 逻辑或连接点
 * @author Yeskery
 * 2023/8/8
 */
public class LogicOrJoinPoint implements BeanJoinPoint {

    /** 代理的连接点集合 */
    private final Collection<JoinPoint> joinPoints;

    /**
     * 构建逻辑或连接点
     * @param joinPoints 代理的连接点集合
     */
    public LogicOrJoinPoint(Collection<JoinPoint> joinPoints) {
        this.joinPoints = joinPoints;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        for (JoinPoint joinPoint : joinPoints) {
            if (joinPoint.isTargetMethod(method)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isTargetMethod(BeanAnnotationScanMetadata metadata, Method method) {
        for (JoinPoint joinPoint : joinPoints) {
            if (joinPoint instanceof BeanJoinPoint) {
                if ((((BeanJoinPoint) joinPoint).isTargetMethod(metadata, method))) {
                    return true;
                }
            } else {
                if (joinPoint.isTargetMethod(method)) {
                    return true;
                }
            }
        }
        return false;
    }
}
