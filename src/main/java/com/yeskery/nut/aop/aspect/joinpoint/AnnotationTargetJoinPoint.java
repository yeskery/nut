package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 基于目标对象类型注解是否匹配的连接点
 * @author Yeskery
 * 2023/8/4
 */
public class AnnotationTargetJoinPoint implements JoinPoint {

    /** 注解类型 */
    private final Class<? extends Annotation> annotationClass;

    /**
     * 构建基于目标对象类型注解是否匹配的连接点
     * @param annotationClass 注解类型
     */
    public AnnotationTargetJoinPoint(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return method.getDeclaringClass().getAnnotation(annotationClass) != null;
    }
}
