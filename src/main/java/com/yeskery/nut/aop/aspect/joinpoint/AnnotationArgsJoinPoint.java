package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;

/**
 * 基于方法参数类是否含有指定注解的连接点
 * @author Yeskery
 * 2023/8/8
 */
public class AnnotationArgsJoinPoint implements JoinPoint {

    /** 方法参数注解类型列表 */
    private final List<Class<? extends Annotation>> argAnnotationClasses;

    /**
     * 构建基于方法参数类是否含有指定注解的连接点
     * @param argAnnotationClasses 方法参数注解类型列表
     */
    public AnnotationArgsJoinPoint(List<Class<? extends Annotation>> argAnnotationClasses) {
        this.argAnnotationClasses = argAnnotationClasses;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        Parameter[] parameters = method.getParameters();
        if (parameters.length != argAnnotationClasses.size()) {
            return false;
        }
        for (int i = 0; i < parameters.length; i++) {
            Class<? extends Annotation> annotationClass = argAnnotationClasses.get(i);
            if (Annotation.class.equals(annotationClass)) {
                continue;
            }
            if (!parameters[i].getType().isAnnotationPresent(annotationClass)) {
                return false;
            }
        }
        return true;
    }
}
