package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.BeanJoinPoint;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.lang.reflect.Method;

/**
 * bean名称连接点
 * @author Yeskery
 * 2023/8/8
 */
public class BeanNameJoinPoint implements BeanJoinPoint {

    /** bean名称 */
    private final String beanName;

    /**
     * 构建bean名称连接点
     * @param beanName bean名称
     */
    public BeanNameJoinPoint(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public boolean isTargetMethod(BeanAnnotationScanMetadata metadata, Method method) {
        return metadata.getName().equals(beanName);
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return false;
    }
}
