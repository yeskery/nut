package com.yeskery.nut.aop.aspect;

import com.yeskery.nut.core.NutException;
import com.yeskery.nut.util.ClassLoaderUtils;

/**
 * 连接点解析器
 * @author Yeskery
 * 2023/8/4
 */
public interface JoinPointParser {

    /**
     * 获取连接点表达式
     * @param value 切点值
     * @return 连接点表达式
     */
    default String getExpress(String value) {
        return value.substring(value.indexOf("(") + 1, value.lastIndexOf(")"));
    }

    /**
     * 将类名转换为class对象
     * @param className 类名
     * @return class对象
     */
    default Class<?> parseClass(String className) {
        try {
            return Class.forName(className, false, ClassLoaderUtils.getClassLoader());
        } catch (ClassNotFoundException e) {
            throw new NutException("Can Not Found Class[" + className + "].");
        }
    }

    /**
     * 是否支持当前的连接点
     * @param value 切点值
     * @return 是否支持当前的连接点
     */
    boolean support(String value);

    /**
     * 创建新的连接点
     * @param value 切点值
     * @return 连接点对象
     */
    JoinPoint createJoinPoint(String value);
}
