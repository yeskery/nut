package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * 基于方法匹配的连接点
 * @author Yeskery
 * 2023/8/8
 */
public class ExecutionJoinPoint implements JoinPoint {

    /** 方法匹配的正则表达式 */
    private final Pattern pattern;

    /**
     * 构建基于方法匹配的连接点
     * @param pattern 方法匹配的正则表达式
     */
    public ExecutionJoinPoint(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return pattern.matcher(method.toString()).matches();
    }
}
