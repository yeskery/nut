package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.WithinJoinPoint;

/**
 * 基于目标对象类型完全一致的连接点解析器
 * @author Yeskery
 * 2023/8/4
 */
public class WithinJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    public JoinPoint createJoinPoint(String value) {
        String className = getExpress(value);
        return new WithinJoinPoint(parseClass(className));
    }

    @Override
    protected String getPrefix() {
        return "within";
    }
}
