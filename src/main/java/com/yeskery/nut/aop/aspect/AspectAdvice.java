package com.yeskery.nut.aop.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 切面增强点
 * @author Yeskery
 * 2023/8/7
 */
public class AspectAdvice {

    /** 切面注解 */
    private Annotation annotation;

    /** 要执行的方法 */
    private Method method;

    /** 连接点 */
    private JoinPoint joinPoint;

    /** bean名称 */
    private String beanName;

    /** bean类型 */
    private Class<?> beanType;

    /**
     * 获取切面注解
     * @return 切面注解
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * 设置切面注解
     * @param annotation 切面注解
     */
    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

    /**
     * 获取要执行的方法
     * @return 要执行的方法
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置要执行的方法
     * @param method 要执行的方法
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * 获取连接点
     * @return 连接点
     */
    public JoinPoint getJoinPoint() {
        return joinPoint;
    }

    /**
     * 设置连接点
     * @param joinPoint 连接点
     */
    public void setJoinPoint(JoinPoint joinPoint) {
        this.joinPoint = joinPoint;
    }

    /**
     * 获取bean名称
     * @return bean名称
     */
    public String getBeanName() {
        return beanName;
    }

    /**
     * 设置bean名称
     * @param beanName bean名称
     */
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    /**
     * 获取bean类型
     * @return bean类型
     */
    public Class<?> getBeanType() {
        return beanType;
    }

    /**
     * 设置bean类型
     * @param beanType bean类型
     */
    public void setBeanType(Class<?> beanType) {
        this.beanType = beanType;
    }
}
