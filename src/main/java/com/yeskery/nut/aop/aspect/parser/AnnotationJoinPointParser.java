package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.AnnotationJoinPoint;

import java.lang.annotation.Annotation;

/**
 * 注解连接点解析器
 * @author Yeskery
 * 2023/8/4
 */
public class AnnotationJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    @SuppressWarnings("unchecked")
    public JoinPoint createJoinPoint(String value) {
        String className = getExpress(value);
        Class<?> clazz = parseClass(className);
        if (!Annotation.class.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException("Only Support Annotation Class, Current Class[" + className + "] Invalid.");
        }
        return new AnnotationJoinPoint((Class<? extends Annotation>) clazz);
    }

    @Override
    protected String getPrefix() {
        return "@annotation";
    }
}
