package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.BeanJoinPoint;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.lang.reflect.Method;

/**
 * 反转的连接点
 * @author Yeskery
 * 2023/8/8
 */
public class ReverseJoinPoint implements BeanJoinPoint {

    /** 代理的连接点 */
    private final JoinPoint joinPoint;

    /**
     * 构建反转的连接点
     * @param joinPoint 代理的连接点
     */
    public ReverseJoinPoint(JoinPoint joinPoint) {
        this.joinPoint = joinPoint;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return !joinPoint.isTargetMethod(method);
    }

    @Override
    public boolean isTargetMethod(BeanAnnotationScanMetadata metadata, Method method) {
        if (joinPoint instanceof BeanJoinPoint) {
            return !(((BeanJoinPoint) joinPoint).isTargetMethod(metadata, method));
        }
        return isTargetMethod(method);
    }
}
