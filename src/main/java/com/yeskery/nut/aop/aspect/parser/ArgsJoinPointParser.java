package com.yeskery.nut.aop.aspect.parser;

import com.yeskery.nut.aop.aspect.BasePrefixMatchJoinParser;
import com.yeskery.nut.aop.aspect.JoinPoint;
import com.yeskery.nut.aop.aspect.joinpoint.ArgsJoinPoint;

import java.util.regex.Pattern;

/**
 * 基于方法参数匹配的连接点解析器
 * @author Yeskery
 * 2023/8/8
 */
public class ArgsJoinPointParser extends BasePrefixMatchJoinParser {

    @Override
    public JoinPoint createJoinPoint(String value) {
        String express = getExpress(value);
        express = "\\(" + express + "\\)";
        express = express.replace("*", "\\S+");
        express = express.replace(".", "\\.");
        return new ArgsJoinPoint(Pattern.compile(express));
    }

    @Override
    protected String getPrefix() {
        return "args";
    }
}
