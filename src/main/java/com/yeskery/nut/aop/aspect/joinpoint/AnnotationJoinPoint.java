package com.yeskery.nut.aop.aspect.joinpoint;

import com.yeskery.nut.aop.aspect.JoinPoint;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 基于注解的连接点
 * @author Yeskery
 * 2023/8/4
 */
public class AnnotationJoinPoint implements JoinPoint {

    /** 注解的类型 */
    private final Class<? extends Annotation> annotationClass;

    /**
     * 构建基于注解的连接点
     * @param annotationClass 注解的类型
     */
    public AnnotationJoinPoint(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    @Override
    public boolean isTargetMethod(Method method) {
        return method.isAnnotationPresent(annotationClass);
    }
}
