package com.yeskery.nut.aop;

import java.lang.reflect.Method;
import java.util.function.Supplier;

/**
 * 默认的目标调用方法
 * @author sprout
 * @version 1.0
 * 2023-04-08 23:35
 */
public class DefaultInvocation extends DefaultExecution implements Invocation {

    /** 目标方法执行结果的提供器 */
    private final Supplier<Object> resultSupplier;

    /** 是否已经调用目标方法 */
    private boolean isInvoked = false;

    /** 目标方法执行结果 */
    private Object result;

    /**
     * 构建一个默认的目标调用方法对象
     * @param method 原始方法
     * @param methodParameters 原始方法参数
     * @param object 原始对象
     * @param proxyObject 代理对象
     * @param resultSupplier 目标方法执行结果的提供器
     */
    public DefaultInvocation(Method method, Object[] methodParameters, Object object, Object proxyObject, Supplier<Object> resultSupplier) {
        super(method, methodParameters, object, proxyObject);
        this.resultSupplier = resultSupplier;
    }

    @Override
    public Object invoke() {
        if (isInvoked) {
            return result;
        }
        result = resultSupplier.get();
        isInvoked = true;
        return result;
    }

    /**
     * 是否已经调用目标方法
     * @return 是否已经调用目标方法
     */
    public boolean isInvoked() {
        return isInvoked;
    }

    /**
     * 获取目标方法执行结果
     * @return 目标方法执行结果
     */
    public Object getResult() {
        return isInvoked() ? result : resultSupplier.get();
    }
}
