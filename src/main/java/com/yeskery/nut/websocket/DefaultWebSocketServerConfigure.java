package com.yeskery.nut.websocket;

import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.PathMetadata;
import com.yeskery.nut.util.PathUtils;

/**
 * 默认的WebSocket服务端配置项对象
 * @author sprout
 * @version 1.0
 * 2023-04-15 22:53
 */
public class DefaultWebSocketServerConfigure implements WebSocketServerConfigure {

    /** 路径元数据 */
    private final PathMetadata pathMetadata;

    /** 是否是动态路径 */
    private boolean dynamic;

    /**
     * 构建默认的WebSocket服务端配置项对象
     * @param path 请求路径
     */
    public DefaultWebSocketServerConfigure(String path) {
        pathMetadata = PathUtils.parse(path);
        if (pathMetadata == null) {
            throw new NutException("Path Must Not Be Null.");
        }
        if (pathMetadata.isDynamic()) {
            dynamic = true;
        }
    }

    @Override
    public String getPath() {
        return pathMetadata.getBasePath();
    }

    @Override
    public boolean isDynamic() {
        return dynamic;
    }

    @Override
    public PathMetadata getPathMetadata() {
        return pathMetadata;
    }
}
