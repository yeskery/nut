package com.yeskery.nut.websocket;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

/**
 * WebSocket会话接口
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:16
 */
public interface Session {

    /**
     * 获取会话id
     * @return 会话id
     */
    String getId();

    /**
     * 获取原始会话对象
     * @return 原始会话对象
     */
    Object getOriginalSession();

    /**
     * 获取请求路径
     * @return 请求路径
     */
    String getRequestPath();

    /**
     * 获取请求参数map
     * @return 请求参数map
     */
    Map<String, List<String>> getRequestParameterMap();

    /**
     * 获取请求参数值
     * @param name 参数名
     * @return 参数值
     */
    default List<String> getRequestParameters(String name) {
        return getRequestParameterMap().get(name);
    }

    /**
     * 获取请求参数值
     * @param name 参数名
     * @return 参数值
     */
    default String getRequestParameter(String name) {
        List<String> parameters = getRequestParameters(name);
        if (parameters == null || parameters.isEmpty()) {
            return null;
        }
        return parameters.get(0);
    }

    /**
     * 获取url参数字符串
     * @return url参数字符串
     */
    String getQueryString();

    /**
     * 获取路径参数map
     * @return 路径参数map
     */
    Map<String, String> getPathParameters();

    /**
     * 获取路径参数
     * @param name 路径参数名
     * @return 路径参数值
     */
    default String getPathParameter(String name) {
        return getPathParameters().get(name);
    }

    /**
     * 发送文本内容
     * @param text 文本内容
     */
    void sendText(String text);

    /**
     * 发送二进制内容
     * @param data 二进制内容
     */
    void sendBinary(ByteBuffer data);

    /**
     * 发送字节数组
     * @param bytes 字节数组
     */
    void sendBytes(byte[] bytes);

    /**
     * 关闭会话
     */
    void close();
}
