package com.yeskery.nut.websocket;

import java.util.Collection;

/**
 * WebSocket注册工厂
 * @author sprout
 * @version 1.0
 * 2023-04-16 00:58
 */
public interface WebSocketRegisterFactory {

    /**
     * 注册WebSocket
     * @param webSocketConfiguration WebSocket配置对象
     */
    void register(WebSocketConfiguration webSocketConfiguration);

    /**
     * 批量注册WebSocket
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    void registerAll(Collection<WebSocketConfiguration> webSocketConfigurations);

    /**
     * 获取注册的WebSocket配置对象集合
     * @return 注册的WebSocket配置对象集合
     */
    Collection<WebSocketConfiguration> getWebSocketConfigurations();
}
