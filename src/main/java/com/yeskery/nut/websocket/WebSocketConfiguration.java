package com.yeskery.nut.websocket;

/**
 * WebSocket配置对象
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:30
 */
public interface WebSocketConfiguration {

    /**
     * 获取WebSocket服务端配置项对象
     * @return WebSocket服务端配置项对象
     */
    WebSocketServerConfigure getWebSocketServerConfigure();

    /**
     * 获取WebSocket处理器
     * @return WebSocket处理器
     */
    WebSocketHandler getWebSocketHandler();
}
