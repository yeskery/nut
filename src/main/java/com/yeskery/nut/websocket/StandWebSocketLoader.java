package com.yeskery.nut.websocket;

import javax.websocket.server.ServerContainer;
import java.util.Collection;

/**
 * 标准WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 20:48
 */
public interface StandWebSocketLoader {

    /**
     * 注册WebSocket服务端点
     * @param serverContainer WebSocket
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    default void registerEndpoints(ServerContainer serverContainer, Collection<WebSocketConfiguration> webSocketConfigurations) {
    }
}
