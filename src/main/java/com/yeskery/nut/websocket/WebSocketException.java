package com.yeskery.nut.websocket;

import com.yeskery.nut.core.NutException;

/**
 * WebSocket异常
 * @author sprout
 * @version 1.0
 * 2023-04-16 13:11
 */
public class WebSocketException extends NutException {

    /**
     * 构建一个 {@link WebSocketException}
     */
    public WebSocketException() {
    }

    /**
     * 构建一个 {@link WebSocketException}
     * @param message message
     */
    public WebSocketException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link WebSocketException}
     * @param message message
     * @param cause cause
     */
    public WebSocketException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link WebSocketException}
     * @param cause cause
     */
    public WebSocketException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link WebSocketException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public WebSocketException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
