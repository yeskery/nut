package com.yeskery.nut.websocket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

/**
 * JavaxWebSocket会话包装对象
 * @author sprout
 * @version 1.0
 * 2023-04-16 13:08
 */
public class JavaxWebSocketSession implements Session {

    /** JavaxWebSocket会话对象 */
    private final javax.websocket.Session session;

    /**
     * 构建JavaxWebSocket会话包装对象
     * @param session JavaxWebSocket会话对象
     */
    public JavaxWebSocketSession(javax.websocket.Session session) {
        this.session = session;
    }

    @Override
    public String getId() {
        return session.getId();
    }

    @Override
    public String getRequestPath() {
        return session.getRequestURI().getPath();
    }

    @Override
    public Map<String, List<String>> getRequestParameterMap() {
        return session.getRequestParameterMap();
    }

    @Override
    public String getQueryString() {
        return session.getQueryString();
    }

    @Override
    public Map<String, String> getPathParameters() {
        return session.getPathParameters();
    }

    @Override
    public Object getOriginalSession() {
        return session;
    }

    @Override
    public void sendText(String text) {
        try {
            session.getBasicRemote().sendText(text);
        } catch (IOException e) {
            throw new WebSocketException(e);
        }
    }

    @Override
    public void sendBinary(ByteBuffer data) {
        try {
            session.getBasicRemote().sendBinary(data);
        } catch (IOException e) {
            throw new WebSocketException(e);
        }
    }

    @Override
    public void sendBytes(byte[] bytes) {
        sendBinary(ByteBuffer.wrap(bytes));
    }

    @Override
    public void close() {
        try {
            session.close();
        } catch (IOException e) {
            throw new WebSocketException(e);
        }
    }
}
