package com.yeskery.nut.websocket;

/**
 * WebSocket配置管理器
 * @author sprout
 * @version 1.0
 * 2023-04-22 16:03
 */
public interface WebSocketConfigurationManager {

    /**
     * 查找websocket配置对象
     * @param path 请求路径
     * @return websocket配置对象
     */
    WebSocketConfiguration findWebSocketConfiguration(String path);
}
