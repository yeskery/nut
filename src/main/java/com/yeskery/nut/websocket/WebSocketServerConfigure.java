package com.yeskery.nut.websocket;

import com.yeskery.nut.core.PathMetadata;

/**
 * WebSocket服务端配置项对象
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:27
 */
public interface WebSocketServerConfigure {

    /**
     * 获取WebSocket的路径
     * @return WebSocket的路径
     */
    String getPath();

    /**
     * 是否是动态路径
     * @return 是否是动态路径
     */
    boolean isDynamic();

    /**
     * 获取路径元数据
     * @return 路径元数据
     */
    PathMetadata getPathMetadata();
}
