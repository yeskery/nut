package com.yeskery.nut.websocket;

import com.yeskery.nut.core.NutException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 默认的WebSocket注册工厂
 * @author sprout
 * @version 1.0
 * 2023-04-16 01:01
 */
public class DefaultWebSocketRegisterFactory implements WebSocketRegisterFactory {

    /** WebSocket配置对象集合 */
    private final List<WebSocketConfiguration> webSocketConfigurationList = new ArrayList<>();

    @Override
    public void register(WebSocketConfiguration webSocketConfiguration) {
        if (webSocketConfiguration == null) {
            throw new NutException("webSocketConfiguration Must Not Be Null.");
        }
        webSocketConfigurationList.add(webSocketConfiguration);
    }

    @Override
    public void registerAll(Collection<WebSocketConfiguration> webSocketConfigurations) {
        webSocketConfigurationList.addAll(webSocketConfigurations);
    }

    @Override
    public Collection<WebSocketConfiguration> getWebSocketConfigurations() {
        return webSocketConfigurationList;
    }
}
