package com.yeskery.nut.websocket;

/**
 * WebSocket处理器
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:22
 */
public interface WebSocketHandler {

    /** 连接创建时的处理方法名 */
    String ON_OPEN_METHOD_NAME = "onOpen";

    /** 连接关闭时的处理方法名 */
    String ON_CLOSE_METHOD_NAME = "onClose";

    /** 收到客户端消息的处理方法名 */
    String ON_MESSAGE_METHOD_NAME = "onMessage";

    /** 连接发生异常的处理方法名 */
    String ON_ERROR_METHOD_NAME = "onError";


    /**
     * 连接创建的回调方法
     * @param session WebSocket会话对象
     */
    default void onOpen(Session session) {
    }

    /**
     * 连接关闭的回调方法
     * @param session WebSocket会话对象
     */
    default void onClose(Session session) {
    }

    /**
     * 接收客户端文本消息的回调方法
     * @param text 文本内容
     * @param session WebSocket会话对象
     */
    default void onMessage(String text, Session session) {

    }

    /**
     * 接收客户端二进制消息的回调方法
     * @param bytes 二进制内容
     * @param session WebSocket会话对象
     */
    default void onMessage(byte[] bytes, Session session) {

    }

    /**
     * 连接发生错误的回调方法
     * @param session WebSocket会话对象
     * @param throwable 发生错误的异常对象
     */
    default void onError(Session session, Throwable throwable) {
    }
}
