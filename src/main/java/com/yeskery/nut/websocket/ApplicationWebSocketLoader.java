package com.yeskery.nut.websocket;

import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 18:50
 */
public class ApplicationWebSocketLoader implements StandWebSocketLoader {

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /**
     * 构建应用上下文WebSocket加载器
     * @param applicationContext 应用上下文
     */
    public ApplicationWebSocketLoader(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取应用上下文
     * @return 应用上下文
     */
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
