package com.yeskery.nut.websocket;

import com.yeskery.nut.core.DynamicRegisterMetadata;
import com.yeskery.nut.core.PathMatcher;

import java.util.Arrays;
import java.util.Objects;

/**
 * 动态WebSocket配置注册元数据
 * @author sprout
 * @version 1.0
 * 2023-04-15 23:49
 */
public class DynamicWebSocketConfigurationRegisterMetadata extends DynamicRegisterMetadata {

    /** WebSocket配置对象 */
    private WebSocketConfiguration webSocketConfiguration;

    /**
     * 构建动态WebSocket配置注册元数据
     * @param pathParameterNames 路径参数名称数组
     * @param pathMatcher 路径参数匹配器
     * @param webSocketConfiguration WebSocket配置对象
     */
    public DynamicWebSocketConfigurationRegisterMetadata(String[] pathParameterNames, PathMatcher pathMatcher, WebSocketConfiguration webSocketConfiguration) {
        super(pathParameterNames, pathMatcher);
        this.webSocketConfiguration = webSocketConfiguration;
    }

    /**
     * 构建WebSocket注册元数据
     * @param webSocketConfiguration WebSocket配置对象
     */
    public DynamicWebSocketConfigurationRegisterMetadata(WebSocketConfiguration webSocketConfiguration) {
        this.webSocketConfiguration = webSocketConfiguration;
    }

    /**
     * 获取WebSocket配置对象
     * @return WebSocket配置对象
     */
    public WebSocketConfiguration getWebSocketConfiguration() {
        return webSocketConfiguration;
    }

    /**
     * 设置WebSocket配置对象
     * @param webSocketConfiguration WebSocket配置对象
     */
    public void setWebSocketConfiguration(WebSocketConfiguration webSocketConfiguration) {
        this.webSocketConfiguration = webSocketConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DynamicWebSocketConfigurationRegisterMetadata that = (DynamicWebSocketConfigurationRegisterMetadata) o;
        return Arrays.equals(getPathParameterNames(), that.getPathParameterNames())
                && Objects.equals(getPathMatcher(), that.getPathMatcher())
                && Objects.equals(webSocketConfiguration, that.webSocketConfiguration);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getPathMatcher(), webSocketConfiguration);
        result = 31 * result + Arrays.hashCode(getPathParameterNames());
        return result;
    }
}
