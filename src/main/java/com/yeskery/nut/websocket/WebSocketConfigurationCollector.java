package com.yeskery.nut.websocket;

import java.util.Collection;

/**
 * WebSocket配置收集器
 * @author sprout
 * @version 1.0
 * 2023-04-15 23:03
 */
@FunctionalInterface
public interface WebSocketConfigurationCollector {

    /**
     * 获取收集的WebSocket配置对象集合
     * @return 收集的WebSocket配置对象集合
     */
    Collection<WebSocketConfiguration> getWebSocketConfigurations();
}
