package com.yeskery.nut.websocket;

import java.util.Collection;

/**
 * WebSocket服务接口
 * @author sprout
 * @version 1.0
 * 2023-04-16 01:06
 */
public interface WebSocketServer {

    /**
     * 服务是否支持WebSocket
     * @return 服务是否支持WebSocket
     */
    default boolean support() {
        return true;
    }

    /**
     * 是否启用WebSocket
     * @return 是否启用WebSocket
     */
    default boolean enable() {
        return true;
    }

    /**
     * 检查服务所需的依赖是否存在，如果不存在可抛出异常来终止Nut的运行
     * @throws Exception 抛出的异常
     */
    void checkDependExist() throws Exception;

    /**
     * 注册WebSocket服务端点
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    void registerEndpoints(Collection<WebSocketConfiguration> webSocketConfigurations);
}
