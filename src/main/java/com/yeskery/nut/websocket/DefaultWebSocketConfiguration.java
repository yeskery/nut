package com.yeskery.nut.websocket;

/**
 * 默认的WebSocket配置对象
 * @author sprout
 * @version 1.0
 * 2023-04-15 21:32
 */
public class DefaultWebSocketConfiguration implements WebSocketConfiguration {

    /** WebSocket服务端配置项对象 */
    private WebSocketServerConfigure webSocketServerConfigure;

    /** WebSocket处理器 */
    private WebSocketHandler webSocketHandler;

    @Override
    public WebSocketServerConfigure getWebSocketServerConfigure() {
        return webSocketServerConfigure;
    }

    @Override
    public WebSocketHandler getWebSocketHandler() {
        return webSocketHandler;
    }

    /**
     * 设置WebSocket服务端配置项对象
     * @param webSocketServerConfigure WebSocket服务端配置项对象
     */
    public void setWebSocketServerConfigure(WebSocketServerConfigure webSocketServerConfigure) {
        this.webSocketServerConfigure = webSocketServerConfigure;
    }

    /**
     * 设置WebSocket处理器
     * @param webSocketHandler WebSocket处理器
     */
    public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
        this.webSocketHandler = webSocketHandler;
    }
}
