package com.yeskery.nut.websocket;

import java.util.Collection;

/**
 * WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-22 15:45
 */
public interface WebSocketLoader {

    /**
     * 注册WebSocket服务端点
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    default void registerEndpoints(Collection<WebSocketConfiguration> webSocketConfigurations) {
    }
}
