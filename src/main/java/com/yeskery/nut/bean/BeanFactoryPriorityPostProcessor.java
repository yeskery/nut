package com.yeskery.nut.bean;

/**
 * BeanFactory的优先级后置处理器
 * @author sprout
 * @version 1.0
 * 2022-08-29 11:36
 */
@FunctionalInterface
public interface BeanFactoryPriorityPostProcessor extends BeanFactoryPostProcessor {
}
