package com.yeskery.nut.bean;

/**
 * Bean属性对象
 * @author sprout
 * 2022-06-21 10:25
 */
public class BeanAttribute {

    /** 属性名称 */
    private String name;

    /** 是否必须属性 */
    private boolean required;

    /** 属性类型 */
    private Class<?> type;

    /**
     * 获取属性名称
     * @return 属性名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置属性名称
     * @param name 属性名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 是否必须属性
     * @return 是否必须属性
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * 设置是否必须属性
     * @param required 是否必须属性
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * 获取属性类型
     * @return 属性类型
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 设置属性类型
     * @param type 属性类型
     */
    public void setType(Class<?> type) {
        this.type = type;
    }
}