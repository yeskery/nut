package com.yeskery.nut.bean;

import java.lang.reflect.Field;
import java.util.function.Supplier;

/**
 * 延迟注入Bean定义对象，该对象用于记录{@link com.yeskery.nut.annotation.bean.LazyInject}注解所定义的Bean信息
 * @author YESKERY
 * 2024/10/31
 */
public class LazyInjectBeanDefinition {

    /** bean对象 */
    private Object bean;

    /** {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象 */
    private Field field;

    /** {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象提供器 */
    private Supplier<Object> lazyInjectSupplier;

    /**
     * 获取bean对象
     * @return bean对象
     */
    public Object getBean() {
        return bean;
    }

    /**
     * 设置bean对象
     * @param bean bean对象
     */
    public void setBean(Object bean) {
        this.bean = bean;
    }

    /**
     * 获取{@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象
     * @return {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象
     */
    public Field getField() {
        return field;
    }

    /**
     * 设置{@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象
     * @param field {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象
     */
    public void setField(Field field) {
        this.field = field;
    }

    /**
     * 获取{@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象提供器
     * @return {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象提供器
     */
    public Supplier<Object> getLazyInjectSupplier() {
        return lazyInjectSupplier;
    }

    /**
     * 设置{@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象提供器
     * @param lazyInjectSupplier {@link com.yeskery.nut.annotation.bean.LazyInject}注解定义的字段对象提供器
     */
    public void setLazyInjectSupplier(Supplier<Object> lazyInjectSupplier) {
        this.lazyInjectSupplier = lazyInjectSupplier;
    }
}
