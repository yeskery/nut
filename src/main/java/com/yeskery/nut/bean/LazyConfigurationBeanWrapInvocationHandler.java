package com.yeskery.nut.bean;

import com.yeskery.nut.scan.ConfigurationBeanAnnotationScanMetadata;
import com.yeskery.nut.scan.bean.ConfigurationBeanCreator;

/**
 * Configuration Bean注解初始化注入的延迟加载包装拦截处理器
 * @author Yeskery
 * 2023/8/16
 */
public class LazyConfigurationBeanWrapInvocationHandler extends BaseLazyWrapInvocationHandler {

    /** Configuration 注解Bean创建类 */
    private final ConfigurationBeanCreator configurationBeanCreator;

    /** Configuration注解的Bean元数据 */
    private final ConfigurationBeanAnnotationScanMetadata configurationBeanAnnotationMetadata;

    /**
     * 构建Configuration Bean注解初始化注入的延迟加载包装拦截处理器
     * @param configurationBeanCreator Configuration 注解Bean创建类
     * @param configurationBeanAnnotationMetadata Configuration注解的Bean元数据
     */
    public LazyConfigurationBeanWrapInvocationHandler(ConfigurationBeanCreator configurationBeanCreator,
                                                      ConfigurationBeanAnnotationScanMetadata configurationBeanAnnotationMetadata) {
        this.configurationBeanCreator = configurationBeanCreator;
        this.configurationBeanAnnotationMetadata = configurationBeanAnnotationMetadata;
    }

    @Override
    protected Object getBean() {
        return configurationBeanCreator.doCreateBean(configurationBeanAnnotationMetadata, false);
    }
}
