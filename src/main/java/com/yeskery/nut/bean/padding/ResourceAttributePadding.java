package com.yeskery.nut.bean.padding;

import com.yeskery.nut.annotation.bean.Lazy;
import com.yeskery.nut.annotation.bean.LazyInject;
import com.yeskery.nut.bean.*;
import com.yeskery.nut.util.BeanUtils;
import com.yeskery.nut.util.StringUtils;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Resource注解属性填充对象
 * @author sprout
 * @version 1.0
 * 2022-11-18 13:34
 */
public class ResourceAttributePadding extends AbstractAttributePadding {

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /** 延迟注入集合 */
    private final List<LazyInjectBeanDefinition> lazyInjectBeanDefinitionList;

    /**
     * 构建Autowired注解属性填充对象
     * @param applicationContext Autowired注解属性填充对象
     * @param lazyInjectBeanDefinitionList 延迟注入集合
     */
    public ResourceAttributePadding(ApplicationContext applicationContext, List<LazyInjectBeanDefinition> lazyInjectBeanDefinitionList) {
        this.applicationContext = applicationContext;
        this.lazyInjectBeanDefinitionList = lazyInjectBeanDefinitionList;
    }

    @Override
    public void padding(Object object, Field[] fields) {
        for (Field field : fields) {
            if (field.isAnnotationPresent(LazyInject.class)) {
                LazyInjectBeanDefinition lazyInjectBeanDefinition = new LazyInjectBeanDefinition();
                lazyInjectBeanDefinition.setBean(object);
                lazyInjectBeanDefinition.setField(field);
                lazyInjectBeanDefinition.setLazyInjectSupplier(() -> getPaddingValue(field));
                lazyInjectBeanDefinitionList.add(lazyInjectBeanDefinition);
            } else {
                try {
                    Object value = getPaddingValue(field);
                    if (value != null) {
                        setObjectFileValue(object, field, value);
                    }
                } catch (Exception e) {
                    throw new BeanException("Bean's Attribute [ " + field.getType().getName() + "] Name [" + field.getName() + "] Padding Fail.", e);
                }
            }
        }
    }

    /**
     * 获取填充属性值
     * @param field 属性
     * @return 填充值
     */
    private Object getPaddingValue(Field field) {
        Resource resource = field.getAnnotation(Resource.class);
        String name = StringUtils.isEmpty(resource.name()) ? field.getName() : resource.name();
        Lazy lazy = field.getAnnotation(Lazy.class);
        try {
            Object value;
            if (lazy != null && lazy.value()) {
                value = ((BaseApplicationContext) applicationContext).getProxyObjectContext().createProxyObject(
                        field.getType(), new LazyInjectWrapInvocationHandler(applicationContext, field.getType(), name));
            } else {
                value = BeanUtils.getDependBeanObject(applicationContext, name, field.getGenericType());
            }
            if (!field.getType().isAssignableFrom(value.getClass())) {
                throw new BeanException("JDK Proxy Bean Object Only Support Interface Inject, " +
                        "If Need Support Class Inject, Need To Introduce Additional Cglib Dependencies.");
            }
            return value;
        } catch (NoSuchBeanException e) {
            return null;
        }
    }
}
