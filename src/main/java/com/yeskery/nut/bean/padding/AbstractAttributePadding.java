package com.yeskery.nut.bean.padding;

import com.yeskery.nut.bean.AttributePadding;
import com.yeskery.nut.util.ReflectUtils;

import java.lang.reflect.Field;

/**
 * 抽象的属性填充类
 * @author sprout
 * @version 1.0
 * 2022-07-09 14:50
 */
public abstract class AbstractAttributePadding implements AttributePadding {

    /**
     * 设置对象字段值
     * @param object 对象
     * @param field 字段
     * @param value 字段值
     * @throws Exception 异常
     */
    protected void setObjectFileValue(Object object, Field field, Object value) throws Exception {
        ReflectUtils.setObjectFieldValue(object, field, value);
    }

}
