package com.yeskery.nut.bean;

import java.lang.reflect.Field;

/**
 * 属性填充接口
 * @author sprout
 * @version 1.0
 * 2022-07-09 14:46
 */
@FunctionalInterface
public interface AttributePadding {

    /**
     * 属性填充
     * @param object 对象
     * @param fields 需要处理的字段
     */
    void padding(Object object, Field[] fields);
}
