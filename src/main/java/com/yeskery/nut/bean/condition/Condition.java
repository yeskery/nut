package com.yeskery.nut.bean.condition;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;

/**
 * Bean条件接口
 * @author sunjay
 * 2023/9/6
 */
public interface Condition {

    /**
     * 判断bean是否匹配的方法
     * @param nutApplication nut应用对象
     * @param beanAnnotationScanMetadata 所有Bean扫描元数据类型
     * @param conditionalAnnotation 条件注解对象
     * @return 是否匹配
     */
    boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation);
}
