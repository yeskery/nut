package com.yeskery.nut.bean.condition;

import com.yeskery.nut.annotation.condition.ConditionalOnMissingPlugin;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * 插件不存在条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnMissingPluginCondition extends OnPluginCondition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnMissingPluginCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        ConditionalOnMissingPlugin conditionalOnMissingPlugin = (ConditionalOnMissingPlugin) conditionalAnnotation;
        boolean result = !doMatches(nutApplication, beanAnnotationScanMetadata, conditionalOnMissingPlugin.value());
        if (!result && logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Plugin Missing Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Plugin Not Found[" + Arrays.stream(conditionalOnMissingPlugin.value()).map(Class::getName).collect(Collectors.joining(", "))
                    + "], Actual Plugin Found.");
        }
        return result;
    }
}
