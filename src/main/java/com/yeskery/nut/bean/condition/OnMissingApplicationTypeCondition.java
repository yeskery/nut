package com.yeskery.nut.bean.condition;

import com.yeskery.nut.annotation.condition.ConditionalOnMissingApplicationType;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 应用类型条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnMissingApplicationTypeCondition extends OnApplicationTypeCondition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnMissingApplicationTypeCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        ConditionalOnMissingApplicationType conditionalOnMissingApplicationType = (ConditionalOnMissingApplicationType) conditionalAnnotation;
        boolean result = !doMatches(nutApplication, conditionalOnMissingApplicationType.value());
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "ApplicationType Missing Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Missing ApplicationType[" + conditionalOnMissingApplicationType.value().name()
                    + "], Actual ApplicationType[" + nutApplication.getApplicationMetadata().getApplicationType().name() + "]");
        }
        return result;
    }
}
