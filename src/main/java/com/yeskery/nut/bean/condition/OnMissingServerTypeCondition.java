package com.yeskery.nut.bean.condition;

import com.yeskery.nut.annotation.condition.ConditionalOnMissingServerType;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 服务类型条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnMissingServerTypeCondition extends OnServerTypeCondition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnMissingServerTypeCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        ConditionalOnMissingServerType conditionalOnMissingServerType = (ConditionalOnMissingServerType) conditionalAnnotation;
        boolean result = !doMatches(nutApplication, conditionalOnMissingServerType.value());
        if (!result && logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "ServerType Missing Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Missing ServerType[" + conditionalOnMissingServerType.value().name()
                    + "], Actual ServerType[" + nutApplication.getApplicationMetadata().getServerType().name() + "]");
        }
        return result;
    }
}
