package com.yeskery.nut.bean.condition;

import com.yeskery.nut.annotation.condition.ConditionalOnMissingBean;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * bean不存在条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnMissingBeanCondition extends OnBeanCondition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnMissingBeanCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        ConditionalOnMissingBean conditionalOnMissingBean = (ConditionalOnMissingBean) conditionalAnnotation;
        boolean result = !doMatches(nutApplication, beanAnnotationScanMetadata, conditionalOnMissingBean.value(), conditionalOnMissingBean.name());
        if (!result && logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Bean Missing Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Bean Not Found[class(" + Arrays.stream(conditionalOnMissingBean.value()).map(Class::getName).collect(Collectors.joining(", "))
                    + "), name(" + String.join(", ", conditionalOnMissingBean.name()) + ")], Actual Bean Found.");
        }
        return result;
    }
}
