package com.yeskery.nut.bean.condition;

import com.yeskery.nut.annotation.condition.ConditionalOnMissingClass;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * 类不存在条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnMissingClassCondition extends OnClassCondition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnClassCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        ConditionalOnMissingClass conditionalOnMissingClass = (ConditionalOnMissingClass) conditionalAnnotation;
        boolean result = !doMatches(conditionalOnMissingClass.value(), conditionalOnMissingClass.name());
        if (!result && logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Class Missing Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Class Not Found[class(" + Arrays.stream(conditionalOnMissingClass.value()).map(Class::getName).collect(Collectors.joining(", "))
                    + "), name(" + String.join(", ", conditionalOnMissingClass.name()) + ")], Actual Class Found.");
        }
        return result;
    }
}
