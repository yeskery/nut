package com.yeskery.nut.bean.condition;

import com.yeskery.nut.application.ApplicationType;
import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Web应用类型条件判断
 * @author sunjay
 * 2023/9/6
 */
public class OnWebApplicationTypeCondition implements Condition {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(OnStandardApplicationTypeCondition.class.getName());

    @Override
    public boolean matches(NutApplication nutApplication, Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata, Object conditionalAnnotation) {
        boolean result = ApplicationType.isWebApplicationType(nutApplication.getApplicationMetadata().getApplicationType());
        if (!result && logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "StandardApplicationType Condition Not Match, Condition[" + this.getClass().getName()
                    + "], Expect Web ServerType, Actual ServerType[" + nutApplication.getApplicationMetadata().getApplicationType().name() + "]");
        }
        return result;
    }
}
