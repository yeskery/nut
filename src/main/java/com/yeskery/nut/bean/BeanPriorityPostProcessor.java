package com.yeskery.nut.bean;

/**
 * Bean的后置优先处理接口，为了执行速度，仅在单例Bean初始化后才会执行
 * @author sprout
 * @version 1.0
 * 2022-11-17 21:02
 *
 * @see com.yeskery.nut.bean.BeanPostProcessor
 */
@FunctionalInterface
public interface BeanPriorityPostProcessor extends BeanPostProcessor {
}
