package com.yeskery.nut.bean;

import com.yeskery.nut.core.Order;

/**
 * BeanFactory的后置处理器
 * @author sprout
 * @version 1.0
 * 2022-07-31 15:02
 */
@FunctionalInterface
public interface BeanFactoryPostProcessor extends Order {

    /**
     * 处理方法
     * @param applicationContext 应用上下文
     * @throws Exception 处理过程中的异常
     */
    void process(ApplicationContext applicationContext) throws Exception;

    @Override
    default int getOrder() {
        return 0;
    }
}
