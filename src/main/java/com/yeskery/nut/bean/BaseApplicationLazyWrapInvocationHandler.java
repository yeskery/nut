package com.yeskery.nut.bean;

/**
 * 基础应用上下文延迟加载的包装拦截处理器
 * @author Yeskery
 * 2023/8/16
 */
public abstract class BaseApplicationLazyWrapInvocationHandler extends BaseLazyWrapInvocationHandler {

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /**
     * 构建基础应用上下文延迟加载的包装拦截处理器
     * @param applicationContext 应用上下文
     */
    public BaseApplicationLazyWrapInvocationHandler(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取应用上下文
     * @return 应用上下文
     */
    protected ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
