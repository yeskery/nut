package com.yeskery.nut.bean;

import java.io.Closeable;
import java.util.Collection;

/**
 * 应用上下文
 * @author sprout
 * 2022-06-20 14:27
 */
public interface ApplicationContext extends Closeable {

    /** 原型 */
    String SCOPE_PROTOTYPE = "PROTOTYPE";

    /** 单例 */
    String SCOPE_SINGLETON = "SINGLETON";

    /**
     * 根据bean名称获取bean对象
     * @param beanName bean名称
     * @return bean对象
     * @throws NoSuchBeanException 无法找到bean时抛出该异常
     */
    Object getBean(String beanName) throws NoSuchBeanException;

    /**
     * 根据bean名称获取bean对象，并将类型转换指定类型
     * @param beanName bean名称
     * @param beanClass bean类对象
     * @param <T> bean类型
     * @return bean对象
     * @throws NoSuchBeanException 无法找到bean时抛出该异常
     */
    <T> T getBean(String beanName, Class<T> beanClass) throws NoSuchBeanException;

    /**
     * 根据类型获取指定类型的bean对象
     * @param beanClass bean类型对象
     * @param <T> bean类型
     * @return bean对象
     * @throws NoSuchBeanException 无法找到bean时抛出该异常
     */
    <T> T getBean(Class<T> beanClass) throws NoSuchBeanException;

    /**
     * 获取指定类型的所有bean
     * @param beanClass bean类型对象
     * @param <T> bean类型
     * @return 指定类型的所有bean
     */
    <T> Collection<T> getBeans(Class<T> beanClass);
}
