package com.yeskery.nut.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link com.yeskery.nut.annotation.bean.Import}注解的注解元数据
 * @author YESKERY
 * 2024/1/8
 */
public class ImportAnnotationMetadata {

    /** {@link com.yeskery.nut.annotation.bean.Import}注解的注解类型元数据集合 */
    private final List<ImportAnnotationTypeMetadata> metadata = new ArrayList<>();

    /**
     * 获取{@link com.yeskery.nut.annotation.bean.Import}注解的注解类型元数据
     * @return {@link com.yeskery.nut.annotation.bean.Import}注解的注解类型元数据
     */
    public List<ImportAnnotationTypeMetadata> getMetadata() {
        return metadata;
    }
}
