package com.yeskery.nut.bean;

/**
 * bean初始化接口
 * @author sprout
 * @version 1.0
 * 2022-08-25 14:26
 */
@FunctionalInterface
public interface InitializingBean {

    /**
     * bean初始化执行方法
     * @throws Exception 初始化时发生异常
     */
    void initialize() throws Exception;
}
