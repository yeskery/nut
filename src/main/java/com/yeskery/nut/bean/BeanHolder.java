package com.yeskery.nut.bean;

import com.yeskery.nut.core.Holder;

/**
 * Bean持有对象
 * @author sprout
 * 2022-06-21 17:45
 */
public class BeanHolder extends Holder<Object> {

    /** bean类型 */
    private Class<?> type;

    /**
     * 构建Bean持有对象
     * @param type bean类型
     * @param object bean对象
     */
    public BeanHolder(Class<?> type, Object object) {
        this.type = type;
        setObject(object);
    }

    /**
     * 构建Bean持有对象
     */
    public BeanHolder() {
    }

    /**
     * 获取bean类型
     * @return bean类型
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * 设置bean类型
     * @param type bean类型
     */
    public void setType(Class<?> type) {
        this.type = type;
    }
}
