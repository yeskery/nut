package com.yeskery.nut.bean;

import com.yeskery.nut.scan.BeanAnnotationScanMetadata;

import java.util.Collection;

/**
 * Bean注册接口
 * @author sprout
 * 2022-06-20 14:48
 */
public interface BeanRegister {

    /**
     * 设置所有Bean扫描元数据类型
     * @param beanAnnotationScanMetadata 所有Bean扫描元数据类型
     */
    void setBeanAnnotationScanMetadata(Collection<BeanAnnotationScanMetadata> beanAnnotationScanMetadata);

    /**
     * 注册bean对象
     * @param beanDefinition bean定义对象
     */
    void registerBean(BeanDefinition beanDefinition);

    /**
     * 注册bean对象
     * @param beanClass bean类型对象
     */
    void registerBean(Class<?> beanClass);

    /**
     * 注册bean对象
     * @param beanAnnotationMetadata Bean扫描元数据类型
     */
    void registerBean(BeanAnnotationScanMetadata beanAnnotationMetadata);

    /**
     * 是否已经包含bean对象
     * @param beanClass bean类型对象
     * @return 是否已经包含bean对象
     */
    boolean containBean(Class<?> beanClass);

    /**
     * 是否已经包含bean对象
     * @param beanName bean名称
     * @return 是否已经包含bean对象
     */
    boolean containBean(String beanName);
}
