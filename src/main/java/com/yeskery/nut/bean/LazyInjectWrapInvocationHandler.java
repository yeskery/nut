package com.yeskery.nut.bean;

import com.yeskery.nut.util.StringUtils;

/**
 * 注入的延迟加载包装拦截处理器
 * @author Yeskery
 * 2023/8/16
 */
public class LazyInjectWrapInvocationHandler extends BaseApplicationLazyWrapInvocationHandler {

    /** bean类型 */
    private final Class<?> clazz;

    /** bean名称 */
    private final String beanName;

    /**
     * 构建字段注入的延迟加载包装拦截处理器
     * @param applicationContext 应用上下文
     * @param clazz bean类型
     * @param beanName bean名称
     */
    public LazyInjectWrapInvocationHandler(ApplicationContext applicationContext, Class<?> clazz, String beanName) {
        super(applicationContext);
        this.clazz = clazz;
        this.beanName = beanName;
    }

    @Override
    protected Object getBean() {
        return StringUtils.isEmpty(beanName)
                ? getApplicationContext().getBean(clazz)
                : getApplicationContext().getBean(beanName);
    }
}
