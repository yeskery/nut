package com.yeskery.nut.bean;

import java.util.Map;

/**
 * bean迭代接口
 * @author sprout
 * @version 1.0
 * 2022-09-20 10:19
 */
public interface BeanIterable {

    /**
     * 获取单例bean对象map
     * @return 单例bean对象map
     */
    Map<String, Object> getSingletonBeans();

    /**
     * 获取原型bean定义对象map
     * @return 原型bean定义对象map
     */
    Map<String, BeanDefinition> getPrototypeBeanDefinitions();
}
