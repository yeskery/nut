package com.yeskery.nut.bean;

import com.yeskery.nut.core.Order;

/**
 * bean定义的后置处理器
 * @author sprout
 * @version 1.0
 * 2022-08-24 10:23
 */
@FunctionalInterface
public interface BeanDefinitionPostProcessor extends Order {

    /**
     * 处理方法
     * @param beanDefinitionRegistry bean定义注册器
     * @throws Exception 处理过程中的异常
     */
    void process(BeanDefinitionRegistry beanDefinitionRegistry) throws Exception;

    @Override
    default int getOrder() {
        return 0;
    }
}
