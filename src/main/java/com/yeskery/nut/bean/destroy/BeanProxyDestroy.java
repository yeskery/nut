package com.yeskery.nut.bean.destroy;

/**
 * Bean销毁代理接口
 * @author sprout
 * @version 1.0
 * 2022-11-18 13:57
 */
public interface BeanProxyDestroy {

    /**
     * bean销毁方法
     * @throws Exception 销毁时发生的异常
     */
    void destroy() throws Exception;

    /**
     * 获取bean的名称
     * @return bean的名称
     */
    String getBeanName();

    /**
     * 获取bean的类型
     * @return bean的类型
     */
    Class<?> getBeanType();
}
