package com.yeskery.nut.bean.destroy;

import com.yeskery.nut.bean.DisposableBean;

/**
 * DisposableBean接口的bean销毁代理接口
 * @author sprout
 * @version 1.0
 * 2022-11-18 13:59
 */
public class DisposableBeanInterfaceProxyDestroy extends AbstractBeanProxyDestroy {

    /** bean销毁接口 */
    private final DisposableBean disposableBean;

    /**
     * 构建DisposableBean接口的bean销毁代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     * @param disposableBean bean销毁接口
     */
    public DisposableBeanInterfaceProxyDestroy(String beanName, Class<?> beanType, DisposableBean disposableBean) {
        super(beanName, beanType);
        this.disposableBean = disposableBean;
    }

    @Override
    public void destroy() throws Exception {
        disposableBean.destroy();
    }
}
