package com.yeskery.nut.bean.destroy;

/**
 * 抽象的Bean销毁代理接口
 * @author sprout
 * @version 1.0
 * 2022-11-18 14:17
 */
public abstract class AbstractBeanProxyDestroy implements BeanProxyDestroy {

    /** bean的名称 */
    private final String beanName;

    /** bean的类型 */
    private final Class<?> beanType;

    /**
     * 构建抽象的Bean销毁代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     */
    protected AbstractBeanProxyDestroy(String beanName, Class<?> beanType) {
        this.beanName = beanName;
        this.beanType = beanType;
    }

    @Override
    public String getBeanName() {
        return beanName;
    }

    @Override
    public Class<?> getBeanType() {
        return beanType;
    }
}
