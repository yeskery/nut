package com.yeskery.nut.bean.destroy;

import java.lang.reflect.Method;

/**
 * PreDestroy注解的bean销毁代理接口
 * @author sprout
 * @version 1.0
 * 2022-11-18 14:04
 */
public class PreDestroyAnnotationProxyDestroy extends AbstractBeanProxyDestroy {

    /** bean对象 */
    private final Object bean;

    /** bean销毁方法 */
    private final Method destroyMethod;

    /**
     * 构建PreDestroy注解的bean销毁代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     * @param bean bean对象
     * @param destroyMethod bean销毁方法
     */
    public PreDestroyAnnotationProxyDestroy(String beanName, Class<?> beanType, Object bean, Method destroyMethod) {
        super(beanName, beanType);
        this.bean = bean;
        this.destroyMethod = destroyMethod;
    }

    @Override
    public void destroy() throws Exception {
        destroyMethod.invoke(bean);
    }
}
