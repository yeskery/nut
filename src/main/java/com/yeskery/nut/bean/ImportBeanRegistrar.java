package com.yeskery.nut.bean;

/**
 * {@link com.yeskery.nut.annotation.bean.Import}注解的注册器接口
 * @author YESKERY
 * 2024/1/8
 */
public interface ImportBeanRegistrar {

    /**
     * 注册bean
     * @param importAnnotationMetadata {@link com.yeskery.nut.annotation.bean.Import}注解的注解元数据
     * @param beanRegister bean注册器
     */
    void registerBeans(ImportAnnotationMetadata importAnnotationMetadata, BeanRegister beanRegister);
}
