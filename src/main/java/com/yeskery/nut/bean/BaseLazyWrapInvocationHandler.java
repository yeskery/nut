package com.yeskery.nut.bean;

import com.yeskery.nut.aop.WrapInvocationHandler;

import java.lang.reflect.Method;

/**
 * 延迟加载的包装拦截处理器
 * @author Yeskery
 * 2023/8/16
 */
public abstract class BaseLazyWrapInvocationHandler implements WrapInvocationHandler {

    /** Bean对象 */
    private volatile Object bean;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (bean == null) {
            synchronized (this) {
                if (bean == null) {
                    bean = getBean();
                }
            }
        }
        return method.invoke(bean, args);
    }

    /**
     * 获取Bean对象
     * @return Bean对象
     */
    protected abstract Object getBean();
}
