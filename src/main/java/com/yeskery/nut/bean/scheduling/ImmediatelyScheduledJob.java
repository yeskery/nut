package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

/**
 * 立即执行任务
 * @author YESKERY
 * 2024/6/5
 */
public class ImmediatelyScheduledJob implements ScheduledJob {
    @Override
    public void handle(ThreadPool threadPool, Runnable runnable) throws Exception {
        threadPool.getThreadPool().execute(runnable);
    }
}
