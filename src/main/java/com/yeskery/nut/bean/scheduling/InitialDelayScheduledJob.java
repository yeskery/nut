package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

import java.util.concurrent.TimeUnit;

/**
 * 初始化延迟调度任务
 * @author YESKERY
 * 2024/6/5
 */
public class InitialDelayScheduledJob implements ScheduledJob {

    /** 初始化延迟值 */
    private final long initialDelay;

    /**
     * 构建初始化延迟调度任务接口
     * @param initialDelay 初始化延迟值
     */
    public InitialDelayScheduledJob(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    @Override
    public void handle(ThreadPool threadPool, Runnable runnable) throws Exception {
        threadPool.getScheduledThreadPool().schedule(runnable, initialDelay, TimeUnit.MILLISECONDS);
    }
}
