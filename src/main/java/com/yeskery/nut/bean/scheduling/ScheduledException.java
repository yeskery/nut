package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.NutException;

/**
 * 定时调度异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class ScheduledException extends NutException {

    /**
     * 构建一个 {@link ScheduledException}
     */
    public ScheduledException() {
    }

    /**
     * 构建一个 {@link ScheduledException}
     * @param message message
     */
    public ScheduledException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link ScheduledException}
     * @param message message
     * @param cause cause
     */
    public ScheduledException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link ScheduledException}
     * @param cause cause
     */
    public ScheduledException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link ScheduledException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ScheduledException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
