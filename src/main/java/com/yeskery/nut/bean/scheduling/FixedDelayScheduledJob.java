package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

import java.util.concurrent.TimeUnit;

/**
 * 固定延迟调度任务
 * @author YESKERY
 * 2024/6/5
 */
public class FixedDelayScheduledJob implements ScheduledJob {

    /** 固定频率值 */
    private final long fixedDelay;

    /**
     * 构建固定延迟调度任务
     * @param fixedDelay 固定频率值
     */
    public FixedDelayScheduledJob(long fixedDelay) {
        this.fixedDelay = fixedDelay;
    }

    @Override
    public void handle(ThreadPool threadPool, Runnable runnable) throws Exception {
        threadPool.getScheduledThreadPool().scheduleAtFixedRate(runnable, 0, fixedDelay, TimeUnit.MILLISECONDS);
    }
}
