package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

import java.util.concurrent.TimeUnit;

/**
 * 固定频率调度任务
 * @author YESKERY
 * 2024/6/5
 */
public class FixedRateScheduleJob implements ScheduledJob {

    /** 固定频率值 */
    private final long fixedRate;

    /**
     * 构建固定频率调度任务
     * @param fixedRate 固定频率值
     */
    public FixedRateScheduleJob(long fixedRate) {
        this.fixedRate = fixedRate;
    }

    @Override
    public void handle(ThreadPool threadPool, Runnable runnable) throws Exception {
        threadPool.getScheduledThreadPool().scheduleWithFixedDelay(runnable, 0, fixedRate, TimeUnit.MILLISECONDS);
    }
}
