package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.annotation.scheduling.Scheduled;

import java.lang.reflect.Method;

/**
 * 定时调度元数据
 * @author sprout
 * @version 1.0
 * 2024-06-04 23:09
 */
public class ScheduledMetadata {

    /** bean对象 */
    private Object bean;

    /** bean方法对象 */
    private Method method;

    /** 定时调度注解 */
    private Scheduled scheduled;

    /**
     * 获取bean对象
     * @return bean对象
     */
    public Object getBean() {
        return bean;
    }

    /**
     * 设置bean对象
     * @param bean bean对象
     */
    public void setBean(Object bean) {
        this.bean = bean;
    }

    /**
     * 获取bean方法对象
     * @return bean方法对象
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 设置bean方法对象
     * @param method bean方法对象
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * 获取定时调度注解
     * @return 定时调度注解
     */
    public Scheduled getScheduled() {
        return scheduled;
    }

    /**
     * 设置定时调度注解
     * @param scheduled 定时调度注解
     */
    public void setScheduled(Scheduled scheduled) {
        this.scheduled = scheduled;
    }
}
