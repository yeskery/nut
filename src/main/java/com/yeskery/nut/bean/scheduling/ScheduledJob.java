package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

/**
 * 调度任务接口
 * @author sprout
 * @version 1.0
 * 2024-06-04 23:49
 */
public interface ScheduledJob {

    /**
     * 执行调度任务
     * @param threadPool 线程池对象
     * @param runnable 执行方法
     * @throws Exception 执行过程中发生的一场
     */
    void handle(ThreadPool threadPool, Runnable runnable) throws Exception;
}
