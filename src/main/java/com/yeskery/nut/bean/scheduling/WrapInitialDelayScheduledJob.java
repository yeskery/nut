package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.core.ThreadPool;

import java.util.concurrent.TimeUnit;

/**
 * 包装的初始化延迟调度任务
 * @author YESKERY
 * 2024/6/5
 */
public class WrapInitialDelayScheduledJob implements ScheduledJob {

    /** 初始化延迟值 */
    private final long initialDelay;

    /** 调度任务接口 */
    private final ScheduledJob scheduledJob;

    /**
     * 构建初始化延迟调度任务接口
     * @param initialDelay 初始化延迟值
     * @param scheduledJob 调度任务接口
     */
    public WrapInitialDelayScheduledJob(long initialDelay, ScheduledJob scheduledJob) {
        this.initialDelay = initialDelay;
        this.scheduledJob = scheduledJob;
    }

    @Override
    public void handle(ThreadPool threadPool, Runnable runnable) throws Exception {
        threadPool.getScheduledThreadPool().schedule((() -> {
            try {
                scheduledJob.handle(threadPool, runnable);
            } catch (Exception e) {
                throw new ScheduledException("ScheduledJob[" + scheduledJob.getClass().getName() + "] Execute Fail.", e);
            }
        }), initialDelay, TimeUnit.MILLISECONDS);
    }
}
