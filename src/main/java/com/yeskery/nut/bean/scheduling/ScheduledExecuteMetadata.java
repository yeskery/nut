package com.yeskery.nut.bean.scheduling;

import com.yeskery.nut.util.CronSequenceGenerator;

/**
 * 定时调度执行元数据
 * @author sprout
 * @version 1.0
 * 2024-06-04 23:35
 */
public class ScheduledExecuteMetadata {

    /** 调度类型 */
    private ScheduleExecuteType type;

    /** cron序列生成器 */
    private CronSequenceGenerator cronSequenceGenerator;

    /** 固定延迟执行时间 */
    private long fixedDelay = -1L;

    /** 固定频率执行时间 */
    private long fixedRate = -1L;

    /** 初始化延迟时间 */
    private long initialDelay = -1L;

    /** 定时调度元数据 */
    private ScheduledMetadata scheduledMetadata;

    /**
     * 获取调度类型
     * @return 调度类型
     */
    public ScheduleExecuteType getType() {
        return type;
    }

    /**
     * 设置调度类型
     * @param type 调度类型
     */
    public void setType(ScheduleExecuteType type) {
        this.type = type;
    }

    /**
     * 获取cron序列生成器
     * @return cron序列生成器
     */
    public CronSequenceGenerator getCronSequenceGenerator() {
        return cronSequenceGenerator;
    }

    /**
     * 设置cron序列生成器
     * @param cronSequenceGenerator cron序列生成器
     */
    public void setCronSequenceGenerator(CronSequenceGenerator cronSequenceGenerator) {
        this.cronSequenceGenerator = cronSequenceGenerator;
    }

    /**
     * 获取固定延迟执行时间
     * @return 固定延迟执行时间
     */
    public long getFixedDelay() {
        return fixedDelay;
    }

    /**
     * 设置固定延迟执行时间
     * @param fixedDelay 固定延迟执行时间
     */
    public void setFixedDelay(long fixedDelay) {
        this.fixedDelay = fixedDelay;
    }

    /**
     * 获取固定频率执行时间
     * @return 固定频率执行时间
     */
    public long getFixedRate() {
        return fixedRate;
    }

    /**
     * 设置固定频率执行时间
     * @param fixedRate 固定频率执行时间
     */
    public void setFixedRate(long fixedRate) {
        this.fixedRate = fixedRate;
    }

    /**
     * 获取初始化延迟时间
     * @return 初始化延迟时间
     */
    public long getInitialDelay() {
        return initialDelay;
    }

    /**
     * 设置初始化延迟时间
     * @param initialDelay 初始化延迟时间
     */
    public void setInitialDelay(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    /**
     * 获取定时调度元数据
     * @return 定时调度元数据
     */
    public ScheduledMetadata getScheduledMetadata() {
        return scheduledMetadata;
    }
    /**
     * 设置定时调度元数据
     * @param scheduledMetadata 定时调度元数据
     */
    public void setScheduledMetadata(ScheduledMetadata scheduledMetadata) {
        this.scheduledMetadata = scheduledMetadata;
    }
}
