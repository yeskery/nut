package com.yeskery.nut.bean.scheduling;

/**
 * 定时调度执行类型
 * @author sprout
 * @version 1.0
 * 2024-06-04 23:46
 */
public enum ScheduleExecuteType {
    /** 立即执行 */
    IMMEDIATELY,
    /** CRON */
    CRON,
    /** 固定延迟 */
    FIXED_DELAY,
    /** 固定频率 */
    FIXED_RATE,
    /** 初始延迟 */
    INITIAL_DELAY;
}
