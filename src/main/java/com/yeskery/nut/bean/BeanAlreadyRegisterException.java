package com.yeskery.nut.bean;

/**
 * bean重复注册异常
 * @author sprout
 * @version 1.0
 * 2022-07-17 22:19
 */
public class BeanAlreadyRegisterException extends BeanException {

    /**
     * 构建一个 {@link BeanAlreadyRegisterException}
     */
    public BeanAlreadyRegisterException() {
    }

    /**
     * 构建一个 {@link BeanAlreadyRegisterException}
     * @param message message
     */
    public BeanAlreadyRegisterException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link BeanAlreadyRegisterException}
     * @param message message
     * @param cause cause
     */
    public BeanAlreadyRegisterException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link BeanAlreadyRegisterException}
     * @param cause cause
     */
    public BeanAlreadyRegisterException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link BeanAlreadyRegisterException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public BeanAlreadyRegisterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
