package com.yeskery.nut.bean;

import com.yeskery.nut.annotation.bean.Import;

import java.util.Collection;
import java.util.Stack;

/**
 * {@link com.yeskery.nut.annotation.bean.Import}注解的注解类型元数据
 * @author YESKERY
 * 2024/1/9
 */
public class ImportAnnotationTypeMetadata {

    /** 原始class */
    private Class<?> originalClass;

    /** 原始类注解集合 */
    private Collection<ClassAndAnnotation> classAndAnnotations;

    /** import注解对象 */
    private Import importAnnotation;

    /** 标注import注解的类对象 */
    private Class<?> importClass;

    /**
     * 原始class
     * @return 原始class
     */
    public Class<?> getOriginalClass() {
        return originalClass;
    }

    /**
     * 设置原始class
     * @param originalClass 原始class
     */
    public void setOriginalClass(Class<?> originalClass) {
        this.originalClass = originalClass;
    }

    /**
     * 获取原始类注解集合
     * @return 原始类注解集合
     */
    public Collection<ClassAndAnnotation> getClassAndAnnotations() {
        return classAndAnnotations;
    }

    /**
     * 设置原始类注解栈
     * @param classAndAnnotations 原始类注解集合
     */
    public void setClassAndAnnotations(Collection<ClassAndAnnotation> classAndAnnotations) {
        this.classAndAnnotations = classAndAnnotations;
    }

    /**
     * 获取import注解对象
     * @return import注解对象
     */
    public Import getImportAnnotation() {
        return importAnnotation;
    }

    /**
     * 设置import注解对象
     * @param importAnnotation import注解对象
     */
    public void setImportAnnotation(Import importAnnotation) {
        this.importAnnotation = importAnnotation;
    }

    /**
     * 获取标注import注解的类对象
     * @return 标注import注解的类对象
     */
    public Class<?> getImportClass() {
        return importClass;
    }

    /**
     * 设置标注import注解的类对象
     * @param importClass 标注import注解的类对象
     */
    public void setImportClass(Class<?> importClass) {
        this.importClass = importClass;
    }
}
