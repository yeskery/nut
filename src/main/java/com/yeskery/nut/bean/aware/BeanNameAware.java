package com.yeskery.nut.bean.aware;

/**
 * Bean名称Aware接口
 * @author sprout
 * @version 1.0
 * 2022-11-17 21:26
 */
public interface BeanNameAware {

    /**
     * 设置Bean名称
     * @param beanName Bean名称
     */
    void setBeanName(String beanName);
}
