package com.yeskery.nut.bean.aware;

import com.yeskery.nut.core.Environment;

/**
 * 环境Aware接口
 * @author sprout
 * @version 1.0
 * 2022-11-18 13:44
 */
public interface EnvironmentAware {

    /**
     * 设置环境
     * @param environment 环境
     */
    void setEnvironment(Environment environment);
}
