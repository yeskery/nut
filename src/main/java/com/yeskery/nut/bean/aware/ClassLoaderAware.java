package com.yeskery.nut.bean.aware;

/**
 * 类加载器Aware接口
 * @author sprout
 * @version 1.0
 * 2022-11-17 21:30
 */
public interface ClassLoaderAware {

    /**
     * 设置类加载器
     * @param classLoader 类加载器
     */
    void setClassLoader(ClassLoader classLoader);
}
