package com.yeskery.nut.bean.aware;

import com.yeskery.nut.application.NutApplication;

/**
 * Nut应用对象Aware接口
 * @author sprout
 * @version 1.0
 * 2022-11-17 21:29
 */
public interface NutApplicationAware {

    /**
     * 设置Nut应用对象
     * @param nutApplication Nut应用对象
     */
    void setNutApplication(NutApplication nutApplication);
}
