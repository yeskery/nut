package com.yeskery.nut.bean.aware;

import com.yeskery.nut.bean.ApplicationContext;

/**
 * 应用上下文Aware接口
 * @author sprout
 * @version 1.0
 * 2022-11-17 21:29
 */
public interface ApplicationContextAware {

    /**
     * 设置应用上下文
     * @param applicationContext 应用上下文
     */
    void setApplicationContext(ApplicationContext applicationContext);
}
