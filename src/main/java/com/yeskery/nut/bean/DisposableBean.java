package com.yeskery.nut.bean;

/**
 * bean销毁接口
 * @author sprout
 * @version 1.0
 * 2022-08-25 14:27
 */
@FunctionalInterface
public interface DisposableBean {

    /**
     * bean销毁执行方法
     * @throws Exception 销毁时发生的异常
     */
    void destroy() throws Exception;
}
