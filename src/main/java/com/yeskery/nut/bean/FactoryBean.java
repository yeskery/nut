package com.yeskery.nut.bean;

/**
 * 工厂Bean接口
 * @author sprout
 * 2022-06-20 14:56
 */
@FunctionalInterface
public interface FactoryBean {

    /**
     * 获取bean对象实例
     * @return bean对象实例
     */
    Object getBean();
}
