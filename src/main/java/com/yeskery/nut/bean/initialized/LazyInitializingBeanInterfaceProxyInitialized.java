package com.yeskery.nut.bean.initialized;

import com.yeskery.nut.bean.LazyInitializingBean;

/**
 * LazyInitializingBean接口的bean延迟初始化代理接口
 * @author sprout
 * @version 1.0
 * 2024-11-06
 */
public class LazyInitializingBeanInterfaceProxyInitialized extends AbstractBeanProxyInitialized {

    /** bean延迟初始化接口 */
    private final LazyInitializingBean lazyInitializingBean;

    /**
     * 构建LazyInitializingBean接口的bean延迟初始化代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     * @param lazyInitializingBean bean延迟初始化接口
     */
    public LazyInitializingBeanInterfaceProxyInitialized(String beanName, Class<?> beanType, LazyInitializingBean lazyInitializingBean) {
        super(beanName, beanType);
        this.lazyInitializingBean = lazyInitializingBean;
    }

    @Override
    public void initialize() throws Exception {
        lazyInitializingBean.initialize();
    }
}
