package com.yeskery.nut.bean.initialized;

/**
 * Bean延迟初始化代理接口
 * @author sprout
 * @version 1.0
 * 2024-11-06
 */
public interface LazyBeanProxyInitialized {

    /**
     * bean初始化方法
     * @throws Exception 初始化时发生的异常
     */
    void initialize() throws Exception;

    /**
     * 获取bean的名称
     * @return bean的名称
     */
    String getBeanName();

    /**
     * 获取bean的类型
     * @return bean的类型
     */
    Class<?> getBeanType();
}
