package com.yeskery.nut.bean.initialized;

/**
 * 抽象的Bean延迟初始化代理接口
 * @author sprout
 * @version 1.0
 * 2024-11-06
 */
public abstract class AbstractBeanProxyInitialized implements LazyBeanProxyInitialized {

    /** bean的名称 */
    private final String beanName;

    /** bean的类型 */
    private final Class<?> beanType;

    /**
     * 构建抽象的Bean延迟初始化代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     */
    protected AbstractBeanProxyInitialized(String beanName, Class<?> beanType) {
        this.beanName = beanName;
        this.beanType = beanType;
    }

    @Override
    public String getBeanName() {
        return beanName;
    }

    @Override
    public Class<?> getBeanType() {
        return beanType;
    }
}
