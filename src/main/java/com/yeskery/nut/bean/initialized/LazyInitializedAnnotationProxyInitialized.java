package com.yeskery.nut.bean.initialized;

import java.lang.reflect.Method;

/**
 * LazyInitialized注解的bean延迟初始化代理接口
 * @author sprout
 * @version 1.0
 * 2024-11-06
 */
public class LazyInitializedAnnotationProxyInitialized extends AbstractBeanProxyInitialized {

    /** bean对象 */
    private final Object bean;

    /** bean延迟初始化方法 */
    private final Method initializeMethod;

    /**
     * 构建LazyInitialized注解的bean延迟初始化代理接口
     * @param beanName bean的名称
     * @param beanType bean的类型
     * @param bean bean对象
     * @param initializeMethod bean延迟初始化方法
     */
    public LazyInitializedAnnotationProxyInitialized(String beanName, Class<?> beanType, Object bean, Method initializeMethod) {
        super(beanName, beanType);
        this.bean = bean;
        this.initializeMethod = initializeMethod;
    }

    @Override
    public void initialize() throws Exception {
        initializeMethod.invoke(bean);
    }
}
