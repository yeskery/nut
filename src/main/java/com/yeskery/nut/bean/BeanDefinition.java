package com.yeskery.nut.bean;

import com.yeskery.nut.aop.ProxyType;

import java.lang.reflect.Constructor;
import java.util.Objects;

/**
 * Bean定义对象
 * @author sprout
 * 2022-06-20 14:50
 */
public class BeanDefinition {

    /** bean名称 */
    private String beanName;

    /** 是否单例 */
    private boolean singleton;

    /** 是否是优先级bean */
    private boolean primary;

    /** 是否代理对象 */
    private boolean proxy;

    /** 是否延迟加载 */
    private boolean lazy;

    /** 是否延迟注入 */
    private boolean lazyInject;

    /** 代理类型 */
    private ProxyType proxyType;

    /** bean类型对象 */
    private Class<?> beanClass;

    /** 构造方法 */
    private Constructor<?> constructor;

    /** 构造方法参数类型 */
    private Class<?>[] constructorParamTypes;

    /** 依赖的bean定义 */
    private BeanAttribute[] depends;

    /**
     * 获取bean名称
     * @return bean名称
     */
    public String getBeanName() {
        return beanName;
    }

    /**
     * 设置bean名称
     * @param beanName bean名称
     */
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    /**
     * 是否单例
     * @return 是否单例
     */
    public boolean isSingleton() {
        return singleton;
    }

    /**
     * 设置是否单例
     * @param singleton 是否单例
     */
    public void setSingleton(boolean singleton) {
        this.singleton = singleton;
    }

    /**
     * 是否优先级bean
     * @return 是否优先级bean
     */
    public boolean isPrimary() {
        return primary;
    }

    /**
     * 设置是否优先级bean
     * @param primary 是否优先级bean
     */
    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    /**
     * 是否代理对象
     * @return 是否代理对象
     */
    public boolean isProxy() {
        return proxy;
    }

    /**
     * 设置是否代理对象
     * @param proxy 是否代理对象
     */
    public void setProxy(boolean proxy) {
        this.proxy = proxy;
    }

    /**
     * 是否延迟加载
     * @return 延迟加载
     */
    public boolean isLazy() {
        return lazy;
    }

    /**
     * 设置延迟加载
     * @param lazy 延迟加载
     */
    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }

    /**
     * 是否延迟注入
     * @return 是否延迟注入
     */
    public boolean isLazyInject() {
        return lazyInject;
    }

    /**
     * 设置是否延迟注入
     * @param lazyInject 是否延迟注入
     */
    public void setLazyInject(boolean lazyInject) {
        this.lazyInject = lazyInject;
    }

    /**
     * 获取代理模式
     * @return 代理模式
     */
    public ProxyType getProxyType() {
        return proxyType;
    }

    /**
     * 设置代理模式
     * @param proxyType 代理模式
     */
    public void setProxyType(ProxyType proxyType) {
        this.proxyType = proxyType;
    }

    /**
     * 获取bean类型对象
     * @return bean类型对象
     */
    public Class<?> getBeanClass() {
        return beanClass;
    }

    /**
     * 设置bean类型对象
     * @param beanClass bean类型对象
     */
    public void setBeanClass(Class<?> beanClass) {
        this.beanClass = beanClass;
    }

    /**
     * 获取构造方法
     * @return 构造方法
     */
    public Constructor<?> getConstructor() {
        return constructor;
    }

    /**
     * 设置构造方法
     * @param constructor 构造方法
     */
    public void setConstructor(Constructor<?> constructor) {
        this.constructor = constructor;
    }

    /**
     * 获取构造方法参数类型
     * @return 构造方法参数类型
     */
    public Class<?>[] getConstructorParamTypes() {
        return constructorParamTypes;
    }

    /**
     * 设置构造方法参数类型
     * @param constructorParamTypes 构造方法参数类型
     */
    public void setConstructorParamTypes(Class<?>[] constructorParamTypes) {
        this.constructorParamTypes = constructorParamTypes;
    }

    /**
     * 获取bean依赖
     * @return bean依赖
     */
    public BeanAttribute[] getDepends() {
        return depends;
    }

    /**
     * 设置bean依赖
     * @param depends bean依赖
     */
    public void setDepends(BeanAttribute[] depends) {
        this.depends = depends;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BeanDefinition that = (BeanDefinition) o;
        return beanName.equals(that.beanName) && beanClass.equals(that.beanClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beanName, beanClass);
    }
}
