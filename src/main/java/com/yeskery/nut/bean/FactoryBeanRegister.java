package com.yeskery.nut.bean;

/**
 * FactoryBean注解接口
 * @author sprout
 * 2022-06-30 10:57
 */
public interface FactoryBeanRegister extends BeanRegister {

    /**
     * 注册bean对象
     * @param beanName bean名称
     * @param primary 是否优先级bean
     * @param bean bean对象
     * @param classes bean要注册的类型
     */
    void registerBean(String beanName, boolean primary, Object bean, Class<?>... classes);

    /**
     * 注册bean对象
     * @param beanName bean名称
     * @param bean bean对象
     * @param classes bean要注册的类型
     */
    default void registerBean(String beanName, Object bean, Class<?>... classes) {
        registerBean(beanName, false, bean, classes);
    }

    /**
     * 注册FactoryBean对象
     * @param beanName bean名称
     * @param factoryBean FactoryBean对象
     * @param classes bean要注册的类型
     */
    default void registerFactoryBean(String beanName, FactoryBean factoryBean, Class<?>... classes) {
        registerBean(beanName, factoryBean.getBean(), classes);
    }
}
