package com.yeskery.nut.bean;

import com.yeskery.nut.core.Order;

/**
 * Bean的后置处理接口，为了执行速度，仅在单例Bean初始化后才会执行
 * @author sprout
 * @version 1.0
 * 2022-11-17 13:48
 */
@FunctionalInterface
public interface BeanPostProcessor extends Order {

    /**
     * 执行方法
     * @param bean bean对象
     * @param beanClass bean的类型
     * @param applicationContext 应用上下文
     * @param factoryBeanRegister FactoryBean注解接口
     */
    void process(Object bean, Class<?> beanClass, ApplicationContext applicationContext, FactoryBeanRegister factoryBeanRegister);

    @Override
    default int getOrder() {
        return 0;
    }
}
