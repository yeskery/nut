package com.yeskery.nut.bean;

import com.yeskery.nut.core.NutException;

/**
 * Bean异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class BeanException extends NutException {

    /**
     * 构建一个 {@link BeanException}
     */
    public BeanException() {
    }

    /**
     * 构建一个 {@link BeanException}
     * @param message message
     */
    public BeanException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link BeanException}
     * @param message message
     * @param cause cause
     */
    public BeanException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link BeanException}
     * @param cause cause
     */
    public BeanException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link BeanException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public BeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
