package com.yeskery.nut.bean;

import java.lang.annotation.Annotation;

/**
 * class和annotation对
 * @author YESKERY
 * 2024/1/9
 */
public class ClassAndAnnotation {

    /** class对象 */
    private Class<?> clazz;

    /** annotation对象 */
    private Annotation annotation;

    /**
     * 获取class对象
     * @return class对象
     */
    public Class<?> getClazz() {
        return clazz;
    }

    /**
     * 设置class对象
     * @param clazz class对象
     */
    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    /**
     * 获取annotation对象
     * @return annotation对象
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * 设置annotation对象
     * @param annotation annotation对象
     */
    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }
}
