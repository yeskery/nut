package com.yeskery.nut.bean;

/**
 * 导入选择器，使用接口来选择要导入的bean
 * @author YESKERY
 * 2024/1/9
 */
public interface ImportSelector {

    /**
     * 要导入的bean类型名称数组
     * @param importAnnotationMetadata {@link com.yeskery.nut.annotation.bean.Import}注解的注解元数据
     * @return 要导入的bean类型名称数组
     */
    String[] selectImports(ImportAnnotationMetadata importAnnotationMetadata);

    /**
     * 要导入的bean类型数组
     * @param importAnnotationMetadata {@link com.yeskery.nut.annotation.bean.Import}注解的注解元数据
     * @return 要导入的bean类型数组
     */
    default Class<?>[] selectImportClasses(ImportAnnotationMetadata importAnnotationMetadata) {
        return new Class[0];
    }
}
