package com.yeskery.nut.bean;

/**
 * 未找到Bean异常
 * @author sprout
 * @version 1.0
 * 2022-06-18 11:25
 */
public class NoSuchBeanException extends BeanException {

    /**
     * 构建一个 {@link NoSuchBeanException}
     */
    public NoSuchBeanException() {
    }

    /**
     * 构建一个 {@link NoSuchBeanException}
     * @param message message
     */
    public NoSuchBeanException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link NoSuchBeanException}
     * @param message message
     * @param cause cause
     */
    public NoSuchBeanException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link NoSuchBeanException}
     * @param cause cause
     */
    public NoSuchBeanException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link NoSuchBeanException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public NoSuchBeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
