package com.yeskery.nut.bean;

/**
 * bean延迟初始化接口，该接口将会在容器启动完成后再对标注了该接口的Bean初始化方法进行调用，而{@link InitializingBean}将会在Bean初始化时
 * 就进行对初始化方法的调用。
 * @author sprout
 * @version 1.0
 * 2022-08-25 14:26
 *
 * @see com.yeskery.nut.annotation.bean.LazyInject
 * @see com.yeskery.nut.annotation.bean.LazyInitialized
 */
@FunctionalInterface
public interface LazyInitializingBean {

    /**
     * bean初始化执行方法
     * @throws Exception 初始化时发生异常
     */
    void initialize() throws Exception;
}
