package com.yeskery.nut.bean;

import com.yeskery.nut.core.Holder;

/**
 * 优先级bean持有类
 * @author Yeskery
 * 2023/8/14
 */
public class PrimaryBeanHolder extends Holder<Object> {

    /**
     * 构造优先级bean持有类
     * @param bean bean对象
     */
    public PrimaryBeanHolder(Object bean) {
        setObject(bean);
    }

    /**
     * 获取bean对象
     * @return bean对象
     */
    public Object getBean() {
        return getObject();
    }
}
