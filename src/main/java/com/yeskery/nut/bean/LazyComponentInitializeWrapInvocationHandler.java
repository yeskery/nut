package com.yeskery.nut.bean;

/**
 * Component注解初始化注入的延迟加载包装拦截处理器
 * @author Yeskery
 * 2023/8/16
 */
public class LazyComponentInitializeWrapInvocationHandler extends BaseApplicationLazyWrapInvocationHandler {

    /** Bean定义对象 */
    private final BeanDefinition beanDefinition;

    /**
     * 构建Component注解初始化注入的延迟加载包装拦截处理器
     * @param applicationContext 应用上下文
     * @param beanDefinition Bean定义对象
     */
    public LazyComponentInitializeWrapInvocationHandler(ApplicationContext applicationContext, BeanDefinition beanDefinition) {
        super(applicationContext);
        this.beanDefinition = new BeanDefinition();
        this.beanDefinition.setBeanName(beanDefinition.getBeanName());
        this.beanDefinition.setBeanClass(beanDefinition.getBeanClass());
        this.beanDefinition.setSingleton(beanDefinition.isSingleton());
        this.beanDefinition.setPrimary(beanDefinition.isPrimary());
        this.beanDefinition.setProxy(beanDefinition.isProxy());
        this.beanDefinition.setLazy(false);
        this.beanDefinition.setProxyType(beanDefinition.getProxyType());
        this.beanDefinition.setConstructor(beanDefinition.getConstructor());
        this.beanDefinition.setConstructorParamTypes(beanDefinition.getConstructorParamTypes());
        this.beanDefinition.setDepends(beanDefinition.getDepends());
    }

    @Override
    protected Object getBean() {
        BaseApplicationContext baseApplicationContext = (BaseApplicationContext) getApplicationContext();
        Object beanInstance = baseApplicationContext.createBeanInstance(beanDefinition);
        baseApplicationContext.initializedBean(beanInstance, beanDefinition, beanDefinition.isLazy());
        return beanInstance;
    }
}
