package com.yeskery.nut.application;

/**
 * Nut 启动类
 * @author sprout
 * 2019-03-14 11:25
 * @version 1.0
 */
public class Nut {

	/**
	 * Nut 启动的主方法
	 * @param port 本地监听的端口号
	 * @param secure 是否已安全套接字方式启动项目
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(Integer port, Boolean secure, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, null, port, secure, args);
		return nutApplication;
	}

	/**
	 * Nut 启动的主方法
	 * @param mainClass 启动主类
	 * @param port 本地监听的端口号
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(Class<?> mainClass, int port, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, mainClass, port, null, args);
		return nutApplication;
	}

	/**
	 * Nut 启动的主方法
	 * @param port 本地监听的端口号
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(int port, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, null, port, null, args);
		return nutApplication;
	}

	/**
	 * 以默认端口：8080 启动 Nut
	 * @param mainClass 启动主类
	 * @param secure 是否以安全套接字启动服务
	 * @param args 启动参数
	 * @return 应用上下文
	 *
	 * @see #run(Integer, Boolean, String[])
	 */
	public static NutApplication run(Class<?> mainClass, Boolean secure, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, mainClass, null, secure, args);
		return nutApplication;
	}

	/**
	 * 以默认端口：8080 启动 Nut
	 * @param secure 是否以安全套接字启动服务
	 * @param args 启动参数
	 * @return 应用上下文
	 *
	 * @see #run(Integer, Boolean, String[])
	 */
	public static NutApplication run(Boolean secure, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, null, null, secure, args);
		return nutApplication;
	}

	/**
	 * 以默认端口：8080 方式启动 Nut
	 * @param mainClass 启动主类
	 * @param args 启动参数
	 * @return 应用上下文
	 *
	 * @see #run(Integer, Boolean, String[])
	 */
	public static NutApplication run(Class<?> mainClass, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, mainClass, null, null, args);
		return nutApplication;
	}

	/**
	 * 以默认端口启动 Nut
	 * @param args 启动参数
	 * @return 应用上下文
	 *
	 * @see #run(Integer, Boolean, String[])
	 */
	public static NutApplication run(String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(null, null, null, null, args);
		return nutApplication;
	}

	/**
	 * Nut 启动的主方法
	 * @param serverType 服务启动类型
	 * @param applicationType 程序运行类型
	 * @param mainClass 启动主类
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(ServerType serverType, ApplicationType applicationType, Class<?> mainClass, String[] args) {
		NutConfiguration nutConfiguration = NutConfigurationBuilder.newInstance()
				.setServerType(serverType)
				.setApplicationType(applicationType)
				.setMainClass(mainClass)
				.build();
        return run(nutConfiguration, args);
	}

	/**
	 * Nut 启动的主方法
	 * @param serverType 服务启动类型
	 * @param mainClass 启动主类
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(ServerType serverType, Class<?> mainClass, String[] args) {
		NutApplication nutApplication = new NutApplication();
		run(serverType, ApplicationType.WEB, mainClass, args);
		return nutApplication;
	}

	/**
	 * Nut 启动的主方法
	 * @param serverType 服务启动类型
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(ServerType serverType, String[] args) {
		NutApplication nutApplication = new NutApplication();
		run(serverType, ApplicationType.WEB, null, args);
		return nutApplication;
	}

	/**
	 * Nut 启动的主方法
	 * @param applicationType 程序运行类型
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(ApplicationType applicationType, String[] args) {
		NutApplication nutApplication = new NutApplication();
		run(null, applicationType, null, args);
		return nutApplication;
	}

	/**
	 * 服务启动
	 * @param nutConfiguration Nut配置对象
	 * @param args 启动参数
	 * @return 应用上下文
	 */
	public static NutApplication run(NutConfiguration nutConfiguration, String[] args) {
		NutApplication nutApplication = new NutApplication();
		nutApplication.run(nutConfiguration, args);
		return nutApplication;
	}

	/**
	 * 服务启动
	 * @param args 启动参数
	 */
	public static void main(String[] args) {
		run(args);
	}
}
