package com.yeskery.nut.application;

import com.yeskery.nut.core.Version;

/**
 * 应用元数据
 * @author sprout
 * @version 1.0
 * 2023-06-11 11:45
 */
public interface ApplicationMetadata {

    /**
     * 获取当前应用类型
     * @return 当前应用类型
     */
    ApplicationType getApplicationType();

    /**
     * 获取当前服务类型
     * @return 当前服务类型
     */
    ServerType getServerType();

    /**
     * 获取当前服务端口号
     * @return 当前服务端口号
     */
    int getServerPort();

    /**
     * 是否以安全方式启动服务
     * @return 是否以安全方式启动服务
     */
    boolean isSecureServer();

    /**
     * 获取当前服务的进程id
     * @return 当前服务的进程id
     */
    String getProcessId();

    /**
     * 获取当前Nut版本号
     * @return 当前Nut版本号
     */
    default String getNutVersion() {
        return Version.VERSION;
    }
}
