package com.yeskery.nut.application.netty;

import com.yeskery.nut.application.NutApplication;
import com.yeskery.nut.application.SecureServerContext;
import com.yeskery.nut.core.Dispatcher;
import com.yeskery.nut.core.ServerContext;
import com.yeskery.nut.core.ServerRequestConfiguration;
import com.yeskery.nut.core.SessionManager;

/**
 * 基于Netty的HttpServer实现HTTPS服务
 * @author sprout
 * 2022-06-16 10:50
 */
public class NettySecureNutServer extends NettyNutServer {

    /** SecureServerContext对象 */
    private final SecureServerContext secureServerContext;

    /**
     * 构建基于Netty的HttpServer实现HTTPS服务
     * @param secureServerContext SecureServerContext对象
     */
    public NettySecureNutServer(SecureServerContext secureServerContext) {
        this.secure = true;
        this.secureServerContext = secureServerContext;
    }

    @Override
    protected HttpServerInitializer buildHttpServerInitializer(NutApplication nutApplication, WebSocketServerContext webSocketServerContext,
                                                               ServerContext serverContext, Dispatcher dispatcher, SessionManager sessionManager,
                                                               ServerRequestConfiguration serverRequestConfiguration) {
        return new HttpServerInitializer(nutApplication, webSocketServerContext, serverContext, dispatcher,
                sessionManager, serverRequestConfiguration, secureServerContext, this);
    }
}
