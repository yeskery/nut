package com.yeskery.nut.application.netty;

/**
 * 空的websocket上下文
 * @author sprout
 * @version 1.0
 * 2023-04-22 16:52
 */
public class EmptyWebSocketServerContext implements WebSocketServerContext {
}
