package com.yeskery.nut.application.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * WebSocket上下文
 * @author sprout
 * @version 1.0
 * 2023-04-22 16:50
 */
public interface WebSocketServerContext {

    /**
     * 是否是WebSocket请求
     * @param ctx 通道处理器上下文
     * @param request 请求对象
     * @return 是否是WebSocket请求
     */
    default boolean isWebSocketRequest(ChannelHandlerContext ctx, FullHttpRequest request) {
        return false;
    }
}
