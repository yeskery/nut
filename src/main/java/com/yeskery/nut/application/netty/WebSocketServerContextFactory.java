package com.yeskery.nut.application.netty;

import com.yeskery.nut.util.ClassUtils;
import com.yeskery.nut.websocket.WebSocketConfigurationRegistry;

/**
 * websocket上下文工厂
 * @author sprout
 * @version 1.0
 * 2023-04-22 16:48
 */
public class WebSocketServerContextFactory {

    /** WebSocket依赖检查class名称 */
    private static final String WS_DEPEND_CHECK_CLASS_NAME = "io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler";

    /**
     * 构建 websocket上下文
     * @param webSocketConfigurationRegistry WebSocket 配置注册中心
     * @param secure 是否以安全方式启动
     * @return websocket上下文
     */
    public WebSocketServerContext buildWebSocketServerContext(WebSocketConfigurationRegistry webSocketConfigurationRegistry, boolean secure) {
        return ClassUtils.isExistTargetClass(WS_DEPEND_CHECK_CLASS_NAME)
                ? new NettyWebSocketServerContext(webSocketConfigurationRegistry, secure)
                : new EmptyWebSocketServerContext();
    }
}
