package com.yeskery.nut.application;

import com.yeskery.nut.core.ControllerManager;
import com.yeskery.nut.core.Dispatcher;
import com.yeskery.nut.core.SessionManager;
import com.yeskery.nut.plugin.PluginManager;

/**
 * 服务事件上下文
 * @author Yeskery
 * 2023/7/20
 */
public class ServerEventContext {
    /** 请求转发器 */
    private Dispatcher dispatcher;
    /** controller管理器 */
    private ControllerManager controllerManager;
    /** 会话管理器 */
    private SessionManager sessionManager;
    /** 插件管理器 */
    private PluginManager pluginManager;
    /** 应用元数据 */
    private ApplicationMetadata applicationMetadata;

    /**
     * 获取请求转发器
     * @return 请求转发器
     */
    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * 设置请求转发器
     * @param dispatcher 请求转发器
     */
    void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    /**
     * 获取controller管理器
     * @return controller管理器
     */
    public ControllerManager getControllerManager() {
        return controllerManager;
    }

    /**
     * 设置controller管理器
     * @param controllerManager controller管理器
     */
    void setControllerManager(ControllerManager controllerManager) {
        this.controllerManager = controllerManager;
    }

    /**
     * 获取会话管理器
     * @return 会话管理器
     */
    public SessionManager getSessionManager() {
        return sessionManager;
    }

    /**
     * 设置会话管理器
     * @param sessionManager 会话管理器
     */
    void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    /**
     * 获取插件管理器
     * @return 插件管理器
     */
    public PluginManager getPluginManager() {
        return pluginManager;
    }

    /**
     * 设置插件管理器
     * @param pluginManager 插件管理器
     */
    void setPluginManager(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }

    /**
     * 获取应用元数据
     * @return 应用元数据
     */
    public ApplicationMetadata getApplicationMetadata() {
        return applicationMetadata;
    }

    /**
     * 设置应用元数据
     * @param applicationMetadata 应用元数据
     */
    void setApplicationMetadata(ApplicationMetadata applicationMetadata) {
        this.applicationMetadata = applicationMetadata;
    }
}
