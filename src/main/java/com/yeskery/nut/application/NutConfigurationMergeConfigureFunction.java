package com.yeskery.nut.application;

import com.yeskery.nut.core.MergeNutConfigure;

import java.util.Map;
import java.util.function.Function;

/**
 * Nut合并配置处理函数
 * @author sprout
 * @version 1.0
 * 2022-08-05 02:26
 */
public class NutConfigurationMergeConfigureFunction implements Function<MergeNutConfigure, MergeNutConfigure> {

    /** Nut配置对象 */
    private final NutConfiguration nutConfiguration;

    /**
     * 构建Nut合并配置处理函数
     * @param nutConfiguration Nut配置对象
     */
    public NutConfigurationMergeConfigureFunction(NutConfiguration nutConfiguration) {
        this.nutConfiguration = nutConfiguration;
    }

    @Override
    public MergeNutConfigure apply(MergeNutConfigure mergeNutConfigure) {
        return new MergeNutConfigure() {
            @Override
            protected Map<String, String> getConfiguration() {
                return mergeNutConfigure.getMergeConfigurationMap();
            }

            @Override
            public ServerType getServerType() {
                return nutConfiguration.getServerType() == null ? super.getServerType() : nutConfiguration.getServerType();
            }
        };
    }
}
