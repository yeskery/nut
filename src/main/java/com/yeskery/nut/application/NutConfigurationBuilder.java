package com.yeskery.nut.application;

import java.util.logging.Level;

/**
 * Nut 配置对象构建器
 * @author sprout
 * 2022-05-13 10:46
 */
public class NutConfigurationBuilder {

    /** Nut 配置对象 */
    private NutConfiguration nutConfiguration;

    /**
     * 私有化构造方法
     */
    private NutConfigurationBuilder() {
    }

    /**
     * 获取配置对象构建器
     * @return 配置对象构建器
     */
    public static NutConfigurationBuilder newInstance() {
        NutConfigurationBuilder builder = new NutConfigurationBuilder();
        builder.nutConfiguration = new NutConfiguration();
        return builder;
    }

    /**
     * 构建配置对象
     * @return 构建好的配置对象
     */
    public NutConfiguration build() {
        return nutConfiguration;
    }

    /**
     * 设置启动主类
     * @param mainClass 启动主类
     * @return 构建器对象
     */
    public NutConfigurationBuilder setMainClass(Class<?> mainClass) {
        this.nutConfiguration.setMainClass(mainClass);
        return this;
    }

    /**
     * 设置程序运行类型
     * @param applicationType 程序运行类型
     * @return 构建器对象
     */
    public NutConfigurationBuilder setApplicationType(ApplicationType applicationType) {
        this.nutConfiguration.setApplicationType(applicationType);
        return this;
    }

    /**
     * 设置服务启动类型
     * @param serverType 服务启动类型
     * @return 构建器对象
     */
    public NutConfigurationBuilder setServerType(ServerType serverType) {
        this.nutConfiguration.setServerType(serverType);
        return this;
    }

    /**
     * 设置服务运行环境
     * @param env 服务运行环境
     * @return 构建器对象
     */
    public NutConfigurationBuilder setEnv(String env) {
        this.nutConfiguration.setEnv(env);
        return this;
    }

    /**
     * 设置本地监听的端口号
     * @param port 本地监听的端口号
     * @return 构建器对象
     */
    public NutConfigurationBuilder setPort(Integer port) {
        this.nutConfiguration.setPort(port);
        return this;
    }

    /**
     * 设置是否以HTTP启动服务
     * @param secure 是否以HTTP启动服务
     * @return 构建器对象
     */
    public NutConfigurationBuilder setSecure(Boolean secure) {
        this.nutConfiguration.setSecure(secure);
        return this;
    }

    /**
     * 设置jks证书库路径
     * @param jksPath jks证书库路径
     * @return 构建器对象
     */
    public NutConfigurationBuilder setJksPath(String jksPath) {
        this.nutConfiguration.setJksPath(jksPath);
        return this;
    }

    /**
     * 设置jks证书库密码
     * @param jksPassword jks证书库密码
     * @return 构建器对象
     */
    public NutConfigurationBuilder setJksPassword(String jksPassword) {
        this.nutConfiguration.setJksPassword(jksPassword);
        return this;
    }

    /**
     * 设置静态资源的目录
     * @param staticDirectories 静态资源的目录
     * @return 构建器对象
     */
    public NutConfigurationBuilder setStaticDirectories(String... staticDirectories) {
        this.nutConfiguration.setStaticDirectories(staticDirectories);
        return this;
    }

    /**
     * 设置静态资源路径
     * @param staticPaths 静态资源路径
     * @return 构建器对象
     */
    public NutConfigurationBuilder setStaticPaths(String... staticPaths) {
        this.nutConfiguration.setStaticPaths(staticPaths);
        return this;
    }

    /**
     * 设置静态资源目录预览是否开启
     * @param staticDirectoryPreview 静态资源目录预览是否开启
     * @return 构建器对象
     */
    public NutConfigurationBuilder setStaticDirectoryPreview(Boolean staticDirectoryPreview) {
        this.nutConfiguration.setStaticDirectoryPreview(staticDirectoryPreview);
        return this;
    }

    /**
     * 设置是否开启脚本
     * @param enableScript 是否开启脚本
     * @return 构建器对象
     */
    public NutConfigurationBuilder setEnableScript(Boolean enableScript) {
        this.nutConfiguration.setEnableScript(enableScript);
        return this;
    }

    /**
     * 设置脚本资源的文件路径
     * @param scriptPaths 脚本资源的文件路径
     * @return 构建器对象
     */
    public NutConfigurationBuilder setScriptPaths(String... scriptPaths) {
        this.nutConfiguration.setScriptPaths(scriptPaths);
        return this;
    }

    /**
     * 设置脚本资源的文件编码
     * @param scriptEncoding 脚本资源的文件编码
     * @return 构建器对象
     */
    public NutConfigurationBuilder setScriptEncoding(String scriptEncoding) {
        this.nutConfiguration.setScriptEncoding(scriptEncoding);
        return this;
    }

    /**
     * 设置是否开启脚本文件监听
     * @param enableScriptWatch 是否开启脚本文件监听
     * @return 构建器对象
     */
    public NutConfigurationBuilder setEnableScriptWatch(Boolean enableScriptWatch) {
        this.nutConfiguration.setEnableScriptWatch(enableScriptWatch);
        return this;
    }

    /**
     * 设置日志处理器
     * @param logHandlers 日志处理器
     * @return 构建器对象
     */
    public NutConfigurationBuilder setLogHandlers(String logHandlers) {
        this.nutConfiguration.setLogHandlers(logHandlers);
        return this;
    }

    /**
     * 设置日志格式化器
     * @param logFormatter 日志格式化器
     * @return 构建器对象
     */
    public NutConfigurationBuilder setLogFormatter(String logFormatter) {
        this.nutConfiguration.setLogFormatter(logFormatter);
        return this;
    }

    /**
     * 设置日志等级
     * @param logLevel 日志等级
     * @return 构建器对象
     */
    public NutConfigurationBuilder setLogLevel(String logLevel) {
        this.nutConfiguration.setLogLevel(logLevel);
        return this;
    }

    /**
     * 设置日志等级
     * @param logLevel 日志等级
     * @return 构建器对象
     */
    public NutConfigurationBuilder setLogLevel(Level logLevel) {
        this.nutConfiguration.setLogLevel(logLevel.getName());
        return this;
    }

    /**
     * 设置日志存储目录
     * @param logDir 日志存储目录
     * @return 构建器对象
     */
    public NutConfigurationBuilder setLogDir(String logDir) {
        this.nutConfiguration.setLogDir(logDir);
        return this;
    }
}
