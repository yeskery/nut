package com.yeskery.nut.application;

import com.yeskery.nut.core.BasicNutConfigure;

/**
 * NutServer构建工厂
 * @author sprout
 * @version 1.0
 * 2022-07-27 18:44
 */
public interface NutServerFactory {

    /**
     * 构建一个新的NutServer服务
     * @param nutApplication Nut应用对象
     * @param basicNutConfigure nut基础配置信息
     * @return 构建好后的NutServer服务
     */
    NutServer buildServer(NutApplication nutApplication, BasicNutConfigure basicNutConfigure);

    /**
     * 获取指定的NutServer的ServerType
     * @param nutServer Nut服务对象
     * @return 指定的NutServer的ServerType
     */
    ServerType getServerType(NutServer nutServer);
}
