package com.yeskery.nut.application;

/**
 * 服务器启动类型
 *
 * @author sprout
 * @version 1.0
 * 2019-03-18 23:54
 */
public enum ServerType {
    /** 自动模式 */
    AUTO,
    /** 阻塞IO，不推荐使用 */
    @Deprecated
    BIO,
    /** 非阻塞IO，不推荐使用 */
    @Deprecated
    NIO,
    /** Sun实现 */
    SUN,
    /** Netty */
    NETTY,
    /** Jetty */
    JETTY,
    /**Tomcat */
    TOMCAT
}
