package com.yeskery.nut.application;

import com.yeskery.nut.core.ApplicationRunner;
import com.yeskery.nut.core.AutoDetector;
import com.yeskery.nut.core.CommandLineRunner;
import com.yeskery.nut.core.Environment;
import com.yeskery.nut.script.expression.NutExpression;
import com.yeskery.nut.script.function.Function;
import com.yeskery.nut.script.function.FunctionContext;

import java.util.Set;

/**
 * Nut 相关配置接口
 * @author YESKERY
 * 2023/11/6
 */
public interface NutConfigure extends NutWebConfigure {

    /**
     * 设置安全的服务上下文
     * @param secureServerContext 安全的服务上下文
     * @return 安全的服务上下文
     */
    SecureServerContext setSecureServerContext(SecureServerContext secureServerContext);

    /**
     * 获取安全的服务上下文
     * @return 安全的服务上下文
     */
    SecureServerContext getSecureServerContext();

    /**
     * 获取 Nut 的 {@link FunctionContext}
     * @return Nut 的 {@link FunctionContext}
     */
    FunctionContext getFunctionContext();

    /**
     * 注册自定义脚本视图表达式
     * @param expression 脚本视图表达式
     */
    void registerCustomScriptExpression(NutExpression expression);

    /**
     * 添加注解Controller基础扫描包
     * @param basePackage 基础扫描包
     */
    void addBasePackage(String basePackage);

    /**
     * 添加当前运行注解Controller基础扫描包
     * @param mainClass 运行主类
     */
    void addBasePackage(Class<?> mainClass);

    /**
     * 获取基础扫描路径
     * @return 基础扫描路径
     */
    Set<String> getBasePackages();

    /**
     * 添加要忽略的自动探测类
     * @param autoDetectorClass 要忽略的自动探测类
     */
    void addIgnoreAutoDetectClass(Class<? extends AutoDetector> autoDetectorClass);

    /**
     * 注册自动探测器
     * @param autoDetector 自动探测器
     */
    void registerAutoDetector(AutoDetector autoDetector);

    /**
     * 设置环境对象
     * @param environment 环境对象
     */
    void setEnvironment(Environment environment);

    /**
     * 设置环境对象
     * @param environment 环境对象名称
     */
    void setEnvironment(String environment);

    /**
     * 获取当前的环境对象
     * @return 环境对象
     */
    Environment getEnvironment();

    /**
     * 添加命令行执行函数
     * @param runner 命令行执行函数
     */
    void addRunner(CommandLineRunner runner);

    /**
     * 添加程序执行函数
     * @param runner 程序执行函数
     */
    void addRunner(ApplicationRunner runner);

    /**
     * 注册脚本自定义函数
     * @param function 脚本自定义函数
     */
    void registerCustomScriptFunction(Function function);
}
