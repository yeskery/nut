package com.yeskery.nut.application;

import java.io.Closeable;

/**
 * Nut Server 接口
 * @author sprout
 * @version 1.0
 * 2019-03-18 23:04
 */
public interface NutServer extends NutServerProcess, Closeable {

    /**
     * Nut Server 启动的方法
     * @param nutServerConfigure Nut Server配置对象
     * @throws Exception 启动发生的所有异常
     */
    void startServer(NutServerConfigure nutServerConfigure) throws Exception;

    /**
     * 获取服务启动后提示信息
     * @param port 端口号
     * @param model 启动服务模式
     * @param secure 是否安全
     * @return 服务启动后提示信息
     */
    default String getServerStartedTip(int port, String model, boolean secure) {
        return "Sprout Nut Run Port At: " + port + ", Model: "+ model +", Secure: " + (secure ? "True" : "False");
    }
}
