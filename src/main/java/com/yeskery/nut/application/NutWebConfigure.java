package com.yeskery.nut.application;

import com.yeskery.nut.bind.CustomMethodParamBindHandler;
import com.yeskery.nut.bind.HttpResponseBodyHandler;
import com.yeskery.nut.bind.ResponseBodyHandler;
import com.yeskery.nut.bind.validator.CustomMethodParamValidatorHandler;
import com.yeskery.nut.core.ControllerManager;
import com.yeskery.nut.core.Dispatcher;
import com.yeskery.nut.core.ServerContext;
import com.yeskery.nut.core.SessionManager;
import com.yeskery.nut.plugin.PluginManager;
import com.yeskery.nut.view.WebViewConfigure;

import java.util.Map;
import java.util.Set;

/**
 * Nut Web 相关配置接口
 * @author sprout
 * @version 1.0
 * 2022-06-12 17:23
 */
public interface NutWebConfigure {

    /**
     * 获取 Nut 的请求分发器
     * @return Nut 的请求分发器
     */
    Dispatcher getDispatcher();

    /**
     * 获取Web视图配置对象
     * @return Web视图配置对象
     */
    WebViewConfigure getWebViewConfigure();

    /**
     * 获取 Nut 的服务上下文
     * @return Nut 的服务上下文
     */
    ServerContext getServerContext();

    /**
     * 获取 Nut 的 Controller 管理器
     * @return Controller 管理器
     *
     * @see ControllerManager
     */
    ControllerManager getControllerManager();

    /**
     * 获取 Nut 的 {@link SessionManager}
     * @return Nut 的 {@link SessionManager}
     */
    SessionManager getSessionManager();

    /**
     * 获取 Nut 的 {@link PluginManager}
     * @return Nut 的 {@link PluginManager}
     */
    PluginManager getPluginManager();

    /**
     * 添加自定义方法参数绑定处理器
     * @param bindHandler 方法参数绑定处理器
     * @param <T> 自定义方法参数类型
     */
    <T> void addMethodParamBindHandler(CustomMethodParamBindHandler<T> bindHandler);

    /**
     * 获取自定义方法参数绑定处理器集合
     * @return 自定义方法参数绑定处理器集合
     */
    Map<Class<?>, CustomMethodParamBindHandler<?>> getMethodParamBindHandlers();

    /**
     * 添加自定义参数验证处理器
     * @param validatorHandler 自定义参数验证处理器
     */
    void addMethodParamValidatorHandler(CustomMethodParamValidatorHandler validatorHandler);

    /**
     * 获取自定义参数验证处理器集合
     * @return 自定义参数验证处理器集合
     */
    Set<CustomMethodParamValidatorHandler> getMethodParamValidatorHandlers();

    /**
     * 添加响应体处理器
     * @param responseBodyHandler 响应体处理器
     */
    void addResponseBodyHandler(ResponseBodyHandler responseBodyHandler);

    /**
     * 获取响应体处理器
     * @return 响应体处理器
     */
    Set<ResponseBodyHandler> getResponseBodyHandlers();

    /**
     * 添加Http响应体处理器
     * @param httpResponseBodyHandler Http响应体处理器
     */
    void addHttpResponseBodyHandler(HttpResponseBodyHandler httpResponseBodyHandler);

    /**
     * 获取Http响应体处理器
     * @return Http响应体处理器
     */
    Set<HttpResponseBodyHandler> getHttpResponseBodyHandlers();

    /**
     * 添加静态资源文件路径
     * @param dirs 需要添加静态资源文件路径
     */
    default void addStaticResourceDirs(String... dirs) {
        getControllerManager().addStaticResourceDirs(dirs);
    }

    /**
     * 获取静态资源访问路径
     * @param paths 静态资源访问路径
     */
    default void addStaticResourcePaths(String... paths) {
        getControllerManager().addStaticResourcePaths(paths);
    }
}
