package com.yeskery.nut.application;

/**
 * 默认的应用元数据
 * @author sprout
 * @version 1.0
 * 2023-06-11 11:50
 */
public class DefaultApplicationMetadata implements ApplicationMetadata {

    /** 应用类型 */
    private ApplicationType applicationType;

    /** 服务类型 */
    private ServerType serverType;

    /** 服务端口号 */
    private int serverPort;

    /** 是否以安全方式启动服务 */
    private boolean secureServer;

    /** 服务进程id */
    private String processId;

    @Override
    public ApplicationType getApplicationType() {
        return applicationType;
    }

    /**
     * 设置应用类型
     * @param applicationType 应用类型
     */
    public void setApplicationType(ApplicationType applicationType) {
        this.applicationType = applicationType;
    }

    @Override
    public ServerType getServerType() {
        return serverType;
    }

    /**
     * 设置服务类型
     * @param serverType 服务类型
     */
    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }

    @Override
    public int getServerPort() {
        return serverPort;
    }

    /**
     * 设置服务端口号
     * @param serverPort 服务端口号
     */
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    @Override
    public boolean isSecureServer() {
        return secureServer;
    }

    /**
     * 设置是否以安全方式启动服务
     * @param secureServer 是否以安全方式启动服务
     */
    public void setSecureServer(boolean secureServer) {
        this.secureServer = secureServer;
    }

    @Override
    public String getProcessId() {
        return processId;
    }

    /**
     * 设置服务进程id
     * @param processId 服务进程id
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }
}
