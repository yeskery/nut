package com.yeskery.nut.application;

import com.yeskery.nut.core.NutException;

/**
 * Nut 配置对象
 * @author sprout
 * 2022-05-13 10:46
 */
public class NutConfiguration {

    /** 启动主类 */
    private Class<?> mainClass;

    /** 程序运行类型 */
    private ApplicationType applicationType = ApplicationType.WEB;

    /** 服务启动类型 */
    private ServerType serverType;

    /** 服务运行环境 */
    private String env;

    /** 本地监听的端口号 */
    private Integer port;

    /** 请求最大限制字节数 */
    private Long maxRequestSize;

    /** 是否以HTTP启动服务 */
    private Boolean secure;

    /** 安全证书类型 */
    private SecureKeyType secureKeyType;

    /** jks证书库路径 */
    private String jksPath;

    /** jks证书库密码 */
    private String jksPassword;

    /** pem公钥证书路径 */
    private String pemPublicKeyPath;

    /** pem私钥证书路径 */
    private String pemPrivateKeyPath;

    /** 静态资源的目录 */
    private String[] staticDirectories;

    /** 静态资源路径 */
    private String[] staticPaths;

    /** 静态资源目录预览是否开启 */
    private Boolean staticDirectoryPreview;

    /** 是否开启脚本 */
    private Boolean enableScript;

    /** 脚本资源的文件路径 */
    private String[] scriptPaths;

    /** 脚本资源的文件编码 */
    private String scriptEncoding;

    /** 是否开启脚本文件监听 */
    private Boolean enableScriptWatch;

    /** 日志处理器 */
    private String logHandlers;

    /** 日志格式化器 */
    private String logFormatter;

    /** 日志等级 */
    private String logLevel;

    /** 日志存储目录 */
    private String logDir;

    /**
     * 获取启动主类
     * @return 启动主类
     */
    public Class<?> getMainClass() {
        return mainClass;
    }

    /**
     * 设置启动主类
     * @param mainClass 启动主类
     */
    public void setMainClass(Class<?> mainClass) {
        this.mainClass = mainClass;
    }

    /**
     * 获取程序运行类型
     * @return 程序运行类型
     */
    public ApplicationType getApplicationType() {
        return applicationType;
    }

    /**
     * 设置程序运行类型
     * @param applicationType 程序运行类型
     */
    public void setApplicationType(ApplicationType applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * 获取服务启动类型
     * @return 服务启动类型
     */
    public ServerType getServerType() {
        return serverType;
    }

    /**
     * 设置服务启动类型
     * @param serverType 服务启动类型
     */
    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }

    /**
     * 获取服务运行环境
     * @return 服务运行环境
     */
    public String getEnv() {
        return env;
    }

    /**
     * 设置服务运行环境
     * @param env 服务运行环境
     */
    public void setEnv(String env) {
        this.env = env;
    }

    /**
     * 获取本地监听的端口号
     * @return 本地监听的端口号
     */
    public Integer getPort() {
        return port;
    }

    /**
     * 设置本地监听的端口号
     * @param port 本地监听的端口号
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * 获取请求最大限制字节数
     * @return 请求最大限制字节数
     */
    public Long getMaxRequestSize() {
        return maxRequestSize;
    }

    /**
     * 设置请求最大限制字节数
     * @param maxRequestSize 请求最大限制字节数
     */
    public void setMaxRequestSize(Long maxRequestSize) {
        this.maxRequestSize = maxRequestSize;
    }

    /**
     * 获取是否以HTTP启动服务
     * @return 是否以HTTP启动服务
     */
    public Boolean getSecure() {
        return secure;
    }

    /**
     * 设置是否以HTTP启动服务
     * @param secure 是否以HTTP启动服务
     */
    public void setSecure(Boolean secure) {
        this.secure = secure;
    }

    /**
     * 获取安全证书类型
     * @return 安全证书类型
     */
    public SecureKeyType getSecureKeyType() {
        return secureKeyType;
    }

    /**
     * 设置安全证书类型
     * @param secureKeyType 安全证书类型
     */
    public void setSecureKeyType(SecureKeyType secureKeyType) {
        this.secureKeyType = secureKeyType;
    }

    /**
     * 设置安全证书类型
     * @param secureKeyType 安全证书类型
     */
    public void setSecureKeyType(String secureKeyType) {
        try {
            setSecureKeyType(SecureKeyType.valueOf(secureKeyType.toUpperCase()));
        } catch (IllegalArgumentException e) {
            throw new NutException("UnSupport Secure Key Type [" + secureKeyType.toUpperCase() +"]", e);
        }
    }

    /**
     * 获取jks证书库路径
     * @return jks证书库路径
     */
    public String getJksPath() {
        return jksPath;
    }

    /**
     * 设置jks证书库路径
     * @param jksPath jks证书库路径
     */
    public void setJksPath(String jksPath) {
        this.jksPath = jksPath;
    }

    /**
     * 获取jks证书库密码
     * @return jks证书库密码
     */
    public String getJksPassword() {
        return jksPassword;
    }

    /**
     * 设置jks证书库密码
     * @param jksPassword jks证书库密码
     */
    public void setJksPassword(String jksPassword) {
        this.jksPassword = jksPassword;
    }

    /**
     * 获取pem公钥证书路径
     * @return pem公钥证书路径
     */
    public String getPemPublicKeyPath() {
        return pemPublicKeyPath;
    }

    /**
     * 设置pem公钥证书路径
     * @param pemPublicKeyPath pem公钥证书路径
     */
    public void setPemPublicKeyPath(String pemPublicKeyPath) {
        this.pemPublicKeyPath = pemPublicKeyPath;
    }

    /**
     * 获取pem私钥证书路径
     * @return pem私钥证书路径
     */
    public String getPemPrivateKeyPath() {
        return pemPrivateKeyPath;
    }

    /**
     * 设置pem私钥证书路径
     * @param pemPrivateKeyPath pem私钥证书路径
     */
    public void setPemPrivateKeyPath(String pemPrivateKeyPath) {
        this.pemPrivateKeyPath = pemPrivateKeyPath;
    }

    /**
     * 获取静态资源的目录
     * @return 静态资源的目录
     */
    public String[] getStaticDirectories() {
        return staticDirectories;
    }

    /**
     * 设置静态资源的目录
     * @param staticDirectories 静态资源的目录
     */
    public void setStaticDirectories(String[] staticDirectories) {
        this.staticDirectories = staticDirectories;
    }

    /**
     * 获取静态资源路径
     * @return 静态资源路径
     */
    public String[] getStaticPaths() {
        return staticPaths;
    }

    /**
     * 设置静态资源路径
     * @param staticPaths 静态资源路径
     */
    public void setStaticPaths(String[] staticPaths) {
        this.staticPaths = staticPaths;
    }

    /**
     * 获取静态资源目录预览是否开启
     * @return 静态资源目录预览是否开启
     */
    public Boolean getStaticDirectoryPreview() {
        return staticDirectoryPreview;
    }

    /**
     * 设置静态资源目录预览是否开启
     * @param staticDirectoryPreview 静态资源目录预览是否开启
     */
    public void setStaticDirectoryPreview(Boolean staticDirectoryPreview) {
        this.staticDirectoryPreview = staticDirectoryPreview;
    }

    /**
     * 获取是否开启脚本
     * @return 是否开启脚本
     */
    public Boolean getEnableScript() {
        return enableScript;
    }

    /**
     * 设置是否开启脚本
     * @param enableScript 是否开启脚本
     */
    public void setEnableScript(Boolean enableScript) {
        this.enableScript = enableScript;
    }

    /**
     * 获取脚本资源的文件路径
     * @return 脚本资源的文件路径
     */
    public String[] getScriptPaths() {
        return scriptPaths;
    }

    /**
     * 设置脚本资源的文件路径
     * @param scriptPaths 脚本资源的文件路径
     */
    public void setScriptPaths(String[] scriptPaths) {
        this.scriptPaths = scriptPaths;
    }

    /**
     * 获取脚本资源的文件编码
     * @return 脚本资源的文件编码
     */
    public String getScriptEncoding() {
        return scriptEncoding;
    }

    /**
     * 设置脚本资源的文件编码
     * @param scriptEncoding 脚本资源的文件编码
     */
    public void setScriptEncoding(String scriptEncoding) {
        this.scriptEncoding = scriptEncoding;
    }

    /**
     * 获取是否开启脚本文件监听
     * @return 是否开启脚本文件监听
     */
    public Boolean getEnableScriptWatch() {
        return enableScriptWatch;
    }

    /**
     * 设置是否开启脚本文件监听
     * @param enableScriptWatch 是否开启脚本文件监听
     */
    public void setEnableScriptWatch(Boolean enableScriptWatch) {
        this.enableScriptWatch = enableScriptWatch;
    }

    /**
     * 获取日志处理器
     * @return 日志处理器
     */
    public String getLogHandlers() {
        return logHandlers;
    }

    /**
     * 设置日志处理器
     * @param logHandlers 日志处理器
     */
    public void setLogHandlers(String logHandlers) {
        this.logHandlers = logHandlers;
    }

    /**
     * 获取日志格式化器
     * @return 日志格式化器
     */
    public String getLogFormatter() {
        return logFormatter;
    }

    /**
     * 设置日志格式化器
     * @param logFormatter 日志格式化器
     */
    public void setLogFormatter(String logFormatter) {
        this.logFormatter = logFormatter;
    }

    /**
     * 获取日志等级
     * @return 日志等级
     */
    public String getLogLevel() {
        return logLevel;
    }

    /**
     * 设置日志等级
     * @param logLevel 日志等级
     */
    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    /**
     * 获取日志存储目录
     * @return 日志存储目录
     */
    public String getLogDir() {
        return logDir;
    }

    /**
     * 设置日志存储目录
     * @param logDir 日志存储目录
     */
    public void setLogDir(String logDir) {
        this.logDir = logDir;
    }

    /**
     * 创建Nut 配置对象
     */
    NutConfiguration() {
    }
}
