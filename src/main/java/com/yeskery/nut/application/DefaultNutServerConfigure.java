package com.yeskery.nut.application;

import com.yeskery.nut.core.*;
import com.yeskery.nut.plugin.PluginManager;

/**
 * 默认的Nut Server配置对象
 * @author sprout
 * @version 1.0
 * 2023-04-15 17:52
 */
public class DefaultNutServerConfigure implements NutServerConfigure {

    /** Nut应用对象 */
    private NutApplication nutApplication;

    /** 端口号 */
    private int port;

    /** 服务请求配置对象 */
    private ServerRequestConfiguration serverRequestConfiguration;

    /** 服务上下文 */
    private ServerContext serverContext;

    /** 请求转发器 */
    private Dispatcher dispatcher;

    /** controller 管理器 */
    private ControllerManager controllerManager;

    /** session管理器 */
    private SessionManager sessionManager;

    /** 插件管理器 */
    private PluginManager pluginManager;

    @Override
    public NutApplication getNutApplication() {
        return nutApplication;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public ServerRequestConfiguration getServerRequestConfiguration() {
        return serverRequestConfiguration;
    }

    @Override
    public ServerContext getServerContext() {
        return serverContext;
    }

    @Override
    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    @Override
    public ControllerManager getControllerManager() {
        return controllerManager;
    }

    @Override
    public SessionManager getSessionManager() {
        return sessionManager;
    }

    @Override
    public PluginManager getPluginManager() {
        return pluginManager;
    }

    /**
     * 设置Nut应用对象
     * @param nutApplication Nut应用对象
     */
    public void setNutApplication(NutApplication nutApplication) {
        this.nutApplication = nutApplication;
    }

    /**
     * 设置端口号
     * @param port 端口号
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * 设置服务请求配置对象
     * @param serverRequestConfiguration 服务请求配置对象
     */
    public void setServerRequestConfiguration(ServerRequestConfiguration serverRequestConfiguration) {
        this.serverRequestConfiguration = serverRequestConfiguration;
    }

    /**
     * 设置服务上下文
     * @param serverContext 服务上下文
     */
    public void setServerContext(ServerContext serverContext) {
        this.serverContext = serverContext;
    }

    /**
     * 设置请求转发器
     * @param dispatcher 请求转发器
     */
    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    /**
     * 设置controller管理器
     * @param controllerManager controller管理器
     */
    public void setControllerManager(ControllerManager controllerManager) {
        this.controllerManager = controllerManager;
    }

    /**
     * 设置session管理器
     * @param sessionManager session管理器
     */
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    /**
     * 设置插件管理器
     * @param pluginManager 插件管理器
     */
    public void setPluginManager(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }
}
