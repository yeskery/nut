package com.yeskery.nut.application.tomcat;

import com.yeskery.nut.websocket.WebSocketConfiguration;
import org.apache.catalina.Context;

import java.util.Collection;

/**
 * Tomcat WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 15:42
 */
public interface TomcatWebSocketLoader {

    /**
     * 添加WebSocket
     * @param context Tomcat Server 上下文对象
     */
    void addWsContextListener(Context context);

    /**
     * 注册WebSocket服务端点
     * @param context Tomcat Server 上下文对象
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    void registerEndpoints(Context context, Collection<WebSocketConfiguration> webSocketConfigurations);
}
