package com.yeskery.nut.application.tomcat;

import com.yeskery.nut.websocket.WebSocketConfiguration;
import org.apache.catalina.Context;

import java.util.Collection;

/**
 * 空的Tomcat WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 15:43
 */
public class EmptyTomcatWebSocketLoader implements TomcatWebSocketLoader {

    /**
     * 构建Tomcat WebSocket加载器
     */
    EmptyTomcatWebSocketLoader() {
    }

    @Override
    public void addWsContextListener(Context context) {

    }

    @Override
    public void registerEndpoints(Context context, Collection<WebSocketConfiguration> webSocketConfigurations) {

    }
}
