package com.yeskery.nut.application.tomcat;

import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.util.ClassUtils;

/**
 * Tomcat WebSocket启动器构造工厂
 * @author sprout
 * @version 1.0
 * 2023-04-16 15:47
 */
public class TomcatWebSocketLoaderFactory {

    /** WebSocket依赖检查class名称 */
    private static final String WS_DEPEND_CHECK_CLASS_NAME = "org.apache.tomcat.websocket.server.WsServerContainer";

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /**
     * 构建Tomcat WebSocket启动器构造工厂
     * @param applicationContext 应用上下文
     */
    public TomcatWebSocketLoaderFactory(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 构建 Tomcat WebSocket启动器
     * @return Tomcat WebSocket启动器
     */
    public TomcatWebSocketLoader buildTomcatWebSocketLoader() {
        return ClassUtils.isExistTargetClass(WS_DEPEND_CHECK_CLASS_NAME)
                ? new JavaxTomcatWebSocketLoader(applicationContext)
                : new EmptyTomcatWebSocketLoader();
    }
}
