package com.yeskery.nut.application.jetty;

import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.websocket.JavaxWebSocketLoader;
import com.yeskery.nut.websocket.WebSocketConfiguration;
import com.yeskery.nut.websocket.WebSocketException;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

import javax.servlet.ServletException;
import javax.websocket.server.ServerContainer;
import java.util.Collection;

/**
 * Jetty WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 20:46
 */
public class JavaxJettyWebSocketLoader extends JavaxWebSocketLoader implements JettyWebSocketLoader {
    /**
     * 构建Jetty WebSocket加载器
     * @param applicationContext 应用上下文
     */
    public JavaxJettyWebSocketLoader(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    @Override
    public void registerEndpoints(ServletContextHandler context, Collection<WebSocketConfiguration> webSocketConfigurations) {
        try {
            ServerContainer serverContainer = WebSocketServerContainerInitializer.initialize(context);
            registerEndpoints(serverContainer, webSocketConfigurations);
        } catch (ServletException e) {
            throw new WebSocketException("Jetty ServerContainer Obtain Fail.", e);
        }
    }
}
