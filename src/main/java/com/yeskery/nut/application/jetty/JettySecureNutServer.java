package com.yeskery.nut.application.jetty;

import com.yeskery.nut.application.SecureServerContext;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ssl.SslContextFactory;

/**
 * 基于Jetty的HttpServer实现HTTPS服务
 * @author sprout
 * @version 1.0
 * 2022-08-05 01:57
 */
public class JettySecureNutServer extends JettyNutServer {

    /** SecureServerContext对象 */
    private final SecureServerContext secureServerContext;

    /**
     * 构建基于Jetty的HttpServer实现HTTPS服务
     * @param secureServerContext SecureServerContext对象
     */
    public JettySecureNutServer(SecureServerContext secureServerContext) {
        this.secure = true;
        this.secureServerContext = secureServerContext;
    }

    @Override
    protected ServerConnector buildServerConnector(Server server) throws Exception {
        HttpConfiguration httpConfig = new HttpConfiguration();
        httpConfig.addCustomizer(new SecureRequestCustomizer());

        HttpConnectionFactory http11 = new HttpConnectionFactory(httpConfig);

        SslContextFactory.Server sslContextFactory = new SslContextFactory.Server();
        sslContextFactory.setSslContext(secureServerContext.getSslContext(SecureServerContext.TLS_V1_2));
        SslConnectionFactory tls = new SslConnectionFactory(sslContextFactory, http11.getProtocol());

        return new ServerConnector(server, tls, http11);
    }

    @Override
    protected String getThreadPoolName() {
        return super.getThreadPoolName() + "[s]";
    }
}
