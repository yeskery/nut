package com.yeskery.nut.application.jetty;

import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.util.ClassUtils;

/**
 * Jetty WebSocket启动器构造工厂
 * @author sprout
 * @version 1.0
 * 2023-04-16 20:56
 */
public class JettyWebSocketLoaderFactory {

    /**
     * WebSocket依赖检查class名称
     */
    private static final String WS_DEPEND_CHECK_CLASS_NAME = "org.eclipse.jetty.websocket.jsr356.server.ServerContainer";

    /** 应用上下文 */
    private final ApplicationContext applicationContext;

    /**
     * 构建Jetty WebSocket启动器构造工厂
     * @param applicationContext 应用上下文
     */
    public JettyWebSocketLoaderFactory(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 构建 Jetty WebSocket启动器
     * @return jetty WebSocket启动器
     */
    public JettyWebSocketLoader buildJettyWebSocketLoader() {
        return ClassUtils.isExistTargetClass(WS_DEPEND_CHECK_CLASS_NAME)
                ? new JavaxJettyWebSocketLoader(applicationContext)
                : new EmptyJettyWebSocketLoader();
    }
}
