package com.yeskery.nut.application.jetty;

import com.yeskery.nut.websocket.WebSocketConfiguration;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.util.Collection;

/**
 * Jetty WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 20:51
 */
public interface JettyWebSocketLoader {

    /**
     * 注册WebSocket服务端点
     * @param context 应用空间处理器
     * @param webSocketConfigurations WebSocket配置对象集合
     */
    void registerEndpoints(ServletContextHandler context, Collection<WebSocketConfiguration> webSocketConfigurations);
}
