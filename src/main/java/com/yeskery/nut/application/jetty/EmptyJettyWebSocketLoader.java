package com.yeskery.nut.application.jetty;

import com.yeskery.nut.websocket.WebSocketConfiguration;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.util.Collection;

/**
 * 空的Jetty WebSocket加载器
 * @author sprout
 * @version 1.0
 * 2023-04-16 20:55
 */
public class EmptyJettyWebSocketLoader implements JettyWebSocketLoader {
    @Override
    public void registerEndpoints(ServletContextHandler context, Collection<WebSocketConfiguration> webSocketConfigurations) {

    }
}
