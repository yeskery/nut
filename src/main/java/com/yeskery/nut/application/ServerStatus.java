package com.yeskery.nut.application;

/**
 * 服务状态枚举
 * @author sprout
 * @version 1.0
 * 2023-04-22 18:08
 */
public enum ServerStatus {
    /** 启动中 */
    STARTING(0),
    /** 启动完成 */
    STARTED(1),
    /** 关闭中 */
    CLOSING(2),
    /** 关闭完成 */
    CLOSED(3);

    /** 状态值 */
    private final int status;

    /**
     * 构建服务状态枚举
     * @param status 状态值
     */
    ServerStatus(int status) {
        this.status = status;
    }

    /**
     * 获取状态值
     * @return 状态值
     */
    public int getStatus() {
        return status;
    }
}
