package com.yeskery.nut.application;

/**
 * 安全证书类型
 * @author sprout
 * @version 1.0
 * 2022-07-27 18:24
 */
public enum SecureKeyType {
    /** JKS证书 */
    JKS,
    /** PEM证书 */
    PEM
}
