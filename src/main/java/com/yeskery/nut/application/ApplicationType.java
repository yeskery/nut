package com.yeskery.nut.application;

/**
 * 程序运行类型
 * @author sprout
 * @version 1.0
 * 2022-07-28 00:54
 */
public enum ApplicationType {
    /** 标准模式(非阻塞)，非web环境模式 */
    STANDARD(true, false, false),
    /** 标准模式(阻塞)，非web环境模式 */
    STANDARD_BLOCKED(true, false, false),
    /** 测试环境模式 */
    TEST(false, false, true),
    /** WEB模式 */
    WEB(false, true, false),
    /** 无状态WEB模式，将不会使用NSessionId进行用户跟踪，Session也无法使用 */
    STATE_LESS_WEB(false, true, false),
    /** WEB测试模式 */
    TEST_WEB(false, true, true),
    /** 无状态WEB测试模式，将不会使用NSessionId进行用户跟踪，Session也无法使用 */
    TEST_STATE_LESS_WEB(false, true, true);

    /** 是否标准模式 */
    private final boolean isStandardModel;
    /** 是否Web模式 */
    private final boolean isWebModel;
    /** 是否测试模式 */
    private final boolean isTestMode;

    ApplicationType(boolean isStandardModel, boolean isWebModel, boolean isTestMode) {
        this.isStandardModel = isStandardModel;
        this.isWebModel = isWebModel;
        this.isTestMode = isTestMode;
    }

    /**
     * 当前应用类型是否为标准模式
     * @param applicationType 程序运行类型
     * @return 当前应用类型是否为测试模式
     */
    public static boolean isStandardApplicationType(ApplicationType applicationType) {
        return applicationType.isStandardModel;
    }

    /**
     * 当前应用类型是否为测试模式
     * @param applicationType 程序运行类型
     * @return 当前应用类型是否为测试模式
     */
    public static boolean isTestApplicationType(ApplicationType applicationType) {
        return applicationType.isTestMode;
    }

    /**
     * 当前应用类型是否为Web模式
     * @param applicationType 程序运行类型
     * @return 当前应用类型是否为Web模式
     */
    public static boolean isWebApplicationType(ApplicationType applicationType) {
        return applicationType.isWebModel;
    }

    /**
     * 当前应用类型是否为无状态Web模式
     * @param applicationType 程序运行类型
     * @return 当前应用类型是否为无状态Web模式
     */
    public static boolean isLessWebApplicationType(ApplicationType applicationType) {
        return applicationType == STATE_LESS_WEB || applicationType == TEST_STATE_LESS_WEB;
    }

    /**
     * 当前应用类型是否为完全Web模式
     * @param applicationType 程序运行类型
     * @return 当前应用类型是否为完全Web模式
     */
    public static boolean isFullWebApplicationType(ApplicationType applicationType) {
        return applicationType == WEB || applicationType == TEST_WEB;
    }
}
