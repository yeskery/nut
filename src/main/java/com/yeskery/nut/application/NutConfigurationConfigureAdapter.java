package com.yeskery.nut.application;

import com.yeskery.nut.core.BaseNutConfigure;
import com.yeskery.nut.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Nut配置对象适配器，适配{@link NutConfigure}配置信息接口
 * @author sprout
 * @version 1.0
 * 2022-07-28 00:10
 */
public class NutConfigurationConfigureAdapter extends BaseNutConfigure {

    /** 多值分隔符 */
    private static final String MULTI_VALUE_SEPARATOR = ",";

    /** Nut配置对象 */
    private final NutConfiguration nutConfiguration;

    /**
     * 构建Nut配置对象适配器
     * @param nutConfiguration Nut配置对象
     */
    public NutConfigurationConfigureAdapter(NutConfiguration nutConfiguration) {
        this.nutConfiguration = nutConfiguration;
    }

    @Override
    protected Map<String, String> getConfiguration() {
        Map<String, String> configurationMap = new HashMap<>(8);
        if (!StringUtils.isEmpty(nutConfiguration.getEnv())) {
            configurationMap.put(PREFIX + CONFIGURE_ENV_NAME, nutConfiguration.getEnv());
        }
        if (nutConfiguration.getPort() != null) {
            configurationMap.put(PREFIX + CONFIGURE_PORT_NAME, nutConfiguration.getPort().toString());
        }
        if (nutConfiguration.getSecure() != null) {
            configurationMap.put(PREFIX + CONFIGURE_SECURE_NAME, nutConfiguration.getSecure().toString());
        }
        if (nutConfiguration.getSecureKeyType() != null) {
            configurationMap.put(PREFIX + CONFIGURE_SECURE_KEY_TYPE_NAME, nutConfiguration.getSecureKeyType().toString());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getJksPath())) {
            configurationMap.put(PREFIX + CONFIGURE_JKS_PATH_NAME, nutConfiguration.getJksPath());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getJksPassword())) {
            configurationMap.put(PREFIX + CONFIGURE_JKS_PASSWORD_NAME, nutConfiguration.getJksPassword());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getPemPublicKeyPath())) {
            configurationMap.put(PREFIX + CONFIGURE_PEM_PUBLIC_KEY_PATH, nutConfiguration.getPemPublicKeyPath());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getPemPrivateKeyPath())) {
            configurationMap.put(PREFIX + CONFIGURE_PEM_PRIVATE_KEY_PATH, nutConfiguration.getPemPrivateKeyPath());
        }
        String[] staticDirectories = nutConfiguration.getStaticDirectories();
        if (staticDirectories != null && staticDirectories.length > 0) {
            configurationMap.put(PREFIX + CONFIGURE_STATIC_DIR_NAME, String.join(MULTI_VALUE_SEPARATOR, staticDirectories));
        }
        String[] staticPaths = nutConfiguration.getStaticPaths();
        if (staticPaths != null && staticPaths.length > 0) {
            configurationMap.put(PREFIX + CONFIGURE_STATIC_PATH_NAME, String.join(MULTI_VALUE_SEPARATOR, staticPaths));
        }
        if (nutConfiguration.getStaticDirectoryPreview() != null) {
            configurationMap.put(PREFIX + CONFIGURE_STATIC_DIRECTORY_PREVIEW_NAME, nutConfiguration.getStaticDirectoryPreview().toString());
        }
        if (nutConfiguration.getEnableScript() != null) {
            configurationMap.put(PREFIX + CONFIGURE_ENABLE_SCRIPT_NAME, nutConfiguration.getEnableScript().toString());
        }
        String[] scriptPaths = nutConfiguration.getScriptPaths();
        if (scriptPaths != null && scriptPaths.length > 0) {
            configurationMap.put(PREFIX + CONFIGURE_SCRIPT_PATH_NAME, String.join(MULTI_VALUE_SEPARATOR, scriptPaths));
        }
        if (!StringUtils.isEmpty(nutConfiguration.getScriptEncoding())) {
            configurationMap.put(PREFIX + CONFIGURE_SCRIPT_ENCODING_NAME, nutConfiguration.getScriptEncoding());
        }
        if (nutConfiguration.getEnableScriptWatch() != null) {
            configurationMap.put(PREFIX + CONFIGURE_ENABLE_SCRIPT_WATCH_NAME, nutConfiguration.getEnableScriptWatch().toString());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getLogHandlers())) {
            configurationMap.put(PREFIX + CONFIGURE_LOG_HANDLERS_NAME, nutConfiguration.getLogHandlers());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getLogFormatter())) {
            configurationMap.put(PREFIX + CONFIGURE_LOG_FORMATTER_NAME, nutConfiguration.getLogFormatter());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getLogLevel())) {
            configurationMap.put(PREFIX + CONFIGURE_LOG_LEVEL_NAME, nutConfiguration.getLogLevel());
        }
        if (!StringUtils.isEmpty(nutConfiguration.getLogDir())) {
            configurationMap.put(PREFIX + CONFIGURE_LOG_DIR_NAME, nutConfiguration.getLogDir());
        }
        return configurationMap;
    }
}
