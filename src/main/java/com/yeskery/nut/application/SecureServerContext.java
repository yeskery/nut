package com.yeskery.nut.application;

import javax.net.ssl.SSLContext;
/**
 * 获取 {@link SSLContext} 的接口，可自定义SSL实现方式
 * @author sprout
 * 2019-03-22 10:17
 * @version 1.0
 */
@FunctionalInterface
public interface SecureServerContext {

	/** Supports some version of SSL; may support other versions */
	String SSL = "SSL";

	/** Supports SSL version 2 or later; may support other versions */
	String SSL_V2 = "SSLv2";

	/** Supports SSL version 3; may support other versions */
	String SSL_V3 = "SSLv3";

	/** Supports some version of TLS; may support other versions */
	String TLS = "TLS";

	/** Supports RFC 2246: TLS version 1.0 ; may support other versions */
	String TLS_V1 = "TLSv1";

	/** Supports RFC 4346: TLS version 1.1 ; may support other versions */
	String TLS_V1_1 = "TLSv1.1";

	/** Supports RFC 5246: TLS version 1.2 ; may support other versions */
	String TLS_V1_2 = "TLSv1.2";

	/**
	 * 获取 {@link SSLContext} 的接口
	 * @param algorithm 加密算法
	 * @return {@link SSLContext} 的接口
	 * @throws Exception 构建 {@link SSLContext} 出现的异常
	 */
	SSLContext getSslContext(String algorithm) throws Exception;

	/**
	 * 获取 {@link SSLContext} 的接口，默认使用 SSLv3
	 * @return {@link SSLContext} 的接口
	 * @throws Exception 构建 {@link SSLContext} 出现的异常
	 */
	default SSLContext getSslContext() throws Exception {
		return getSslContext(SSL_V3);
	}
}
