package com.yeskery.nut.application;

import com.yeskery.nut.core.*;
import com.yeskery.nut.plugin.PluginManager;

/**
 * Nut Server配置接口
 * @author sprout
 * @version 1.0
 * 2023-04-15 17:46
 */
public interface NutServerConfigure {

    /**
     * 获取Nut应用对象
     * @return Nut应用对象
     */
    NutApplication getNutApplication();

    /**
     * 获取端口号
     * @return 端口号
     */
    int getPort();

    /**
     * 获取服务请求配置对象
     * @return 服务请求配置对象
     */
    ServerRequestConfiguration getServerRequestConfiguration();

    /**
     * 获取服务上下文
     * @return 服务上下文
     */
    ServerContext getServerContext();

    /**
     * 获取请求转发器
     * @return 请求转发器
     */
    Dispatcher getDispatcher();

    /**
     * 获取controller 管理器
     * @return controller 管理器
     */
    ControllerManager getControllerManager();

    /**
     * 获取session 管理器
     * @return session 管理器
     */
    SessionManager getSessionManager();

    /**
     * 获取插件管理器
     * @return 插件管理器
     */
    PluginManager getPluginManager();
}
