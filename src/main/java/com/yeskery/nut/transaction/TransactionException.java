package com.yeskery.nut.transaction;

import com.yeskery.nut.core.NutException;

/**
 * 事务异常
 * @author sprout
 * @version 1.0
 * 2022-08-27 13:49
 */
public class TransactionException extends NutException {
    /**
     * 构建一个 {@link TransactionException}
     */
    public TransactionException() {
    }

    /**
     * 构建一个 {@link TransactionException}
     * @param message message
     */
    public TransactionException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link TransactionException}
     * @param message message
     * @param cause cause
     */
    public TransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link TransactionException}
     * @param cause cause
     */
    public TransactionException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link TransactionException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public TransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
