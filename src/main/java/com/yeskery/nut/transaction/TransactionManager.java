package com.yeskery.nut.transaction;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Optional;

/**
 * 事务管理器接口
 * @author sprout
 * @version 1.0
 * 2022-08-27 14:07
 */
public interface TransactionManager extends TransactionResourceManager {

    /** 默认事务管理器名称 */
    String DEFAULT_TRANSACTION_MANAGER_NAME = "transactionManager";

    /**
     * 设置数据源
     * @param dataSource 数据源
     */
    void setDataSource(DataSource dataSource);

    /**
     * 设置数据源
     * @param connection 连接对象
     */
    void setConnection(Connection connection);

    /**
     * 开始事务
     * @param level 事务隔离级别
     * @param propagation 事务传播行为
     */
    void startTransaction(TransactionIsolationLevel level, Propagation propagation);

    /**
     * 开始事务
     * @param level 事务隔离级别
     */
    default void startTransaction(TransactionIsolationLevel level) {
        startTransaction(level, Propagation.REQUIRED);
    }

    /**
     * 开始事务
     * @param propagation 事务传播行为
     */
    default void startTransaction(Propagation propagation) {
        startTransaction(TransactionIsolationLevel.DEFAULT, propagation);
    }

    /**
     * 开始事务
     */
    default void startTransaction() {
        startTransaction(TransactionIsolationLevel.DEFAULT, Propagation.REQUIRED);
    }

    /**
     * 事务提交
     */
    void commitTransaction();

    /**
     * 事务回滚
     */
    void rollbackTransaction();

    /**
     * 获取当前的事务对象
     * @return 当前的事务对象
     */
    Transaction getCurrentTransaction();

    /**
     * 获取当前的连接对象
     * @return 当前的连接对象
     */
    Connection getCurrentConnection();

    /**
     * 获取当前的事务对象
     * @return 当前的事务对象
     */
    Optional<Transaction> getCurrentTransactionOptional();

    /**
     * 获取当前的连接对象
     * @return 当前的连接对象
     */
    Optional<Connection> getCurrentConnectionOptional();

    /**
     * 注册事务完成后回调方法
     * @param afterCompletedTransactionCallback 事务完成后回调方法
     */
    void registerAfterCompletedTransactionCallback(TransactionCallback afterCompletedTransactionCallback);

    /**
     * 注册事务提交前回调方法
     * @param beforeCommitTransactionCallback 事务提交前回调方法
     */
    void registerBeforeCommitTransactionCallback(TransactionCallback beforeCommitTransactionCallback);

    /**
     * 注册事务提交后回调方法
     * @param afterCommitTransactionCallback 事务提交后回调方法
     */
    void registerAfterCommitTransactionCallback(TransactionCallback afterCommitTransactionCallback);

    /**
     * 注册事务回滚后回调方法
     * @param afterRollbackTransactionCallback 事务回滚后回调方法
     */
    void registerAfterRollbackTransactionCallback(TransactionCallback afterRollbackTransactionCallback);
}
