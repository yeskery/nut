package com.yeskery.nut.transaction;

/**
 * 事务回调接口
 * @author sprout
 * @version 1.0
 * 2022-08-27 23:49
 */
@FunctionalInterface
public interface TransactionCallback {

    /**
     * 回调方法
     * @throws TransactionException 回调异常
     */
    void callback() throws TransactionException;
}
