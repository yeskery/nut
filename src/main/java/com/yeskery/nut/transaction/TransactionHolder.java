package com.yeskery.nut.transaction;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 事务持有对象
 * @author sprout
 * @version 1.0
 * 2022-08-27 14:05
 */
public class TransactionHolder {

    /** 事务对象 */
    private final Transaction transaction;

    /** 当前事务的连接对象 */
    private final Connection connection;

    /** 事务传播行为 */
    private final Propagation propagation;

    /** 事务资源map */
    private final Map<Object, Object> resourceMap;

    /** 提交前的事务回调处理集合 */
    private final List<TransactionCallback> beforeCommitCallbacks;

    /** 提交后的事务回调处理集合 */
    private final List<TransactionCallback> afterCommitCallbacks;

    /** 回滚后的事务回调处理集合 */
    private final List<TransactionCallback> afterRollbackCallbacks;

    /** 完成后的事务回调处理集合 */
    private final List<TransactionCallback> afterCompletedCallbacks;

    /** 事务执行次数 */
    private int count;

    /**
     * 构建事务持有对象
     * @param transaction 事务对象
     * @param connection 当前事务的连接对象
     * @param propagation 事务传播行为
     */
    public TransactionHolder(Transaction transaction, Connection connection, Propagation propagation) {
        this.transaction = transaction;
        this.connection = connection;
        this.propagation = propagation;
        this.resourceMap = new HashMap<>();
        this.beforeCommitCallbacks = new ArrayList<>();
        this.afterCommitCallbacks = new ArrayList<>();
        this.afterRollbackCallbacks = new ArrayList<>();
        this.afterCompletedCallbacks = new ArrayList<>();
        this.count = 1;
    }

    /**
     * 获取事务执行次数
     * @return 事务执行次数
     */
    public int getCount() {
        return count;
    }

    /**
     * 设置事务执行次数
     * @param count 事务执行次数
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取事务对象
     * @return 事务对象
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * 获取当前事务的连接对象
     * @return 当前事务的连接对象
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * 获取事务传播行为
     * @return 事务传播行为
     */
    public Propagation getPropagation() {
        return propagation;
    }

    /**
     * 获取事务资源map
     * @return 事务资源map
     */
    public Map<Object, Object> getResourceMap() {
        return resourceMap;
    }

    /**
     * 获取提交前的事务回调处理集合
     * @return 提交前的事务回调处理集合
     */
    public List<TransactionCallback> getBeforeCommitCallbacks() {
        return beforeCommitCallbacks;
    }

    /**
     * 获取提交后的事务回调处理集合
     * @return 提交后的事务回调处理集合
     */
    public List<TransactionCallback> getAfterCommitCallbacks() {
        return afterCommitCallbacks;
    }

    /**
     * 获取回滚后的事务回调处理集合
     * @return 回滚后的事务回调处理集合
     */
    public List<TransactionCallback> getAfterRollbackCallbacks() {
        return afterRollbackCallbacks;
    }

    /**
     * 获取完成后的事务回调处理集合
     * @return 完成后的事务回调处理集合
     */
    public List<TransactionCallback> getAfterCompletedCallbacks() {
        return afterCompletedCallbacks;
    }
}
