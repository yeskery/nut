package com.yeskery.nut.transaction;

import java.sql.Connection;

/**
 * 事务接口
 * @author sprout
 * @version 1.0
 * 2022-08-27 13:28
 */
public interface Transaction {

    /**
     * 获取当前的连接对象
     * @return 当前的连接对象
     */
    Connection getConnection();

    /**
     * 事务提交
     */
    void commit();

    /**
     * 事务回滚
     */
    void rollback();

    /**
     * 事务关闭
     */
    void close();
}
