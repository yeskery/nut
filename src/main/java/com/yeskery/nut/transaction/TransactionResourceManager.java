package com.yeskery.nut.transaction;

/**
 * 事务资源管理器
 * @author sprout
 * @version 1.0
 * 2022-08-27 14:47
 */
public interface TransactionResourceManager {

    /**
     * 保存事务资源
     * @param name 资源名称
     * @param resource 资源对象
     */
    void putTransactionResource(Object name, Object resource);

    /**
     * 获取事务资源
     * @param name 资源名称
     * @return 资源对象
     */
    Object getTransactionResource(Object name);

    /**
     * 获取指定类型的事务资源
     * @param name 资源名称
     * @param clazz 资源类型类对象
     * @param <T> 资源类型
     * @return 指定类型的事务资源对象
     */
    @SuppressWarnings("unchecked")
    default <T> T getTransactionResource(Object name, Class<T> clazz) {
        return (T) getTransactionResource(name);
    }

    /**
     * 移除事务资源
     * @param name 资源名称
     */
    void removeTransactionResource(Object name);
}
