package com.yeskery.nut.transaction;

/**
 * 事务传播行为
 * @author sprout
 * @version 1.0
 * 2022-08-28 11:24
 */
public enum Propagation {

    /** (默认传播行为)，如果当前有事务，其他就用当前事务，如果当前没有事务，会以新事务运行。 */
    REQUIRED,

    /** 不管当前有没有事务，会以新事务运行。 */
    REQUIRES_NEW,

    /** 如果当前有事务，就以当前事务运行，如果当前没有事务，就以非事务运行。 */
    SUPPORTS,

    /** 如果当前有事务，就以当前事务运行，如果没有当前没有事务，就抛出异常 */
    MANDATORY,

    /** 以非事务执行。 */
    NOT_SUPPORTED,

    /** 以非事务执行，如果当前存在事务，抛出异常。 */
    NEVER
}
