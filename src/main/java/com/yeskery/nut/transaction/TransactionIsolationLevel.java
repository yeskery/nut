package com.yeskery.nut.transaction;

/**
 * 事务隔离级别
 * @author sprout
 * @version 1.0
 * 2022-08-27 13:40
 */
public enum TransactionIsolationLevel {
    /** 默认 */
    DEFAULT(-1),
    /** 事务不支持 */
    NONE(0),
    /** 读未提交 */
    READ_UNCOMMITTED(1),
    /** 读已提交 */
    READ_COMMITTED(2),
    /** 可重复读 */
    REPEATABLE_READ(4),
    /** 串行化 */
    SERIALIZABLE(8),
    /** 快照(Sql Server 2005之后版本提供) */
    SQL_SERVER_SNAPSHOT(4096);

    /** 级别值 */
    private final int level;

    /**
     * 构造一个事务隔离级别枚举对象
     * @param level 级别值
     */
   TransactionIsolationLevel(int level) {
        this.level = level;
    }

    /**
     * 获取事务隔离级别值
     * @return 事务隔离级别值
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * 用级别值获取事务隔离级别枚举对象
     * @param code 级别值
     * @return 事务隔离级别枚举对象
     */
    public static TransactionIsolationLevel valueOf(int code) {
        for (TransactionIsolationLevel level : values()) {
            if (level.level == code) {
                return level;
            }
        }
        return null;
    }
}
