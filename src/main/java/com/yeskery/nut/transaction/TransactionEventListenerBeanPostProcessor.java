package com.yeskery.nut.transaction;

import com.yeskery.nut.annotation.event.TransactionalEventListener;
import com.yeskery.nut.bean.ApplicationContext;
import com.yeskery.nut.event.ApplicationEvent;
import com.yeskery.nut.event.ApplicationListener;
import com.yeskery.nut.event.BaseEventListenerBeanPostProcessor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 事务事件监听器bean后置处理器
 * @author Yeskery
 * 2023/8/9
 */
public class TransactionEventListenerBeanPostProcessor extends BaseEventListenerBeanPostProcessor {

    @Override
    protected Class<? extends Annotation> getEventListenerAnnotationType() {
        return TransactionalEventListener.class;
    }

    @Override
    protected Class<? extends ApplicationEvent> getSupportApplicationEventType(Object eventListenerAnnotation) {
        return ((TransactionalEventListener) eventListenerAnnotation).value();
    }

    @Override
    protected ApplicationListener<?> getApplicationListener(Object bean, Class<?> beanClass, Method method,
                                                            ApplicationContext applicationContext,
                                                            Object eventListenerAnnotation) {
        TransactionalEventListener transactionalEventListener = (TransactionalEventListener) eventListenerAnnotation;
        return new TransactionApplicationListener(
                applicationContext.getBean(TransactionManager.class), transactionalEventListener.phase(),
                transactionalEventListener.value(),
                transactionalEventListener.order(), bean, method);
    }
}
