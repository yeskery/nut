package com.yeskery.nut.transaction;

/**
 * 事务执行的阶段
 * @author sunjay
 * 2023/9/5
 */
public enum TransactionPhase {
    /** 事务提交之前 */
    BEFORE_COMMIT,
    /** 事务提交之后 */
    AFTER_COMMIT,
    /** 事务回滚之后 */
    AFTER_ROLLBACK,
    /** 事务完成之后 */
    AFTER_COMPLETION;
}
