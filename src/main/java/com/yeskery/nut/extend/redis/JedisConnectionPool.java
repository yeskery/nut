package com.yeskery.nut.extend.redis;

import com.yeskery.nut.util.cache.ConnectionCreator;
import com.yeskery.nut.util.cache.ConnectionPoolCache;
import redis.clients.jedis.Jedis;

/**
 * Jedis连接池对象
 * @author sprout
 * @version 1.0
 * 2023-07-09 15:56
 */
public class JedisConnectionPool extends ConnectionPoolCache<Jedis> {

    /**
     * 构建Jedis连接池对象
     * @param connectionCreator 连接创建器
     * @param maxConnectionSize 最大连接数
     * @param maxWaitTimeout 最大等待毫秒数
     */
    public JedisConnectionPool(ConnectionCreator<Jedis> connectionCreator, int maxConnectionSize, int maxWaitTimeout) {
        super(connectionCreator, maxConnectionSize, maxWaitTimeout);
    }

    /**
     * 构建Jedis连接池对象
     * @param connectionCreator 连接创建器
     * @param maxConnectionSize 最大连接数
     */
    public JedisConnectionPool(ConnectionCreator<Jedis> connectionCreator, int maxConnectionSize) {
        super(connectionCreator, maxConnectionSize);
    }

    @Override
    protected void closeConnection(Jedis connection) {
        connection.close();
    }

    @Override
    protected boolean isConnectionClosed(Jedis connection) {
        return !connection.isConnected();
    }
}
