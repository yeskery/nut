package com.yeskery.nut.extend.redis;

import com.yeskery.nut.application.ServerEventContext;
import com.yeskery.nut.plugin.NutApplicationSupportBasePlugin;
import com.yeskery.nut.plugin.PluginBeanMetadata;
import com.yeskery.nut.plugin.ServerEventPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Jedis插件 For Redis
 * @author sprout
 * @version 1.0
 * 2023-07-09 15:30
 */
public class JedisPlugin extends NutApplicationSupportBasePlugin implements ServerEventPlugin {

    /** 日志对象 */
    private static final Logger logger = Logger.getLogger(JedisPlugin.class.getName());

    /** 默认最大连接池大小 */
    private static final int DEFAULT_MAX_CONNECTION_SIZE = 10;

    /** Jedis连接池对象 */
    private final JedisConnectionPool jedisConnectionPool;

    /**
     * 构建一个Jedis插件
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     * @param database 数据库序号
     * @param maxConnectionSize 最大连接数
     * @param maxWaitTimeout 最大等待毫秒数
     */
    public JedisPlugin(String host, int port, String password, int database, int maxConnectionSize, int maxWaitTimeout) {
        jedisConnectionPool = new JedisConnectionPool(new JedisConnectionCreator(host, port, password, database),
                maxConnectionSize, maxWaitTimeout);
    }

    /**
     * 构建一个Jedis插件
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     * @param database 数据库序号
     * @param maxConnectionSize 最大连接数
     */
    public JedisPlugin(String host, int port, String password, int database, int maxConnectionSize) {
        jedisConnectionPool = new JedisConnectionPool(new JedisConnectionCreator(host, port, password, database),
                maxConnectionSize);
    }

    /**
     * 构建一个Jedis插件
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     * @param database 数据库序号
     */
    public JedisPlugin(String host, int port, String password, int database) {
        this(host, port, password, database, DEFAULT_MAX_CONNECTION_SIZE);
    }

    /**
     * 构建一个Jedis插件
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     */
    public JedisPlugin(String host, int port, String password) {
        this(host, port, password, 0);
    }

    /**
     * 构建一个Jedis插件
     * @param host 主机
     * @param port 端口号
     */
    public JedisPlugin(String host, int port) {
        this(host, port, null);
    }

    /**
     * 构建一个Jedis插件
     */
    public JedisPlugin() {
        this("127.0.0.1", 6379);
    }

    @Override
    protected Collection<PluginBeanMetadata> getRegisterPluginBeanMetadata() {
        List<PluginBeanMetadata> pluginBeanMetadataList = new ArrayList<>(2);
        pluginBeanMetadataList.add(new PluginBeanMetadata("jedisConnectionPool",
                jedisConnectionPool, JedisConnectionPool.class));
        pluginBeanMetadataList.add(new PluginBeanMetadata("jedisExecutor",
                new JedisExecutor(jedisConnectionPool), JedisExecutor.class));
        return pluginBeanMetadataList;
    }

    @Override
    public void beforeClose(ServerEventContext serverEventContext) {
        try {
            jedisConnectionPool.close();
        } catch (IOException e) {
            logger.logp(Level.SEVERE, this.getClass().getName(), "beforeClose",
                    "Jedis Connection Pool Close Fail.", e);
        }
    }
}
