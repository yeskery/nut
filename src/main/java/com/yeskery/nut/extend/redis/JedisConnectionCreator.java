package com.yeskery.nut.extend.redis;

import com.yeskery.nut.util.StringUtils;
import com.yeskery.nut.util.cache.ConnectionCreator;
import redis.clients.jedis.Jedis;

/**
 * Jedis连接构造器
 * @author sprout
 * @version 1.0
 * 2023-07-09 16:04
 */
public class JedisConnectionCreator implements ConnectionCreator<Jedis> {

    /** 主机 */
    private final String host;

    /** 端口号 */
    private final int port;

    /** 密码 */
    private final String password;

    /** 数据库序号 */
    private final int database;

    /**
     * 构建Jedis连接构造器
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     * @param database 数据库序号
     */
    public JedisConnectionCreator(String host, int port, String password, int database) {
        this.host = host;
        this.port = port;
        this.password = password;
        this.database = database;
    }

    /**
     * 构建Jedis连接构造器
     * @param host 主机
     * @param port 端口号
     * @param password 密码
     */
    public JedisConnectionCreator(String host, int port, String password) {
        this(host, port, password, 0);
    }

    @Override
    public Jedis create() {
        Jedis jedis = new Jedis(host, port);
        if (!StringUtils.isEmpty(password)) {
            jedis.auth(password);
        }
        jedis.select(database);
        return jedis;
    }
}
