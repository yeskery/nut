package com.yeskery.nut.extend.i18n;

import java.util.Locale;

/**
 * 国际化消息服务
 * @author sprout
 * 2022-05-28 11:41
 */
public interface MessageService {

    /**
     * 获取国际化消息
     * @param key 消息key
     * @return 国际化消息
     */
    default String getMessage(String key) {
        return getMessage(key, new Object[0]);
    }

    /**
     * 获取国际化消息
     * @param key 消息key
     * @param params 消息参数
     * @return 国际化消息
     */
    String getMessage(String key, Object... params);

    /**
     * 获取国际化消息
     * @param locale 区域
     * @param key 消息key
     * @return 国际化消息
     */
    default String getMessage(Locale locale, String key) {
        return getMessage(locale, key, new Object[0]);
    }

    /**
     * 获取国际化消息
     * @param locale 区域
     * @param key 消息key
     * @param params 消息参数
     * @return 国际化消息
     */
    String getMessage(Locale locale, String key, Object... params);
}
