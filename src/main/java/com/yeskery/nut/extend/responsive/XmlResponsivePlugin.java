package com.yeskery.nut.extend.responsive;

import com.yeskery.nut.extend.responsive.convert.SimpleResponsiveConvert;

/**
 * xml响应式处理插件
 * @author sprout
 * 2022-06-08 17:15
 */
public class XmlResponsivePlugin extends BaseResponsivePlugin {

    /** 响应式转换器插件 */
    private final ResponsiveConvert responsiveConvert;

    /**
     * 构建一个响应式处理插件
     * @param responsiveConvert 响应式转换器插件
     */
    public XmlResponsivePlugin(ResponsiveConvert responsiveConvert) {
        this.responsiveConvert = responsiveConvert;
    }

    /**
     * 构建一个响应式处理插件
     */
    public XmlResponsivePlugin() {
        this(new SimpleResponsiveConvert());
    }

    @Override
    protected DefaultResponsive getResponsive() {
        return new XmlResponsive(responsiveConvert);
    }

    @Override
    protected String getTypeName() {
        return "xml";
    }
}
