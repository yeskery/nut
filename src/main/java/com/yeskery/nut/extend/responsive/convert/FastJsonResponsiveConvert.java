package com.yeskery.nut.extend.responsive.convert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.lang.reflect.Type;

/**
 * 基于FastJson的转换器
 * @author sprout
 * 2022-06-16 10:15
 */
public class FastJsonResponsiveConvert extends CustomResponsiveConvert {

    @Override
    public String doConvertTo(Object data) {
        return JSON.toJSONString(data);
    }

    @Override
    public Object doConvertFrom(String data) {
        return JSON.parseObject(data);
    }

    @Override
    public <T> T doConvertFromString(String data, Class<T> clazz) {
        return JSON.parseObject(data, clazz);
    }

    @Override
    public <T> T convertFromStringByType(String data, Type classType) {
        return JSON.parseObject(data, new TypeReference<T>(){
            @Override
            public Type getType() {
                return classType;
            }
        });
    }
}
