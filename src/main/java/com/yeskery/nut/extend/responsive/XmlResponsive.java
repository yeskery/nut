package com.yeskery.nut.extend.responsive;

import com.yeskery.nut.core.MediaType;

/**
 * xml响应式处理对象
 * @author sprout
 * 2022-06-08 17:21
 */
public class XmlResponsive extends DefaultResponsive {

    /**
     * 构建xml响应式处理对象
     * @param responsiveConvert 响应转换接口
     */
    public XmlResponsive(ResponsiveConvert responsiveConvert) {
        super(responsiveConvert, MediaType.APPLICATION_XML);
    }
}
