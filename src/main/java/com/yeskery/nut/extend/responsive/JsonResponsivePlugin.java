package com.yeskery.nut.extend.responsive;

import com.yeskery.nut.extend.responsive.convert.SimpleResponsiveConvert;

/**
 * json响应式处理插件
 * @author sprout
 * 2022-06-08 17:15
 */
public class JsonResponsivePlugin extends BaseResponsivePlugin {

    /** 响应式转换器插件 */
    private final ResponsiveConvert responsiveConvert;

    /**
     * 构建一个响应式处理插件
     * @param responsiveConvert 响应式转换器插件
     */
    public JsonResponsivePlugin(ResponsiveConvert responsiveConvert) {
        this.responsiveConvert = responsiveConvert;
    }

    /**
     * 构建一个响应式处理插件
     */
    public JsonResponsivePlugin() {
        this(new SimpleResponsiveConvert());
    }

    @Override
    protected DefaultResponsive getResponsive() {
        return new JsonResponsive(responsiveConvert);
    }

    @Override
    protected String getTypeName() {
        return "json";
    }
}
