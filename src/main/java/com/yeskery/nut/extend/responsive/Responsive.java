package com.yeskery.nut.extend.responsive;

/**
 * 响应式处理对象
 * @author sprout
 * 2022-06-08 17:10
 */
public interface Responsive {

    /**
     * 从请求体获取请求对象
     * @param clazz 请求类对象
     * @param <T> 请求类型
     * @return 请求对象
     */
    <T> T getBody(Class<T> clazz);

    /**
     * 向响应中传入响应对象
     * @param data 响应对象
     * @param <T> 响应类型
     */
    <T> void writeBody(T data);
}
