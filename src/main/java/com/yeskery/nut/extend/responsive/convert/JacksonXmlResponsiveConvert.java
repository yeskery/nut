package com.yeskery.nut.extend.responsive.convert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.plugin.PluginBeanMetadata;
import com.yeskery.nut.plugin.PluginBeanMetadataExtender;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;

/**
 * 基于Jackson的xml转换器
 * @author sprout
 * 2022-06-16 10:22
 */
public class JacksonXmlResponsiveConvert extends CustomResponsiveConvert implements PluginBeanMetadataExtender {

    /** XmlMapper */
    private final XmlMapper xmlMapper;

    /**
     * 构建基于Jackson的转换器
     */
    public JacksonXmlResponsiveConvert() {
        this.xmlMapper = new XmlMapper();
    }

    /**
     * 构建基于Jackson的转换器
     * @param xmlMapper XmlMapper对象
     */
    public JacksonXmlResponsiveConvert(XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    @Override
    public String doConvertTo(Object data) {
        try {
            return xmlMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new NutException("Serialization Failure.", e);
        }
    }

    @Override
    public Object doConvertFrom(String data) {
        try {
            return xmlMapper.readValue(data, Object.class);
        } catch (JsonProcessingException e) {
            throw new NutException("Serialization Failure.", e);
        }
    }

    @Override
    public <T> T doConvertFromString(String data, Class<T> clazz) {
        try {
            return xmlMapper.readValue(data, clazz);
        } catch (JsonProcessingException e) {
            throw new NutException("Serialization Failure.", e);
        }
    }

    @Override
    public <T> T convertFromStringByType(String data, Type type) {
        try {
            return xmlMapper.readValue(data, new TypeReference<T>() {
                @Override
                public Type getType() {
                    return type;
                }
            });
        } catch (JsonProcessingException e) {
            throw new NutException("Serialization Failure.", e);
        }
    }

    @Override
    public Collection<PluginBeanMetadata> getExtendPluginBeanMetadata() {
        return Collections.singleton(new PluginBeanMetadata("xmlMapper", xmlMapper, XmlMapper.class));
    }
}
