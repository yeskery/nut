package com.yeskery.nut.extend.responsive;

import com.yeskery.nut.core.Convert;

import java.lang.reflect.Type;

/**
 * 响应转换接口
 * @author sprout
 * 2022-06-08 17:09
 */
public interface ResponsiveConvert extends Convert<Object, String> {

    /**
     * 从 {@link java.lang.String} 转换成 {@link T} 类型
     * @param data {@link java.lang.String} 对象
     * @param clazz {@link T} 类对象
     * @param <T> {@link T} 类型
     * @return {@link T} 对象
     */
    <T> T convertFromString(String data, Class<T> clazz);

    /**
     * 从 {@link java.lang.String} 转换成 {@link T} 类型
     * @param data {@link java.lang.String} 对象
     * @param type 类对象
     * @param <T> {@link T} 类型
     * @return {@link T} 对象
     */
    <T> T convertFromStringByType(String data, Type type);
}
