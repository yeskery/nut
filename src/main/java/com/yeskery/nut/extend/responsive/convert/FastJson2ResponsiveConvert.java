package com.yeskery.nut.extend.responsive.convert;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;

import java.lang.reflect.Type;

/**
 * 基于FastJson2的转换器
 * @author sprout
 * 2022-06-16 10:15
 */
public class FastJson2ResponsiveConvert extends CustomResponsiveConvert {
    @Override
    public String doConvertTo(Object data) {
        return JSON.toJSONString(data);
    }

    @Override
    public Object doConvertFrom(String data) {
        return JSON.parseObject(data);
    }

    @Override
    public <T> T doConvertFromString(String data, Class<T> clazz) {
        return JSON.parseObject(data, clazz);
    }

    @Override
    public <T> T convertFromStringByType(String data, Type type) {
        return JSON.parseObject(data, TypeReference.get(type));
    }
}
