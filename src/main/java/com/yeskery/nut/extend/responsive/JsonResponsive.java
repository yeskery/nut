package com.yeskery.nut.extend.responsive;

import com.yeskery.nut.core.MediaType;

/**
 * json响应式处理对象
 * @author sprout
 * 2022-06-08 17:21
 */
public class JsonResponsive extends DefaultResponsive {

    /**
     * 构建json响应式处理对象
     * @param responsiveConvert 响应转换接口
     */
    public JsonResponsive(ResponsiveConvert responsiveConvert) {
        super(responsiveConvert, MediaType.APPLICATION_JSON);
    }
}
