package com.yeskery.nut.extend.responsive.convert;

import com.yeskery.nut.extend.responsive.ResponsiveConvert;

/**
 * 自定义响应转换接口
 * @author sunjay
 * 2023/9/27
 */
public abstract class CustomResponsiveConvert implements ResponsiveConvert {

    /** 响应转换接口 */
    private ResponsiveConvert responsiveConvert;

    @Override
    public String convertTo(Object data) {
        return responsiveConvert == null ? doConvertTo(data) : responsiveConvert.convertTo(data);
    }

    @Override
    public Object convertFrom(String data) {
        return responsiveConvert == null ? doConvertFrom(data) : responsiveConvert.convertFrom(data);
    }

    @Override
    public <T> T convertFromString(String data, Class<T> clazz) {
        return responsiveConvert == null ? doConvertFromString(data, clazz) : responsiveConvert.convertFromString(data, clazz);
    }

    /**
     * 获取响应转换接口
     * @return 响应转换接口
     */
    public ResponsiveConvert getResponsiveConvert() {
        return responsiveConvert;
    }

    /**
     * 设置响应转换接口
     * @param responsiveConvert 响应转换接口
     */
    public void setResponsiveConvert(ResponsiveConvert responsiveConvert) {
        this.responsiveConvert = responsiveConvert;
    }

    /**
     * 执行对象转换字符串
     * @param data 对象
     * @return 转换后的字符串
     */
    protected abstract String doConvertTo(Object data);

    /**
     * 执行字符串转对象
     * @param data 字符串
     * @return 转换后的对象
     */
    protected abstract Object doConvertFrom(String data);

    /**
     * 执行字符串转制定类型对象
     * @param data 字符串
     * @param clazz 对象类
     * @return 转换后的对象
     * @param <T> 对象类型
     */
    protected abstract <T> T doConvertFromString(String data, Class<T> clazz);
}
