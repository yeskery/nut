package com.yeskery.nut.extend.memory;

/**
 * 基于内存的存储接口
 * @author sprout
 * @version 1.0
 * 2022-06-03 11:30
 */
public interface Storage {

    /**
     * 根据键值对进行保存
     * @param key 键
     * @param value 值
     * @return 是否保存成功
     */
    boolean set(String key, Object value);

    /**
     * 根据键获取对应的值
     * @param key 键
     * @return 对应的值
     */
    Object get(String key);

    /**
     * 根据key删除对应的值
     * @param key 键
     * @return 是否删除成功
     */
    boolean remove(String key);

    /**
     * 清空所有的值
     * @return 是否清空成功
     */
    boolean clear();

    /**
     * 存储是否为空
     * @return 存储是否为空
     */
    boolean isEmpty();

    /**
     * 根据键获取对应的值
     * @param key 键
     * @param clazz 值的类
     * @param <T> 值的类型
     * @return 对应的值
     */
    @SuppressWarnings("unchecked")
    default <T> T get(String key, Class<T> clazz) {
        return (T) get(key);
    }
}
