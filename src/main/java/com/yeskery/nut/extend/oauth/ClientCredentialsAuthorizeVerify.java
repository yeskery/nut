package com.yeskery.nut.extend.oauth;

/**
 * 客户端凭证模式授权校验
 * @author YESKERY
 * 2024/10/21
 */
public interface ClientCredentialsAuthorizeVerify<T extends ClientCredentialsRequest> extends ClientAuthorizeVerify<T> {
}
