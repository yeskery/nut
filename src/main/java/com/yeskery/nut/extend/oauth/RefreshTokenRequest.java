package com.yeskery.nut.extend.oauth;

/**
 * 授权码刷新请求对象
 * @author YESKERY
 * 2024/10/21
 */
public class RefreshTokenRequest extends ClientCredentialsRequest {

    /** 授权类型 */
    private String grantType;

    /** 刷新token */
    private String refreshToken;

    /**
     * 获取授权类型
     * @return 授权类型
     */
    public String getGrantType() {
        return grantType;
    }

    /**
     * 设置授权类型
     * @param grantType 授权类型
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    /**
     * 获取刷新token
     * @return 刷新token
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 设置刷新token
     * @param refreshToken 刷新token
     */
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
