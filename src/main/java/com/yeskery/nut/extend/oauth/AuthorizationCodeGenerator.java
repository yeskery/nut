package com.yeskery.nut.extend.oauth;

/**
 * 授权码生成器
 * @author YESKERY
 * 2024/10/21
 */
public interface AuthorizationCodeGenerator {

    /**
     * 生成授权码
     * @param value 值
     * @return 授权码
     */
    String generateAuthorizationCode(String value);

    /**
     * 获取授权码值
     * @param authorizationCode 授权码
     * @return 访问授权码值
     */
    String getAuthorizationCodeValue(String authorizationCode);

    /**
     * 校验授权码是否有效
     * @param authorizationCode 授权码
     * @return 是否有效
     */
    boolean validateAuthorizationCode(String authorizationCode);

    /**
     * 删除授权码
     * @param authorizationCode 授权码
     */
    void deleteAuthorizationCode(String authorizationCode);
}
