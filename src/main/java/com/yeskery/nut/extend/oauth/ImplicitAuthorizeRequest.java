package com.yeskery.nut.extend.oauth;

/**
 * 隐藏式令牌授权请求
 * @author YESKERY
 * 2024/10/23
 */
public class ImplicitAuthorizeRequest extends BaseRequest {

    /** 授权类型 */
    private String responseType;

    /** 授权范围 */
    private String scope;

    /** 重定向地址 */
    private String redirectUri;

    /**
     * 获取授权类型
     * @return 授权类型
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * 设置授权类型
     * @param responseType 授权类型
     */
    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    /**
     * 获取授权范围
     * @return 授权范围
     */
    public String getScope() {
        return scope;
    }

    /**
     * 设置授权范围
     * @param scope 授权范围
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * 获取重定向地址
     * @return 重定向地址
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * 设置重定向地址
     * @param redirectUri 重定向地址
     */
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }
}
