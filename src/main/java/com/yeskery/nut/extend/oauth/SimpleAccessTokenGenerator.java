package com.yeskery.nut.extend.oauth;

import java.util.UUID;

/**
 * 基于UUID的简单访问令牌生成器
 * @author YESKERY
 * 2024/10/22
 */
public class SimpleAccessTokenGenerator<T extends BaseRequest> extends BaseAccessTokenGenerator<T> {

    /**
     * 构造基于UUID的简单访问令牌生成器
     * @param tokenStore token存储接口
     * @param expires 访问token过期时间
     * @param refreshExpires 刷新token过期时间
     */
    public SimpleAccessTokenGenerator(TokenStore tokenStore, long expires, long refreshExpires) {
        super(tokenStore, expires, refreshExpires);
    }

    @Override
    protected String createAccessToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
