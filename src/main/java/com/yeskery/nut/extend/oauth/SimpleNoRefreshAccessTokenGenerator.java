package com.yeskery.nut.extend.oauth;

import java.util.UUID;

/**
 * 基于UUID的简单无刷新访问令牌生成器
 * @author YESKERY
 * 2024/10/22
 */
public class SimpleNoRefreshAccessTokenGenerator<T extends BaseRequest> extends NoRefreshAccessTokenGenerator<T> {

    /**
     * 构造基于UUID的简单无刷新访问令牌生成器
     * @param tokenStore token存储接口
     * @param expires 访问token过期时间
     */
    public SimpleNoRefreshAccessTokenGenerator(TokenStore tokenStore, long expires) {
        super(tokenStore, expires);
    }

    @Override
    protected String createAccessToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
