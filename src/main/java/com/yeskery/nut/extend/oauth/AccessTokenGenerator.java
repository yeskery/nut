package com.yeskery.nut.extend.oauth;

/**
 * 访问令牌生成器
 * @param <T> 授权码请求对象
 * 2024/10/21
 */
public interface AccessTokenGenerator<T extends BaseRequest> {

    /**
     * 生成访问令牌
     * @param request 授权码请求对象
     * @param authTokenValueHandler 令牌值处理器
     * @param <R> 访问令牌对象
     * @return 访问令牌
     */
    <R> AccessTokenResponse<R> generateAccessToken(T request, AuthTokenValueHandler authTokenValueHandler);

    /**
     * 生成访问令牌
     * @param authTokenValueHandler 令牌值处理器
     * @param <R> 访问令牌对象
     * @return 访问令牌
     */
    <R> AccessTokenResponse<R> generateAccessToken(AuthTokenValueHandler authTokenValueHandler);

    /**
     * 生成访问令牌
     * @param request 授权码请求对象
     * @param <R> 访问令牌对象
     * @return 访问令牌
     */
    default <R> AccessTokenResponse<R> generateAccessToken(T request) {
        return generateAccessToken(request, () -> "");
    }

    /**
     * 生成访问令牌
     * @param <R> 访问令牌对象
     * @return 访问令牌
     */
    default <R> AccessTokenResponse<R> generateAccessToken() {
        return generateAccessToken(() -> "");
    }

    /**
     * 获取访问令牌值
     * @param accessToken 访问令牌
     * @return 访问令牌值
     */
    String getAccessTokenValue(String accessToken);

    /**
     * 验证访问令牌
     * @param accessToken 访问令牌
     * @return 是否验证通过
     */
    boolean validateAccessToken(String accessToken);
}
