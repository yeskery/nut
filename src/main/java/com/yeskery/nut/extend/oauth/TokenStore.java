package com.yeskery.nut.extend.oauth;

/**
 * token保存接口
 * @author YESKERY
 * 2024/10/21
 */
public interface TokenStore {

    /**
     * 保存token
     * @param accessToken accessToken
     * @param value 值
     * @param expires 过期时间(秒)
     * @return 保存结果
     */
    boolean save(String accessToken, String value, long expires);

    /**
     * 获取token值
     * @param accessToken accessToken
     * @return token值
     */
    String get(String accessToken);

    /**
     * 验证token是否有效
     * @param accessToken accessToken
     * @return 是否有效
     */
    boolean validate(String accessToken);

    /**
     * 删除token
     * @param accessToken accessToken
     * @return 删除结果
     */
    boolean delete(String accessToken);
}
