package com.yeskery.nut.extend.oauth;

/**
 * 密码式令牌授权请求
 * @author YESKERY
 * 2024/10/23
 */
public class ClientCredentialsGrantRequest extends ClientCredentialsRequest {

    /** 授权类型 */
    private String grantType;

    /**
     * 获取授权类型
     * @return 授权类型
     */
    public String getGrantType() {
        return grantType;
    }

    /**
     * 设置授权类型
     * @param grantType 授权类型
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }
}
