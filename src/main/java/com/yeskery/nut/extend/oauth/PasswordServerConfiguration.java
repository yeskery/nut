package com.yeskery.nut.extend.oauth;

import com.yeskery.nut.core.Method;

/**
 * 密码令牌模式配置
 * @author YESKERY
 * 2024/10/23
 */
public class PasswordServerConfiguration {

    /** 授权uri */
    private String grantUri = "/oauth/token";
    /** 授权请求方法 */
    private Method grantMethod = Method.POST;
    /** 刷新uri */
    private String refreshUri = "/oauth/token/refresh";
    /** 刷新请求方法 */
    private Method refreshMethod = Method.POST;
    /** 鉴权校验器 */
    private ClientAuthorizeVerify<PasswordGrantRequest> authorizeVerify;
    /** 访问令牌生成器 */
    private AccessTokenGenerator<PasswordGrantRequest> accessTokenGenerator;
    /** 刷新校验 */
    private ClientCredentialsAuthorizeVerify<RefreshTokenRequest> refreshVerify;
    /** 刷新令牌生成器 */
    private RefreshAccessTokenGenerator<RefreshTokenRequest> refreshAccessTokenGenerator;
    /** 访问令牌响应处理器 */
    private AccessTokenResponseHandler accessTokenResponseHandler;
    /** 认证令牌值处理器 */
    private AuthTokenValueHandler authTokenValueHandler;

    /**
     * 获取授权uri
     * @return 授权uri
     */
    public String getGrantUri() {
        return grantUri;
    }

    /**
     * 设置授权uri
     * @param grantUri 授权uri
     */
    public void setGrantUri(String grantUri) {
        this.grantUri = grantUri;
    }

    /**
     * 获取授权请求方法
     * @return 授权请求方法
     */
    public Method getGrantMethod() {
        return grantMethod;
    }

    /**
     * 设置授权请求方法
     * @param grantMethod 授权请求方法
     */
    public void setGrantMethod(Method grantMethod) {
        this.grantMethod = grantMethod;
    }

    /**
     * 获取刷新uri
     * @return 刷新uri
     */
    public String getRefreshUri() {
        return refreshUri;
    }

    /**
     * 设置刷新uri
     * @param refreshUri 刷新uri
     */
    public void setRefreshUri(String refreshUri) {
        this.refreshUri = refreshUri;
    }

    /**
     * 获取刷新请求方法
     * @return 刷新请求方法
     */
    public Method getRefreshMethod() {
        return refreshMethod;
    }

    /**
     * 设置刷新请求方法
     * @param refreshMethod 刷新请求方法
     */
    public void setRefreshMethod(Method refreshMethod) {
        this.refreshMethod = refreshMethod;
    }

    /**
     * 获取鉴权校验器
     * @return 鉴权校验器
     */
    public ClientAuthorizeVerify<PasswordGrantRequest> getAuthorizeVerify() {
        return authorizeVerify;
    }

    /**
     * 设置鉴权校验器
     * @param authorizeVerify 鉴权校验器
     */
    public void setAuthorizeVerify(ClientAuthorizeVerify<PasswordGrantRequest> authorizeVerify) {
        this.authorizeVerify = authorizeVerify;
    }

    /**
     * 获取访问令牌生成器
     * @return 访问令牌生成器
     */
    public AccessTokenGenerator<PasswordGrantRequest> getAccessTokenGenerator() {
        return accessTokenGenerator;
    }

    /**
     * 设置访问令牌生成器
     * @param accessTokenGenerator 访问令牌生成器
     */
    public void setAccessTokenGenerator(AccessTokenGenerator<PasswordGrantRequest> accessTokenGenerator) {
        this.accessTokenGenerator = accessTokenGenerator;
    }

    /**
     * 获取刷新校验
     * @return 刷新校验
     */
    public ClientCredentialsAuthorizeVerify<RefreshTokenRequest> getRefreshVerify() {
        return refreshVerify;
    }

    /**
     * 设置刷新校验
     * @param refreshVerify 刷新校验
     */
    public void setRefreshVerify(ClientCredentialsAuthorizeVerify<RefreshTokenRequest> refreshVerify) {
        this.refreshVerify = refreshVerify;
    }

    /**
     * 获取刷新令牌生成器
     * @return 刷新令牌生成器
     */
    public RefreshAccessTokenGenerator<RefreshTokenRequest> getRefreshAccessTokenGenerator() {
        return refreshAccessTokenGenerator;
    }

    /**
     * 设置刷新令牌生成器
     * @param refreshAccessTokenGenerator 刷新令牌生成器
     */
    public void setRefreshAccessTokenGenerator(RefreshAccessTokenGenerator<RefreshTokenRequest> refreshAccessTokenGenerator) {
        this.refreshAccessTokenGenerator = refreshAccessTokenGenerator;
    }

    /**
     * 获取访问令牌响应处理器
     * @return 访问令牌响应处理器
     */
    public AccessTokenResponseHandler getAccessTokenResponseHandler() {
        return accessTokenResponseHandler;
    }

    /**
     * 设置访问令牌响应处理器
     * @param accessTokenResponseHandler 访问令牌响应处理器
     */
    public void setAccessTokenResponseHandler(AccessTokenResponseHandler accessTokenResponseHandler) {
        this.accessTokenResponseHandler = accessTokenResponseHandler;
    }

    /**
     * 获取访问令牌值处理器
     * @return 访问令牌值处理器
     */
    public AuthTokenValueHandler getAuthTokenValueHandler() {
        return authTokenValueHandler;
    }

    /**
     * 设置访问令牌值处理器
     * @param authTokenValueHandler 访问令牌值处理器
     */
    public void setAuthTokenValueHandler(AuthTokenValueHandler authTokenValueHandler) {
        this.authTokenValueHandler = authTokenValueHandler;
    }
}
