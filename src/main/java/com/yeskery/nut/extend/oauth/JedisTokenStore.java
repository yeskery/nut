package com.yeskery.nut.extend.oauth;

import com.yeskery.nut.extend.redis.JedisExecutor;

/**
 * 基于jedis的TokenStore实现
 * @author YESKERY
 * 2024/10/21
 */
public class JedisTokenStore implements TokenStore {

    /** Jedis执行器 */
    private final JedisExecutor jedisExecutor;

    /**
     * 构建基于jedis的TokenStore实现
     * @param jedisExecutor Jedis执行器
     */
    public JedisTokenStore(JedisExecutor jedisExecutor) {
        this.jedisExecutor = jedisExecutor;
    }

    @Override
    public boolean save(String accessToken, String value, long expires) {
        jedisExecutor.execute(jedis -> jedis.setex(accessToken, (int) expires, value));
        return true;
    }

    @Override
    public String get(String accessToken) {
        return jedisExecutor.execute(jedis -> jedis.get(accessToken));
    }

    @Override
    public boolean validate(String accessToken) {
        return jedisExecutor.execute(jedis -> jedis.exists(accessToken));
    }

    @Override
    public boolean delete(String accessToken) {
        jedisExecutor.execute(jedis -> jedis.del(accessToken));
        return true;
    }
}
