package com.yeskery.nut.extend.oauth;

/**
 * 基础请求参数
 * @author YESKERY
 * 2024/10/21
 */
public class BaseRequest {

    /** 响应类型 */
    private String clientId;

    /**
     * 获取客户端ID
     * @return 客户端ID
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * 设置客户端ID
     * @param clientId 客户端ID
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
