package com.yeskery.nut.extend.oauth;

import com.yeskery.nut.util.StringUtils;

/**
 * 刷新访问令牌生成器
 * @author YESKERY
 * 2024/10/22
 */
public class RefreshAccessTokenGenerator<T extends RefreshTokenRequest> implements AccessTokenGenerator<T> {

    /** 基础访问令牌生成器 */
    private final BaseAccessTokenGenerator<T> accessTokenGenerator;

    /**
     * 构造刷新访问令牌生成器
     * @param accessTokenGenerator 基础访问令牌生成器
     */
    public RefreshAccessTokenGenerator(BaseAccessTokenGenerator<T> accessTokenGenerator) {
        this.accessTokenGenerator = accessTokenGenerator;
    }

    @Override
    public <R> AccessTokenResponse<R> generateAccessToken(T request, AuthTokenValueHandler authTokenValueHandler) {
        String value = accessTokenGenerator.getAccessTokenValue(request.getRefreshToken());
        if (StringUtils.isEmpty(value)) {
            throw new AuthorizationException("Server_Refresh_Access_Token_Value_Invalid", "Invalid Refresh Access Token Invalid");
        }
        AccessTokenResponse<R> response = accessTokenGenerator.createNewAccessToken();
        accessTokenGenerator.getTokenStore().save(response.getAccessToken(), value, accessTokenGenerator.getExpires());
        accessTokenGenerator.getTokenStore().save(request.getRefreshToken(), value, accessTokenGenerator.getRefreshExpires());
        return response;
    }

    @Override
    public <R> AccessTokenResponse<R> generateAccessToken(AuthTokenValueHandler authTokenValueHandler) {
        return accessTokenGenerator.generateAccessToken();
    }

    @Override
    public String getAccessTokenValue(String accessToken) {
        return accessTokenGenerator.getAccessTokenValue(accessToken);
    }

    @Override
    public boolean validateAccessToken(String accessToken) {
        return accessTokenGenerator.validateAccessToken(accessToken);
    }
}
