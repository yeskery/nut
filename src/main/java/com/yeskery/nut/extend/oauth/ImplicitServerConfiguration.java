package com.yeskery.nut.extend.oauth;

import com.yeskery.nut.core.Method;

/**
 * 隐藏式令牌模式配置
 * @author YESKERY
 * 2024/10/23
 */
public class ImplicitServerConfiguration {

    /** 鉴权uri */
    private String authorizeUri = "/oauth/authorize";
    /** 鉴权请求方法 */
    private Method authorizeMethod = Method.POST;
    /** 鉴权转发uri */
    private String authorizeForwardUri;
    /** 鉴权转发请求方法 */
    private Method authorizeForwardMethod;
    /** 鉴权校验器 */
    private ClientAuthorizeVerify<ImplicitAuthorizeRequest> authorizeVerify;
    /** 访问令牌生成器 */
    private AccessTokenGenerator<?> accessTokenGenerator;

    /**
     * 获取鉴权uri
     * @return 鉴权uri
     */
    public String getAuthorizeUri() {
        return authorizeUri;
    }

    /**
     * 设置鉴权uri
     * @param authorizeUri 鉴权uri
     */
    public void setAuthorizeUri(String authorizeUri) {
        this.authorizeUri = authorizeUri;
    }

    /**
     * 获取鉴权请求方法
     * @return 鉴权请求方法
     */
    public Method getAuthorizeMethod() {
        return authorizeMethod;
    }

    /**
     * 设置鉴权请求方法
     * @param authorizeMethod 鉴权请求方法
     */
    public void setAuthorizeMethod(Method authorizeMethod) {
        this.authorizeMethod = authorizeMethod;
    }

    /**
     * 获取鉴权转发uri
     * @return 鉴权转发uri
     */
    public String getAuthorizeForwardUri() {
        return authorizeForwardUri;
    }

    /**
     * 设置鉴权转发uri
     * @param authorizeForwardUri 鉴权转发uri
     */
    public void setAuthorizeForwardUri(String authorizeForwardUri) {
        this.authorizeForwardUri = authorizeForwardUri;
    }

    /**
     * 获取鉴权转发请求方法
     * @return 鉴权转发请求方法
     */
    public Method getAuthorizeForwardMethod() {
        return authorizeForwardMethod;
    }

    /**
     * 设置鉴权转发请求方法
     * @param authorizeForwardMethod 鉴权转发请求方法
     */
    public void setAuthorizeForwardMethod(Method authorizeForwardMethod) {
        this.authorizeForwardMethod = authorizeForwardMethod;
    }

    /**
     * 获取鉴权校验器
     * @return 鉴权校验器
     */
    public ClientAuthorizeVerify<ImplicitAuthorizeRequest> getAuthorizeVerify() {
        return authorizeVerify;
    }

    /**
     * 设置鉴权校验器
     * @param authorizeVerify 鉴权校验器
     */
    public void setAuthorizeVerify(ClientAuthorizeVerify<ImplicitAuthorizeRequest> authorizeVerify) {
        this.authorizeVerify = authorizeVerify;
    }

    /**
     * 获取访问令牌生成器
     * @return 访问令牌生成器
     */
    public AccessTokenGenerator<?> getAccessTokenGenerator() {
        return accessTokenGenerator;
    }

    /**
     * 设置访问令牌生成器
     * @param accessTokenGenerator 访问令牌生成器
     */
    public void setAccessTokenGenerator(AccessTokenGenerator<?> accessTokenGenerator) {
        this.accessTokenGenerator = accessTokenGenerator;
    }
}
