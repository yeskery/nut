package com.yeskery.nut.extend.oauth;

import java.util.UUID;

/**
 * 基于UUID的授权码生成器
 * @author YESKERY
 * 2024/10/21
 */
public class SimpleAuthorizationCodeGenerator extends BaseAuthorizationCodeGenerator {

    /**
     * 构造基于session的授权码生成器
     * @param tokenStore token保存接口
     * @param expires 授权码过期时间
     */
    public SimpleAuthorizationCodeGenerator(TokenStore tokenStore, long expires) {
        super(tokenStore, expires);
    }

    @Override
    protected String createAuthorizationCode() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
