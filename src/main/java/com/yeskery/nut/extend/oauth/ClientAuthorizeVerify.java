package com.yeskery.nut.extend.oauth;

/**
 * 客户端授权校验
 * @author YESKERY
 * 2024/10/21
 */
public interface ClientAuthorizeVerify<T extends BaseRequest> {

    /**
     * 校验客户端是否有效
     * @param request 请求参数
     * @return 校验结果
     */
    boolean verify(T request);
}
