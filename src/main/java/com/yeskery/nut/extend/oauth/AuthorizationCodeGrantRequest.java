package com.yeskery.nut.extend.oauth;

/**
 * 授权码请求对象
 * @author YESKERY
 * 2024/10/21
 */
public class AuthorizationCodeGrantRequest extends ClientCredentialsRequest {

    /** 授权类型 */
    private String grantType;

    /** 重定向地址 */
    private String redirectUri;

    /** 授权码 */
    private String code;

    /**
     * 获取授权类型
     * @return 授权类型
     */
    public String getGrantType() {
        return grantType;
    }

    /**
     * 设置授权类型
     * @param grantType 授权类型
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    /**
     * 获取重定向地址
     * @return 重定向地址
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * 设置重定向地址
     * @param redirectUri 重定向地址
     */
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    /**
     * 获取授权码
     * @return 授权码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置授权码
     * @param code 授权码
     */
    public void setCode(String code) {
        this.code = code;
    }
}
