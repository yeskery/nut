package com.yeskery.nut.extend.oauth;

/**
 * 认证token处理器
 * @author YESKERY
 * 2024/10/30
 */
public interface AuthTokenValueHandler {

    /**
     * 获取认证token值
     * @return 认证token值
     */
    String authTokenValue();
}
