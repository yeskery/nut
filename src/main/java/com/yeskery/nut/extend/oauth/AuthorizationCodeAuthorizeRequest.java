package com.yeskery.nut.extend.oauth;

/**
 * 授权码请求对象
 * @author YESKERY
 * 2024/10/21
 */
public class AuthorizationCodeAuthorizeRequest extends BaseRequest {

    /** 响应类型 */
    private String responseType;

    /** 重定向地址 */
    private String redirectUri;

    /** 授权范围 */
    private String scope;

    /**
     * 获取响应类型
     * @return 响应类型
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * 设置响应类型
     * @param responseType 响应类型
     */
    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    /**
     * 获取重定向地址
     * @return 重定向地址
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * 设置重定向地址
     * @param redirectUri 重定向地址
     */
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    /**
     * 获取授权范围
     * @return 授权范围
     */
    public String getScope() {
        return scope;
    }

    /**
     * 设置授权范围
     * @param scope 授权范围
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
}
