package com.yeskery.nut.extend.oauth;

import com.yeskery.nut.core.NutException;

/**
 * 授权异常
 * @author sprout
 * 2022-06-02 18:42
 */
public class AuthorizationException extends NutException {

    /** 错误类型 */
    private final String type;

    /**
     * 构建一个 {@link AuthorizationException}
     * @param type 错误类型
     * @param message message
     */
    public AuthorizationException(String type, String message) {
        super(message);
        this.type = type;
    }

    /**
     * 构建一个 {@link AuthorizationException}
     * @param type 错误类型
     * @param message message
     * @param cause cause
     */
    public AuthorizationException(String type, String message, Throwable cause) {
        super(message, cause);
        this.type = type;
    }

    /**
     * 构建一个 {@link AuthorizationException}
     * @param type 错误类型
     * @param cause cause
     */
    public AuthorizationException(String type, Throwable cause) {
        super(cause);
        this.type = type;
    }

    /**
     * 构建一个 {@link AuthorizationException}
     * @param type 错误类型
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public AuthorizationException(String type, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.type = type;
    }

    /**
     * 获取错误类型
     * @return 错误类型
     */
    public String getType() {
        return type;
    }
}
