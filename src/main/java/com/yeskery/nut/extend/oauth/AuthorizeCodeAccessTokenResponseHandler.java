package com.yeskery.nut.extend.oauth;

/**
 * 授权码AccessTokenResponse处理器
 * @author YESKERY
 * 2024/10/22
 */
public interface AuthorizeCodeAccessTokenResponseHandler<T> extends AccessTokenResponseHandler<T> {

    /**
     * 生成AccessTokenResponse处理方法
     * @param accessTokenResponse 响应对象
     * @param authorizeCodeValue 授权码对应的值
     */
    void generateHandle(AccessTokenResponse<T> accessTokenResponse, String authorizeCodeValue);

    @Override
    default void generateHandle(AccessTokenResponse<T> accessTokenResponse) {
    }
}
