package com.yeskery.nut.extend.oauth;

/**
 * AccessTokenResponse处理器
 * @author YESKERY
 * 2024/10/22
 */
public interface AccessTokenResponseHandler<T> {

    /**
     * 生成AccessTokenResponse处理方法
     * @param accessTokenResponse 响应对象
     */
    void generateHandle(AccessTokenResponse<T> accessTokenResponse);

    /**
     * 刷新AccessTokenResponse处理方法
     * @param accessTokenResponse 响应对象
     * @param refreshTokenValue 刷新令牌对应的值
     */
    void refreshHandle(AccessTokenResponse<T> accessTokenResponse, String refreshTokenValue);
}
