package com.yeskery.nut.extend.oauth;

/**
 * 密码式令牌授权请求
 * @author YESKERY
 * 2024/10/23
 */
public class PasswordGrantRequest extends BaseRequest {

    /** 授权类型 */
    private String grantType;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /**
     * 获取授权类型
     * @return 授权类型
     */
    public String getGrantType() {
        return grantType;
    }

    /**
     * 设置授权类型
     * @param grantType 授权类型
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    /**
     * 获取用户名
     * @return 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取密码
     * @return 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
