package com.yeskery.nut.extend.oauth;

/**
 * 客户端凭证模式请求参数
 * @author YESKERY
 * 2024/10/21
 */
public class ClientCredentialsRequest extends BaseRequest {

    /** 客户端密钥 */
    private String clientSecret;

    /**
     * 获取客户端密钥
     * @return 客户端密钥
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * 设置客户端密钥
     * @param clientSecret 客户端密钥
     */
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}
