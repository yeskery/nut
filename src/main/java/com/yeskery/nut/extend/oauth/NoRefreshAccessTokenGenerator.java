package com.yeskery.nut.extend.oauth;

/**
 * 无刷新访问令牌生成器
 * @author YESKERY
 * 2024/10/25
 */
public abstract class NoRefreshAccessTokenGenerator<T extends BaseRequest> extends BaseAccessTokenGenerator<T> {
    /**
     * 构造无刷新访问令牌生成器
     * @param tokenStore token存储接口
     * @param expires 访问token过期时间
     */
    protected NoRefreshAccessTokenGenerator(TokenStore tokenStore, long expires) {
        super(tokenStore, expires, -1);
    }

    @Override
    public <R> AccessTokenResponse<R> generateAccessToken(AuthTokenValueHandler authTokenValueHandler) {
        AccessTokenResponse<R> response = createNewAccessToken();
        getTokenStore().save(response.getAccessToken(), getAccessTokenValue(response, authTokenValueHandler), getExpires());
        return response;
    }
}
