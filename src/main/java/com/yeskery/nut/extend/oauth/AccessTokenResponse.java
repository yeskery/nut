package com.yeskery.nut.extend.oauth;

/**
 * AccessToken响应对象
 * @author YESKERY
 * 2024/10/21
 */
public class AccessTokenResponse<T> {

    /** 访问令牌 */
    private String accessToken;

    /** 刷新令牌 */
    private String refreshToken;

    /** 令牌类型 */
    private String tokenType;

    /** 授权范围 */
    private String scope;

    /** 过期时间 */
    private Long expiresIn;

    /** 附加数据 */
    private T data;

    /**
     * 设置AccessToken
     * @param accessToken 访问令牌
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 获取AccessToken
     * @return accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置Token类型
     * @param tokenType 令牌类型
     */
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     * 获取Token类型
     * @return tokenType
     */
    public String getTokenType() {
        return tokenType;
    }

    /**
     * 获取过期时间
     * @return expiresIn
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * 设置过期时间
     * @param expiresIn 过期时间
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * 设置刷新令牌
     * @param refreshToken 刷新令牌
     */
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * 获取刷新令牌
     * @return refreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 设置授权范围
     * @param scope 授权范围
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * 获取授权范围
     * @return scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * 获取附加数据
     * @return data
     */
    public T getData() {
        return data;
    }

    /**
     * 设置附加数据
     * @param data 附加数据
     */
    public void setData(T data) {
        this.data = data;
    }
}
