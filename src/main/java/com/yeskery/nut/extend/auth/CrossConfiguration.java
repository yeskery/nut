package com.yeskery.nut.extend.auth;

/**
 * 跨域配置对象
 * @author YESKERY
 * 2024/6/19
 */
public class CrossConfiguration {

    /** 允许的请求头 */
    private String allowedHeaders;

    /** 允许的请求方法 */
    private String allowedMethods = "GET,POST,PUT,DELETE,OPTIONS";

    /** 允许的请求域 */
    private String allowedOrigins = "*";

    /** 是否允许携带凭证 */
    private Boolean allowedCredentials = true;

    /** 预检请求的有效期 */
    private Long maxAge = 3600L;

    /**
     * 获取允许的请求头
     * @return 允许的请求头
     */
    public String getAllowedHeaders() {
        return allowedHeaders;
    }

    /**
     * 设置允许的请求头
     * @param allowedHeaders 允许的请求头
     */
    public void setAllowedHeaders(String allowedHeaders) {
        this.allowedHeaders = allowedHeaders;
    }

    /**
     * 获取允许的请求方法
     * @return 允许的请求方法
     */
    public String getAllowedMethods() {
        return allowedMethods;
    }

    /**
     * 设置允许的请求方法
     * @param allowedMethods 允许的请求方法
     */
    public void setAllowedMethods(String allowedMethods) {
        this.allowedMethods = allowedMethods;
    }

    /**
     * 获取允许的请求域
     * @return 允许的请求域
     */
    public String getAllowedOrigins() {
        return allowedOrigins;
    }

    /**
     * 设置允许的请求域
     * @param allowedOrigins 允许的请求域
     */
    public void setAllowedOrigins(String allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    /**
     * 获取是否允许携带凭证
     * @return 是否允许携带凭证
     */
    public Boolean getAllowedCredentials() {
        return allowedCredentials;
    }

    /**
     * 设置是否允许携带凭证
     * @param allowedCredentials 是否允许携带凭证
     */
    public void setAllowedCredentials(Boolean allowedCredentials) {
        this.allowedCredentials = allowedCredentials;
    }

    /**
     * 获取预检请求的有效期
     * @return 预检请求的有效期
     */
    public Long getMaxAge() {
        return maxAge;
    }

    /**
     * 设置预检请求的有效期
     * @param maxAge 预检请求的有效期
     */
    public void setMaxAge(Long maxAge) {
        this.maxAge = maxAge;
    }
}
