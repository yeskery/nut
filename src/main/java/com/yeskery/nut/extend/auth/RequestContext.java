package com.yeskery.nut.extend.auth;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认请求上下文
 * @author sprout
 * @version 1.0
 * 2022-11-01 17:50
 */
public class RequestContext {

    /** 用于保存数据的ThreadLocal */
    private static final ThreadLocal<Map<String, Object>> LOCAL_DATA_MAP = new InheritableThreadLocal<>();

    /**
     * 获取key对应的值对象
     * @param key key
     * @return 对应的值对象
     */
    public static Object get(String key) {
        Map<String, Object> map = LOCAL_DATA_MAP.get();
        return map == null ? null : map.get(key);
    }

    /**
     * 获取key对应的值对象，并转换为指定类型
     * @param key key
     * @param clazz 转换类对象
     * @param <T> 转换类型
     * @return 对应的值对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T get(String key, Class<T> clazz) {
        Map<String, Object> map = LOCAL_DATA_MAP.get();
        return map == null ? null : (T) map.get(key);
    }

    /**
     * 设置key和对应的值对象
     * @param key key
     * @param value 对应的值对象
     * @return 是否设置成功
     */
    static boolean set(String key, Object value) {
        Map<String, Object> map = LOCAL_DATA_MAP.get();
        if (map == null) {
            return false;
        }
        map.put(key, value);
        return true;
    }

    /**
     * 初始化ThreadLocal
     */
    static void initialize() {
        Map<String, Object> map = LOCAL_DATA_MAP.get();
        if (map == null) {
            LOCAL_DATA_MAP.set(new HashMap<>(16));
        }
    }

    /**
     * 清理ThreadLocal
     */
    static void remove() {
        LOCAL_DATA_MAP.remove();
    }
}
