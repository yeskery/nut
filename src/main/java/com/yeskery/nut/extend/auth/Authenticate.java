package com.yeskery.nut.extend.auth;

import com.yeskery.nut.core.Controller;
import com.yeskery.nut.core.Request;

/**
 * 认证接口
 * @author sprout
 * 2022-06-02 10:35
 */
@FunctionalInterface
public interface Authenticate {

    /**
     * 认证是否通过
     * @param request 请求对象
     * @param controller 请求控制器
     * @return 认证是否通过
     */
    boolean approve(Request request, Controller controller);
}
