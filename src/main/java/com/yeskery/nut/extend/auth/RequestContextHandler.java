package com.yeskery.nut.extend.auth;

import com.yeskery.nut.core.KeyAndValue;
import com.yeskery.nut.core.Request;

import java.util.List;

/**
 * 请求上下文处理器
 * @author sprout
 * @version 1.0
 * 2022-11-01 18:12
 */
@FunctionalInterface
public interface RequestContextHandler {

    /**
     * 获取请求上下文资源对象数组
     * @param request 请求对象
     * @return 请求上下文资源对象集合
     */
    List<KeyAndValue<String, ?>> getRequestContextSources(Request request);
}
