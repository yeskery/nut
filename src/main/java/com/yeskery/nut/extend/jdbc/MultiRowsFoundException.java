package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.core.NutException;

/**
 * 多个数据行异常，在只返回一条数据的SQL中，返回了多条数据则会抛出该异常
 * @author sprout
 * 2022-06-02 18:42
 */
public class MultiRowsFoundException extends NutException {


    /**
     * 构建一个 {@link MultiRowsFoundException}
     */
    public MultiRowsFoundException() {
    }

    /**
     * 构建一个 {@link MultiRowsFoundException}
     * @param message message
     */
    public MultiRowsFoundException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link MultiRowsFoundException}
     * @param message message
     * @param cause cause
     */
    public MultiRowsFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link MultiRowsFoundException}
     * @param cause cause
     */
    public MultiRowsFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link MultiRowsFoundException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public MultiRowsFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
