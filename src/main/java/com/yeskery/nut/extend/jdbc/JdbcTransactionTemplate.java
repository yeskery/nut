package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.transaction.TransactionException;

/**
 * JDBC事务模板
 * @author sprout
 * @version 1.0
 * 2022-06-05 00:22
 */
public interface JdbcTransactionTemplate  extends JdbcSource {

    /**
     * 开启事务
     * @throws TransactionException 事务异常
     */
    void startTransaction() throws TransactionException;

    /**
     * 提交事务
     * @throws TransactionException 事务异常
     */
    void commitTransaction() throws TransactionException;

    /**
     * 回滚事务
     * @throws TransactionException 事务异常
     */
    void rollbackTransaction() throws TransactionException;

    /**
     * 当前事务是否已经激活
     * @return 当前事务是否已经激活
     */
    boolean isActive();
}
