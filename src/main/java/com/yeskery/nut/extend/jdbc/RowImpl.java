package com.yeskery.nut.extend.jdbc;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 行对象实现
 * @author YESKERY
 * 2024/8/30
 */
public class RowImpl extends LinkedHashMap<String, Object> implements Row {
    @Override
    public Object getObject(String columnName) {
        return super.get(columnName);
    }

    @Override
    public Object getObject(Integer index) {
        if (index == null || index < 1) {
            throw new DataAccessException("Index[" + index + "] Invalid.");
        }
        return entrySet().stream().skip(index).findFirst().map(Map.Entry::getValue).orElse(null);
    }
}
