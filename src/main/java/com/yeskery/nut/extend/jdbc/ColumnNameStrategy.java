package com.yeskery.nut.extend.jdbc;

/**
 * 列名字策略，指定从对象字段名称到数据库字段的转换策略
 * @author YESKERY
 * 2024/10/9
 */
@FunctionalInterface
public interface ColumnNameStrategy {

    /**
     * 获取数据库列名
     * @param fieldName 对象字段名
     * @return 数据库列名
     */
    String getColumnName(String fieldName);
}
