package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.util.JdbcUtils;
import com.yeskery.nut.util.StringUtils;

/**
 * 分页SQL行结果集分裂器
 * @author YESKERY
 * 2024/8/29
 */
public class PageSqlRowResultSetSpliterator extends PageSqlResultSetSpliterator<Row> {

    /**
     * 构建分页SQL结果集分裂器
     * @param jdbcTemplate JDBC操作模板
     * @param sql sql
     * @param size 每页的条数
     */
    public PageSqlRowResultSetSpliterator(JdbcTemplate jdbcTemplate, String sql, int size) {
        super(jdbcTemplate, sql, size, rs -> {
            RowImpl row = new RowImpl();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String name = rs.getMetaData().getColumnLabel(i);
                if (StringUtils.isEmpty(name)) {
                    name = rs.getMetaData().getColumnName(i);
                }
                row.putIfAbsent(name, JdbcUtils.getResultSetValue(rs, i));
            }
            return row;
        });
    }

    /**
     * 构建分页SQL结果集分裂器
     * @param jdbcTemplate JDBC操作模板
     * @param sql sql
     * @param params 参数
     * @param size 每页的条数
     */
    public PageSqlRowResultSetSpliterator(JdbcTemplate jdbcTemplate, String sql, Object[] params, int size) {
        super(jdbcTemplate, sql, params, size, rs -> {
            RowImpl row = new RowImpl();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String name = rs.getMetaData().getColumnLabel(i);
                if (StringUtils.isEmpty(name)) {
                    name = rs.getMetaData().getColumnName(i);
                }
                row.putIfAbsent(name, JdbcUtils.getResultSetValue(rs, i));
            }
            return row;
        });
    }
}
