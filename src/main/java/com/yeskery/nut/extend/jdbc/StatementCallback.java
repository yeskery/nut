package com.yeskery.nut.extend.jdbc;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Statement回调处理类
 * @author sprout
 * 2022-06-02 13:47
 */
public interface StatementCallback<T> {

    /**
     * Statement回调处理方法
     * @param stmt Statement
     * @return 处理结果
     * @throws SQLException SQLException
     */
    T callback(Statement stmt) throws SQLException;
}
