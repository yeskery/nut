package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.core.NutException;

/**
 * 数据不存在时，返回该异常
 * @author sprout
 * 2022-06-02 18:42
 */
public class DataNotFoundException extends NutException {


    /**
     * 构建一个 {@link DataNotFoundException}
     */
    public DataNotFoundException() {
    }

    /**
     * 构建一个 {@link DataNotFoundException}
     * @param message message
     */
    public DataNotFoundException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link DataNotFoundException}
     * @param message message
     * @param cause cause
     */
    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link DataNotFoundException}
     * @param cause cause
     */
    public DataNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link DataNotFoundException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public DataNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
