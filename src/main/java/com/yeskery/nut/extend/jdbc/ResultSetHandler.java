package com.yeskery.nut.extend.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 结果集处理类，该接口不会调用{@link ResultSet#next()}方法，需要手动处理数据
 * @author YESKERY
 * 2024/8/29
 */
public interface ResultSetHandler {

    /**
     * 处理结果集
     * @param rs 结果集
     * @throws SQLException SQLException
     */
    void handle(ResultSet rs) throws SQLException;
}
