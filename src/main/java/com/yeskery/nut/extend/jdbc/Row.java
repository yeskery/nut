package com.yeskery.nut.extend.jdbc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZoneId;
import java.util.Date;

/**
 * 数据行对象
 * @author YESKERY
 * 2024/8/30
 */
public interface Row {

    /**
     * 获取列对象
     * @param columnName 列名
     * @return 列对象
     */
    Object getObject(String columnName);

    /**
     * 获取列对象
     * @param index 索引
     * @return 列对象
     */
    Object getObject(Integer index);

    /**
     * 获取值
     * @param columnName 列名
     * @param clazz 类型
     * @return 值
     * @param <T> 类型
     */
    @SuppressWarnings("unchecked")
    default <T> T getValue(String columnName, Class<T> clazz) {
        return (T) getObject(columnName);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default String getString(String columnName) {
        Object object = getObject(columnName);
        return object == null ? null : object.toString();
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Boolean getBoolean(String columnName) {
        return getValue(columnName, Boolean.class);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Byte getByte(String columnName) {
        try {
            return getValue(columnName, Byte.class);
        } catch (ClassCastException e) {
            return Byte.valueOf(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Short getShort(String columnName) {
        try {
            return getValue(columnName, Short.class);
        } catch (ClassCastException e) {
            return Short.valueOf(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Integer getInteger(String columnName) {
        try {
            return getValue(columnName, Integer.class);
        } catch (ClassCastException e) {
            return Integer.getInteger(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Long getLong(String columnName) {
        try {
            return getValue(columnName, Long.class);
        } catch (ClassCastException e) {
            return Long.getLong(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Float getFloat(String columnName) {
        try {
            return getValue(columnName, Float.class);
        } catch (ClassCastException e) {
            return Float.valueOf(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default Double getDouble(String columnName) {
        try {
            return getValue(columnName, Double.class);
        } catch (ClassCastException e) {
            return Double.valueOf(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default BigInteger getBigInteger(String columnName) {
        try {
            return getValue(columnName, BigInteger.class);
        } catch (ClassCastException e) {
            return new BigInteger(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default BigDecimal getBigDecimal(String columnName) {
        try {
            return getValue(columnName, BigDecimal.class);
        } catch (ClassCastException e) {
            return new BigDecimal(getString(columnName));
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default byte[] getBytes(String columnName) {
        return getValue(columnName, byte[].class);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default java.sql.Date getSqlDate(String columnName) {
        return getValue(columnName, java.sql.Date.class);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default java.sql.Timestamp getSqlTimestamp(String columnName) {
        return getValue(columnName, java.sql.Timestamp.class);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default java.sql.Time getSqlTime(String columnName) {
        return getValue(columnName, java.sql.Time.class);
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default java.util.Date getDate(String columnName) {
        try {
            return getValue(columnName, java.util.Date.class);
        } catch (ClassCastException e) {
            Object object = getObject(columnName);
            if (object instanceof java.time.LocalDateTime) {
                return Date.from(((java.time.LocalDateTime) object).atZone(ZoneId.systemDefault()).toInstant());
            }
            throw e;
        }
    }

    /**
     * 获取值
     * @param columnName 列名
     * @return 值
     */
    default java.time.LocalDateTime getLocalDateTime(String columnName) {
        return getValue(columnName, java.time.LocalDateTime.class);
    }

    /**
     * 获取值
     * @param index 索引
     * @param clazz 类型
     * @return 值
     * @param <T> 类型
     */
    @SuppressWarnings("unchecked")
    default <T> T getValue(Integer index, Class<T> clazz) {
        return (T) getObject(index);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default String getString(Integer index) {
        Object object = getObject(index);
        return object == null ? null : object.toString();
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Boolean getBoolean(Integer index) {
        return getValue(index, Boolean.class);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Byte getByte(Integer index) {
        try {
            return getValue(index, Byte.class);
        } catch (ClassCastException e) {
            return Byte.valueOf(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Short getShort(Integer index) {
        try {
            return getValue(index, Short.class);
        } catch (ClassCastException e) {
            return Short.valueOf(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Integer getInteger(Integer index) {
        try {
            return getValue(index, Integer.class);
        } catch (ClassCastException e) {
            return Integer.getInteger(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Long getLong(Integer index) {
        try {
            return getValue(index, Long.class);
        } catch (ClassCastException e) {
            return Long.getLong(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Float getFloat(Integer index) {
        try {
            return getValue(index, Float.class);
        } catch (ClassCastException e) {
            return Float.valueOf(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default Double getDouble(Integer index) {
        try {
            return getValue(index, Double.class);
        } catch (ClassCastException e) {
            return Double.valueOf(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default BigInteger getBigInteger(Integer index) {
        try {
            return getValue(index, BigInteger.class);
        } catch (ClassCastException e) {
            return new BigInteger(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default BigDecimal getBigDecimal(Integer index) {
        try {
            return getValue(index, BigDecimal.class);
        } catch (ClassCastException e) {
            return new BigDecimal(getString(index));
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default byte[] getBytes(Integer index) {
        return getValue(index, byte[].class);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default java.sql.Date getSqlDate(Integer index) {
        return getValue(index, java.sql.Date.class);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default java.sql.Timestamp getSqlTimestamp(Integer index) {
        return getValue(index, java.sql.Timestamp.class);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default java.sql.Time getSqlTime(Integer index) {
        return getValue(index, java.sql.Time.class);
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default java.util.Date getDate(Integer index) {
        try {
            return getValue(index, java.util.Date.class);
        } catch (ClassCastException e) {
            Object object = getObject(index);
            if (object instanceof java.time.LocalDateTime) {
                return Date.from(((java.time.LocalDateTime) object).atZone(ZoneId.systemDefault()).toInstant());
            }
            throw e;
        }
    }

    /**
     * 获取值
     * @param index 索引
     * @return 值
     */
    default java.time.LocalDateTime getLocalDateTime(Integer index) {
        return getValue(index, java.time.LocalDateTime.class);
    }
}
