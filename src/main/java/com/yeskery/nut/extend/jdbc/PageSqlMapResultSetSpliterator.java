package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.util.JdbcUtils;
import com.yeskery.nut.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 分页SQL Map 结果集分裂器
 * @author YESKERY
 * 2024/8/29
 */
public class PageSqlMapResultSetSpliterator extends PageSqlResultSetSpliterator<Map<String, Object>> {

    /**
     * 构建分页SQL结果集分裂器
     * @param jdbcTemplate JDBC操作模板
     * @param sql sql
     * @param size 每页的条数
     */
    public PageSqlMapResultSetSpliterator(JdbcTemplate jdbcTemplate, String sql, int size) {
        super(jdbcTemplate, sql, size, rs -> {
            Map<String, Object> map = new HashMap<>();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String name = rs.getMetaData().getColumnLabel(i);
                if (StringUtils.isEmpty(name)) {
                    name = rs.getMetaData().getColumnName(i);
                }
                map.putIfAbsent(name, JdbcUtils.getResultSetValue(rs, i));
            }
            return map;
        });
    }

    /**
     * 构建分页SQL结果集分裂器
     * @param jdbcTemplate JDBC操作模板
     * @param sql sql
     * @param params 参数
     * @param size 每页的条数
     */
    public PageSqlMapResultSetSpliterator(JdbcTemplate jdbcTemplate, String sql, Object[] params, int size) {
        super(jdbcTemplate, sql, params, size, rs -> {
            Map<String, Object> map = new HashMap<>();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String name = rs.getMetaData().getColumnLabel(i);
                if (StringUtils.isEmpty(name)) {
                    name = rs.getMetaData().getColumnName(i);
                }
                map.putIfAbsent(name, JdbcUtils.getResultSetValue(rs, i));
            }
            return map;
        });
    }
}
