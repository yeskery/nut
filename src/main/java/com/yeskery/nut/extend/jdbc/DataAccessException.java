package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.core.NutException;

/**
 * 数据访问异常
 * @author sprout
 * 2022-06-02 18:42
 */
public class DataAccessException extends NutException {


    /**
     * 构建一个 {@link DataAccessException}
     */
    public DataAccessException() {
    }

    /**
     * 构建一个 {@link DataAccessException}
     * @param message message
     */
    public DataAccessException(String message) {
        super(message);
    }

    /**
     * 构建一个 {@link DataAccessException}
     * @param message message
     * @param cause cause
     */
    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构建一个 {@link DataAccessException}
     * @param cause cause
     */
    public DataAccessException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个 {@link DataAccessException}
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public DataAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
