package com.yeskery.nut.extend.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * PreparedStatement回调处理类
 * @author sprout
 * 2022-06-02 13:47
 */
public interface PreparedStatementCallback<T> {

    /**
     * PreparedStatement回调处理方法
     * @param pstmt PreparedStatement
     * @return 处理结果
     * @throws SQLException SQLException
     */
    T callback(PreparedStatement pstmt) throws SQLException;
}
