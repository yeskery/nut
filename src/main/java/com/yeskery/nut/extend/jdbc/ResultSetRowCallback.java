package com.yeskery.nut.extend.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 结果集回调类，该接口会调用{@link ResultSet#next()}方法，只需要返回每行需要的对象
 * @author sprout
 * 2022-06-02 13:56
 */
public interface ResultSetRowCallback<T> {

    /**
     * 结果集回调方法
     * @param rs 结果集
     * @return 结果对象
     * @throws SQLException SQLException
     */
    T callback(ResultSet rs) throws SQLException;
}
