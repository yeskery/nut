package com.yeskery.nut.extend.jdbc;

import javax.sql.DataSource;

/**
 * jdbc数据源接口
 * @author sprout
 * 2022-06-02 11:41
 */
public interface JdbcSource {

    /**
     * 获取MySQL数据源
     * @return MySQL数据源
     */
    DataSource getDataSource();
}
