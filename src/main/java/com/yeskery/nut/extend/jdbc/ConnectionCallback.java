package com.yeskery.nut.extend.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 连接回调处理方法
 * @author YESKERY
 * 2024/8/29
 */
public interface ConnectionCallback<T> {

    /**
     * 连接回调处理方法
     * @param connection 连接对象
     * @return 处理结果
     * @throws SQLException SQLException
     */
    T callback(Connection connection) throws SQLException;
}
