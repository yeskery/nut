package com.yeskery.nut.extend.jdbc;

import com.yeskery.nut.util.cache.ConnectionCreator;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * 基础的不支持连接池数据源
 * @author sprout
 * 2022-06-02 17:29
 */
public class WrapBasicDataSource implements DataSource {

    /** 连接创建器 */
    private final ConnectionCreator<Connection> connectionCreator;

    /**
     * 构建基础的不支持连接池数据源
     * @param connectionCreator 连接创建器
     */
    public WrapBasicDataSource(ConnectionCreator<Connection> connectionCreator) {
        if (connectionCreator == null) {
            throw new DataAccessException("ConnectionCreator Must Not Be Null.");
        }
        this.connectionCreator = connectionCreator;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return connectionCreator.create();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new SQLException("Unsupported Operation.");
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("Unsupported Operation.");
    }
}
