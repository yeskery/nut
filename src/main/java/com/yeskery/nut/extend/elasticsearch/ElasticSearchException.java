package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.core.NutException;

/**
 * ElasticSearch执行异常结果
 * @author YESKERY
 * 2024/9/3
 */
public class ElasticSearchException extends NutException {

    /** 状态码 */
    private final int status;

    /**
     * 构建一个 {@link ElasticSearchException}
     * @param status 状态码
     * @param message message
     */
    public ElasticSearchException(int status, String message) {
        super(message.replace("\r", " ").replace("\n", " "));
        this.status = status;
    }

    /**
     * 构建一个 {@link ElasticSearchException}
     * @param status 状态码
     * @param message message
     * @param cause cause
     */
    public ElasticSearchException(int status, String message, Throwable cause) {
        super(message.replace("\r", " ").replace("\n", " "), cause);
        this.status = status;
    }

    /**
     * 构建一个 {@link ElasticSearchException}
     * @param status 状态码
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    public ElasticSearchException(int status, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message.replace("\r", " ").replace("\n", " "), cause, enableSuppression, writableStackTrace);
        this.status = status;
    }
}
