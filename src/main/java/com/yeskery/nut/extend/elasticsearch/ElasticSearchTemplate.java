package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.core.NameAndValue;
import com.yeskery.nut.util.http.ResponseEntity;

import java.util.Collection;
import java.util.List;

/**
 * ElasticSearch模板接口，提供轻量级基础方法，不依赖ES依赖
 * @author YESKERY
 * 2024/9/3
 */
public interface ElasticSearchTemplate extends ElasticSearchIndexOperations {

    /**
     * 创建文档
     * @param indexName 索引名称
     * @param id 索引id
     * @param document 索引文档，如果类型为String，则直接使用该字符串，否则会使用将对象序列化为json字符串
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> createDocument(String indexName, String id, Object document) throws ElasticSearchException;

    /**
     * 更新文档
     * @param indexName 索引名称
     * @param id 索引id
     * @param document 索引文档，如果类型为String，则直接使用该字符串，否则会使用将对象序列化为json字符串
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateDocument(String indexName, String id, Object document) throws ElasticSearchException;

    /**
     * 更新文档
     * @param indexName 索引名称
     * @param id 索引id
     * @param nameAndValues 更新的字段和值数组
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateDocument(String indexName, String id, Collection<NameAndValue> nameAndValues) throws ElasticSearchException;

    /**
     * 更新文档，通过查询DSL更新
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @param nameAndValues 更新的字段和值数组
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateDocumentByQuery(String indexName, String query, NameAndValue... nameAndValues) throws ElasticSearchException;

    /**
     * 更新文档，通过查询DSL更新
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @param nameAndValues 更新的字段和值集合
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateDocumentByQuery(String indexName, String query, Collection<NameAndValue> nameAndValues) throws ElasticSearchException;

    /**
     * 删除文档
     * @param indexName 索引名称
     * @param id 索引id
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteDocument(String indexName, String id) throws ElasticSearchException;

    /**
     * 删除文档
     * @param indexName 索引名称
     * @param ids 索引id数组
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteDocument(String indexName, String... ids) throws ElasticSearchException;

    /**
     * 删除文档
     * @param indexName 索引名称
     * @param ids 索引id集合
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteDocument(String indexName, Collection<String> ids) throws ElasticSearchException;

    /**
     * 通过查询DSL删除文档
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteDocumentByQuery(String indexName, String query) throws ElasticSearchException;

    /**
     * 删除所有文档
     * @param indexName 索引名称
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteAllDocument(String indexName) throws ElasticSearchException;

    /**
     * 统计条数查询
     * @param indexName 索引名称
     * @param requestBody 请求体，{"query": {"match_all": {}}}
     * @return 查询查询条数
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    Long count(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 统计条数查询
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @return 查询查询条数
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    Long countByQuery(String indexName, String query) throws ElasticSearchException;

    /**
     * 统计条数查询
     * @param indexName 索引名称
     * @return 查询查询条数
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    Long count(String indexName) throws ElasticSearchException;

    /**
     * 查询所有文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @return ES返回的结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> searchAll(String indexName) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param requestBody 查询体DSL，{"query": {"match_all": {}}, "sort": []...}
     * @return ES返回的结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> search(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @param from 开始位置，0
     * @param size 查询条数，10
     * @param sort 排序字段，[{"fieldName": {"order": "desc"}}...]
     * @return ES返回的结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> search(String indexName, String query, long from, long size, String sort) throws ElasticSearchException;

    /**
     * 通过id搜索文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param id 文档id
     * @return ES返回的结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> searchById(String indexName, String id) throws ElasticSearchException;

    /**
     * 通过id搜索文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param id 文档id
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> T searchById(String indexName, String id, Class<T> clazz) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param requestBody 查询体DSL，{"query": {"match_all": {}}, "sort": []...}
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> T searchObject(String indexName, String requestBody, Class<T> clazz) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param requestBody 查询体DSL，{"query": {"match_all": {}}, "sort": []...}
     * @return 搜索的文档id
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    String searchId(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 通过id搜索文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param id 文档id
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> T searchObjectById(String indexName, String id, Class<T> clazz) throws ElasticSearchException;

    /**
     * 查询所有文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> List<T> searchAll(String indexName, Class<T> clazz) throws ElasticSearchException;

    /**
     * 查询所有文档，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @return 搜索的文档id集合
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    List<String> searchAllIds(String indexName) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param requestBody 查询体DSL，{"query": {"match_all": {}}, "sort": []...}
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> List<T> searchList(String indexName, String requestBody, Class<T> clazz) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param requestBody 查询体DSL，{"query": {"match_all": {}}, "sort": []...}
     * @return 搜索的文档id集合
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    List<String> searchIdList(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @param from 开始位置
     * @param size 查询条数
     * @param sort 排序字段，[{"fieldName": {"order": "desc"}}...]
     * @param clazz 要查询的类型
     * @return ES返回的结果
     * @param <T> 要查询的类型
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    <T> List<T> searchList(String indexName, String query, long from, long size, String sort, Class<T> clazz) throws ElasticSearchException;

    /**
     * 文档搜索，该方法会直接返回ES返回的结果
     * @param indexName 索引名称
     * @param query 查询DSL，{"match_all": {}}
     * @param from 开始位置
     * @param size 查询条数
     * @param sort 排序字段，[{"fieldName": {"order": "desc"}}...]
     * @return 搜索的文档id集合
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    List<String> searchIdList(String indexName, String query, long from, long size, String sort) throws ElasticSearchException;

    /**
     * 批量操作
     * @param indexName 索引名称
     * @param bulkRequests bulk请求集合
     * @return 批量操作结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    BulkResponse bulk(String indexName, Collection<BulkRequest> bulkRequests) throws ElasticSearchException;
}
