package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.core.Method;
import com.yeskery.nut.util.http.ResponseEntity;

/**
 * ElasticSearch操作接口
 * @author YESKERY
 * 2024/9/3
 */
public interface ElasticSearchOperations {

    /**
     * 执行ElasticSearch操作请求
     * @param uri uri
     * @param method 请求方法
     * @param body 请求体
     * @return ElasticSearch响应
     */
    ResponseEntity<byte[]> execute(String uri, Method method, String body);

    /**
     * 执行ElasticSearch操作请求
     * @param uri uri
     * @param method 请求方法
     * @param body 请求体
     * @return ElasticSearch响应字符串
     */
    default ResponseEntity<String> executeString(String uri, Method method, String body) {
        ResponseEntity<byte[]> responseEntity = execute(uri, method, body);
        ResponseEntity<String> stringResponseEntity = new ResponseEntity<>();
        stringResponseEntity.setStatus(responseEntity.getStatus());
        stringResponseEntity.setHeaders(responseEntity.getHeaders());
        if (responseEntity.getBody() != null && responseEntity.getBody().length > 0) {
            stringResponseEntity.setBody(new String(responseEntity.getBody()));
        } else {
            stringResponseEntity.setBody("");
        }
        return stringResponseEntity;
    }
}
