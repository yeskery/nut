package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.util.StringUtils;

/**
 * ElasticSearch配置对象
 * @author YESKERY
 * 2024/9/3
 */
public class ElasticSearchConfiguration {

    /** uri */
    private final String uri;

    /** 认证方式 */
    private final String authorizationType;

    /** 用户名 */
    private final String username;

    /** 密码 */
    private final String password;

    /**
     * 构建ElasticSearch配置对象
     * @param uri uri
     * @param authorizationType 认证方式
     * @param username 用户名
     * @param password 密码
     */
    public ElasticSearchConfiguration(String uri, String authorizationType, String username, String password) {
        if (StringUtils.isEmpty(uri)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "ElasticSearch uri is empty");
        }
        this.uri = uri.endsWith("/") ? uri.substring(0, uri.length() - 1) :uri;
        if (StringUtils.isEmpty(authorizationType)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "ElasticSearch authenticationType is empty");
        }
        this.authorizationType = authorizationType;
        if (StringUtils.isEmpty(username)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "ElasticSearch username is empty");
        }
        this.username = username;
        if (StringUtils.isEmpty(password)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "ElasticSearch password is empty");
        }
        this.password = password;
    }

    /**
     * 构建ElasticSearch配置对象
     * @param uri uri
     * @param username 用户名
     * @param password 密码
     */
    public ElasticSearchConfiguration(String uri, String username, String password) {
        this(uri, "Basic", username, password);
    }

    /**
     * 获取uri
     * @return uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * 获取认证方式
     * @return 认证方式
     */
    public String getAuthorizationType() {
        return authorizationType;
    }

    /**
     * 获取用户名
     * @return 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 获取密码
     * @return 密码
     */
    public String getPassword() {
        return password;
    }
}
