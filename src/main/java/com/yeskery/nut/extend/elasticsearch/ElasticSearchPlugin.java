package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.plugin.ApplicationContextSupportBasePlugin;
import com.yeskery.nut.plugin.PluginBeanMetadata;

import java.util.Collection;
import java.util.Collections;

/**
 * ElasticSearch插件
 * @author YESKERY
 * 2024/9/4
 */
public class ElasticSearchPlugin extends ApplicationContextSupportBasePlugin {

    /** ElasticSearch配置对象 */
    private final ElasticSearchConfiguration elasticSearchConfiguration;

    /**
     * 构建ElasticSearch插件
     * @param elasticSearchConfiguration ElasticSearch配置对象
     */
    public ElasticSearchPlugin(ElasticSearchConfiguration elasticSearchConfiguration) {
        this.elasticSearchConfiguration = elasticSearchConfiguration;
    }

    @Override
    protected Collection<PluginBeanMetadata> getRegisterPluginBeanMetadata() {
        return Collections.singleton(new PluginBeanMetadata("elasticSearchTemplate",
                new ElasticSearchTemplateImpl(elasticSearchConfiguration, getApplicationContext()),
                ElasticSearchIndexOperations.class, ElasticSearchTemplate.class, ElasticSearchOperations.class,
                ElasticSearchTemplateImpl.class));
    }
}
