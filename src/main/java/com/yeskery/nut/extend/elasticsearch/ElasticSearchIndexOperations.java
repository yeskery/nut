package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.util.http.ResponseEntity;

/**
 * ElasticSearch索引操作接口
 * @author YESKERY
 * 2024/9/3
 */
public interface ElasticSearchIndexOperations extends ElasticSearchOperations {

    /**
     * 创建索引
     * @param indexName 索引名称
     * @param mappings 索引映射, <code>{"properties": {...}}</code>
     * @param settings 索引设置, <code>{"index": {...}...}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> createIndex(String indexName, String mappings, String settings) throws ElasticSearchException;

    /**
     * 创建索引
     * @param indexName 索引名称
     * @param requestBody 请求体，<code>{"mapping": {"properties": {...}}, "setting": {"index": {...}...}}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> createIndex(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 删除索引
     * @param indexName 索引名称
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> deleteIndex(String indexName) throws ElasticSearchException;

    /**
     * 更新索引
     * @param indexName 索引名称
     * @param requestBody 请求体，<code>{"mapping": {"properties": {...}}, "setting": {"index": {...}...}}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateIndex(String indexName, String requestBody) throws ElasticSearchException;

    /**
     * 更新索引
     * @param indexName 索引名称
     * @param mappings 索引映射, <code>{"properties": {...}}</code>
     * @param settings 索引设置, <code>{"index": {...}...}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateIndex(String indexName, String mappings, String settings) throws ElasticSearchException;

    /**
     * 更新索引映射
     * @param indexName 索引名称
     * @param mappings 索引映射, <code>{"properties": {...}}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateIndexMappings(String indexName, String mappings) throws ElasticSearchException;

    /**
     * 更新索引设置
     * @param indexName 索引名称
     * @param settings 索引设置, <code>{"index": {...}...}</code>
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> updateIndexSettings(String indexName, String settings) throws ElasticSearchException;

    /**
     * 设置索引别名
     * @param indexName 索引名称
     * @param alias 别名
     * @return 执行结果
     * @throws ElasticSearchException 如果执行失败则抛出该异常
     */
    ResponseEntity<String> setIndexAlias(String indexName, String alias) throws ElasticSearchException;
}
