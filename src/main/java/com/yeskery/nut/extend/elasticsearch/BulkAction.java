package com.yeskery.nut.extend.elasticsearch;

/**
 * 批量操作类型枚举
 * @author YESKERY
 * 2024/9/12
 */
public enum BulkAction {
    /** 当文档不存在时进行创建 */
    CREATE("create"),
    /** 创建新文档或更新已有文档 */
    INDEX("index"),
    /** 局部更新文档 */
    UPDATE("update"),
    /** 删除文档 */
    DELETE("delete");
    /** 动作 */
    private final String action;

    /**
     * 构建批量操作类型枚举
     * @param action 动作
     */
    BulkAction(String action) {
        this.action = action;
    }

    /**
     * 获取动作
     * @return 动作
     */
    public String getAction() {
        return action;
    }
}
