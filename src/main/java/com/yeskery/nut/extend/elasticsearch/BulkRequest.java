package com.yeskery.nut.extend.elasticsearch;

import com.yeskery.nut.core.ResponseCode;
import com.yeskery.nut.util.StringUtils;

/**
 * 批量请求对象
 * @author YESKERY
 * 2024/9/12
 */
public class BulkRequest {

    /** 批量操作动作 */
    private final BulkAction action;

    /** 文档id */
    private final String id;

    /** 文档内容 */
    private final String document;

    /** 索引名称 */
    private String indexName;

    /**
     * 构造批量请求对象
     * @param action 批量操作动作
     * @param id 文档id
     * @param document 文档内容
     */
    private BulkRequest(BulkAction action, String id, String document) {
        if (action == null) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "BulkAction Must Not Be Null.");
        }
        this.action = action;
        if (StringUtils.isEmpty(id)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "Id Must Not Be Empty.");
        }
        this.id = id;
        this.document = document;
    }

    /**
     * 创建批量请求create对象
     * @param id 文档id
     * @param document 文档内容,json格式
     * @return BulkRequest对象
     */
    public static BulkRequest create(String id, String document) {
        if (StringUtils.isEmpty(document)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "Document Must Not Be Empty.");
        }
        return new BulkRequest(BulkAction.CREATE, id, document);
    }

    /**
     * 创建批量请求index对象
     * @param id 文档id
     * @param document 文档内容,json格式
     * @return BulkRequest对象
     */
    public static BulkRequest index(String id, String document) {
        if (StringUtils.isEmpty(document)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "Document Must Not Be Empty.");
        }
        return new BulkRequest(BulkAction.INDEX, id, document);
    }

    /**
     * 创建批量请求update对象
     * @param id 文档id
     * @param document 文档内容,json格式
     * @return BulkRequest对象
     */
    public static BulkRequest update(String id, String document) {
        if (StringUtils.isEmpty(document)) {
            throw new ElasticSearchException(ResponseCode.BAD_REQUEST.getCode(), "Document Must Not Be Empty.");
        }
        return new BulkRequest(BulkAction.UPDATE, id, document);
    }

    /**
     * 创建批量请求delete对象
     * @param id 文档id
     * @return BulkRequest对象
     */
    public static BulkRequest delete(String id) {
        return new BulkRequest(BulkAction.DELETE, id, null);
    }

    /**
     * 设置索引名称
     * @param indexName 索引名称
     */
    void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    @Override
    public String toString() {
        String result = "{\"" + action.getAction() + "\": {\"_index\": \"" + indexName + "\", \"_id\": \"" + id + "\"}}\r\n";
        if (!StringUtils.isEmpty(document)) {
            if (action == BulkAction.UPDATE) {
                result += "{\"doc\":" + document + "}\r\n";
            } else {
                result += document + "\r\n";
            }
        }
        return result;
    }
}
