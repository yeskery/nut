package com.yeskery.nut.extend.elasticsearch;

import java.util.List;

/**
 * 批量响应对象
 * @author YESKERY
 * 2024/9/12
 */
public class BulkResponse {

    /** 是否处理成功 */
    private boolean success;

    /** 响应项 */
    private List<Item> items;

    /**
     * 是否处理成功
     * @return 是否处理成功
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * 设置是否处理成功
     * @param success 是否处理成功
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * 获取响应项
     * @return 响应项
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * 设置响应项
     * @param items 响应项
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * 批量响应项对象
     */
    public static class Item {
        /** 批量操作类型 */
        private BulkAction action;

        /** id */
        private String id;

        /** 索引 */
        private String index;

        /** 状态 */
        private int status;

        /** 错误内容  */
        private String error;

        /**
         * 获取批量操作类型
         * @return 批量操作类型
         */
        public BulkAction getAction() {
            return action;
        }

        /**
         * 设置批量操作类型
         * @param action 批量操作类型
         */
        public void setAction(BulkAction action) {
            this.action = action;
        }

        /**
         * 获取id
         * @return id
         */
        public String getId() {
            return id;
        }

        /**
         * 设置id
         * @param id id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * 获取索引
         * @return 索引
         */
        public String getIndex() {
            return index;
        }

        /**
         * 设置索引
         * @param index 索引
         */
        public void setIndex(String index) {
            this.index = index;
        }

        /**
         * 获取状态
         * @return 状态
         */
        public int getStatus() {
            return status;
        }

        /**
         * 设置状态
         * @param status 状态
         */
        public void setStatus(int status) {
            this.status = status;
        }

        /**
         * 获取错误内容
         * @return 错误内容
         */
        public String getError() {
            return error;
        }

        /**
         * 设置错误内容
         * @param error 错误内容
         */
        public void setError(String error) {
            this.error = error;
        }
    }
}
