package com.yeskery.nut.extend.mybatisplus;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisSqlSessionFactoryBuilder;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.extend.mybatis.BaseMyBatisPlugin;
import org.apache.ibatis.session.Configuration;

import javax.sql.DataSource;

/**
 * 基于注解的MyBatis-Plus插件
 * @author sprout
 * @version 1.0
 * 2022-08-25 21:37
 */
public class MyBatisPlusPlugin extends BaseMyBatisPlugin {

    /**
     * 构建基于注解的MyBatis-Plus插件
     * @param basePackages 要扫描的基础包路径数组
     */
    public MyBatisPlusPlugin(String... basePackages) {
        super(new MybatisSqlSessionFactoryBuilder().build(new MybatisConfiguration()));
        setMyBatisScanBasePackages(basePackages);
    }

    /**
     * 构建基于注解的MyBatis-Plus插件
     * @param basePackages 要扫描的基础包数组
     * @param dataSource 数据源
     */
    public MyBatisPlusPlugin(String[] basePackages, DataSource dataSource) {
        super(new MybatisSqlSessionFactoryBuilder().build(new MybatisConfiguration()), dataSource);
        setMyBatisScanBasePackages(basePackages);
    }

    /**
     * 设置MyBatis扫描的基础路径
     * @param basePackages MyBatis扫描的基础路径
     */
    private void setMyBatisScanBasePackages(String[] basePackages) {
        if (basePackages == null || basePackages.length == 0) {
            throw new NutException("MyBatis Mapper Base Package Must Not Be Empty.");
        }
        Configuration configuration = getConfiguration();
        for (String basePackage : basePackages) {
            configuration.addMappers(basePackage);
        }
    }
}
