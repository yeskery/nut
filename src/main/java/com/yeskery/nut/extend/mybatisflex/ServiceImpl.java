package com.yeskery.nut.extend.mybatisflex;

import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.service.IService;
import com.yeskery.nut.annotation.bean.Autowired;
/**
 * Service默认实现类
 * @author Yeskery
 * 2023/7/24
 */
public class ServiceImpl<M extends BaseMapper<T>, T> implements IService<T> {

    /** Mapper对象 */
    @Autowired
    private M mapper;

    @Override
    public BaseMapper<T> getMapper() {
        return mapper;
    }
}
