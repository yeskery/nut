package com.yeskery.nut.extend.mybatisflex;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * 空数据源
 * @author Yeskery
 * 2023/7/26
 */
public class EmptyDataSource implements DataSource {
    @Override
    public Connection getConnection() throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new UnsupportedOperationException("Un Support Operation From Empty DataSource.");
    }
}
