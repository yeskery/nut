package com.yeskery.nut.extend.mybatis;

import com.yeskery.nut.core.ClassPathEnvironmentResource;
import com.yeskery.nut.core.DefaultEnvironment;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.sql.DataSource;

/**
 * 基于XML配置的MyBatis插件
 * @author sprout
 * @version 1.0
 * 2022-07-07 21:38
 */
public class XmlMyBatisPlugin extends BaseMyBatisPlugin {

    /** 默认配置文件路径 */
    private static final String DEFAULT_MYBATIS_CONFIG_PATH = "mybatis-config.xml";

    /**
     * 构建MyBatis插件
     * @param mybatisConfigPath 默认配置文件路径
     */
    public XmlMyBatisPlugin(String mybatisConfigPath) {
        super(new SqlSessionFactoryBuilder().build(new ClassPathEnvironmentResource(new DefaultEnvironment(), mybatisConfigPath).getInputStream()));
    }

    /**
     * 构建MyBatis插件
     * @param mybatisConfigPath 默认配置文件路径
     * @param dataSource 数据源
     */
    public XmlMyBatisPlugin(String mybatisConfigPath, DataSource dataSource) {
        this(mybatisConfigPath);
        setDataSource(dataSource);
    }

    /**
     * 构建MyBatis插件
     */
    public XmlMyBatisPlugin() {
        this(DEFAULT_MYBATIS_CONFIG_PATH);
    }

    /**
     * 构建MyBatis插件
     * @param dataSource 数据源
     */
    public XmlMyBatisPlugin(DataSource dataSource) {
        this(DEFAULT_MYBATIS_CONFIG_PATH, dataSource);
    }
}
