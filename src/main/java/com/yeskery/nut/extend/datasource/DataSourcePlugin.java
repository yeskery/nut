package com.yeskery.nut.extend.datasource;

import com.yeskery.nut.core.Order;
import com.yeskery.nut.plugin.PriorityPlugin;
import com.yeskery.nut.plugin.ServerEventPlugin;

import javax.sql.DataSource;

/**
 * 数据源插件
 * @author sprout
 * @version 1.0
 * 2022-07-10 20:14
 */
public interface DataSourcePlugin extends ServerEventPlugin, PriorityPlugin {

    /**
     * 获取数据源对象
     * @return 数据源对象
     */
    DataSource getDataSource();

    @Override
    default int getOrder() {
        return Order.MAX;
    }
}
