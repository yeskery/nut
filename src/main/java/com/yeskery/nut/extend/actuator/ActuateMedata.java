package com.yeskery.nut.extend.actuator;

import com.yeskery.nut.core.Method;
import com.yeskery.nut.core.NutException;
import com.yeskery.nut.core.RequestHandler;
import com.yeskery.nut.util.StringUtils;

/**
 * 应用管理接口元数据
 * @author YESKERY
 * 2024/9/18
 */
public class ActuateMedata {

    /** 名称 */
    private String name;

    /** uri */
    private final String uri;

    /** 请求方法 */
    private final Method method;

    /** 请求处理函数 */
    private final RequestHandler requestHandler;

    /**
     * 构建应用管理接口元数据
     * @param name 名称
     * @param uri uri
     * @param method 请求方法
     * @param requestHandler 请求处理函数
     */
    public ActuateMedata(String name, String uri, Method method, RequestHandler requestHandler) {
        this(uri, method, requestHandler);
        this.name = name;
    }

    /**
     * 构建应用管理接口元数据
     * @param uri uri
     * @param method 请求方法
     * @param requestHandler 请求处理函数
     */
    public ActuateMedata(String uri, Method method, RequestHandler requestHandler) {
        if (StringUtils.isEmpty(uri)) {
            throw new NutException("Uri Must Not Be Empty.");
        }
        if (!uri.startsWith("/")) {
            uri = "/" + uri;
        }
        this.uri = uri;
        if (method == null) {
            throw new NutException("Method Must Not Be Null.");
        }
        this.method = method;
        if (requestHandler == null) {
            throw new NutException("RequestHandler Must Not Be Null.");
        }
        this.requestHandler = requestHandler;
    }

    /**
     * 获取名称
     * @return 名称
     */
    public String getName() {
        return StringUtils.isEmpty(name) ? "Service" : name;
    }

    /**
     * 获取uri
     * @return uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * 获取请求方法
     * @return 请求方法
     */
    public Method getMethod() {
        return method;
    }

    /**
     * 获取请求处理函数
     * @return 请求处理函数
     */
    public RequestHandler getRequestHandler() {
        return requestHandler;
    }
}
