package com.yeskery.nut.extend.actuator;

import java.util.Collection;

/**
 * 应用管理接口信息
 * @author YESKERY
 * 2024/9/18
 */
public interface Actuate {

    /**
     * 获取应用管理接口元数据集合
     * @return 应用管理接口元数据集合
     */
    Collection<ActuateMedata> getActuateMedata();
}
