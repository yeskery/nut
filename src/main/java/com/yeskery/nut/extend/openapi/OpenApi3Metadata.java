package com.yeskery.nut.extend.openapi;

import java.util.List;

/**
 * OpenAPi3元数据信息，可以将该对象注入到应用上下文中
 * @author sunjay
 * 2023/9/24
 */
public class OpenApi3Metadata {

    /** 标题 */
    private String title;

    /** 描述 */
    private String description;

    /** 联系邮箱 */
    private String contactEmail;

    /** 服务条款地址 */
    private String termsOfService;

    /** 证书名称 */
    private String licenseName;

    /** 证书url */
    private String licenseUrl;

    /** 服务版本 */
    private String version;

    /** 拓展文档描述 */
    private String externalDocsDescription;

    /** 扩展文档url */
    private String externalDocsUrl;

    /** 服务访问地址 */
    private List<String> serverUrls;

    /**
     * 获取标题
     * @return 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取描述
     * @return 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取联系邮箱
     * @return 联系邮箱
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * 设置联系邮箱
     * @param contactEmail 联系邮箱
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * 获取服务条款地址
     * @return 服务条款地址
     */
    public String getTermsOfService() {
        return termsOfService;
    }

    /**
     * 设置服务条款地址
     * @param termsOfService 服务条款地址
     */
    public void setTermsOfService(String termsOfService) {
        this.termsOfService = termsOfService;
    }

    /**
     * 获取证书名称
     * @return 证书名称
     */
    public String getLicenseName() {
        return licenseName;
    }

    /**
     * 设置证书名称
     * @param licenseName 证书名称
     */
    public void setLicenseName(String licenseName) {
        this.licenseName = licenseName;
    }

    /**
     * 获取证书url
     * @return 证书url
     */
    public String getLicenseUrl() {
        return licenseUrl;
    }

    /**
     * 设置证书url
     * @param licenseUrl 证书url
     */
    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    /**
     * 获取服务版本
     * @return 服务版本
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置服务版本
     * @param version 服务版本
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 获取扩展文档描述
     * @return 扩展文档描述
     */
    public String getExternalDocsDescription() {
        return externalDocsDescription;
    }

    /**
     * 设置扩展文档描述
     * @param externalDocsDescription 扩展文档描述
     */
    public void setExternalDocsDescription(String externalDocsDescription) {
        this.externalDocsDescription = externalDocsDescription;
    }

    /**
     * 获取扩展文档url
     * @return 扩展文档url
     */
    public String getExternalDocsUrl() {
        return externalDocsUrl;
    }

    /**
     * 设置扩展文档url
     * @param externalDocsUrl 扩展文档url
     */
    public void setExternalDocsUrl(String externalDocsUrl) {
        this.externalDocsUrl = externalDocsUrl;
    }

    /**
     * 获取服务访问地址
     * @return 服务访问地址
     */
    public List<String> getServerUrls() {
        return serverUrls;
    }

    /**
     * 设置服务访问地址
     * @param serverUrls 服务访问地址
     */
    public void setServerUrls(List<String> serverUrls) {
        this.serverUrls = serverUrls;
    }
}
